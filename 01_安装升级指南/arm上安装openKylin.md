---
title: 在ARM上安装openKylin
description: 
published: true
date: 2023-01-12T02:42:26.148Z
tags: 
editor: markdown
dateCreated: 2023-01-12T02:36:15.135Z
---

# 一、在 coolpi 上安装openKylin

[在 coolpi 上安装openKylin](./在arm设备上安装/在coolpi上安装openKylin.md)



# 二、在树莓派上安装openKylin

[在树莓派上安装openKylin](./在arm设备上安装/在树莓派上安装openKylin.md)

# 三、在 chilliepi(双椒派) 上 安装 openKylin

[在 chilliepi(双椒派) 上 安装 openKylin](./在arm设备上安装/在chilliepi（双椒派）上安装openKylin.md)

# 四、在 飞腾派 上 安装 openKylin

[在 飞腾派 上 安装 openKylin](./在arm设备上安装/在飞腾派上安装openKylin.md)
