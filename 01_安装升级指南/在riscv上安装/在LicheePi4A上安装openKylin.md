## 烧录到emmc
### 准备emmc烧录环境
准备好一根Type-C数据线，一般开发板包装盒会配套提供。

openkylin适配LicheePi4A的镜像可以通过以下链接下载：
> https://www.openkylin.top/downloads

通过以下命令将文件解压：
> tar -xvf openKylin-Embedded-V2.0-Release-licheepi4a-riscv64.tar.xz

拿出准备好的Type-C数据线，按住开发板板上BOOT键，进入fastboot模式，连接开发板Type-C口与电脑USB接口，接好红灯亮后可松开BOOT键

需要使用X86架构设备

在 Linux 下，使用 lsusb 查看设备，会显示以下设备： ID 2345:7654 T-HEAD USB download gadget

进入解压后的文件夹，其中包含两个版本的uboot文件，根据自己开发板内存大小选择uboot,并修改烧录脚本thead-image-linux.sh中的uboot

执行脚本烧录镜像：
sudo ./thead-image-linux.sh

终端会打印烧录日志，烧录完成后拔调数据线，上电启动即可。

### 第一次启动
首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行LicheePi4A首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin

### 启动后分配emmc剩余空间
> sudo btrfs filesystem resize max /



## 烧录到SD卡
### 准备SD卡
openkylin适配LicheePi4A的镜像可以通过以下链接下载：
https://www.openkylin.top/downloads
通过以下命令解压：
> tar -xvf openKylin-2.0-licheepi4a-riscv64.tar.xz
进入解压后的文件夹

### 制作SD卡启动盘
首先使用磁盘工具将SD卡格式化。之后通过命令行将镜像刷入sd卡，请运行：
> sudo dd if=</path/to/image.ext4> of=/dev/mmcblk0 bs=1M status=progress
此命令假设您已将 SD 卡插入开发板的 SD 卡插槽中。 如果您使用的是 USB 适配器，它可能会显示为 /dev/sdb 或类似的内容而不是 /dev/mmcblk0

注意：要非常小心上一个命令中的“of”参数。 如果使用了错误的磁盘，您可能会丢失数据。也可通过磁盘工具的回复磁盘映像功能来将镜像刷入sd卡。

### 连接到串行控制台
可以使用 openkylin 计算机来监视LicheePi4A启动过程的串口输出，通过USB转串口线对应连接开发板U0-TX,U0-RX,GND管脚并连接到计算机上，通过运行以下命令来打开串口：
> sudo minicom -s
在串口设置中，修改A - 串行设备为：/dev/ttyUSB0之后保存并打开串口。

### 第一次启动和修改开发板u-boot设置
在计算机中启动LicheePi4A的minicom串口。将烧录好的sd卡插入LicheePi4A卡槽并连接好电源线。开发板启动到u-boot时快速按下回车使开发板停在u-boot。在u-boot中输入以下命令设置开发板从SD卡启动：
> env set -f set_bootargs 'setenv bootargs console=ttyS0,115200 root=/dev/mmcblk1 rootfstype=ext4 rootwait rw earlycon clk_ignore_unused loglevel=7 eth=$ethaddr rootrwoptions=rw,noatime rootrwreset=${factory_reset} init=/lib/systemd/systemd'
> env save
> reset

注：第一条是一条命令，很长，在您的显示设备可能会换行
之后在u-boot中输入reset来重启开发板，这样开发板就可以从sd卡启动openKylin系统。
首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行LicheePi4A首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是：
> username：openkylin
> password：openkylin

### 启动后分配剩余空间
> sudo btrfs filesystem resize max /

注：如有其他烧录问题，如windows系统烧录可能需要安装驱动、烧录日志图片展示等可参考矽速科技相关文档
https://wiki.sipeed.com/hardware/zh/lichee/th1520/lpi4a/4_burn_image.html
