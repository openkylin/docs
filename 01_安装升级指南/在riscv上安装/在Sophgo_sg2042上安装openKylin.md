## 准备SD卡
openkylin适配Sophgo sg2042的镜像可以通过以下链接下载
> https://www.openkylin.top/downloads

通过以下命令解压
> unxz  openkylin-1.0-sophgo-sg2042-riscv64.img.xz

以上路径请根据自己的实际路径去解压

## 制作SD卡启动盘
首先使用磁盘工具将SD卡格式化。
之后通过命令行将镜像刷入SD卡，请运行：
> sudo dd if=</path/to/image.img> of=/dev/mmcblk0 bs=1M status=progress

注：</path/to/image.img>的含义是下载的镜像路径，不用写尖括号和里边的英文，路径尽可能不要有空格

此命令假设您已将SD卡插入开发板的SD卡插槽中。 如果您使用的是USB读卡器，它可能会显示为 /dev/sdb 或类似的内容而不是 /dev/mmcblk0

注意：要非常小心上一个命令中的“of”参数。 如果使用了错误的磁盘，您可能会丢失数据。也可通过磁盘工具的回复磁盘映像功能来将镜像刷入sd卡。

## 烧录后分配SD卡剩余空间
按照以下步骤将SD卡剩余空间分配到根分区。
注：此命令假设您SD卡的设备号为/dev/sdb，分盘时请根据实际的设备号进行分盘。
> sudo apt install cloud-guest-utils
> sudo growpart /dev/sdb 2
> sudo resize2fs /dev/sdb2

## 连接到串行控制台
可以使用 openkylin 计算机来监视Sophgo sg2042启动过程的串口输出，将计算机连接到Sophgo sg2042上第三个Micro USB端口，通过运行以下命令来打开串口：
sudo minicom -s
在串口设置中，修改A - 串行设备为：/dev/ttyUSB0之后保存并打开串口。

## 第一次启动
将烧录并分盘后的SD卡插入Sophgo sg2042卡槽并按下电源按钮。首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行Sophgo sg2042首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin
