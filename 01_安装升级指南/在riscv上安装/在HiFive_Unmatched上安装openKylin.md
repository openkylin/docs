## 烧录到SD卡
### 准备SD卡

即使计划长期使用 NVMe SSD，第一步也需要 SD 卡。 这样我们才可以利用 Unmatched 上的 NVMe 驱动器稍后设置 SSD。 openkylin适配Unmatched的镜像可以通过以下链接下载
> https://www.openkylin.top/downloads

通过以下命令解压

> unxz  openkylin-1.0-hifive-unmatched-riscv64.img.xz

以上路径请根据自己的实际路径去解压

### 制作SD卡启动盘
首先使用磁盘工具将SD卡格式化。
之后通过命令行将镜像刷入SD卡，请运行：
> sudo dd if=</path/to/image.img> of=/dev/mmcblk0 bs=1M status=progress

注：</path/to/image.img>的含义是下载的镜像路径，不用写尖括号和里边的英文，路径尽可能不要有空格

此命令假设您已将 SD 卡插入计算机的 SD 卡插槽中。 如果您使用的是 USB 读卡器，它可能会显示为 /dev/sdb 或类似的内容而不是 /dev/mmcblk0

注意：要非常小心上一个命令中的“of”参数。 如果使用了错误的磁盘，您可能会丢失数据。也可通过磁盘工具的恢复磁盘映像功能来将镜像刷入sd卡。

### 烧录后分配SD卡剩余空间
执行以下命令将SD卡剩余空间分配到根分区。
注：此命令假设您SD卡的设备号为/dev/sdb，分盘时请根据实际的设备号进行分盘。
sudo apt install cloud-utils
sudo growpart /dev/sdb 4
sudo resize2fs /dev/sdb4

### 修改U-BOOT配置文件
> sudo mount /dev/mmcblk0p4 /mnt 
> sudo mount /dev/mmcblk0p3 /mnt/boot 
> sudo chroot /mnt

以上/dev开头的路径请根据自己的实际路径修改

使用文本编辑器打开”/etc/default/u-boot“（这个是chroot以后的路径，实际在本机的路径是"/mnt/etc/default/u-boot"）,并添加：

> U_BOOT_ROOT="root=/dev/mmcblk0p4"

然后运行：
> u-boot-update

使用文本编辑器打开”/boot/extlinux/extlinux.conf“（这个是chroot以后的路径，实际在本机的路径是"/mnt/boot/extlinux/extlinux.conf"，在其中两个空行中添加如下：

>fdt /hifive-unmatched-a00.dtb

然后退出：
> exit
> sudo umount /mnt/boot
> sudo umount /mnt

### 第一次启动
首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行Unmatched首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin

同时也支持以下两种登录方式
### 连接到串行控制台
HiFive 的入门指南 14 解释了如何从各种不同的操作系统连接到串行控制台。 如果使用 openkylin 计算机来监控串行输出，将该计算机连接到 Unmatched 上 SD 卡插槽旁边的Micro USB 端口并运行

> sudo screen /dev/ttyUSB1 115200

按下电源按钮后，启动输出将开始出现在会话中。

## 烧录到NVMe
### 将 openkylin RISC-V 安装到 NVMe 驱动器
将 NVMe 驱动器与 Unmatched 一起使用会在性能和可用性方面产生巨大差异。 让它工作需要一点努力，但相信我这是值得的。 SiFive 推荐三星 970 EVO Plus。 我使用了三星 970 EVO（不是 plus），效果很好。 在 NVMe 驱动器上安装 openkylin RISC-V 的最简单方法是从 SD 卡启动并使用 Unmatched 本身上的 M.2 连接器。

启动后，将openkylin的镜像下载到 Unmatched
> https://www.openkylin.top/downloads

通过以下命令解压
> unxz  openkylin-1.0-hifive-unmatched-riscv64.img.xz

以上路径请根据自己的实际路径去解压

通过运行确保 NVMe 驱动器存在
> ls -l /dev/nvme*

在我的主板上，NVMe 驱动器显示为 /dev/nvme0n1，首先使用磁盘工具将 NVMe硬盘格式化。之后通过运行以下命令将映像刷入 NVMe
> sudo dd if=</path/to/image.img> of=/dev/nvme0n1 bs=1M status=progress

注：</path/to/image.img>的含义是下载的镜像路径，不用写尖括号和里边的英文，路径尽可能不要有空格

### 烧录后分NVMe硬盘剩余空间
执行以下命令将NVMe硬盘剩余空间分配到根分区。
注：此命令假设您SD卡的设备号为/dev/sdb，分盘时请根据实际的设备号进行分盘。
sudo apt install cloud-utils
sudo growpart /dev/sdb 4
sudo resize2fs /dev/sdb4

恭喜！ 您现在在 HiFive Unmatched 的 NVMe 驱动器上安装了 openkylin RISC-V。 然而，仍然有一个问题。 Unmatched 仍然需要存在 SD 卡才能启动，并且存在可能导致它在 SD 卡而不是 NVMe 驱动器上安装根文件系统的竞争条件。 为防止出现这种情况，请通过运行将新刷入的 NVMe 驱动器和 chroot 挂载到其中
> sudo mount /dev/nvme0n1p4 /mnt
> sudo moubt /dev/nvme0n1p3 /mnt/boot
> sudo chroot /mnt

注意：之前的 chroot 命令只有在使用 riscv64 计算机执行时才有效。 这就是本教程建议使用 Unmatched 上的 M.2 驱动器设置 NVMe 驱动器的原因之一

使用您最喜欢的文本编辑器来编辑 “/etc/default/u-boot”，并添加：
> U_BOOT_ROOT="root=/dev/nvme0n1p4"

要应用这些更改，请运行
> u-boot-update

使用文本编辑器打开”/boot/extlinux/extlinux.conf“，在其中两个空行中添加如下：
> fdt /hifive-unmatched-a00.dtb

通过运行 exit 退出 chroot 环境：
>exit
>sudo umount /mnt/boot
>sudo umount /mnt
然后重新启动系统, 它现在将从您的 NVMe 驱动器启动，您将获得显着的性能提升！
