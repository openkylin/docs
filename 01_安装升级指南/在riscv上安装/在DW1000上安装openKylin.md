## 准备SD卡
openkylin适配DW1000的镜像可以通过以下链接下载
> https://www.openkylin.top/downloads

通过以下命令解压
> tar xf openKylin-Embedded-V1.0-Release-DW1000-RV64G.tar.xf

以上路径请根据自己的实际路径去解压

## 制作SD卡启动盘
首先使用磁盘工具将SD卡格式化。
> sudo dd if=</path/to/image.img> of=/dev/sdb1 bs=1M status=progress


此命令假设您已将SD卡linux PC的SD卡插槽中。 如果您使用的是USB读卡器，它会显示为 /dev/sdb或者类似内容

注意：要非常小心上一个命令中的“of”参数。 如果使用了错误的磁盘，您可能会丢失数据。也可通过磁盘工具的回复磁盘映像功能来将镜像刷入sd卡。

## 烧录后分配SD卡剩余空间
按照以下步骤将SD卡剩余空间分配到根分区。
注：此命令假设您SD卡的设备号为/dev/sdb，分盘时请根据实际的设备号进行分盘。
> sudo apt install cloud-guest-utils
> sudo growpart /dev/sdb 1
> sudo resize2fs /dev/sdb1


## 第一次启动
将烧录并分盘后的SD卡插入DW1000卡槽并按下电源按钮。首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行DW1000首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin
