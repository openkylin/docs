## 下载镜像

openkylin 适配 Lotus2 的镜像可以通过以下链接下载

> https://www.openkylin.top/downloads

下载 openKylin-1.0-embedded-lotus2-riscv64.tar.gz 到本地

## 制作 SD 卡启动盘

首先将 sd 卡分区格式化，然后将文件系统解压到 sd 卡即可
以 sd 卡设备节点为/dev/sdb 为例

```sh
# 分区、格式化
sudo parted -s /dev/sdb mktable gpt
sudo parted -s /dev/sdb mkpart primary 1 100%
sudo mkfs.ext4 -L system /dev/sdb1
sudo mount /dev/sdb1 /mnt
# 拷贝文件系统
sudo tar xf openKylin-1.0-embedded-lotus2-riscv64.tar.gz -C /mnt
sync
sudo umount /mnt
# 弹出sd卡
sudo eject /dev/sdb
```

## 启动系统

Lotus2 没有图形显示接口，因此需要通过串口连接操作，按照下图所示，使用双公头 USB 线将 USB 转串口与电脑连接

![riscv64Lotus2开发板信息.png](./assets/在Lotus2上安装openKylin/riscv64Lotus2开发板信息.png)

插上电源，上电后，电脑上即可将串口识别出来，通过串口即可登录系统，默认用户名/密码是

```
> username：openkylin
> password：openkylin
```
