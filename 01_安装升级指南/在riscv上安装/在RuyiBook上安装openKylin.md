## 准备emmc烧录环境
准备好一根Type-C数据线，一般如意笔记本包装盒会配套提供。

openkylin适配RuyiBook的镜像可以通过以下链接下载：
> https://www.openkylin.top/downloads

通过以下命令将文件解压：
> tar -xvf openKylin-Embedded-V2.0-Release-RuyiBook-riscv64.tar.xz

需要使用X86架构设备

进入解压后的文件夹，执行脚本烧录镜像：
sudo ./burn_techvision.sh

拿出准备好的Type-C数据线，使用取卡针或别针按住笔记本右侧前方的烧录按钮，进入fastboot模式，连接笔记本左侧前方的Type-C口与电脑USB接口。注意在烧录过程中不要将如意笔记本通电。

终端会打印烧录日志，烧录完成后拔掉数据线。

## 第一次启动
烧录后先不要连接电源，长按开机按键10秒左右直到笔记本左侧绿色灯熄灭，之后连接笔记本电源并按开机键开机。首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行如意笔记本的首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin
