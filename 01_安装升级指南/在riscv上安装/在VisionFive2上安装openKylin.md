## 准备SD卡
openkylin适配VisionFive 2的镜像可以通过以下链接下载
> https://www.openkylin.top/downloads

通过以下命令解压
> unxz openKylin-Embedded-V2.0-Release-visionfive2-riscv64.img.xz

以上路径请根据自己的实际路径去解压

## 制作SD卡启动盘
首先使用磁盘工具将SD卡格式化。
之后通过命令行将镜像刷入SD卡，请运行：
> sudo dd if=</path/to/image.img> of=/dev/mmcblk0 bs=1M status=progress

注：</path/to/image.img>的含义是下载的镜像路径，不用写尖括号和里边的英文，路径尽可能不要有空格

此命令假设您已将 SD 卡插入开发板的 SD 卡插槽中。 如果您使用的是 USB 读卡器，它可能会显示为 /dev/sdb 或类似的内容而不是 /dev/mmcblk0

注意：要非常小心上一个命令中的“of”参数。 如果使用了错误的磁盘，您可能会丢失数据。也可通过磁盘工具的回复磁盘映像功能来将镜像刷入sd卡。

## 烧录后分配SD卡剩余空间
按照以下步骤将SD卡剩余空间分配到根分区。
注：此命令假设您SD卡的设备号为/dev/sdb，分盘时请根据实际的设备号进行分盘。
> sudo apt install cloud-guest-utils
> sudo growpart /dev/sdb 4
> sudo resize2fs /dev/sdb4

## 第一次启动
将烧录好的sd卡插入VisionFive 2卡槽并连接好电源线。首次启动之后，系统中会存在一个默认用户，当桌面环境启动之后，您可以通过默认用户进行VisionFive 2首次登陆，后期可以根据自己需求进行用户或密码的更改。
默认用户名/密码是
> username：openkylin
> password：openkylin
