# **openKylin系统安装指南**

本文档主要针对社区用户使用最多的X86架构下的三种安装场景分别进行了说明，这三种场景安装所需要的步骤有些差异，请根据您的实际安装场景选择以下对应的章节。

另外，需要在RISC-V、ARM开发板安装openKylin操作系统的用户（高阶玩家），请通过文末的附录跳转到对应文档。

# **安装前的准备**

 1、一台x86_64位电脑；

 2、一个可以**格式化**的优盘，容量不小于8GB；

*tips：1G=1024MB，为方便计算，可以估算成 1G=1000MB*

## **配置要求**

### **流畅体验建议配置**

● CPU：2018年以后的主流产品（6核及以上）

● 内存：8GB及以上（*tips：虚拟机建议分配4GB及以上*）

● 硬盘：128GB及以上空闲存储空间

### **系统安装最低配置**

● CPU：2015年以后的产品（双核）

● 内存：不小于4GB（*tips：虚拟机至少分配2GB*）

● 硬盘：不小于50GB空闲存储空间



# **实体机单系统安装**

## **第一步：下载openKylin镜像**

前往官网下载x86_64的镜像（https://www.openkylin.top/downloads/628-cn.html）

*tips：下载完镜像文件后，请先检查文件MD5值是否和官网上的一致，如果不一致请重新下载*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/1-%E5%AE%98%E7%BD%91%E4%B8%8B%E8%BD%BD%E9%95%9C%E5%83%8F.png)

官网同时还提供多个镜像站下载（https://www.openkylin.top/downloads/mirrors-cn.html）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/2-%E9%95%9C%E5%83%8F%E7%AB%99.png)



## **第二步：选择一个启动盘制作工具制作启动盘**

推荐采用rufus或者ventoy制作openKylin系统启动盘。

1、rufus

前往官网（http://rufus.ie/）下载安装rufus工具，然后按下图配置，完成启动盘制作

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/3-rufus.png)

2、ventoy

前往官网（https://www.ventoy.net/）下载ventoy工具，按照以下步骤制作启动盘后，将待安装镜像拷贝到优盘里即可

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/4-ventoy.png)

## **第三步：通过启动盘（Ventoy为例）启动电脑**

电脑关机，将做好的启动盘插入电脑USB接口，然后启动电脑的同时不断按delete、F2、F10或F12键，进入BIOS界面。

*tips：不同品牌、型号的电脑有所差异，可以百度搜索一下您的电脑具体是按哪个键进BIOS*

关闭安全启动（secure boot），并将U盘作为第一启动项，然后保存并重启

*tips：一般F10键为“保存并重启”，BIOS界面一般会有提示*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/5-%E8%AE%BE%E7%BD%AEbios.jpeg)

然后将进入Ventoy启动盘界面，选择openKylin 1.0镜像。如下图所示：

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/6-ventoy%E9%80%89%E6%8B%A9%E9%95%9C%E5%83%8F.jpeg)

进入Grub引导界面，选择“Try without installing”，进入openKylin试用桌面

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/39-%E5%AE%89%E8%A3%85grub%E5%BC%95%E5%AF%BC%E9%A1%B9.jpeg)

tips：前两个选项启动的是6.1内核；后两个选项启动的是5.15内核



## **第四步：开始安装系统**

在openKylin试用桌面，点击桌面上的“安装openKylin”图标，开始进行安装前配置

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/7-%E8%AF%95%E7%94%A8%E6%A1%8C%E9%9D%A2.png)

选择语言

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/8-%E9%80%89%E6%8B%A9%E8%AF%AD%E8%A8%80.png)

选择时区

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/40-%E9%80%89%E6%8B%A9%E6%97%B6%E5%8C%BA.png)

创建用户

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/9_%E5%88%9B%E5%BB%BA%E7%94%A8%E6%88%B7.png)

选择安装方式（分区）

1、全盘安装（适合新手小白）

*tips：全盘安装会将硬盘全部格式化，请确保硬盘上已无需要保留的文件*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/10_%E9%80%89%E6%8B%A9%E5%AE%89%E8%A3%85%E6%96%B9%E5%BC%8F.png)

2、自定义安装（适合高阶玩家）

点击“创建分区表”，清空当前分区（*tips：此时不会真的进行擦除*）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/11-%E6%B8%85%E7%A9%BA%E5%88%86%E5%8C%BA%E8%A1%A8.png)

然后按照您的需求创建分区。一般至少需要创建：根分区（用于“ext4”，挂载点“/”，空间分配大于15GB）：

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/21-%E5%88%9B%E5%BB%BA%E6%A0%B9%E5%88%86%E5%8C%BA.png)

efi分区（用于“efi”、空间分配256MB~2GB）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/13-%E5%88%9B%E5%BB%BAefi%E5%88%86%E5%8C%BA.png)

点击确定，并确认您选择的分区策略

*tips：此时反悔还来得及**哦**，点击“开始安装”后将先进行****格式化****，您将失去此硬盘上原有的内容*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/14-%E7%A1%AE%E8%AE%A4%E5%88%86%E5%8C%BA%E7%AD%96%E7%95%A5.png)

开始安装进程

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/15-%E5%BC%80%E5%A7%8B%E5%AE%89%E8%A3%85%E8%BF%9B%E7%A8%8B.png)
## **第五步：完成安装**

安装进度结束后，将提示安装完成。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/16-%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90.jpeg)

点击“现在重启”，然后按照提示拔出U盘并按“enter”键，系统将自动重启进入系统，此时您的电脑就成功安装openKylin系统啦！！

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/17-%E6%8B%94%E6%8E%89%E4%BC%98%E7%9B%98%E9%87%8D%E5%90%AF.jpeg)



# **实体机双系统安装**

## **第一步：下载openKylin镜像**

前往官网下载x86_64的镜像（https://www.openkylin.top/downloads/628-cn.html）

*tips：下载完镜像文件后，请先检查文件MD5值是否和官网上的一致，如果不一致请重新下载*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/1-%E5%AE%98%E7%BD%91%E4%B8%8B%E8%BD%BD%E9%95%9C%E5%83%8F.png)

官网同时还提供多个镜像站下载（https://www.openkylin.top/downloads/mirrors-cn.html）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/2-%E9%95%9C%E5%83%8F%E7%AB%99.png)



## **第二步：选择一个启动盘制作工具制作启动盘**

推荐采用rufus或者ventoy制作openKylin系统启动盘。

1、rufus

前往官网（http://rufus.ie/）下载安装rufus工具，然后按下图配置，完成启动盘制作

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/3-rufus.png)

2、ventoy

前往官网（https://www.ventoy.net/）下载ventoy工具，按照以下步骤制作启动盘后，将待安装镜像拷贝到优盘里即可

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/4-ventoy.png)



## **第三步：将电脑磁盘进行分区**

默认机器已安装windows系统，打开磁盘管理工具，选择分割的磁盘空间，右键点击该磁盘，选择“压缩卷”。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/18-%E7%A3%81%E7%9B%98%E5%88%86%E5%8C%BA-1.png)

此时会弹出压缩窗口，输入压缩空间量的大小，此处展示约分配 135GB （tips：分配空间不少于 50 GB）。确认压缩空间量后点击“压缩”。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/19-%E7%A3%81%E7%9B%98%E5%88%86%E5%8C%BA-2.png)

压缩结束后，会多出一块“未分配”的空闲空间，将用于安装openKylin操作系统。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/20-%E7%A3%81%E7%9B%98%E5%88%86%E5%8C%BA-3.png)


## **第四步：通过启动盘（Ventoy为例）启动电脑**

电脑关机，将做好的启动盘插入电脑USB接口，然后启动电脑的同时不断按delete、F2、F10或F12键，进入BIOS界面。

*tips：不同品牌、型号的电脑有所差异，可以百度搜索一下您的电脑具体是按哪个键进BIOS*

关闭安全启动（secure boot），并将U盘作为第一启动项，然后保存并重启

*tips：一般F10键为“保存并重启”，BIOS界面一般会有提示*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/5-%E8%AE%BE%E7%BD%AEbios.jpeg)

然后将进入Ventoy启动盘界面，选择openKylin 1.0镜像。如下图所示：

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/6-ventoy%E9%80%89%E6%8B%A9%E9%95%9C%E5%83%8F.jpeg)

进入Grub引导界面，选择“Try without installing”，进入openKylin试用桌面

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/39-%E5%AE%89%E8%A3%85grub%E5%BC%95%E5%AF%BC%E9%A1%B9.jpeg)

tips：前两个选项启动的是6.1内核；后两个选项启动的是5.15内核



## **第五步：开始安装系统**

在openKylin试用桌面，点击桌面上的“安装openKylin”图标，开始进行安装前配置

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/7-%E8%AF%95%E7%94%A8%E6%A1%8C%E9%9D%A2.png)

选择语言

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/8-%E9%80%89%E6%8B%A9%E8%AF%AD%E8%A8%80.png)

选择时区

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/40-%E9%80%89%E6%8B%A9%E6%97%B6%E5%8C%BA.png)

创建用户

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/9_%E5%88%9B%E5%BB%BA%E7%94%A8%E6%88%B7.png)

选择”自定义安装“，将第三步分好的空闲分区（如图所示最下面的空闲分区）设置为openKylin系统的根分区（"/"）

*tips：双系统安装只能选择自定义安装，全盘安装会破坏原系统；不需要设置efi分区*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/21-%E5%88%9B%E5%BB%BA%E6%A0%B9%E5%88%86%E5%8C%BA.png)

确认您选择的分区策略后，点击开始安装

*tips：此时反悔还来得及**哦**，点击“开始安装”后将先进行****格式化****，您将失去此硬盘上原有的内容*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/22-%E7%A1%AE%E8%AE%A4%E5%88%86%E5%8C%BA%E7%AD%96%E7%95%A5%E5%8F%8C%E7%B3%BB%E7%BB%9F.png)

开始安装进程

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/23-%E5%AE%89%E8%A3%85%E8%BF%9B%E7%A8%8B.png)

## **第六步：完成安装**

安装进度结束后，将提示安装完成。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/16-%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90.jpeg)

点击“现在重启”，然后按照提示拔出U盘并按“enter”键，系统将自动重启

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/17-%E6%8B%94%E6%8E%89%E4%BC%98%E7%9B%98%E9%87%8D%E5%90%AF.jpeg)

在grub界面选择openKylin启动项

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/41-%E5%8F%8C%E7%B3%BB%E7%BB%9F%E5%BC%95%E5%AF%BC%E9%80%89%E6%8B%A9.jpg)

# **虚拟机安装**

## **第一步：下载openKylin镜像**

前往官网下载x86_64的镜像（https://www.openkylin.top/downloads/628-cn.html）

*tips：下载完镜像文件后，请先检查文件MD5值是否和官网上的一致，如果不一致请重新下载*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/1-%E5%AE%98%E7%BD%91%E4%B8%8B%E8%BD%BD%E9%95%9C%E5%83%8F.png)

官网同时还提供多个镜像站下载（https://www.openkylin.top/downloads/mirrors-cn.html）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/2-%E9%95%9C%E5%83%8F%E7%AB%99.png)



## **第二步：创建虚拟机**

在原有系统上安装虚拟机软件，本文档以VMware Workstation为例，点击“创建新的虚拟机”

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/24-%E6%89%93%E5%BC%80%E8%99%9A%E6%8B%9F%E6%9C%BA%E8%BD%AF%E4%BB%B6.png)

默认配置即可，点击下一步

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/25-%E8%99%9A%E6%8B%9F%E6%9C%BA%E9%85%8D%E7%BD%AE.png)

勾选“安装程序光盘映像文件”，点击“浏览”，选择您下载好的openKylin操作系统镜像文件后，点击下一步

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/26-%E9%80%89%E6%8B%A9%E9%95%9C%E5%83%8F.png)

默认配置即可，点击下一步

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/27-%E9%80%89%E6%8B%A9%E7%B3%BB%E7%BB%9F%E7%B1%BB%E5%9E%8B.png)

编辑虚拟机名称，选择安装位置后，点击下一步

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/28-%E7%BC%96%E8%BE%91%E8%99%9A%E6%8B%9F%E6%9C%BA%E5%90%8D%E7%A7%B0.png)

根据提示调整磁盘大小，虚拟磁盘选默认的配置即可，点击下一步

*tips：分配磁盘空间不少于50GB*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/29-%E8%AE%BE%E7%BD%AE%E8%99%9A%E6%8B%9F%E6%9C%BA%E7%A3%81%E7%9B%98%E5%A4%A7%E5%B0%8F.png)

确认虚拟机配置，如需要自定义调整，点击“自定义硬件”。确认完硬件配置之后点击“完成”

*tips：**为尽量保证虚拟机体验流畅，**建议内存不少于4GB；CPU内核不少于4个*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/30-%E7%A1%AE%E8%AE%A4%E7%A1%AC%E4%BB%B6%E9%85%8D%E7%BD%AE.png)

完成虚拟机创建。点击：“开启此虚拟机”

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/31-%E5%AE%8C%E6%88%90%E8%99%9A%E6%8B%9F%E6%9C%BA%E5%88%9B%E5%BB%BA.png)

选择“试用试用开放麒麟而不安装（T）”，进入openKylin试用桌面

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/32-%E5%BC%80%E5%90%AF%E8%99%9A%E6%8B%9F%E6%9C%BA.png)




## **第三步：开始安装系统**

在openKylin试用桌面，点击桌面上的“安装openKylin”图标，开始进行安装前配置

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/7-%E8%AF%95%E7%94%A8%E6%A1%8C%E9%9D%A2.png)

选择语言

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/8-%E9%80%89%E6%8B%A9%E8%AF%AD%E8%A8%80.png)

选择时区

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/40-%E9%80%89%E6%8B%A9%E6%97%B6%E5%8C%BA.png)

创建用户

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/9_%E5%88%9B%E5%BB%BA%E7%94%A8%E6%88%B7.png)

选择安装方式（分区）

1、全盘安装（适合新手小白）

*tips：虚拟机安装不用担心格式化会丢失文件*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/10_%E9%80%89%E6%8B%A9%E5%AE%89%E8%A3%85%E6%96%B9%E5%BC%8F.png)

2、自定义安装（适合高阶玩家）

点击“创建分区表”，清空当前分区（*tips：此时不会真的进行擦除*）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/11-%E6%B8%85%E7%A9%BA%E5%88%86%E5%8C%BA%E8%A1%A8.png)

然后按照您的需求创建分区。一般至少需要创建：根分区（用于“ext4”，挂载点“/”，空间分配大于15GB）：

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/21-%E5%88%9B%E5%BB%BA%E6%A0%B9%E5%88%86%E5%8C%BA.png)

efi分区（用于“efi”、空间分配256MB~2GB）

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/13-%E5%88%9B%E5%BB%BAefi%E5%88%86%E5%8C%BA.png)

点击确定，并确认您选择的分区策略

*tips：此时反悔还来得及**哦**，点击“开始安装”后将先进行****格式化****，您将失去此硬盘上原有的内容*

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/14-%E7%A1%AE%E8%AE%A4%E5%88%86%E5%8C%BA%E7%AD%96%E7%95%A5.png)

开始安装进程

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/15-%E5%BC%80%E5%A7%8B%E5%AE%89%E8%A3%85%E8%BF%9B%E7%A8%8B.png)

## **第四步：完成安装**

安装进度结束后，将提示安装完成。

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/33-%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90.png)

点击“现在重启”，然后按照提示拔出U盘并按“enter”键，系统将自动重启进入系统，此时您的电脑就成功安装openKylin系统啦！！

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/34-logo%E7%95%8C%E9%9D%A2.png)



## **参考视频**

*视频演示*





# **系统安装FAQ**

Q：系统安装到96%时卡住了怎么办？  

A：安装进度到96%时一般需要多等待一会（具体时间长短取决于硬件的性能）。如果等待时间过长（超过半个小时），请确认给efi分区分配的空间是否不足，重新分配空间再试一下  



Q：使用 USB 启动盘安装时，出现"try openKylin without installation"或“install openKylin”，Enter 选择“安装”后，显示器黑屏无任何显示，该怎么办？  

A：方法一：显示黑屏，可能是显卡显示的支持有问题，尝试手动修复。  

移动光标到"install openKylin" , 按"e"进入编辑模式，进入命令行模式，  

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/35-%E5%AE%89%E8%A3%85grub%E7%95%8C%E9%9D%A2.jpeg)

找到''quite splash''，在后面添加“nomodeset”，然后按  F10 键安装。  

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/36-%E5%AE%89%E8%A3%85grub%E7%95%8C%E9%9D%A2-2.jpeg)

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/37-%E5%AE%89%E8%A3%85grub%E7%95%8C%E9%9D%A2-3.png)

tips：依照不同显卡进行不同显卡驱动选项的添加，比如使用的是Nvidia显卡，添加 nomodeset



Q：系统安装完成之后，提示未检测到无线网卡，该怎么办？  

A：重启，然后在 grub 界面选择高级选项，选择5.15版本的内核重新进入系统，查看网络是否恢复正常  



Q：安装到最后一步提示安装失败怎么办？  

A：一般是因为镜像文件在下载或者拷贝过程中破损导致的。遇到安装失败报错时，请先检查镜像文件的MD5值和官网的是否一致。计算MD5值的方法：  

Linux系统，Win+T键打开终端，执行命令：

`md5sum openkylin-1.0-x86_64.iso`

Windows系统，搜索框输入cmd调出终端，执行命令：

`certUtil -hashfile openkylin-1.0-x86_64.iso MD5`

Q：选择启动盘启动，无法成功进入grub界面  

A：请检查BIOS设置了是都关闭了安全检查（secure boot）  


Q：如果双系统安装完成后没有启动项选择界面，该怎么解决？

A：可能是启动项出现问题，可以下载安装用 EasyBCD 软件修复启动项。

Q：自定义安装，配置完分区后提示没有根分区或efi分区，该怎么办？

A：根分区对应的是“/”，efi分区需要在分区时将“用于”类型改为 efi，这两个分区是必须要创建的。另外，数据备份分区（对应的是“/data”）、备份还原分区（对应的是“/backup”）以及交换分区（linux-swap），请根据您的需要确认这些分区是否创建。

Q：配置完分区提示“只能存在一个 efi分区”，该怎么办？

A：应该是已有的 Windows 系统也存在 efi 分区，如果是双系统安装，我们需要把自己添加的 efi 分区进行删除。如果是单系统安装（windows系统将被覆盖），将已存在的efi分区删掉，重新添加efi分区

Q：如何查看自己的电脑的BIOS是UEFI还是Legacy？

A：以windows系统为例，按"win+r"快捷键回车确认，输入"msinfo32"，回车，出现系统信息界面，可查看 BIOS 模式，如下图所示：

![输入图片说明](assets/%E7%B3%BB%E7%BB%9F%E5%AE%89%E8%A3%85/38-biso%E6%9F%A5%E7%9C%8B.png)

# **附录**

RISC-V开发板安装openKylin操作

[riscv上安装openKylin](./riscv上安装openKylin.md)

ARM开发板安装openKylin操作系统

[arm上安装openKylin](./arm上安装openKylin.md)