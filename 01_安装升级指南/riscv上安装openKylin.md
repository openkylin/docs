---
title: 在RISC-V上安装openKylin
description: 
published: true
date: 2022-07-22T06:40:16.074Z
tags: 
editor: markdown
dateCreated: 2022-07-22T06:40:16.074Z
---

# 一、在 Milk-V Pioneer上安装openKylin

[在Milk-V_Pioneer上安装openKylin](./在riscv上安装/在Milk-V_Pioneer上安装openKylin.md)


# 二、在 VisionFive 2上安装openKylin

[在VisionFive2上安装openKylin](./在riscv上安装/在VisionFive2上安装openKylin.md)


# 三、在 LicheePi 4A上安装openKylin

[在LicheePi4A上安装openKylin](./在riscv上安装/在LicheePi4A上安装openKylin.md)


# 四、在 SpacemiT K1上安装openKylin

[在SpacemiT_K1上安装openKylin](./在riscv上安装/在SpacemiT_K1上安装openKylin.md)


# 五、在 RuyiBook 上安装openKylin

[在RuyiBook上安装openKylin](./在riscv上安装/在RuyiBook上安装openKylin.md)


# 六、在 HiFive Unmatched 上安装openKylin

[在HiFive_Unmatched上安装openKylin](./在riscv上安装/在HiFive_Unmatched上安装openKylin.md)


# 七、在 Sophgo sg2042 上安装openKylin

[在Sophgo_sg2042上安装openKylin](./在riscv上安装/在Sophgo_sg2042上安装openKylin.md)


# 八、在 Lotus2 上安装 openKylin

[在Lotus2上安装openKylin](./在riscv上安装/在Lotus2上安装openKylin.md)
