---
title: 安装指南（MacOS）
description: 
published: true
date: 2024-05-08T07:38:12.847Z
tags: 
editor: markdown
dateCreated: 2024-05-08T07:24:21.392Z
---

# 安装指南(For MacOS)

本篇适用于搭载Intel、Apple Silicon芯片的Mac笔记本，详细介绍了在macOS中通过虚拟机安装openKylin开源操作系统的安装教程，（Windows、Linux安装教程请移步 [openKylin系统安装指南](./openKylin系统安装指南.md)）

# 准备工作 
安装使用openKylin开源操作系统的第一步，就是获取openKylin开源操作系统的镜像文件，我们可以直接在openKylin官网进行下载。

下载链接：https://www.openKylin.top/downloads 选择合适的版本（以x86架构64位为例）

![输入图片说明](./assets/安装指南（For_MacOS）/download-x86.png)
# 一．安装VMware Fusion

VMware Fusion 提供了无需重新启动即可在 Apple Mac 上运行 Windows、Linux 等操作系统的最佳方式。

Fusion 13 支持运行 macOS 12 及更高版本的 Intel 和 Apple Silicon Mac，提供面向开发人员、IT 管理员和日常用户的功能特性。

使用以下链接即可开始免费使用功能齐全的 30 天试用版（无需注册）

下载链接：https://www.vmware.com/cn/products/fusion/fusion-evaluation.html

![输入图片说明](./assets/安装指南（For_MacOS）/VMware-Fusion-download.png)

# 二．安装openKylin开源操作系统

打开虚拟机软件 VMware Fusion，在文件菜单中点击新建；

![输入图片说明](./assets/安装指南（For_MacOS）/%E6%96%B0%E5%BB%BA%E8%99%9A%E6%8B%9F%E6%9C%BA.png)

将准备工作中下载的ISO文件拖入窗口，创建虚拟机；

![输入图片说明](./assets/安装指南（For_MacOS）/%E5%AE%89%E8%A3%85%E8%99%9A%E6%8B%9F%E6%9C%BA.png)

进入虚拟机引导界面后，按照默认选项，点击继续；

选择“稍后安装操作系统”，点击下一步；

客户机操作系统选择“Linux”，版本选择Ubuntu 64位，点击下一步；

输入虚拟机的名称以及选择安装的路径，点击下一步；

然后可以在硬件配置界面对内存、处理器等进行一系列的设置，设置完成后点击右下角的关闭按钮，回到新建虚拟机导向后，点击完成。此时会自动跳转到虚拟机的开机界面，点击“开启此虚拟机”开始进行系统安装。

![输入图片说明](./assets/安装指南（For_MacOS）/%E5%AE%8C%E6%88%90%E8%99%9A%E6%8B%9F%E6%9C%BA.png)


语言选择设置“中文（简体）”，点击下一步；


时区选择“上海”，点击下一步；


用户信息设置完成后，点击下一步；


等待安装完成，点击“现在重启”按钮，就可以在虚拟机上使用openKylin开源操作系统了。