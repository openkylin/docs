## 一．镜像下载

通过以下链接下载：

- 4G版本: https://www.openkylin.top/downloads/download-smp.php?id=39
- 2G版本: https://www.openkylin.top/downloads/download-smp.php?id=40

根据个人需要下载对应的版本，下面以2G版本为例说明，下载得到 openKylin-1.0.1-phytiumpi-2G-arm64.img.xz 文件

## 二．镜像烧录

### 方法 1: 使用 dd 命令

- 烧录镜像
  ```
  xz -k -d -v openKylin-1.0.1-phytiumpi-2G-arm64.img.xz
  sudo dd if=openKylin-1.0.1-phytiumpi-2G-arm64.img of=/dev/<your sdcard> status=progress
  ```

### 方法 2: 使用 rpi-imager 工具

烧录程序安装：https://www.raspberrypi.com/software/

![arm64烧录程序安装地址.png](./assets/在arm设备上安装_common/arm64烧录程序安装地址.png)

插入 SD 卡，打开 rpi-imager，选择自定义镜像，然后选择镜像文件

![arm64烧录程序镜像选择.png](./assets/在arm设备上安装_common/arm64烧录程序镜像选择.png)

选择 SD 卡，点击 WRITE，等待制作完成

![arm64烧录程序SD卡选择.png](./assets/在arm设备上安装_common/arm64烧录程序SD卡选择.png)

## 三. 启动系统

- 插上 SDCard, 插上电源, 启动系统，默认用户名/密码是

```
> username：openkylin
> password：openkylin
```
