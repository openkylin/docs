## 一．镜像下载

通过以下链接下载：

https://www.openkylin.top/downloads

下载得到 openKylin-1.0-chilliepi-arm64.img.xz 文件

## 二. 板卡信息

### 1. 接口信息

![arm64双椒派板卡信息.jpg](./assets/在chilliepi（双椒派）上安装openKylin/arm64双椒派板卡信息.jpg)

### 2. 串口信息

![arm64双椒派串口信息.png](./assets/在chilliepi（双椒派）上安装openKylin/arm64双椒派串口信息.png)

## 三．镜像烧录

注意：<font color='red'>**双椒派上推荐使用金士顿 SD 卡**</font>

### 方法 1: 使用 dd 命令

- 烧录镜像
  ```
  xz -k -d -v openKylin-1.0-chilliepi-arm64.img.xz
  sudo dd if=openKylin-1.0-chilliepi-arm64.img of=/dev/<your sdcard> status=progress
  ```

### 方法 2: 使用 rpi-imager 工具

烧录程序安装：https://www.raspberrypi.com/software/

![arm64烧录程序安装地址.png](./assets/在arm设备上安装_common/arm64烧录程序安装地址.png)

插入 SD 卡，打开 rpi-imager，选择自定义镜像，然后选择镜像文件

![arm64烧录程序镜像选择.png](./assets/在arm设备上安装_common/arm64烧录程序镜像选择.png)

选择 SD 卡，点击 WRITE，等待制作完成

![arm64烧录程序SD卡选择.png](./assets/在arm设备上安装_common/arm64烧录程序SD卡选择.png)

## 四. chilliepi 上启动 openkylin

- 连接好键盘鼠标；插好 SD 卡；将 chilliepi 开发板通过 DP 线与显示器连接，建议通过电缆直连支持 DP 接口的显示器，如果通过转换器可能出现不兼容无显示的情况
  ![arm64双椒派接线方式.jpg](./assets/在chilliepi（双椒派）上安装openKylin/arm64双椒派接线方式.jpg)
- 通过串口连接开发板，插上电源, 快速按任意键, 使其进入到 u-boot 命令行界面
- 设置 U-boot 启动参数
  ```sh
  setenv bootargs console=ttyAMA1,115200  audit=0 earlycon=pl011,0x2800d000 root=/dev/mmcblk1p2 rootdelay=3 rw;
  setenv bootcmd 'mmc dev 1;fatload mmc 1:1 0x90000000 e2000d-chilli.dtb;fatload mmc 1:1 0x90100000 Image;booti 0x90100000 - 0x90000000;'
  saveenv
  ```
- 拔下电源, 拔下 SD 卡；插上 SDCard, 再插上电源, 即可启动系统。默认用户名/密码是
  ```
  > username：openkylin
  > password：openkylin
  ```

![arm64登录界面-embedded.png](./assets/在arm设备上安装_common/arm64登录界面-embedded.png)
![arm64桌面-embedded.png](./assets/在arm设备上安装_common/arm64桌面-embedded.png)

## 五．其他事项

### 1.磁盘大小

制作镜像的时候，默认的根分区大小是在文件系统大小的基础上额外加了一些空间，如果出现磁盘空间不足的情况
![arm64空间不足-embedded.png](./assets/在chilliepi（双椒派）上安装openKylin/arm64空间不足-embedded.png)

可以使用如下命令调整一下根分区大小，打开终端，执行如下命令可以将根分区扩展到最大
```sh
sudo resize-assistant
```
![arm64空间resize-embedded.png](./assets/在chilliepi（双椒派）上安装openKylin/arm64空间resize-embedded.png)

### 2.关闭显示器或休眠后唤醒黑屏

建议用户在控制面板的电源里设置从不关闭显示器与电源，可避免该问题

![arm64电源取消休眠与关闭显示器-embedded.png](./assets/在chilliepi（双椒派）上安装openKylin/arm64电源取消休眠与关闭显示器-embedded.png)