## 一．镜像下载

通过以下链接下载：

https://www.openkylin.top/downloads

下载后右键解压得到.img文件，xz后缀的镜像文件则可以不用解压直接使用


## 二．镜像烧录

烧录程序安装：https://www.raspberrypi.com/software/

![arm64烧录程序安装地址.png](./assets/在arm设备上安装_common/arm64烧录程序安装地址.png)


插入SD卡，打开rpi-imager，选择自定义镜像，然后选择安装好的.img镜像文件

![arm64烧录程序镜像选择.png](./assets/在arm设备上安装_common/arm64烧录程序镜像选择.png)


选择SD卡，点击WRITE，等待制作完成

![arm64烧录程序SD卡选择.png](./assets/在arm设备上安装_common/arm64烧录程序SD卡选择.png)



## 三．树莓派openkylin启动

接好树莓派的电源线、显示器线，连接好键盘鼠标，插好SD卡

![arm64树莓派接线方式.png](./assets/在树莓派上安装openKylin/arm64树莓派接线方式.png)

进入到登录界面，然后点击登录，即可登录到桌面

![arm64登录界面.png](./assets/在arm设备上安装_common/arm64登录界面-embedded.png)
![arm64桌面.png](./assets/在arm设备上安装_common/arm64桌面-embedded.png)



## 四．其他事项
### 1.系统卡顿

该问题由窗管占用cpu率较高导致，进入桌面后，到控制面板关闭特效模式，可以使得卡顿能够有效缓解

![arm64控制面板特效关闭.png](./assets/在arm设备上安装_common/arm64控制面板特效关闭.png)


### 2.修改密码

由于系统没有引导安装，因此刚装完的系统是没有设置密码的，需要用户自己装机后手动配置一遍密码，以便于使用系统时遇到需要输入密码的场景

![arm64修改密码.png](./assets/在arm设备上安装_common/arm64修改密码.png)

