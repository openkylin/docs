# virt-manager&openKylin-x86虚拟机安装指南
## 背景
本指南以Fedora服务器上的虚拟机管理器virt-manager为例，安装openKylin系统，并完成openKylin的相关配置。

## 安装流程
### 1. virt-manager下载
在本地或远端配置好的服务器安装virt-manager
```
dnf install virt-manager libvirt qemu-kvm -y
service libvirtd start
```
PS: 可能需要`sudo`权限

经过一段时间的安装，在shell中输入`virt-manager`确定是否成功安装成功。

PS2: 若在服务器配置，则ssh登录时需要-X参数`ssh -X username@x.x.x.x`，使用此命令需要配置
X11代理，具体配置参见[此文](../其他/ssh-X11代理配置.md)。

### 2. iso准备
[官网](https://www.openkylin.top/downloads/index-cn.html)中下载iso镜像
同时也可以利用wget命令直接下载到本地:
```
wget https://mirror.lzu.edu.cn/openkylin-cdimage/yangtze/openkylin-0.9.5-x86_64.iso
```

### 3. 安装开始
运行`virt-manager`，选择文件-新建虚拟机

![step1](./assets/virt-manager&openKylin-x86虚拟机安装/step1.png)

选择导入现有磁盘映像

![step2](./assets/virt-manager&openKylin-x86虚拟机安装/step2.png)

选择存储iso的路径，并选择操作系统为`Generic OS.`

![step3](./assets/virt-manager&openKylin-x86虚拟机安装/step3.png)

之后选择合适的内存&CPU个数

![step4](./assets/virt-manager&openKylin-x86虚拟机安装/step4.png)

选择合适的硬盘大小分配给虚拟机，最好大于20G

![step4-2](./assets/virt-manager&openKylin-x86虚拟机安装/step4-2.png)

进入该菜单，选择安装开放麒麟

![step5](./assets/virt-manager&openKylin-x86虚拟机安装/step5.png)

之后语言选择设置“中文（简体）”，点击下一步

![step6](./assets/virt-manager&openKylin-x86虚拟机安装/step6.png)

时区选择“上海”，点击下一步

![step7](./assets/virt-manager&openKylin-x86虚拟机安装/step7.png)

用户信息设置完成后，点击下一步

![step8](./assets/virt-manager&openKylin-x86虚拟机安装/step8.png)

选择全盘安装，并点击磁盘位置。

![step9](./assets/virt-manager&openKylin-x86虚拟机安装/step9.png)

格式化全部磁盘，点击开始安装，重启后，安装即完成。

![step10](./assets/virt-manager&openKylin-x86虚拟机安装/step10.png)

## 拓展知识——Linux上应用广泛的虚拟机管理软件
#### KVM
KVM是在包含虚拟化扩展的硬件上为Linux提供的完整虚拟化解决方案。KVM为各种各样的操作系统提供硬件虚拟化，包括Linux、Windows、macOS、ReactOS和Haiku。使用KVM，可以在未修改的Linux或Windows镜像上运行多个虚拟机。每个虚拟机都有自己的虚拟硬件：网卡、硬盘、显卡等等。

![KVM](./assets/virt-manager&openKylin-x86虚拟机安装/KVM.jpg)
#### QEMU
QEMU是一个通用的、开源的机器仿真器。当用作仿真器时，QEMU可以在另一台机器(如自己的x86_64 PC)上运行操作系统和程序。当用作虚拟器时，QEMU通过使用KVM直接在主机CPU上执行客户代码来实现近乎本机的性能。

![QEMU](./assets/virt-manager&openKylin-x86虚拟机安装/QEMU.jpg)
#### Libvirt
Libvirt是一个库和守护程序，提供了用于管理虚拟化主机的稳定的开源API。它针对多个虚拟机管理程序，包括QEMU，KVM，LXC，Xen，OpenVZ，VMWare ESX，VirtualBox等。

![Libvirt](./assets/virt-manager&openKylin-x86虚拟机安装/Libvirt.jpg)
#### Virt-manager
Virt-manager是用于通过libvirt管理虚拟机的桌面用户界面。它主要针对KVM虚机，但也管理Xen和LXC。它还包括命令行配置工具virt-install。其实virt-manager就是服务于虚拟机，而且易于使用的管理工具。如可以使用virt-manager在Linux上运行Windows环境，反之也可以。

![virt-manager](./assets/virt-manager&openKylin-x86虚拟机安装/virt-manager.jpg)