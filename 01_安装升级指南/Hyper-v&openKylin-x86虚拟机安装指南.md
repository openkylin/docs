# Hyper-v&openKylin-x86虚拟机安装指南
---

## 〇、背景
- **Hyper-v**是微软自Windows 8开始引入的虚拟化技术，在Windows 10中得到了进一步的完善，使用他的优点是可以充分释放性能(开启后Windows都是跑在Hyper-v上的，性能损失很小)，并且不需要安装虚拟机软件。
- **openKylin**是麒麟软件推出的Linux发行版，是中国首款根社区自主可控的Linux，本指南主要说明win11上使用Hyper-v安装openKylin系统，并完成相关初步配置。

## 一、准备工作
### 1.1 下载openKylin-x86镜像文件
可以从openKylin官方网站下载最新版本的镜像文件，官网地址为[https://www.openkylin.top/downloads/index-cn.html](https://www.openkylin.top/downloads/index-cn.html)。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/下载镜像.png)
### 1.2 安装Hyper-v
1. 按`windows+x`，点击设置
2. 搜索“启用或关闭”，点击显示所有结果，找到“启用或关闭Windows功能”，点击打开
![alt text](./assets/Hyper-v&openKylin-x86虚拟机安装/启用或关闭.png)
![](./assets/Hyper-v&openKylin-x86虚拟机安装/启用或关闭windows功能.png)
3. 勾选“Hyper-v虚拟机平台”和“Hyper-v虚拟交换机管理器”，点击确定
![](./assets/Hyper-v&openKylin-x86虚拟机安装/勾选Hyper-v.png)
4. 等待安装完成后重启电脑

## 二、创建虚拟机
### 2.1 打开虚拟机管理器
在开始菜单中搜索`hyper`即可找到虚拟机管理器，选择并打开
![](./assets/Hyper-v&openKylin-x86虚拟机安装/找到虚拟机管理器.png)
![](./assets/Hyper-v&openKylin-x86虚拟机安装/虚拟机管理器.png)
### 2.2 创建虚拟机
先点选自己的电脑，然后点击右侧的新建虚拟机
![](./assets/Hyper-v&openKylin-x86虚拟机安装/新建虚拟机.png)
### 2.3 设置虚拟机名称和位置
名称就写ok吧，位置就不要默认的C盘了这样会导致C盘爆炸，自定义一个位置：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/名称和位置.png)
### 2.4 设置代数
用第二代，支持uefi启动。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/代数.png)
### 2.5 分配内存
直接就默认的完事，反正可以动态内存，这里一定要确保你电脑剩余内存有你设置的这么大，否则虚拟机启动不了。
笔者当时开了一堆网页导致虚拟机启动失败，关闭浏览器回收了一些内存后才完成启动。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/内存.png)
### 2.6 设置网络
肯定要联网的，所以选择`default switch`即可，这样就可以和主机共享网络了。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/网络.png)
### 2.7 创建虚拟硬盘
选择默认的参数即可。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/虚拟硬盘.png)
不用担心这个数值太大占用硬盘太多，他其实是最大大小，vhdx是动态管理大小的，比如笔者安装完实际大小只有16.8G:
![](./assets/Hyper-v&openKylin-x86虚拟机安装/实际大小.png)


### 2.8 安装选项
这里选择你下载好的虚拟机镜像文件即可：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/选择镜像.png)
### 2.9 完成创建
点击完成即可，然后就可以启动虚拟机了。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/完成创建.png)
### 2.10 关闭安全启动
为了保险起见，我们关闭安全启动，这样安装的成功率更高。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/关闭安全启动.png)

## 三、安装openKylin
### 3.1 启动虚拟机
选择虚拟机然后点启动：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/启动虚拟机.png)
然后再点击连接：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/链接虚拟机.png)
此时就进入了体验环节(因为先点的启动，等点连接时候已经进入了live系统)：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/体验openKylin.png)
### 3.2 进入安装程序
点击`安装openKylin`：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/安装openKylin.png)
然后就跟普通的x86安装一样了，选择语言一步步配置往下走即可。
这里建议安装时候直接一根到底，比较方便，而且不像全盘安装容易出现系统分区不足的情况，也不会过多占用不用的空间：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/一根到底.png)
最后就是确认操作和开始安装：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/开始安装.png)
### 3.3 安装完成和卸载镜像
等待安装完成一般都比较快，完成后，选择关机：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/安装完成.png)
然后点击设置，卸载安装镜像文件：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/卸载镜像.png)
### 3.4 登录系统
然后点启动：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/启动.png)
等待启动后，输入密码即可登录系统：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/登录系统.png)
## 四、修改分辨率
此时我们会发现分别率是比较低的，窗口比较小，用着不舒服。
![](./assets/Hyper-v&openKylin-x86虚拟机安装/默认分辨率.png)
这其实很好办，我们只需要在虚拟机里设置分辨率即可生效：
![](./assets/Hyper-v&openKylin-x86虚拟机安装/修改分辨率.png)

## 五、配置openKylin
这块与其他方式安装均无区别，今天就不多做叙述，虚拟机有个好处可以直接点保存，不用关机，下次启动还是上次的状态。
## 六、总结
使用Hyper-v安装openKylin系统，可以充分利用Windows的性能，并且安装过程比较简单，推荐使用。