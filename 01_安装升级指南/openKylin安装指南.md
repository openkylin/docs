# 作者: 暗暗
# 日期：2023年3月20日

# OpenKylin简介

openKylin（开放麒麟） 社区是由基础软硬件企业、非营利性组织、社团组织、高等院校、科研机构和个人开发者共同创立的一个开源社区，旨在以“共创”为核心、以“开源聚力、共创未来”为社区理念，在开源、自愿、平等、协作的基础上，通过开源、开放的方式与企业构建合作伙伴生态体系，共同打造桌面操作系统顶级社区，推动Linux 开源技术及其软硬件生态繁荣发展。

![输入图片说明](./assets/openKylin安装指南/openKylin_intro.png)

# 镜像下载地址

[https://www.openkylin.top/downloads](https://www.openkylin.top/downloads)

我们根据自己设备CPU规格选择合适的版本下载，一般的PC或者笔记本请选择x86平台的镜像即可。

# 创建USB引导安装盘

在Windows中我们通常可以使用Rufus、UltraISO、balenaEtcher、unetbootin等软件来进行创建引导安装盘。

**以Rufus为例：**

- 通过rufus创建引导安装盘

![输入图片说明](./assets/openKylin安装指南/openrufus.png)

在Linux中我们推推进使用 balenaEtcher、unetbootin、Deepin Boot Maker等软件将下载的镜像烧录到U盘中。

# 安装系统
由于OpenKylin系统支持平板模式，所以在一带触摸功能的笔记本上会有更好的体验。
本文中的设备型号为 `ThinkPadx380Yoga` 非常便携的一款商务本，虽然款式有点老但是性能足以跑OpenKylin系统了。

![输入图片说明](./assets/openKylin安装指南/Lenovo_yoga_X380.png)

### 1、插入引导安装盘

将制作好的系统引导安装盘插入USB口

![输入图片说明](./assets/openKylin安装指南/openkylin_uboot.png)

### 2、USB引导启动

【联想】开机按`F12`进入引导选择界面，选择USB设备启动

![输入图片说明](./assets/openKylin安装指南/Lenovo_F12_Uboot.png)

> 其他品牌型号设备可以尝试以F9 F2 以及 Del键 进入USB引导项。

### 3、安装界面

进入安装界面后，我们可以使用键盘的上下键来选择引导项，如果什么都不操作，则经过几秒钟后会自动进入试用系统，当然在试用系统中我们也可以进行系统的安装。

![输入图片说明](./assets/openKylin安装指南/Openkylin_setup_01.png)

### 4、开始安装

运行桌面上的 `安装OpenKylin` 图标开始安装系统。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_02.png)

- **选择语言**

进入安装界面后首先是选择语言界面，默认为`中文简体`，初次使用的用户就不需要做更改，直接进入下一步。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_03.png)

- **选择时区**

在世界地图上点击相应位置确定当前时区，接着下一步继续，一般点击 亚洲/上海市区。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_04.png)

- **创建用户**

在此界面，创建用户名、计算机名称、及登录密码，密码需要两次匹配验证才可以进入下一步。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_05.png)

- **选择安装方式**

直接勾选默认块磁盘开始安装。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_06.png)

- **确认安装**

在这一步确认磁盘分区情况，如果想要自定义分区的，可以返回进行手动分区。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_07.png)

- **完成安装**

等待安装完成后，根据提示移除除安装U盘后再按`Enter`重启系统。

![输入图片说明](./assets/openKylin安装指南/openkylin_setup_08.png)

# 开始使用

重启系统后我们就可以登录系统开始使用了，初次启动OpenKylin，UI界面和Windows相差无疑，一些常用的快捷键也都能够通用。

![输入图片说明](./assets/openKylin安装指南/Openkylin_start.png)

### PC/平板 模式切换

openKylin系统支持两种系统界面模式的的切换

- **PC模式**

![输入图片说明](./assets/openKylin安装指南/openkylin_used_01.png)

点击导航栏处的气泡图标可以进行平板模式切换

![输入图片说明](./assets/openKylin安装指南/openkylin_used_02.png)

- **平板模式**

这款机器将键盘折叠到屏幕背后时会自动进入平板模式，这一点非常不错。

![输入图片说明](./assets/openKylin安装指南/openkylin_used_03.png)

点击导航栏右侧功能图标后可以切换回PC模式

![输入图片说明](./assets/openKylin安装指南/openkylin_used_04.png)

### 安装应用

打开应用商店我们可以进行软件安装，目前的软件仓库不全，期待有更多的软件进行适配。openKylin统也支持移动端App应用，从而解决了Linux系统软件少的这个问题。

![输入图片说明](./assets/openKylin安装指南/openkylin_used_05.png)
