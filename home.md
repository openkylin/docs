---
title: 首页
description: 
published: true
date: 2022-07-21T09:52:15.253Z
tags: 
editor: markdown
dateCreated: 2022-03-10T06:06:00.301Z
---

# 首页
欢迎来到openKylin文档平台，在这里您将了解到有关openKylin的相关介绍。

# openKylin开源社区

openKylin 社区（以下简称 “社区”）是在开源、自愿、平等和协作的基础上，由各种企业、非营利性组织、社团组织、高等院校、科研机构和个人开发者共同组成的一个开源社区。通过透明而公开的治理，来夯实开源协作开发，成为一个面向全球的开源社区。

openKylin 社区欢迎并鼓励所有人的参与，包括但不限于宣传、协助等，如：
- 宣传：如果您想索取我们的一些文章，或您想把一些新闻放到我们的新闻网页上
- 活动：研讨会与展览会或其他任何型式的聚会活动
- 协助：参与SIG兴趣小组共同协作等

联系我们：
- [网站](https://www.openkylin.top/community/)
- [邮箱](<contact@openkylin.top>)
- [论坛](https://forum.openkylin.top/portal.php)

我们欢迎任何人的贡献，且提供多方面的交流途径。目前社区有多种沟通渠道，请参考[社区交流](https://www.openkylin.top/community/community-cn.html)，欢迎大家加入社区沟通，营造良好技术氛围。

想要更深入了解社区，请前往：
- [关于我们](./07_关于社区/关于我们.md)
- [社区简介](./07_关于社区/社区简介.md)
- [社区组织架构](./07_关于社区/社区组织架构/社区治理组织架构.md)

# openKylin文档平台
负责人：docs sig（文档兴趣小组）

openKylin社区docs sig，致力于完善openKylin社区文档，帮助用户更好更方便的使用openKylin系统，主要工作内容是书写各类说明文档，包括但是不限于教程、答疑等内容

## 基本信息

### 仓库地址

[docs](https://gitee.com/openkylin/docs)

[FAQ文档规范](./04_社区贡献/FAQ文档贡献指南.md)


手机用户因为码云的自适应问题默认看到的是README文件，仓库具体内容访问此链接：[手机端仓库目录](https://gitee.com/openkylin/docs/tree/master)

### 小组成员

- 陌生人
- chipo
- AICloudOpser
- delong1998

### 通信方式

- 邮件列表：<docs@lists.openkylin.top>，[邮件列表订阅页面](https://mailweb.openkylin.top/postorius/lists/docs.lists.openkylin.top/)

## 工作内容

- 收集各种与openKylin有关的问题，并根据自己的理解写出对应的解决方法
- 书写各类教程，要求都可以在openKylin上使用，包括但是不限于教程、答疑等内容

# 文档贡献指南
## 内容要求
- 简洁明了，易于理解，必要时需要使用复杂的术语和概念，避免使用过于复杂的语法和逻辑。
- 面向普通用户的文档建议从用户的角度出发，以使用场景为案例，解决问题优先，避免只讲概念和技术知识。
- 文档内容要准确无误，技术上实事求是，避免使用夸张和误导性的语言。
- 要符合实际使用需求，应该以成熟稳定的技术和方法为主，避免使用过时的技术和方法，避免使用未经测试和验证的技术。
- 提交的文档不应该包含个人隐私信息，侵犯知识产权，违反法律法规的内容。
- 内容方向没有特殊要求，只要可以在openKylin上使用即可
- 内容需要照顾到新手用户，所有的流程都要尽可能详细
- 内容需要有标题、作者和创建时间，其它要求暂无
- 禁止带有侮辱性词汇，禁止政治敏感词汇
- 禁止发布广告
- 禁止发布违反法律法规的信息
- 文件名及分类文件夹尽可能言简意赅
- 结尾统一用.md的后缀，编码为统一的无BOM头的UTF-8
- 图片等资源统一放置在和文档同级的资源目录并分类
- 图片高度建议在640px左右、宽度不超过820px、图片一般为.png格式，大小不超过150K
- 为避免产权侵犯，引起纠纷，文档配图请使用原创图片或无版权图片
- 图片建议根据内容命名，只用数字序列不利于后续图片的继承
- 禁止任何分支强制推送
- 主分支不接受除dev分支以外的任何pr请求同时也不接受任何推送
- 主仓库的英文版建议放到en对应的目录，sig组成员会有专人去进行审核
- 如果路径有空格则全部用下划线隔开
- 翻译类的文档，整篇翻译的不接受轻量级pr，只能选择fork以后的pr，改动不多的可以接受轻量级pr
- 建议跳转链接在仓库内使用相对路径，除非链接是仓库之外的，尽量避免http开头的绝对路径


## 参与方法

- 参与这项工作没有更多的要求，只要愿意尝试且按照指南开展工作，遇到疑问或无法决定的问题时同其他人讨论，就一定可以做出高质量的成果
- sig组成员在仓库上建立对应的分支，格式为dev-名字，例如dev-moshengren，dev-chipo，在自己的分支上书写对应的内容
- 非sig成员可以在gitee上fork一下仓库，在自己仓库的dev分支进行修改，然后按照[内容要求](#内容要求)添加、补充或修改内容，完成后通过提pr的方式来提交到主仓库,通过审核后即可合并
- 非sig成员可以通过上述邮件列表<docs@lists.openkylin.top>或单独发送邮件给邮件列表所有者<docs@lists.openkylin.top>沟通来申请加入sig，建议先订阅，非订阅人员的邮件会被暂时拦截，邮件列表订阅连接：[docs订阅页面](https://mailweb.openkylin.top/postorius/lists/docs.lists.openkylin.top/)
- [sig申请加入流程](./07_关于社区/SIG管理指南/SIG组的申请与撤销流程)
- 参与贡献前可以先在仓库提一个Issue（只限于添加），说明想要书写的内容，方便查看是否有人正在写自己想写的内容，如果想要修改某些内容可以修改以后提pr
- 如果不太会写markdown格式的文档可以在搜索引擎搜索markdown文档语法或者通过邮件列表沟通，让sig成员帮忙转换

## 关于许可证

仓库默认使用CC BY-SA 4.0许可证，如果有其他需要可以在自己的文档下标明具体的CC许可证版本

以下内容插入文档末尾在文档平台生成页面的时候会自动变成对应的许可证
```
版权声明：本文为<xxx>原创，<xxx>修改，依据 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 许可证进行授权，转载请附上出处链接及本声明。

```