## 【小白课程】openKylin用户手册原理解析，一招教你学会自定义！

openKylin用户手册是详细描述openKylin操作系统的功能和用户界面、让用户了解如何使用该软件的说明书。通过阅读openKylin用户手册，能够更快更好地上手和使用openKylin操作系统。今天就带大家简单了解下openKylin用户手册的实现原理以及如何自定义用户手册内容。

![图片](https://www.openkylin.top/upload/202301/1673400351868967.png)

### 一、用户手册实现原理介绍

1.QtWebkit简介

openKylin操作系统上用户手册的启动、展示、跳转是在QtWebkit基础上实现的。下面我们来简单介绍一下QtWebkit。QtWebkit模块提供了一个在qt中使用web browser的engine，这使得我们在qt的应用程序中使用万维网上的内容变得很容易， 而且对其网页内容的控制也可以通过native controls实现。QtWebKit提供用于呈现超文本标记语言(HTML)、可扩展超文本标记语言(XHTML)和可伸缩矢量图形(SVG)文档的工具，这些文档使用级联样式表(CSS)样式，并使用JavaScript编写脚本。

2.用户手册跳转的接口

为了方便用户跳转到对应组件的帮助文档，用户手册提供了接口，使得其他组件调用接口传递参数后，可以直接打开用户手册对应内容，组件通过点击F1和菜单-帮助选项进行调用。DaemonIpcDbus::showGuideDaemonIpcDbus::showGuide提供了dbus接口，组件只需要传递对应参数调用，手册这边会根据参数，打开用户手册并跳转至对应内容。

3.用户手册运行流程上面介绍了用户手册跳转接口，下面重点介绍下用户手册运行的大致流程。

首先需要实例化QWebView，开启和禁用部分设置，加载用户手册网页文件。

![图片](https://www.openkylin.top/upload/202301/1673400403559673.png)

![图片](https://www.openkylin.top/upload/202301/1673400415449881.png)

其次，载入html时发送信号，将QObject对象传给JS，这样JS就能调用QObject的public slots函数

![图片](https://www.openkylin.top/upload/202301/1673400425220458.png)

![图片](https://www.openkylin.top/upload/202301/1673400459129591.png)

![图片](https://www.openkylin.top/upload/202301/1673400473845933.png)

再次，web端调用qt接口，获取文档信息和目录结构，动态生成首页应用。js端调用qt端函数获取信息：

![图片](https://www.openkylin.top/upload/202301/1673400482997460.png)

最后，qt端获取手册文件结构，获取首页图标名称和文件夹名称，以及对应的文档路径：

![图片](https://www.openkylin.top/upload/202301/1673400491336760.png)

通过获取到的图片名称、文档路径、文件夹名称；加载图标信息，完成首页应用的加载。openKylin用户手册首页的加载流程大致就如上面所述，但实际还会涉及到Markdown文件的渲染展示、目录和内容的跟随效果、手册目录级的跳转和自动读取文档的更新日期等，这里就不一一介绍啦。

### 二、用户手册内容自定义

大家在系统集成新组件时，会希望新增组件后，把对应的组件手册内容也自动加入用户手册中并能够进行跳转，那么这里就给大家介绍一下如何集成自己的手册内容！

#### 1.文件夹结构

![图片](https://www.openkylin.top/upload/202301/1673400502196692.png)

ull Requ
需要包含语言文件夹，一个图标；其中文件夹名称和图标名称保持一致，会用于首页图标展示以及用户手册dbus接口调用参数。

#### 2.Markdown文件结构

![图片](https://www.openkylin.top/upload/202301/1673400514730147.png)

这里Markdown文档的一级标题会作为首页应用的展示名称。

##3 3.修改install文件

![图片](https://www.openkylin.top/upload/202301/1673400526886467.png)

将准备好的文件夹，放到用户手册的资源文件下面，当用户手册加载时便会自动读取，进行加载展示在首页。按照以上步骤进行操作，就可以完成用户手册内容的自定义，实现系统组件手册内容的即增即减啦！你学会了嘛？





来源：谢嘉华

审核：openKylin
