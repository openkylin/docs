# 准备工作
## 注册gitee账号
https://gitee.com/
## 签署cla
https://cla.openkylin.top/
## 申请成为单包维护者
```
https://gitee.com/openkylin/community/tree/master/packages
name: ci-test-sample
path: ci-test-sample
maintainers:
- name: xiewei
  openkylinid: xiewei
  displayname: 谢炜
  email: xiewei@kylinos.cn
```
管理员审核通过后，后台将自动创建仓库，地址如下：https://gitee.com/openkylin/whfg-test

# 初始化仓库并上传demo代码
## 按照说明初始化仓库
```
mkdir demo
cd demo
git clone https://gitee.com/openkylin/whgf-test.git
```
## 上传自己的源代码到gitee仓库，并创建openkylin/nile打包分支
```
touch README.md
mkdir user/
mkdir user/bin
touch user/bin/whgf-test
```
## 在程序文件中写入以下脚本代码后保存文件：
```
#!/bin/sh
echo "Hello openKylin, start your first app!"
```
## 增加可执行权限：
```
chmod +x usr/bin/whgf-test
```

## 上传代码到仓库并创建打包分支
```
git add .
git commit -m "first commit"
git push
```
创建openKylin 2.0系列打包分支

## 本地拉取上游更新，并切换到打包分支
```
git pull
git checkout openkylin/nile
```
回到上一级目录


# 生成debian目录
```
cp -r whgf-test/ whgf-test-1.0.0/
tar -cvzf whgf-test-1.0.0.tar.gz whgf-test-1.0.0/
cd whgf-test-1.0.0
dh_make -f ../whgf-test-1.0.0.tar.gz -s -y
```

分别然后补充changlog/control/文件里的信息
修改format文件里的格式为native

# 将修改上传到仓库
```
git add debian
git commit -m "add debian"
git push
```

openKylin自动化开发者平台将自动构建上传软件包，审核通过后进行编译

# 集成到镜像
## 手动集成
软件包编译完成后，进行下载，然后集成到镜像中去
https://gitee.com/openkylin/docs/blob/master/04_%E7%A4%BE%E5%8C%BA%E8%B4%A1%E7%8C%AE/%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97/openKylin%E7%B3%BB%E7%BB%9F%E9%95%9C%E5%83%8FISO%E5%AE%9A%E5%88%B6%E6%8C%87%E5%8D%97.md