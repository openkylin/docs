# 如何在openkylin上个性化定制开关机动画

开关机动画是openkylin系统的重要组成部分，其主要功能是在Linux内核启动的早期遮盖内核打印日志，并在内核刷新屏幕分辨率时保证屏幕显示的流畅性。openkylin使用plymouth组件作为开关机动画显示程序。Linux系统在启动时，从加载内核到进入登录界面时会进行多次屏幕刷新，影响观感，plymouth通过内核中对“内核模式设置”（Kernel Mode-Setting），能够向用户提供一个更加流畅和连续的开关机观感体验。

## plymouth的功能

plymouth支持开关机动画定制，通过添加自定义的图片和脚本，用户可以使用自己定制的开关机动画，实现多样化的开关机体验。同时，当需要进行长时间的后台操作（如系统安装、备份）时，可以手动执行命令启动plymouth程序显示动画界面，实时显示进度等信息，同时能够避免在后台操作运行期间用户进行其他操作，保证后台操作不受干扰。

## 如何定制自己的开关机动画

### plymouth主题相关内容的存放位置

/usr/share/plymouth/themes/目录下存放了各个主题包的素材，以openkylin主题为例，下图是/usr/share/plymouth/themes/openkylin下的内容：

![descript](./assets/如何在openkylin上制作个性化开关机动画/1720961423.2230046.jpeg)

其中，所有的png文件为plymouth动画显示需要的序列帧和显示其他信息所需的素材:

![descript](./assets/如何在openkylin上制作个性化开关机动画/1720961423.2243407.jpeg)

openkylin.plymouth文件记录了openkylin主题所需的图片和脚本的路径，如图：

![descript](./assets/如何在openkylin上制作个性化开关机动画/1720961423.2249289.jpeg)

openkylin.script为openkylin主题的运行脚本，包含了该主题的所有显示逻辑。

### 创建自己的主题目录

创建目录：mkdir /usr/share/plymouth/themes/mytheme

### 在新添加的主题目录中添加自己的素材

添加动画所需的png图片，创建mytheme.plymouth，内容如图：

![](./assets/如何在openkylin上制作个性化开关机动画/1720961423.225891.png)创建mytheme.script，内容可参考openkylin.script内容。

### 修改配置文件和显示脚本

在mytheme.plymouth中指定/usr/share/plymouth/themes/mytheme为图片存放路径，指定/usr/share/plymouth/themes/mytheme.script为显示脚本

根据实际业务需要，修改mytheme.script脚本。

### 注册配置文件信息

执行命令：

sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/mytheme/mytheme.plymouth 160

sudo update-alternatives --set default.plymouth /usr/share/plymouth/themes/mytheme/mytheme.plymouth

### 更新initramfs

执行命令：

sudo update-initramfs -u

### 重启系统

重启系统后即可使新的自定义主题生效

### 修改前后对比

修改前的开关机动画：

![](./assets/如何在openkylin上制作个性化开关机动画/1720961423.2269077.jpeg)修改后的关机动画：

![](./assets/如何在openkylin上制作个性化开关机动画/1720961423.2271826.jpeg)## 如何快速修改系统开关机主题

如果需要快速修改系统开关机主题，可以直接将自定义的序列帧图片1.png~72.png复制到/usr/share/plymouth/themes/openkylin/下，替换原有图片（注意，自定义图片数量和序号要和原有文件数量和序号一致，如果原主题有1.png~100.png共一百张图片，则自定义的图片也需要有命名为1.png~100.png的一百张图片），然后执行sudo initramfs -u，重启后即可使新主题生效。需要说明的是，这样修改后，旧主题的内容将会被覆盖，不能恢复。

## 参考资料

### plymouth官方网站：

<https://www.freedesktop.org/wiki/Software/Plymouth/>

### plymouth脚本语法：

<https://www.freedesktop.org/wiki/Software/Plymouth/Scripts/>

### plymouth源代码地址：

<https://gitlab.freedesktop.org/plymouth/plymouth>

