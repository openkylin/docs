# openkylin基础 设置 查看系统默认快捷键
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 设备 - 快捷键

![image](./assets/查看系统默认快捷键/ok-sck-1.png)

快捷键

![image](./assets/查看系统默认快捷键/ok-sck-2.png)

我主要是用 "截图" 和 "截取一个区域的截图"，这两个快捷键。

&emsp;

