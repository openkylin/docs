# <center>从NVIDIA官网安装NVIDIA闭源显卡驱动</center>
#### <center>作者：林统度</center>
#### <center>2022-11-06 18:00:00</center>
## GPU-Docs/NVIDIA-GPU-Supported(支持NVIDIA显卡)
一、从NVIDIA官网上下载对应的NVIDIA闭源显卡驱动。
   在浏览器使用搜索引擎搜索“NVIDIA Unix Driver”或者是在浏览器的地址栏输入“https://www.nvidia.com/en-us/drivers/unix/”（英伟达中文官网“https://www.nvidia.cn/drivers/unix”也可以）进入NVIDIA官网的显卡UNIX系统驱动程序页面，选择Linux x86_64/AMD64 。在Linux x86_64/AMD64下根据需要选择 Latest Production Branch Version 最新的开发分支版本、Latest New Feature Branch Version 最新的功能特色分支版本或者是 Latest Legacy GPU version 最新的停止更新的GPU驱动版本。一般选择 Latest Legacy GPU version {390.xx.series} 及以上的进行下载。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041601588081_%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE2022-03-04155609.png)
   进去绿色的链接后再点击 SUPPROTED PRODUCTS 产品支持列表查看自己的NVIDIA显卡型号是否在里面，如没有就返回NVIDIA Unix Archive英伟达Unix系统驱动程序页面继续寻找所需显卡驱动。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041602357887_%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE2022-03-04155757.png)

二、停用系统附带开源的NVIDIA显卡驱动nouveau。
   打开“计算机”、“我的电脑”或者文件管理器，然后打开系统盘，再打开/etc/modprobe.d/，新建一个文本文档txt，打开在里面输入两行命令。第一行：blacklist nouveau 第二行：options nouveau modeset=0。将这个文本文档txt另存为此文件夹，修改名称为blacklist-nouveau，后缀从.txt改为.conf。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041703478511_IMG_20220304_163247.jpg)
如果modprobe.d文件夹显示被锁住的话请点击鼠标右键选择“以管理员身份打开”或者打开终端输入命令"sudo nano /etc/modprode.d/blacklist-nouveau.conf"然后输入管理员密码，通过Nano文本编辑器新建 blacklist-nouveau.conf文件。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041603201847_%E6%88%AA%E5%9B%BE_20220304153721.png)
   接着在终端输入命令“sudo update-initramfs -u”更新Linux 内核Kernel 的 initramfs 文件，然后输入sudo reboot重新启动Linux系统。重新启动后按 Ctrl键+Alt键+F2键 进入命令行模式，输入命令“lsmod | grep nouveau”，如果没有结果输出则证明系统没有加载nouveau驱动，成功禁用了nouveau驱动。

三、在命令行编译安装NVIDIA闭源显卡驱动。
   在命令行模式下输入命令“sudo service lightdm stop”停止 lightdm 运行，然后输入命令“cd 下载的NVIDIA显卡驱动所在的文件夹路径”，再输入命令“sudo su”开启管理员模式。然后输入命令“sudo chmod +x 下载的NVIDIA显卡驱动名称”给予下载的NVIDIA显卡驱动修改系统的权限，最后输入命令“sudo ./下载的NVIDIA显卡驱动名称”运行NVIDIA显卡驱动编译。
   安装过程会出现第一个弹窗："Would you like to register the kernel module sources with DKMS ?This will allow DKMS to automatically build a new module ,if you install a different kernel later. "意思大概是“是否要向DKMS注册内核模块源代码？这将允许DKMS自动构建一个新的模块，如果您以后安装另一个内核的话“。请选择No。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041704538162_IMG_20220304_165021_edit_16691001473494.jpg)
第二个弹窗："Install NVIDIA ' s 32-bit compatibility libraries? "意思是“需要安装NVIDIA的32位兼容性库？”。选择Yes。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041707155089_IMG_20220304_165105_edit_16733711034946.jpg)
第三个弹窗："Would you like to run the nvidia-xconfig utility to automatically update your X conf iguration file so that the NVIDIR X driver will be used when you restart X ? Any pre-existing configuration file will be backed up. "意思大概是“是否要运行nvidia-xconfig实用程序，自动更新你的 X conf 配置文件，以便在重新启动X时使用 NVIDIA X 驱动程序？将备份任何预先存在的配置文件”。选择No。如果选择Yes，重启电脑后会无法进入桌面。

四、验证NVIDIA显卡驱动是否安装成功
   完成安装后输入命令"nvidia-smi"验证NVIDIA显卡驱动是否安装成功，如果有结果显示就证明安装成功了。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/20220304162553456_nvidiasmi.png)

版权声明：本文为<深度Deepin论坛用户林统度>原创，原文链接：[[新手教程] 如何使用命令行安装NVIDIA显卡驱动-林统度-深度Deepin论坛](https://bbs.deepin.org/post/232502)，<码林平>转载，依据 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 许可证进行授权，转载请附上出处链接及本声明。

## GPU-Docs/NVIDIA-GPU-Supported(支持NVIDIA显卡) 如何使用命令行在远程服务器上安装NVIDIA显卡

一、从NVIDIA官网上下载对应的NVIDIA闭源显卡驱动（和前者一样）

   在浏览器使用搜索引擎搜索“NVIDIA Unix Driver”或者是在浏览器的地址栏输入“https://www.nvidia.com/en-us/drivers/unix/”（英伟达中文官网“https://www.nvidia.cn/drivers/unix”也可以）进入NVIDIA官网的显卡UNIX系统驱动程序页面，选择Linux x86_64/AMD64 。在Linux x86_64/AMD64下根据需要选择 Latest Production Branch Version 最新的开发分支版本、Latest New Feature Branch Version 最新的功能特色分支版本或者是 Latest Legacy GPU version 最新的停止更新的GPU驱动版本。一般选择 Latest Legacy GPU version {390.xx.series} 及以上的进行下载。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041601588081_%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE2022-03-04155609.png)
   进去绿色的链接后再点击 SUPPROTED PRODUCTS 产品支持列表查看自己的NVIDIA显卡型号是否在里面，如没有就返回NVIDIA Unix Archive英伟达Unix系统驱动程序页面继续寻找所需显卡驱动。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/202203041602357887_%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE2022-03-04155757.png)

二、禁用Nouveau

1、输入以下命令进入管理员权限
```
$ sudo -s
```
2、禁用ubuntu自带的驱动（Nouveau）
3、查看nouveau是否禁用
```
$ lsmod | grep nouveau
```
有显示内容，则表示没有禁用，需要禁用。
4、在/etc/modprobe.d/blacklist.conf中把nouveau添加到黑名单。
```
$ 打开vim /etc/modprobe.d/blacklist.conf
```
在最后添加如下命令，保存退出：
```
$ blacklist nouveau
```
5、使用以下命令更新使其生效：
```
$ update-initramfs -u
```
6、使用如下命令重启服务器：
```
$ reboot
```
7、再次检查是否禁用：
```
$ lsmod | grep nouveau
```
如果该命令输入后回车，没有任何消息回显，则禁用成功，可以继续，否则重新开始上面步骤。

三、安装驱动程序

将驱动程序使用xftp之类的软件上传到服务器目录下，cd到上传的对应目录安装,输入如下命令安装：
```
$ sh NVIDIA-LINUX-x86_64-515.57.run --no-opengl-files
```
一路按照默认的OK就可以啦。

四、验证NVIDIA显卡驱动是否安装成功

   完成安装后输入命令"nvidia-smi"验证NVIDIA显卡驱动是否安装成功，如果有结果显示就证明安装成功了。
![输入图片说明](../assets/%E4%BB%8ENVIDIA%E5%AE%98%E7%BD%91%E5%AE%89%E8%A3%85%E9%97%AD%E6%BA%90%E6%98%BE%E5%8D%A1%E9%A9%B1%E5%8A%A8/20220304162553456_nvidiasmi.png)

## 使用终端直接安装NVIDIA显卡驱动

直接在终端执行

```

sudo apt install nvidia-driver

```