# openkylin基础 刻录 创建ISO镜像
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

左下角logo - 其他 - 刻录

![image](./assets/刻录_创建ISO镜像/ok-createiso-1.png)

添加刻录数据

![image](./assets/刻录_创建ISO镜像/ok-createiso-2.png)

创建镜像

![image](./assets/刻录_创建ISO镜像/ok-createiso-3.png)

检查创建的对象

![image](./assets/刻录_创建ISO镜像/ok-createiso-4.png)

ISO里装文件的形式，一般可以用在：虚拟机通过挂载镜像的方式，来传送文件

&emsp;

