# 基于KylinCode+DeepSeek实现AI编程助手


### 第一步：
安装并运行ollama服务及DeepSeek-R1模型，具体见上一期文章：https://docs.openkylin.top/zh/02_%E5%9F%BA%E7%A1%80%E6%93%8D%E4%BD%9C/AI%E6%A8%A1%E5%9E%8B%E7%AE%A1%E7%90%86/%E5%9F%BA%E4%BA%8EopenKylin%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E5%B9%B6%E8%BF%90%E8%A1%8CDeepSeek-R1%E5%BC%80%E6%BA%90%E6%A8%A1%E5%9E%8B

![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EKylinCode+DeepSeek%E5%AE%9E%E7%8E%B0AI%E7%BC%96%E7%A8%8B%E5%8A%A9%E6%89%8B/1.png)

### 第二步：
打开KylinCode集成开发工具，在插件商店搜索并安装“Continue”，这是一款开源AI编程助手插件。安装后如下图所示：

![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EKylinCode+DeepSeek%E5%AE%9E%E7%8E%B0AI%E7%BC%96%E7%A8%8B%E5%8A%A9%E6%89%8B/2.png)

### 第三步：
在“Continue”插件点击“Add Chat model”。

![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EKylinCode+DeepSeek%E5%AE%9E%E7%8E%B0AI%E7%BC%96%E7%A8%8B%E5%8A%A9%E6%89%8B/3.png)

选择我们在第一步已运行的ollama服务及DeepSeek-R1模型，然后点击“Connect”完成DeepSeek-R1模型配置，如下图所示：

![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EKylinCode+DeepSeek%E5%AE%9E%E7%8E%B0AI%E7%BC%96%E7%A8%8B%E5%8A%A9%E6%89%8B/4.png)

### 第四步：
按提示选择需要处理的代码文件，并说明需要DeepSeeK如何处理。如下图所示，请DeepSeek帮忙分析一下这段代码：

![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EKylinCode+DeepSeek%E5%AE%9E%E7%8E%B0AI%E7%BC%96%E7%A8%8B%E5%8A%A9%E6%89%8B/5.png)