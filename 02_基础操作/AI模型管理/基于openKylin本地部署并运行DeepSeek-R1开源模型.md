# 基于openKylin本地部署并运行DeepSeek-R1开源模型

## 第一步：下载并安装ollama


●方式一

执行如下命令：

`curl -fsSL https://ollama.com/install.sh | sh`

●方式二　

前往github平台自行下载后解压，下载地址如下：


`https://github.com/ollama/ollama/releases/tag/v0.5.7`


执行如下命令进行解压缩：


`sudo tar -C /usr -xzf ollama-linux-amd64.tgz`

●方式三　

如以上两种方式均无法成功下载，可通过关注openKylin公众号，回复ollama，获取百度网盘链接进行下载。　

## 第二步：启动ollama服务　

执行如下命令启动ollama服务：　

`ollama serve`
![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EopenKylin%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E5%B9%B6%E8%BF%90%E8%A1%8CDeepSeek-R1%E5%BC%80%E6%BA%90%E6%A8%A1%E5%9E%8B/%E5%90%AF%E5%8A%A8ollama.png)

然后打开一个新的终端窗口，查看是否已启动成功，如下图所示：　
![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EopenKylin%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E5%B9%B6%E8%BF%90%E8%A1%8CDeepSeek-R1%E5%BC%80%E6%BA%90%E6%A8%A1%E5%9E%8B/%E6%9F%A5%E7%9C%8Bollama%E7%89%88%E6%9C%AC%E5%8F%B7.png)

## 第三步：下载并安装运行DeepSeek-R1模型　

执行如下命令下载并安装运行DeepSeek-R1 1.5b模型　

`ollama run deepseek-r1:1.5b`
![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EopenKylin%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E5%B9%B6%E8%BF%90%E8%A1%8CDeepSeek-R1%E5%BC%80%E6%BA%90%E6%A8%A1%E5%9E%8B/%E4%B8%8B%E8%BD%BDdeepseek%E6%A8%A1%E5%9E%8B.png)

注意：本次演示以1.5b模型为例，若您的电脑性能更高，可下载安装DeepSeek-R1更高规格的模型，效果更好哦！具体官方命令如下：　

DeepSeek-R1-Distill-Qwen-1.5B

`ollama run deepseek-r1:1.5b`

　
DeepSeek-R1-Distill-Qwen-7B　　

`ollama run deepseek-r1:7b`　　

DeepSeek-R1-Distill-Llama-8B　　

`ollama run deepseek-r1:8b`　　

DeepSeek-R1-Distill-Qwen-14B　　

`ollama run deepseek-r1:14b`　　

DeepSeek-R1-Distill-Qwen-32B　　

`ollama run deepseek-r1:32b`　　

DeepSeek-R1-Distill-Llama-70B　　

`ollama run deepseek-r1:70b`　　


至此，已完成DeepSeek-R1模型的部署，接下来我们来问它一个问题测试一下：　
![输入图片说明](../assets/%E5%9F%BA%E4%BA%8EopenKylin%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E5%B9%B6%E8%BF%90%E8%A1%8CDeepSeek-R1%E5%BC%80%E6%BA%90%E6%A8%A1%E5%9E%8B/%E6%B5%8B%E8%AF%95%E9%97%AE%E7%AD%94%E8%BF%90%E8%A1%8C%E7%BB%93%E6%9E%9C.png)

可以看到，基于openKylin开源操作系统本地部署的DeepSeek-R1 1.5b模型，在较短的时间内即可给出它的分析过程及最终答案。


