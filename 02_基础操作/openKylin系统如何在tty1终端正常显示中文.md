# openKylin系统如何在tty1终端正常显示中文
用户如果遇到桌面卡死需要到tty1终端里去查看日志时，往往因为无法查看中文目录而感到棘手。其实有多种方式可以个性化配置您的tty1窗口，让它可以正常显示中文。本文档将主要介绍通过fbterm的方式
## 第一步
执行如下命令安装fbterm、fcitx5-frontend-fbterm这两个包：

```
sudo apt install fbterm fcitx5-frontend-fbterm
```

## 第二步
在~/.fbtermrc文件里，设置字体大小、输入法，如下图所示：

![输入图片说明](assets/openKylin%E7%B3%BB%E7%BB%9F%E5%A6%82%E4%BD%95%E5%9C%A8tty1%E6%A8%A1%E5%BC%8F%E6%AD%A3%E5%B8%B8%E6%98%BE%E7%A4%BA%E4%B8%AD%E6%96%87/1.png)

![输入图片说明](assets/openKylin%E7%B3%BB%E7%BB%9F%E5%A6%82%E4%BD%95%E5%9C%A8tty1%E6%A8%A1%E5%BC%8F%E6%AD%A3%E5%B8%B8%E6%98%BE%E7%A4%BA%E4%B8%AD%E6%96%87/2.png)

## 第三步
执行如下命令设置在fbterm终端下可使用快捷键
```
sudo setcap 'cap_sys_tty_config+ep' /usr/bin/fbterm
```
## 第四步
在~/.bashrc文件中，增加如下配置，将fbterm设置成tty桌面默认终端


```
alias fbterm='LANG=zh_CN.UTF-8 fbterm'

if [[ "$TERM" = linux ]] && [[ "$(ps otty= $$)" =~ tty ]] && type fbterm &>/dev/null; then

        fbterm

fi
```

## 最终效果
![输入图片说明](assets/openKylin%E7%B3%BB%E7%BB%9F%E5%A6%82%E4%BD%95%E5%9C%A8tty1%E6%A8%A1%E5%BC%8F%E6%AD%A3%E5%B8%B8%E6%98%BE%E7%A4%BA%E4%B8%AD%E6%96%87/3.png)