 **作者：暗暗** 

 **时间：2023年3月1日** 

# OpenKylin新手使用指南

### 1、系统更新

我们使用快捷键 <kbd> Ctrl </kbd> + <kbd> Alt </kbd> + <kbd> T </kbd> 打开终端并输入以下命令完成系统更新。

```shell
sudo apt update
sudo apt upgrade
```
### 2、触摸板指针移动速度慢，该怎么调？

我们使用快捷键 <kbd> Win </kbd> + <kbd> I </kbd> 打开置中心，在 `设备` 中找到 `触控板` 功能。

点击右侧面板中的指针速度状态条，我们可以不断尝试移动速度，速度调整到适合自己的习惯为止。

![输入图片说明](./assets/OpenKylin新手使用指南/mouse_speed_set.png)

### 3、系统常用快捷键

![输入图片说明](./assets/OpenKylin新手使用指南/systemhotkey.png)

### 4、截图快捷键

![输入图片说明](./assets/OpenKylin新手使用指南/jietu_hotkey.png)

### 5、开启ssh登录

- **安装 openssh-server** 

```shell
sudo apt install openssh-server -y
```
- **修改配置文件 `ssh_config`** 

```shell
sudo vi /etc/ssh/ssh_config
```

>找到`# PasswordAuthentication yes`，将前面的#号删除保存退出

![输入图片说明](./assets/OpenKylin新手使用指南/vi_ssh_config.png)

- **重启ssh服务** 

```shell
sudo service sshd restart
```

- **查看ip地址**

```shell
ifconfig
或
ip address
``` 

![ifconfig 查看ip地址](./assets/OpenKylin新手使用指南/ifconfig01.png)

- **ssh远程登录** 

我们在windows的终端通过ssh登录命令访问

```cmd
# 示例 ssh 账户名@IP地址
ssh anan@192.168.124.82
```

![输入图片说明](./assets/OpenKylin新手使用指南/ssh_link_openkylin.png)

### 6、Apt 软件安装命令 

apt 命令执行需要超级管理员权限(root)，因此在前面需要加上`sudo` 命令

```shell
# 安装软件
sudo apt install 软件包
# 卸载软件
sudo apt remove 软件包
# 更新软件
sudo apt upgrade
# 例如
sudo apt install openssh-server
sudo apt install putty
```
-  **详细命令参数** 

```shell
apt []
      list - 根据名称列出软件包
      search - 搜索软件包描述
      show - 显示软件包细节
      install - 安装软件包
      reinstall - 重新安装软件包
      remove - 移除软件包
      autoremove - 卸载所有自动安装且不再使用的软件包
      update - 更新可用软件包列表
      upgrade - 通过 安装/升级 软件来更新系统
      full-upgrade - 通过 卸载/安装/升级 来更新系统
      edit-sources - 编辑软件源信息文件
      satisfy - 使系统满足依赖关系字符串
```

- **详细帮助可以通过命令查看** 

```shell
apt -h
```

### 7、wget 下载命令 

wget是一个从网络上自动下载文件的自由工具，支持通过HTTP、HTTPS、FTP三个最常见的TCP/IP协议下载。

对于网络上已经提供下载路径的程序或者包我们可以通过 wget 命令进行下载，我们最常用于deb软件包的下载。

- **用法：**

```shell
wget [选项参数]... [Url].....
``` 

- **示例：** 

```shell
示例
wget http://www.xxx.com/download/putty.deb
```

- **详细帮助可以通过命令查看** 

```shell
wget -h
```

### 8、dpkg 安装命令 

软件安装命令 dpkg

dpkg是一个debian包管理工具，能够对deb格式的包进行安装、卸载、获取信息等操作。

- **用法：** 

```shell
dpkg -i package_file
dpkg --install package_file
```
- **示例：** 

```shell
# 接上一个示例下载的putty.deb文件
dpkg -i putty.deb
```

- **详细帮助可以通过命令查看** 

```shell
dpkg --help
```
### 9、使用命令打开控制面板

点击各功能模块时可以看到问题的报错信息

```
ukui-control-center
``` 
