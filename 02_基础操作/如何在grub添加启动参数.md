# <center>如何在grub添加启动参数</center>
#### <center>作者：小K</center>
#### <center>2022-04-22 23:36:00</center>
#### <center>Edit：f4prime</center>

## 如何在grub添加启动参数
对于grub2，ubuntu给了一个官方的配置文件`/etc/default/grub`。大部分情况下grub2的设置都可以在这个文件中搞定，而且这个文件结构也比较简单，修改起来也容易。完全没有必要直接改`/boot/grub/grub.cfg`或者`/etc/grub.d/`下的文件。

修改`/etc/default/grub`只需简单一个命令：
```sh
sudo vim /etc/default/grub
```
下面是系统默认的内容，以及最常用的修改菜单显示时间和默认操作系统的方法：
```sh
 #If you change this file, run 'update-grub' afterwards to update
 #/boot/grub/grub.cfg.

GRUB_DEFAULT=0 #将0改为saved，可让grub记住上次启动时选择的系统
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_TIMEOUT="5" #显示启动选择菜单的时间
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX=""
```

修改完成后执行以下命令即可自动更新`/boot/grub/grub.cfg`。

```sh
$ sudo update-grub  //生成grub的配置文件
```
