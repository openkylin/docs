# 在openKylin RISC-V编译运行Godot

为了扩展openkylin RISC-V游戏生态，一直在探索Godot这一开源游戏引擎在RISC-V设备上原生编译，以下是编译指南。特别感谢感谢Github开发者[MBCX](https://github.com/MBCX)的RISC-V交叉编译方法提供帮助和参考！


![openKylin RISC-V Godot](assets/%E5%9C%A8openKylin-RISC-V%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CGodot/openKylinRISCVGodot.jpg)


## 一、基础准备

编译Godot至少需要4G内存，如果不足请开启swap，文章中演示的设备是8G内存。

安装openKylin 2.0 RISC-V镜像，这里演示使用的是SpacemiT K1（Muse Book），请下载安装对应板卡的镜像。

[在SpacemiT_K1上安装openKylin | openKylin文档平台](https://docs.openkylin.top/zh/01_%E5%AE%89%E8%A3%85%E5%8D%87%E7%BA%A7%E6%8C%87%E5%8D%97/%E5%9C%A8riscv%E4%B8%8A%E5%AE%89%E8%A3%85/%E5%9C%A8SpacemiT_K1%E4%B8%8A%E5%AE%89%E8%A3%85openKylin)

## 二、克隆源代码

克隆Godot 4.3源代码

```bash
sudo apt update
sudo apt install git
git clone https://github.com/godotengine/godot.git -b 4.3-stable --depth=1
```

## 三、安装编译依赖

安装Godot编译依赖
```bash
sudo apt update
sudo apt install -y \
  build-essential \
  pkg-config \
  libx11-dev \
  libxcursor-dev \
  libxinerama-dev \
  libgl1-mesa-dev \
  libglu1-mesa-dev \
  libasound2-dev \
  libpulse-dev \
  libudev-dev \
  libxi-dev \
  libxrandr-dev \
  libwayland-dev
```
其中，openKylin 2.0的build-essential中gcc和g++版本默认是12，需要使用update-alternatives修改默认版本，切换到gcc-13和g++-13

```bash
# 安装gcc-13 g++-13
sudo apt install gcc-13 g++-13

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 100
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13 100

# 输入gcc-13对应的数字序号
sudo update-alternatives --config gcc
```

![切换GCC版本](assets/%E5%9C%A8openKylin-RISC-V%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CGodot/%E5%88%87%E6%8D%A2GCC%E7%89%88%E6%9C%AC.png)

```bash
# g++也是相同的步骤。

sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-12 100
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-13 100

# 输入g++-13对应的数字序号
sudo update-alternatives --config g++
```

Godot项目使用SCons构建，SCons是一个开放源代码、以Python语言编写的自动化构建工具，在openKylin使用pip包安装程序安装SCons。
```bash
sudo apt install python3-pip

sudo pip install scons -i https://pypi.tuna.tsinghua.edu.cn/simple --break-system-packages
```
检查SCons版本
```bash
openkylin@openkylin:~/godot$ scons -v
SCons by Steven Knight et al.:
	SCons: v4.8.0.7c688f694c644b61342670ce92977bf4a396c0d4, Sun, 07 Jul 2024 16:52:07 -0700, by bdbaddog on M1Dog2021
	SCons path: ['/usr/local/lib/python3.12/dist-packages/SCons']
Copyright (c) 2001 - 2024 The SCons Foundation
```
安装mold和Clang-17
```
sudo apt install mold clang-17
```
检查Clang版本，并确保编译器后端为GCC13，应该有如下输出：
`Selected GCC installation: /usr/bin/../lib/gcc/riscv64-linux-gnu/13`
```
openkylin@openkylin:~/godot$ clang -v
Openkylin clang version 17.0.6 (9ok4)
Target: riscv64-unknown-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
Found candidate GCC installation: /usr/bin/../lib/gcc/riscv64-linux-gnu/12
Found candidate GCC installation: /usr/bin/../lib/gcc/riscv64-linux-gnu/13
Found candidate GCC installation: /usr/bin/../lib/gcc/riscv64-linux-gnu/8
Selected GCC installation: /usr/bin/../lib/gcc/riscv64-linux-gnu/13
```

## 四、开始构建

cd进入源码目录构建
```bash
cd godot
```

指定目标架构为`rv64`，启用Clang作为LLVM编译器前端，指定链接器为`mold`（GNU ld无法正确链接）并同时禁用链接时优化。
```
arch="rv64" use_llvm="yes" linker="mold" lto="none"
```
启用fb文本服务器后端（Godot有两种文本后端`fb`和`adv`）
```
module_text_server_fb_enabled="yes"
```

同时需要禁用一些在RISC-V设备可能没法正常工作的Godot模块

```
# 禁用Theora视频编码支持
module_theora_enabled="no"
# 禁用去噪模块
module_denoise_enabled="no"
# 禁用光线投射模块
module_raycast_enabled="no"
# 禁用Xatlas纹理展开模块
module_xatlas_unwrap_enabled="no"
```

使用以下命令构建Godot编辑器
```
scons -j8 arch="rv64" use_llvm="yes" linker="mold" lto="none" \
     target="editor" platform="linux" \
     precision="single" module_text_server_fb_enabled="yes" \
     module_theora_enabled="no" \
     module_denoise_enabled="no" \
     module_raycast_enabled="no" \
     module_xatlas_unwrap_enabled="no"
```
使用以下命令构建Debug导出模板
```
scons -j8 arch="rv64" use_llvm="yes" linker="mold" lto="none" \
     target="template_debug" platform="linux" \
     precision="single" module_text_server_fb_enabled="yes" \
     module_theora_enabled="no" \
     module_denoise_enabled="no" \
     module_raycast_enabled="no" \
     module_xatlas_unwrap_enabled="no"
```
使用以下命令构建Release导出模板
```
scons -j8 arch="rv64" use_llvm="yes" linker="mold" lto="none" \
     target="template_release" platform="linux" \
     precision="single" module_text_server_fb_enabled="yes" \
     module_theora_enabled="no" \
     module_denoise_enabled="no" \
     module_raycast_enabled="no" \
     module_xatlas_unwrap_enabled="no"
```

![编译Godot](assets/%E5%9C%A8openKylin-RISC-V%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CGodot/%E7%BC%96%E8%AF%91Godot.png)


## 五、运行Godot

cd进入bin目录

```bash
cd bin
```
运行Godot编辑器，openKylin 2.0镜像默认使用wlcom（基于Wayland协议），IMG的GPU可以使用GLES，因此需要加上启动参数`--display-driver wayland opengl_es3`，如果设备可以外接AMD GPU可以尝试用GL（比如SG2042）。


```
chmod +x godot.linuxbsd.editor.rv64.llvm

./godot.linuxbsd.editor.rv64.llvm --display-driver wayland opengl_es3
```

![运行Godot](assets/%E5%9C%A8openKylin-RISC-V%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CGodot/%E8%BF%90%E8%A1%8CGodot.png)


## 六、设置项目启动参数

在调试项目时，Godot仍会使用默认的软渲染管线，因此需要将`--display-driver wayland opengl_es3`添加到项目设置

![运行参数](assets/%E5%9C%A8openKylin-RISC-V%E7%BC%96%E8%AF%91%E8%BF%90%E8%A1%8CGodot/%E8%BF%90%E8%A1%8C%E5%8F%82%E6%95%B0.png)
