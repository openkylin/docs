# 安装 Visual Studio Code

## 使用软件商店安装

打开软件商店，选择全部分类->开发

在开发标签中找到 Visual Studio Code 软件，点击安装

![选择Visual Studio Code](assets/install-vs-code/2025-01-20_08-54-30.png)

等待一段时间后,软件商店提示安装完成

![Visual Studio Code 安装完成](assets/install-vs-code/2025-01-20_08-55-20.png)

点击打开按钮，即可打开 Visual Studio Code

![Visual Studio Code 界面展示](assets/install-vs-code/2025-01-20_08-56-53.png)
