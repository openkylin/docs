# 使用HMCL启动器畅玩minecraft

---

**更新日期**：2024.08.27
**系统版本**：openKylin 2.0 x86_64

---

## 〇、前言

Minecraft是一款非常受欢迎的沙盒游戏，而HMCL（Hypixel Minecraft Launcher）是一款功能强大的Minecraft启动器，可以方便地管理和启动Minecraft游戏。
本文将介绍在openKylin上如何使用HMCL启动器畅玩Minecraft。

## 一、准备工作
### 1.1 安装Java运行环境(JRE)
1. 打开终端，输入`sudo apt install openjdk-21-jre`
![](assets/openjdk.png)
**注意**：1.21版本的minecoraft需要openjdk-21以上才能运行，但是hmcl暂时无法在23版本的openjdk下启动，所以这里选择21版本。

### 1.2 下载并安装HMCL启动器
1. 打开浏览器，访问HMCL官方网站下载页面：[https://hmcl.huangyuhui.net/download/](https://hmcl.huangyuhui.net/download/)
2. 下载jar版本
![](assets/下载hmcl.png)
3. 下载完成后，将jar文件移动到合适的位置，例如`~/game`
![](assets/保存到game文件夹.png)

## 二、使用HMCL启动器畅玩minecraft
### 2.1 启动HMCL启动器
1. 刚才打开终端，进入到你存放`hmcl启动器`的目录，如果跟笔者一样的话就是输入`cd ~/game`
2. 输入`java -jar hmcl.jar`启动HMCL启动器，首次启动会下载必要组件
![](assets/下载组件.png)
3. 下载完成后，就启动了HMCL启动器
![](assets/hmcl启动器.png)



### 2.2 快速启动 
#### 2.2.1 别名启动
显然每次输入那么多命令有点烦，我们可以编辑`.bashrc`，创建别名。
1. 打开终端，输入`sudo nano ~/.bashrc`
2. 在文件末尾添加`alias hmcl='java -jar ~/game/hmcl.jar'`，`ctrl+s`保存，`ctrl+x`退出
![](assets/编辑bashrc.png)
3. 输入`source ~/.bashrc`，使别名生效
4. 输入`hmcl`启动HMCL启动器
![](assets/别名启动.png)

#### 2.2.2 创建桌面快捷方式
当然也可以创建`.desktop`文件放到桌面上，这样就可以直接双击启动了。
1. 打开终端，输入`nano ~/桌面/hmcl.desktop`
2. 输入以下内容：
```cpp{.line-numbers}
[Desktop Entry]
Name=HMCL
Exec=java -jar /home/username/game/hmcl.jar
Icon=/home/username/game/hmcl.png
Type=Application
Categories=Game;
```
   - **注意**：`/home/username/game/hmcl.jar`和`/home/username/game/hmcl.png`需要替换为你自己的路径，而且是绝对路径。
3. 保存并退出，双击桌面上的HMCL图标即可启动。
![](assets/desktop启动.png)
### 2.3 下载版本
此时我们还没有游戏版本，所以需要下载游戏版本，此时直接点启动游戏会自动跳转到有关界面。
![](assets/下载游戏版本.png)
![](assets/下载整合包.png)
![](assets/等待下载完成.png)
![](assets/安装成功.png)


### 2.4 启动游戏
安装成功后回到首页，点击启动游戏即可：
![](assets/启动游戏.png)
![](assets/耐心等待.png)
![](assets/启动成功.png)


## 三、注意事项 
1. HMCL启动器需要Java运行环境，如果游戏不能正常运行，大概率是前面Java运行环境安装的问题，请仔细检查有关配置。
2. 安装模组的时候请注意是forge的还是fabric，如不匹配无法正常加载。
3. 尽量不要使用一键升级模组，很容易发生模组不兼容启动不了游戏的情况，很难排查到底是哪个模组的问题。

## 四、结语
使用HMCL启动器畅玩Minecraft非常简单，只需要下载并安装HMCL启动器，然后下载游戏版本，就可以开始游戏了。
HMCL启动器还支持多账户登录、下载和安装各种模组、插件和资源包，大家可以自行摸索。

