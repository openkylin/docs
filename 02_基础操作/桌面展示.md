# openkylin基础 桌面展示
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

![image](./assets/桌面展示/ok-desktop-1.png)

开始菜单

![image](./assets/桌面展示/ok-desktop-2.png)

软件商店-移动应用

![image](./assets/桌面展示/ok-ss-1.png)

我用的是虚拟机，安装移动应用需要初始化移动运行环境，而移动运行环境是不支持虚拟机的，所以尝试失败了，以后有机会找个物理机再进行尝试。

![image](./assets/桌面展示/ok-ss-2.png)

电源菜单

![image](./assets/桌面展示/ok-power-1.png)

有休眠 睡眠 锁屏 注销 重启 关机

&emsp;


