# <center>ssh免密登录</center>
#### <center>作者：jianfma</center>
#### <center>2023-04-02 9:25:00</center>

1. 确保服务器的ssh服务已安装，若没有安装在安装ssh服务。
```bash
sudo apt install openssh-server
```
2. 设置ssh服务的配置文件。确保一下内容去掉了注释。
```bash
sudo vim /etc/ssh/sshd_config

找到以下内容，并去掉注释符”#“
=========================
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile  .ssh/authorized_keys
=========================
```
3. 配置ssh用户的`.ssh/authorized_keys`文件。若`~/.ssh/authorized_keys`不存在,则建立.ssh文件夹和authorized_keys文件。将要免密电脑的`id_rsa.pub`的内容追加到`~/.ssh/authorized_keys`中.
```bash
cat id_rsa.pub >> ~/.ssh/authorized_keys
```
4. 配置文件的权限。
```bash
chmod 600 ~/.ssh/authorized_keys 
#设置.ssh目录权限
chmod 700 -R ~/.ssh
```
5. 重启sshd服务。
```bash
sudo service sshd restart
```

附录: 要免密电脑的`id_rsa.pub`的路径通常位置为`~/.ssh/id_rsa.pub`。
若不存在则需要生成，通常使用的生成ssh公私钥的命令为`ssh-keygen -t rsa -C 邮箱地址`。`ssh-keygen`命令在windows11、linux、macos中默认安装，可以直接使用。