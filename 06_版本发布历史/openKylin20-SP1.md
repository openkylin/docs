---
title: openKylin2.0-SP1
description: 
published: true
date: 2025-03-13T09:14:08.769Z
tags: 
editor: markdown
dateCreated: 2025-01-02T18:11:32.516Z
---

# openKylin-2.0-SP1

| 版本 | 架构 | 使用场景 | 发布时间 | 下载地址 |
| --- | --- | --- | --- | --- |
|openkylin2_0_SP1|x86_64|X86架构/通用机型/桌面场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=80)|
|openkylin2_0_SP1|LoongArch|LoongArch64架构/通用机型/桌面场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=81)|
|openkylin2_0_SP1|ARM64|ARM64架构/通用机型/桌面场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=90)|
|openkylin2_0_SP1_arm_raspi|ARM32|ARM32架构/树莓派开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=82)|
|openkylin2_0_SP1_arm_phytiumpi_2G|ARM32|ARM32架构/飞腾派开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=83)|
|openkylin2_0_SP1_arm_phytiumpi_4G|ARM32|ARM32架构/飞腾派开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=84)|
|openkylin2_0_SP1_arm_phytiumpi_4G|ARM32|ARM32架构/飞腾派开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=84)|
|openkylin2_0_SP1_riscv_lichepi4a|RISC-V|RISC-V架构/lichepi4a开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=86)|
|openkylin2_0_SP1_riscv_visionfive2|RISC-V|RISC-V架构/visionfive2开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=87)|
|openkylin2_0_SP1_riscv_spacemit|RISC-V|RISC-V架构/spacemit开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=88)|
|openkylin2_0_SP1_riscv_milk|RISC-V|RISC-V架构/Milk-V开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=85)|
|openkylin2_0_SP1_riscv_ruyibook|RISC-V|RISC-V架构/ruyibook开发板/嵌入式场景|2024-12-20|[点击下载](https://www.openkylin.top/downloads/download-smp.php?id=89)|

## 版本发布新闻

[多架构赋能！openKylin 2.0 SP1正式发布](https://www.openkylin.top/news/3605-cn.html)
