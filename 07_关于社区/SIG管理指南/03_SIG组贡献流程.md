---
title: 03-SIG组贡献流程
description: SIG组如何参与社区贡献
dateCreated: 2024-09-30
---


## SIG组的贡献方向

SIG组由技术委员会审核通过后，便可基于SIG组工作方向开展相关的工作，可以是文档贡献、翻译贡献、生态贡献、技术贡献或社区治理层面的贡献。SIG组的相关贡献可在社区以PR、Issue的形式进行量化，社区结合各SIG组的年度贡献数据进行相关奖项的评选，如技术创新奖、最佳贡献奖等。


## SIG组的贡献流程

**第一步：申请仓库**：

通过修改SIG组的sig.yaml文件中packages字段添加新的仓库，然后提交PR申请，以Community SIG为例，如要创建community仓库，可在packages下面添加community，然后提交PR申请。如图：
![申请仓库](./assets/申请仓库.png)

packages字段命名规范：以小写字母+"-"的方式，如ukui-session-manager、kylin-miraclecast等。

**第二步：仓库初始化**：

仓库申请PR审核通过后，系统会自动创建这个仓库，仓库路径为https://gitee.com/openkylin/ 仓库名 ， 需要sig.yaml文件中定义的Maintainer角色对仓库进行初始化。
初始化方式为通过配置本地git，远程push代码到仓库下。

**第三步：PR提交与合入**：

仓库的Committer以PR的方式提交贡献到仓库，仓库Maintainer对PR进行审核，审核通过后正式合入到仓库，可以是文档，也可以是代码。
非代码和文档类的贡献也可以Issue任务的方式记录到仓库下，形成工作事项存档。

**第四步：贡献数据统计**：

SIG组贡献PR数、Issue数将在后台形成数据统计，可作为年度评选参考依据。
