
 *[Newbie Course] Analysis of the principles of the openKylin User Manual, one trick to teach you how to customize! **

The openKylin user manual is a manual that describes the functions and user interface of the openKylin operating system in detail, allowing users to understand how to use the software. By reading the openKylin user manual, you can get started and use the openKylin operating system faster and better. Today, I will take you to briefly understand the implementation principle of the openKylin user manual and how to customize the content of the user manual.

![图片](./assets/1673400403559673.jpeg)

1. Introduction to the realization principle of the user manual

1.1 Introduction to QtWebkit The start, display, and jump of the user manual on the openKylin operating system are realized on the basis of QtWebkit. Let's briefly introduce QtWebkit. The QtWebkit module provides an engine for using web browser in qt, which makes it easy for us to use the content on the World Wide Web in qt applications, and the control of its web content can also be realized through native controls. QtWebKit provides tools for rendering Hypertext Markup Language (HTML), Extensible Hypertext Markup Language (XHTML), and Scalable Vector Graphics (SVG) documents, which are styled using Cascading Style Sheets (CSS) and written in JavaScript script.

1.2 The user manual jump interface In order to facilitate the user to jump to the help document of the corresponding component, the user manual provides an interface, so that after other components call the interface to pass parameters, they can directly open the corresponding content of the user manual, and the component can be clicked on F1 and the menu - help option to make the call. DaemonIpcDbus::showGuideDaemonIpcDbus::showGuide provides a dbus interface, the component only needs to pass the corresponding parameters to call, and the manual will open the user manual according to the parameters and jump to the corresponding content.

1.3 User Manual Operation Process The user manual jump interface is introduced above, and the following will focus on the general process of user manual operation.

First, you need to instantiate QWebView, enable and disable some settings, and load the user manual webpage file.

![图片](./assets/1673400415449881.jpeg)

![图片](https://www.openkylin.top/upload/202301/1673400415449881.png)

Secondly, send a signal when loading html, and pass the QObject object to JS, so that JS can call the public slots function of QObject.

![图片](https://www.openkylin.top/upload/202301/1673400425220458.png)

![图片](https://www.openkylin.top/upload/202301/1673400459129591.png)

![图片](https://www.openkylin.top/upload/202301/1673400473845933.png)

Thirdly, the web end calls the qt interface to obtain document information and directory structure, and dynamically generates the home page application. The js side calls the qt side function to obtain information:

![图片](./assets/1673400482997460.jpeg)

Finally, the qt side obtains the manual file structure, obtains the icon name and folder name of the homepage, and the corresponding document path:

![图片](https://www.openkylin.top/upload/202301/1673400491336760.png)

Through the obtained image name, document path, folder name, loading icon information, complete the loading of the home page application. The loading process of the openKylin user manual home page is rough as described above, but the actual rendering of the Markdown file is also involved, as is the following effect of the table of contents and content; the jumping of the manual at the directory level; the automatic reading of the document's update date, and so on. Here I won't introduce them one by one.

2. User manual content customization

When you integrate new components in the system, you hope that after adding new components, the content of the corresponding component manual will be automatically added to the user manual and be able to jump; then here is an introduction to how to integrate your own manual content!

2.1 Folder Structure

![图片](./assets/1673400502196692.jpeg)

ull Requ needs to contain a language folder and an icon; the folder name and the icon name are consistent, and will be used for home page icon display and user manual dbus interface call parameters.

2.2 Markdown older Structure

![图片](./assets/1673400514730147.png)

Here, the first-level title of the Markdown document will be used as the display name of the homepage application.

2.3 Modify the Install File

![图片](./assets/1673400526886467.png)

Put the prepared folder under the resource file of the user manual. When the user manual is loaded, it will be automatically read, loaded and displayed on the home page. Follow the above steps to complete the customization of the content of the user manual, and realize the simultaneous increase and decrease of the content of the system component manual! Have you learned it?

