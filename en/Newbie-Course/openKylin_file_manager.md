openKylin File Manager, as a kind of software that provides user interface to manage files, can help users to handle daily work and manage files stored in local and network, and also provides a series of basic operations such as renaming, creating, opening, viewing, editing, moving and deleting. Among them, openKylin File Manager and related packages installation commands are as follows:

```shell
sudo apt install peony peony-extensions
```

Here's a brief look at the structure and features of the openKylin file manager.

## Introduction to File Manager in openKylin

### Structure of File Manager

The openKylin file system structure differs from the Windows file system structure in that there is no concept of a disk letter (the so-called disk letter is the identifier of DOS and Windows systems for disk storage devices). openKylin OS chooses to store all files in the root directory.

![General View](./assets/OpenKylin_file_manager/general_view.png)

### Command Operation on File Manager

It is well known that Linux commands can be used to understand the system, but for the average user, it is not very convenient if a system can only be browsed to manipulate file directories or files simply using the command line. Here are several comparisons between browsing files using the file manager and browsing files from the command line.

1. Change current directory to HOME

    ![Change Dir to home](./assets/OpenKylin_file_manager/change_dir_to_home.png)

2. Show the hidden files and folders

    ![Show the hidden files and folders](./assets/OpenKylin_file_manager/show_hidden_files_and_folders.png)

3. View the property of a file

    ![Show file property](./assets/OpenKylin_file_manager/show_file_property.png)

## Introduction to features of File Manager in openKylin

### Batch forward/reverse selection

In daily use, we often encounter the need to select multiple files at the same time, sometimes in a continuous file arrangement, and sometimes not, so how do we operate when we encounter the above situation?

1. As all we know, `Ctrl`+`a` is widly used in Windows to select all files and folders in file manager. So dose openKylin.

    ![Select All](./assets/OpenKylin_file_manager/select_all.png)

2. To batch select, you need to select one file or folder as beginning postion, then press `Shift` and select another one as ending postion, items between beginning and ending are all selected. However, sometimes you do not want to select them continuously,you can press `Ctrl` and select what you want.

    ![Select Batch](./assets/OpenKylin_file_manager/select_batch.png)

3. To reverse select, you just need to select what you do not want, then right click and use `reverse choose`.

    ![Select Reverse](./assets/OpenKylin_file_manager/select_reverse.png)

    ![Result](./assets/OpenKylin_file_manager/select_reverse_result.png)

### Searching files

1. Recent Use

    If you need to open a recently edited document, but its exact path is a bit complicated, you can use the "Recent Use" feature to quickly locate the desired file location. The system will sort the most recently opened documents based on your document usage history and show you the storage path of each document.

    ![Recently used files](./assets/OpenKylin_file_manager/recently_used.png)

2. Searching

    If you need to find a file from a long time ago, at this point you can locate this file in the search results by searching for the keyword of the file name.

    ![Search](./assets/OpenKylin_file_manager/search.png)

### Copy path of a file to terminal

If you need to make a copy of the file path, all you need to do is open a terminal window, click on the name of the folder at the top of the file manager window, right click and select the copy option, then paste it in the terminal window and you will get the exact path of the file.

![Copy the path to file](./assets/OpenKylin_file_manager/copy_path_to_file.png)

---

All above is the basic introduction of openKylin File Manager, hope it can help you. Currently, the openKylin file manager is still being improved and updated, so if you have any suggestions or needs in the process of using it, you can go to the openKylin Gitee repository to give feedback in the form of Issue.
