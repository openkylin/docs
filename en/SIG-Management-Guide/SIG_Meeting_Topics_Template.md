## openKylin sig-xxx 1.0 plan

\---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This document is for the openKylin sig-xxx 1.0 issue collection, meeting date: xx/xx/xxxx



Participants

\---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



Please fill in participant information at




Name (gitee_ID)



Meeting schedule

\---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



9:30pm ~ 9:45pm: Topic 1

9:45pm ~ 10:00pm: Topic 2

...







Topics:

\---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Please fill in the content of the topic you want to submit, including the topic sponsor, a brief description of the topic, an online address for issue feedback, existing technical proposals or PR, existing discussion notes, etc.

(No priority and order of priority for all issues in the issue declaration stage, after the completion of requirements collection by the SIG group version planning leader and all maintainers in accordance with the specific circumstances of all collected requirements (type, technical difficulty, workload, etc.), according to the meeting schedule to specify the meeting agenda, the meeting schedule will be announced three days before the work session)





1. Example: Topic 1 (Proposer):

- You should need to write clear details of the proposed topic, including but not limited to: xxx,xxx

- User scenario: who is using it in what scenario and how

- Related issues: issue_link1, issue_link2, issue_link3

- Previous discussions or technical solutions pre-research: xxx xxx xxx