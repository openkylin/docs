## Introduction

 The openKylin community is a free and open community. In order to ensure the openness and transparency of the community work, to strengthen the communication between the community contributors, enthusiasts and maintainers of the openKylin OS distribution, to expand the influence of the openKylin community, and to attract more Linux enthusiasts to join the openKylin community, we hope to establish a SIG group to organize and coordinate the various matters in the community work, and to make the openKylin community an active open source community for the Linux OS.

## Principles

1. all SIG groups in the openKylin community are open for anyone and any organization to participate.
2. The `README.md` file of the SIG contains information about the SIG to which the project belongs, communication methods, members and contact information. We welcome you to actively participate in the communication within the SIG through the contact information mentioned in the `README.md` file, including the mailing list, open meetings, etc.
3. Each SIG group is composed of project leader (Owner) and core maintainer (Maintainer), and major decisions within the group must be decided by all members with more than 2/3 votes and reported to the Technical Committee.
4. Each SIG group can have its own mailing list, community, etc., and its own contribution strategy, but it must have its own SIG charter.
5. The types of matters responsible between SIGs in the community are not allowed to cross over, and it is necessary to keep the types of matters isolated between the groups.
6. The meetings within each SIG need to be held regularly.

## Organizational Structure

The openKylin community currently has the following SIG groups:

- Architecture
- Infrastructure
- Versions
- Kernel
- Security
- Compatibility
- DE
- QA
- HTML5
- RISC-V
- Input Method
- ...


