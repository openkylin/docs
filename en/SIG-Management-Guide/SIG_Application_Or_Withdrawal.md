# How to join SIG
**Application**.
1. Sign the CLA https://cla.openkylin.top/
2. Check the SIG of interest through the SIG list https://www.openkylin.top/join/sig-cn.html and directly enter the link to the SIG project for code contribution, or subscribe to the mailing list https://mailweb.openkylin.top, participate in SIG meetings https://www.openkylin.top/sig/meeting-cn.html and so on, to participate in the technical discussion and community maintenance of the corresponding SIG project.
3. Join the developer exchange group (CLA backend management actively pull you into the group)

**Become a maintainer condition (recommended to)**.
Benefits: When developers meet the following two or more conditions, they can apply for SIG group and become a group maintainer:

1. Submit five or more valid commissions.
2. Raise five or more valid issues.
3. Fix five or more issues.
4. Actively participate in SIG meetings and make constructive suggestions in the meetings.

# How to apply Or withdraw SIG

## Individual / Corporate Application

**Application**.
After all SIG members [sign personal cla](https://cla.openkylin.top), please follow these steps to execute the request.
1. Fork the project [openKylin / community](https://gitee.com/openkylin/community) under your Gitee by the relevant proposer. Create your own new SIG directory in the sig directory under your Gitee project, and follow the [SIG Group Charter Template](https://gitee.com/openkylin/docs/blob/master/SIG%E7%AE%A1%E7%90%86%E6%8C%87%E5%8D%97/SIG%E7%BB%84%E7%AB%A0%E7%A8%8B%E6%A8%A1%E7%89%88.md) and create the corresponding `README.md` under your Gitee project, and complete the new SIG charter;
2. In the SIG directory you just created, create the sig.yaml file according to the [sig.yaml template](https://gitee.com/openkylin/community/blob/master/sig/README.md), and complete the sig.yaml file;
3. After completing the above two steps, commit the above changes to Gitee and submit a PR request to the [openKylin / community](https://gitee.com/openkylin/community) project to create a SIG group, fill out the relevant information, the technical committee will review the relevant information in advance and further communicate at the next The technical committee will review the information in advance and communicate further at the next regular meeting.

**Review**: The Technical Committee participants will communicate with the proposer and review and comment on the SIG related business scope and maintenance objectives.

**Approval**: After the Technical Committee has passed the review, the corresponding mailing list will be created for the SIG group you applied for, and official confirmation information and the mailing list account will be sent by email to the email address you bound when you signed the CLA, and the person in charge of the infrastructure SIG group will complete the creation of the corresponding SIG group warehouse and permission processing.

**Operation**: SIG is officially operated, and the group members communicate through mailing list and group meeting. At the early stage of the operation of a new SIG group, a member of the technical committee can be appointed as the mentor of the SIG group to guide the SIG group to ensure that the SIG group quickly gets on the right track.


## Corporate Application

**Application**.
After the person in charge of the business [sign the business cla](https://cla.openkylin.top) and ask the relevant business employees of that business [sign the employee cla](https://cla.openkylin.top), then please follow the steps below to execute the application.
1. Fork the project [openKylin / community](https://gitee.com/openkylin/community) under your Gitee by the relevant proposer. And create your own new SIG directory in the sig directory under your Gitee project, and follow the [SIG Group Charter Template](https://gitee.com/openkylin/docs/blob/master/SIG%E7%AE%A1%E7%90%86%E6%8C%87%E5%8D%97/) SIG%E7%BB%84%E7%AB%A0%E7%A8%8B%E6%A8%A1%E7%89%88.md) and create the corresponding `README.md` under your Gitee project, and complete the new SIG charter;
2. In the SIG directory you just created, create the sig.yaml file according to the [sig.yaml template](https://gitee.com/openkylin/community/blob/master/sig/README.md) and fill out the sig.yaml file;
3. After completing the above two steps, commit the above changes to Gitee and submit a PR request to the [openKylin / community](https://gitee.com/openkylin/community) project to create a SIG group. The technical committee will review the information in advance and communicate further at the next regular meeting.

**Review**: The Technical Committee participants will communicate with the person in charge and review and comment on the SIG related business scope and maintenance objectives.

**Approval**: After the Technical Committee has passed the review, the corresponding mailing list will be created for the SIG group you applied for, and the official confirmation information and mailing list account will be sent by email to the email address you bound when you signed the cla, and the person in charge of the infrastructure SIG group will complete the creation of the corresponding SIG group warehouse and permission processing.

**Operation**: SIG is officially operated, and the group members communicate through mailing list and group meeting. At the initial stage of the operation of a new SIG group, a member of the technical committee can be appointed as the mentor of the SIG group to provide guidance for the SIG group to ensure that the SIG group quickly gets on the right track.

## Withdrawal of SIG
### Principles of Withdrawal
The following circumstances can occur when the SIG owner or the technical committee member submits a request to cancel the SIG group. 

* The SIG group has been very inactive for a long time and cannot maintain daily operation. Including but not limited to the existence of: long time (more than 6 months) has not held regular meetings, never participated in the community SIG group related activities (including release), has not been responsible for the repository or the repository responsible for a long time no code updates, can not respond to the community feedback issues in a timely manner and other inactive phenomenon.
* SIG group is responsible for an important module or technical direction in the openKylin release, but the current work cannot meet the requirements of the openKylin release for that module or technical direction, which hinders the release and technical development of the openKylin release.
* the SIG group's goal planning, technical direction, etc. overlap with another SIG group.
* Other circumstances that the Technical Committee considers necessary to abolish the SIG group.

### Withdrawal Process
#### Submit a withdrawal request
SIG group revocation requests should be submitted by the SIG group Owner or technical committee member in the following manner.
1. Fork the project [openKylin / community](https://gitee.com/openkylin/community) to your Gitee. and delete the directory corresponding to that SIG group in the [community/sig](https://gitee.com/openkylin/community/sig) directory.
2. After completing the above steps, commit the changes to Gitee and submit a PR request to the [openKylin / community](https://gitee.com/openkylin/community) project to remove the SIG group. After filling out the relevant information (detailed reasons for removing the SIG group), the technical committee will advance The technical committee will review the information and discuss and vote on the issue at the next regular meeting.

#### Discuss and vote
The SIG withdrawal application shall be discussed and decided by voting at the regular meeting of the Technical Committee. AAll members of the Technical Committee are required to participate in the voting, which is divided into affirmative votes, negative votes, and abstentions, and can be voted directly at the regular meeting or in response to an email vote after the meeting. Two-thirds or more of the members must vote in favor of the application in order for it to be approved. The voting results are publicized via the mailing list.  

When the SIG group is withdrawn, the packages under the name of the SIG group that need to be maintained will be temporarily divided into the Packaging SIG group and publicized through the mailing list, and these packages can be claimed and maintained by other SIG groups or other new SIG groups established.