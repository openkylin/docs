# SIG group Owner co-option, revocation specification
## SIG group Owner co-option specification
### Co-opted candidates
SIG group Owners shall be elected from SIG group Maintainers.

### Submit an application
Applicants are required to complete the submission of application materials at least three days before the SIG meeting, and the application materials template is as follows. After writing, complete the submission of application materials and issue declaration by replying to the next SIG meeting issue a collection email.
```
I. Basic information.
    1, Applicant: name (email address)
    2、Referee: name (Note: The applicant must get at least one recommendation from the owners of this SIG group or technical committee members)
    3, in the SIG group in charge of which area of work: xxxxx
    
II. the main contributions to the SIG group listed.
    1、xxxx
    2. ......
```
### Voting
All SIG Owners and maintainers will vote on the application. The vote will be divided into affirmative, negative and abstaining votes, either directly at the meeting or by email after the meeting. The application can be approved only if two-thirds or more votes are cast. The voting result must be reported to the technical committee, which has the right to veto the election result within the SIG group.
 

## SIG group Owner revocation specification
### Principles of Revocation
* SIG group Owners have a low level of activity and participation for a long period of time, including but not limited to: not participating in regular SIG group meetings, SIG group decisions, SIG group activities, etc. for a long period of time (more than 6 months).
* SIG group Owner has limited ability to lead the SIG group to accomplish the set goals and work plan, which affects the development of SIG group and community. 
* SIG group Owners are unable to continue their duties as SIG group Owners due to personal or other reasons. 
* Other circumstances that require the revocation of the SIG group Owner as deemed by the core members of the SIG group or the technical committee.

### Submit an application
The application for revocation of SIG group Owner should be submitted by other core SIG group personnel (other Owners, Maintainer), the Owner himself or a member of the Technical Committee, with specific reasons for revocation, and the application for revocation should be completed by replying to the issue collection email and issue declaration at least three days before the next SIG group meeting.

### Voting
All SIG owners and maintainers will vote on the application. The vote will be divided into affirmative, negative and abstaining votes, either directly at the meeting or by email after the meeting. The application can be approved only if two-thirds or more votes are cast. The voting result must be reported to the Technical Committee, which has the right to veto the election result within the SIG group.
