## SIG Group Name
> SIG naming convention.  
> 1. full spelling of SIG name, only one word is capitalized; multiple words are capitalized with no hyphen in between, e.g., Release, BootAndInstall, etc...
> 2. special nouns or SIG names that must be abbreviated are all capitalized, e.g., RISC-V, QA, etc...  
> 3. SIG naming shall not infringe on the legitimate rights and interests of any third party (including but not limited to unauthorized use of third party trademarks, trade names, logos, etc.).
> 4. the community has the final right to review the naming of SIG.

SIG Group Introduction

## Work Objectives

SIG Group Objectives

## SIG Members

SIG-owner(`SIG-owner@email.com`)

SIG-maintainer(`SIG-maintainer@email.com`)

## SIG Maintenance Package List

- SIG-package
- SIG-package
...

## SIG Mailing List
> Format：(SIG name lowercase)@lists.openkylin.top（After the SIG application is approved, the administrator will create the mailing list）

## SIG Group Regular Meeting
> Clarify the time and period of regular SIG meetings, such as weekly, bi-weekly or monthly meetings, etc.