# How to change information of SIG

## Change of SIG name

1. SIG group submits a request to change the name PR, pay attention to the changes to cover all the places on gitee where the SIG group name is displayed to avoid inconsistent SIG group names. Generally involve: community/sig/xxxx directory name, the name field in sig.yaml file
The SIG group name in the README.md file.
2. the administrator reviews whether it conforms to the SIG group naming specification, etc.
3. the administrator notifies the community operators to modify the SIG group names in the openKylin website and mailing list, etc.


SIG group naming specification
1. the SIG name with full spelling, the initial letter is capitalized if there is only one word; for multiple words, the initial letter of each word is capitalized and no hyphen is added in between, for example: Release, BootAndInstall, etc.. 
2. special nouns or SIG names that must be abbreviated are capitalized, for example: RISC-V, QA, etc. 
3. the SIG naming shall not infringe upon the legitimate rights and interests of any third party (including but not limited to the unauthorized use of third party trademarks, trade names, logos, etc.).
4. the community has the final right to review the SIG naming.


## Change of team members/membership privileges
* If you have a member change request after the SIG group is created, please follow these steps to execute the request.
1. Fork the project [openKylin / community](https://gitee.com/openkylin/community) to your Gitee project by the relevant proponent. Modify the `README.md` in the corresponding SIG group's directory under your Gitee project according to the changes;
2. Complete the changes to the sig.yaml file in the corresponding SIG group directory;
3. After completing the above two steps, submit the above changes to Gitee and submit a PR request to the [openKylin / community](https://gitee.com/openkylin/community) project for SIG group membership changes (if new members are added, the new members will need to sign a cla first). After filling in the relevant information, the technical committee will review the relevant information in advance and communicate further at the next regular meeting.


## Change of maintenance package list
* If you have a package list update request after the SIG group is created, please execute the request as follows.
1. Fork the project [openKylin / community](https://gitee.com/openkylin/community) under your Gitee by the relevant proposer. Modify the `README.md` in the SIG group directory under your Gitee project according to the changes;
2. Complete the changes to the sig.yaml file in the corresponding SIG group directory;
3. After completing the above two steps, commit the above changes to Gitee and submit a PR request to the [openKylin / community](https://gitee.com/openkylin/community) project for changes to the SIG group package list, fill out the relevant information, the technical committee will review the information in advance and The technical committee will review the information in advance and communicate further at the next regular meeting.