## openKylin Community SIG Conference Guide


### Introduction

SIG should hold regular meetings or other working meetings regularly to discuss SIG planning, work matters, task division, priorities and other related work. SIG meetings should follow the principle of open source and openness, and the discussion process of topic collection, technical discussion and meeting minutes are open to all users in the community.

### Conference Type

**Single SIG Group Working Meeting**: The working meeting within a single SIG group is organized by the Maintainer of that SIG group, including topic collection, agenda arrangement, moderated discussion, and meeting minutes output, etc.

**Cross-SIG working meeting**: Collaborative working meeting between cross-SIG groups, which requires each related SIG Maintainer to contact and communicate in advance by email or other means, and is organized by each related SIG Maintainer, including topic collection, agenda arrangement, moderated discussion, minutes output, etc.

### Topic Collection

Each SIG creates the corresponding meeting collection directory under [gitee project](https://gitee.com/openkylin/community) community/sig/xxxx, and adds this meeting collection file in the meeting collection directory [SIG Meeting Issue Collection Template](./SIG_Meeting_Topics_Template.md), which is used to collect the topics and arrangements of this conference, and synchronize the conference directory and conference links to the mailing list of Community SIG.

After the topics are collected, the SIG Maintainer will arrange the meeting agenda and [meeting registration](https://docs.qq.com/sheet/DTlhGZWxiY1VFSHpy) according to the meeting time according to the specific conditions (type, technical difficulty, workload, etc.) of all collected topics. The meeting schedule is publicly released on the openKylin website and SIG mailing list 3 days before the meeting, so that participants can easily understand the meeting agenda.

### Hold a meeting

The meeting will be hosted by each SIG Maintainer, and the meeting will be started according to the pre-defined meeting agenda. During the meeting, attention should be paid to time control to ensure that all the topics already on the meeting agenda will be given the corresponding discussion time. Each participant needs to fill in his or her name and Gitee_ID as required in the participant part of the meeting topic collection file, and the topic will be regarded as automatically abandoned if he or she is not present and no substitute participant is designated.

The discussion of each topic can be divided into the following stages:

1. Topic presentation: The topic sponsor will present the topic, and the rest of the audience will not be allowed to interrupt during the presentation stage.

2. Discussion: Each participant will discuss the topic accordingly, and all participants can participate in the discussion, and the moderator will be responsible for recording the views and key opinions of each party.

3. Summary: After consensus is reached, the moderator outputs the conclusion of the topic based on the consensus. If no consensus is reached on the spot, a specific time for another discussion should be negotiated.

After all the topics are discussed, the SIG Maintainer team will prioritize and divide the topics according to the discussion of each topic and the actual situation of SIG group.

### Meeting Minutes

The person in charge of each SIG group will compile and complete the minutes of the meeting within three days after the meeting, and release the minutes publicly on the corresponding meeting collection directory on gitee, the mailing list of this SIG, Release SIG and other related SIGs, so that developers and users can understand the work planning and arrangement of each SIG, and the minutes need to contain the following contents.

1. all the topics that were discussed and the conclusions of the topic

2. the person in charge of each task and the duration of their work plan
   ......

 
### Remarks

The above SIG meeting organization process is a trial version, if the SIGs encounter any problems in the actual implementation process，Please contact contact@openkylin.top by email, thanks!