---
title: OpenKylin Community Member Guide
description: 
published: true
date: 2022-05-17T08:23:20.757Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:36.576Z
---

# OpenKylin Community Member Guide

On June 24, 2022, openKylin, China's first root desktop operating system community, with the theme of "creating the power of open source for a shared limitless future," successfully held an online launch conference and was officially released. With many VIPs in attendance, and countless developers watching, openKylin once again made CCTV news and was the top search on many social media platforms. Since the release, we've received so many questions from our followers, the most frequently asked question is how to join and contribute to the OpenKylin Community correctly.

## 1. Sign the CLA

The CLA, or Contributor License Agreement, must be signed in order to avoid potential issues. Whether you're a business, employee, or an individual developer, everyone must sign the corresponding CLA before contributing to the openKylin community. The CLA can be found at https://cla.openkylin.top. The following is a brief introduction to the three types of CLA and their signing process.

### 1.1 Commercial CLA

The CLA signing process for enterprise is more complicated. Enterprise sign the openKylin CLA can be added to the community membership system. Enterprise membership privileges are as follow:

- Apply to be elected as an organization of council membership; entitled to make the roadmap of the community's development, designated development plan and implement ideas;
- Apply to create SIG; entitled to assign SIG Owner as a candidate to participate the Technical Committee Election;
- Apply to join any existing SIGs, participate in technical discussions and development etc.
- Apply to be elected as a member of the Openkylin Ecological Committee; participate in community operation activities, product co-branding, gift exchange, etc.

Enterprise CLA signing process is shown in the figure below.

![](./assets/CLASigningprocess.png)

1. First, submit the basic information on the Commercial CLA page. The business, point of contact, position, and verification code are all required fields. The other fields are not required;
2. After clicking the sign button, you will receive an email which includes the basic information submitted from last step along with three attachments. Follow the email’s instructions to complete the next steps;
3. After your information has passed the review process by the community administration, your documents will be uploaded to the CLA platform and your business admin account will be created automatically, this account will be sent to the email entered in step one;
4. Log in to your business account on the CLA platform and change the password. This completes the signing process for Commercial CLA (this step is not required but in order to remember the password and avoid mistakenly deleting the email, it is recommended to complete this step);

TIPS!

The account sent to yout business email is the business administrator account which is used to review and audit related employees' signature details. If the business administrator also would like to contribute on Gitee, they will also need to sign the Employee CLA using the same email domain of the business and link it to their Gitee ID. That way you can pass the CI CLA signature check!

### 1.2. Employee CLA

After your business has completed the Commercial CLA signing process, employees can use their company domain email to sign the employee CLA. The process is illustrated in the figure below.

![](./assets/EmployeeCLASigningProcess.png)

1. Open the employee signing page, fill in the basic information, click the send verification code button, enter the verification code from the email received, check the box “I have read the agreement and agree,” click the sign button, a pop-up will appear indicating it has been submitted;

2. Contact the business account administrator, they will then activate the employee signing request you submitted on the employee management page;

3. After the administrator activates your request, you will receive a notification email indicating the CLA was signed and the process is complete;

### 1.3. Individual CLA

If you are an individual developer or your company has not signed a the business CLA, you can sign the individual CLA and then submit contributions. The process for signing the individual CLA is similar to the employee CLA and is illustrated below:

![](./assets/IndividualCLASigningprocess.png)

1. Open the individual CLA page, fill out the basic information, click the send verification code button, enter the code sent to your email, check the box “I have read the agreement and agree,” click the sign button, a pop-up will appear indicating it has been submitted;

2. Open the individual CLA email, you will receive an email that your CLA has passed.

TIPS!
- One CLA can be signed per email address
- The employee and individual CLA must be filled in with the correct gitee ID, if it is filled out incorrectly, PR’s submitted will not pass the CLA CI checks and will be tagged as openkylin-cla/no

### 1.4. The differences between the three forms
Some of you may be wondering, why are there three different CLAs? Don’t worry, the explanation is below:

![](./assets/DifferencesbetweenCLAs.png)

Employee CLA must be done through their business. If the developer’s company has signed a CLA, the employees of that company can sign an employee CLA. Contributions made as employees (including but not limited to PR, ISSUE, ETC.) all fall under the business contributions to the openKylin community.

Contributions made as an individual will fall under the individual.

# 2. Submit Contributions

After completing the first step of CLA signing, you can start submitting contributions to the openKylin community. The following is a demonstration of how to submit your PR contributions with a simple repository.

openKylin uses the popular git workflow. Before contributing, we need a basic understanding of git.

### 2.1 git environment configuration

As the saying goes, to do a good job an artisan needs the best tools. Before explaining how to submit contributions, the git environment needs to be configured locally. First, you’ll need to locally install the git environment, this can be done with the following command.
    
    # debian
    apt install git
    # arch Linux
    pacman -S git

After completing the install, enter “git version” into the terminal, if the output is similar to the below figure, the install was successful.

![](./assets/gitversion.png)

After confirming git was installed, you can configure your personal information using terminal commands. You only need to configure the bare minimum information.

    git config --global user.email "zhangsan@gmail.com" # 配置提交邮箱
    git config --global user.name "zhangsan" # 配置提交者用户名

TIPS!

It is recommended to configure your email and name with the email linked to your gitee account and your user ID, this way you’ll be able to see your contribution heatmap record on your profile main page!

### 2.2 Submit PR

In the previous section, you completed the local git environment. Next is the code contribution process. Before doing this, you need to go to gitee and create an account. If you don’t have a gitee account, you can click this link https://gitee.com/signup, and create a gitee account. Then you can contribute to openKylin through gitee. The contribution process is illustrated below using openKylin/ukui-menu repository as an example of how to contribute an openKylin PR from gitee.

![](./assets/SubmitPR.png)

1. To fork a repository on gitee (this article uses ukui-menu as an example), as shown in the figure, click the fork button and the repository will be copied to your individual account.
![](./assets/fork_Example.png)
2. After forking the repository to your account, you’ll be able to locally modify the code, use the following commands to clone locally.

        cd /home/test/data/work/openkylin/code # 找一个中意的路径
        git clone https://gitee.com/jiangwei124/ukui-menu # 克隆ukui-menu的代码到本地
3. After completing the second step, the repository is cloned to your account locally. Next us and IDE that you’re comfortable with to open ukui-menu project, or, if you prefer, you can use command line operations. A very simple example is shown here, adding to the README-zh.md file. If you aren’t a technical expert, it is generally recommended to make community contributions from the document beginning, since this is the simplest and easiest way to pass community checks.

        cd /home/test/data/work/openkylin/code/ukui-menu
        touch README-zh.md # 新建
        vim README-zh.md # 输入内容

After finishing translating README.md and saving it to README-zh.md, you can use git to push your commit to your gitee repository. On gitee you can see the below image that your push was successful.

        cd /home/test/data/work/openkylin/code/ukui-menu
        git add README-zh.md # 将文件添加到暂存区
        git commit -m "add README-zh.md file" # 提交修改
        git push # 推送修改到远程仓库
![](./assets/Succesfullypushtopersonalrepo.png)

4. After pushing your commit, you can make a PR on gitee. Click the Pull Requests option, then on the right, click new Pull Request. After entering the required information, click Create Pull Request, then you have successfully submitted your first PR.

![](./assets/giteesubmitPR.png)

|    TIPS!     
        |1. When selecting a branch, be careful not to incorrectly select the branch from your individual repository. Also, the target branch selection needs to be a branch of openKylin/yangtze;
        
|2. If there is an associated Issue, you can associate this Pull Request with the Issue. This way when the PR is accepted and merged, the Issue will automatically change to Complete status;

5. Now your first openKylin community contribution is basically complete. Then wait for CI to automatically compile your code, if the CI process is successful, it will automatically merge your PR, if it fails, a repository admin will look into your PR!

### 2.3 Check PR status

After submitting a PR, the record of the submission details can be viewed by clicking the PR details page. On the details page you can see the details in the image below.

![](./assets/CheckPRStatus.png)

Click the link in the green box in the figure above, you can jump to the compiler log, and check the detailed compiler information. All the PR details of interest to you can be viewed from the PR details page.

You can check to see if the PR you submitted is in the latest ISO release by going to openKylin ISO download page to downlaod the latest ISO and check.

### 2.4 Submit an Issue

In addition to PRs, Issues are also form of contributing. For example, if you’re using openKylin and you encounter a bug, you can submit an issue to the openKylin group.

- If it’s a system level bug, it can be submitted under the openKylin/community repository issues;
- If it’s an application level bug, it can be submitted under the corresponding openKylin repository issues. For example, if you’re using Kylin Media Player and you realize there’s a bug while playing video, you can submit an Issue to the openKylin/kylin-video repository;

![](./assets/SubmitIssue.png)

| TIPS!

When submitting an issue, describe it as clearly as possible. If there’s a log file, click the upload attachment button to upload the log to the issue!

## 3. Joining a SIG

SIG stands for Special Interest Group. If you’d like to get more deeply involved in the openKylin community, you can apply to join a SIG. You can view current SIGs at https://www.openkylin.top/join/sig-cn.html. The process for developers to join a SIG are as follows.

![](./assets/joiningSIG.png)

Next is an example of how to join a SIG using the HTML5 SIG
1. Fork the openKylin/community repository to your individual gitee acount. See section 2.2;
2. Make a local clone of the community repository, and edit community/sig/html5/sig.yaml and community/sig/html5README.md, file modification content are as follows:

sig.yaml

        name :  html5 
        description :  html5 team 
        owner : 
        -  pierre4long 
        -  insomnia_kylin 
        maintainers : 
        -  name :  oatoam 
        -  name :  tylwj 
        -  name :  wonschange 
        -  name :  jiangwei124  # Add 
        packages : 
        -  gecko_b2g 
        -  gaia_homescreen 
        -  manifests 
        -  gaia_system 
        -  gaia-buildinkeyborad

README.md

        ### Maintainers 
        - Tao Mao(maotao@kylinos.cn) 
        - Wenjie Li(liwenjie@yhkylin.cn) 
        - Changqi Wang(wanghcangqi@kylinos.cn) 
        - Jiang Wei(jiangwei1@kylinos.cn) # New

3. After your changes are complete, submit them to the repository under your gitee account. Then, as indicated in section 2.2, submit the PR to the openKylin/community repository. Finally, after the SIG Owner review, the CI process will automatically merge your changes into the main repository!

| TIPS!

- Ensure the information in the name field in the sig.yaml file is your gitee ID. Otherwise, even if your changes pass the SIG Owner review, it will not be merged into the openKylin/community recopsitory by the CI process.

- Signing a CLA is a prerequisite for applying to join a SIG.

## 4. Apply to be a package Maintainer

Given the authority afforded SIG members, you must make a certain amount of contributions before the technology committee will approve your SIG application. It is generally recommended for developers to start by applying to be a single package Maintainer. The process for becoming a single package Maintainer is illustrated below. The single package Maintainer readme can be seen here.

![](./assets/becomesinglepackagemaintainer.png)

4.1 Apply to become a new single package Maintainer

Suppose you’d like to become the ukui-menu individual package Maintainer, but the community/packages directory doesn’t like that packages information, you can apply to be the individual package maintainer through the process below. 

1. First, fork the repository to your gitee account and then clone it locally

        cd /home/test/openkylin/community # Find a path you like
        git clone https://gitee.com/jiangwei124/community.git

2. Add a new ukui-menu.yaml file under the community/packages path. Enter your information as in the example below.

        name :  ukui-menu 
        path :  ukui-menu  
        maintainers : 
        -  name :  zhangsan  # gitee id 
        openkylinid : displayname : Zhang San email : zhangsan@gmail.com  

| TIPS!

If the repository path name is the same as the package name, the path field can be left blank. The package name doesn’t necessarily need to conform to the gitee naming standards. You can designate a different path in order to avoid issues when creating the repository!

3. Submit your changes to your personal gitee repository. Then submit a PR to the openKylin community repository, wait for the administrator check, then you will have access to the ukui-menu repository and your application to be a new individual package maintainer is complete!

4.2 Apply to maintain an individual package for current maintainers

Applying to become an individual package maintainer as a current maintainer is similar to the previous section but a little easier. First, you need to fork the openKylin/community repository to your gitee account. Then you just need to modify the package’s corresponding yaml file. Suppose you wanted to become the maintainer of ukui-menu, add your corresponding information to the packages/ukui-menu.yaml file as in the example below.

        name :  ukui-menu 
        path :  ukui-menu  
        maintainers : 
        -  name :  zhangsan 
        openkylinid : displayname : Zhang San email : zhangsan@gmail.com # Add your personal information - name : lisi # gitee id openkylinid : displayname : Li Si email : lisi@gmail.com

This concludes the openKylin community contribution guide. For further questions, please submit an issue or join the developer wechat group!