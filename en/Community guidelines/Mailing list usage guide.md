---
title: Mailing list usage guide
description: 
published: true
date: 2022-06-23T07:23:30.999Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:17:23.928Z
---

# Mailing list usage guide

## Mailing list introduction
- Access address：<a target="blank" href="https://mailweb.openkylin.top/postorius/lists/">mailweb.openkylin.top</a>
- Introduction to the mail group: Each mail group will have a special mailbox to manage the mail group, take **UKUI** as an example, as shown below.
	ukui@lists.openkylin.top (Email group Email address)
  ukui-request@lists.openkylin.top(Email group Email address)
	ukui-owner@lists.openkylin.top(Email group administrator email address)



## How to subscribe to the SIG group
1. Enter[Mailing list home page](https://mailweb.openkylin.top/postorius/lists/),Click to go to the SIG group you want to subscribe to.
---

![Mailing list home page.png](/Community guidelines/img/Mailing list home page.png)

---

2. After entering the corresponding mail group, as shown in the picture below, fill in the email address and name in the input box, click Subscribe button below, and you can receive the confirmation email of subscribing to SIG.

---

![Mailing list subscription page.png](/Community guidelines/img/Mailing list subscription page.png)

---

3. The subscribing mailbox receives a confirmation message and enters the verification information as prompted to join the SIG group.


4. If you subscribe successfully, you will receive a welcome letter from the mail group, as shown below.

---

![Mailing lists welcome the mailing form.png](/Community guidelines/img/Mailing lists welcome the mailing form.png)

---

<span style="color:red">Note: If the above steps are normal but you do not receive the email, please contact contact@openkylin.top to explain the situation.</span>


## How to unsubscribe from the SIG group
1. Take the **UKUI** group as an example, send an email to ukui-request@list.openkylin.top, and the subject and body of the email contain "**unsubscribe**", as shown below.
---
![Mailing list unsubscribe form.png](/Community guidelines/img/Mailing list unsubscribe form.png)

---

2. Then you will receive an unsubscribe verification email, as shown below


---
![Mailing list unsubscribe mail form.png](/Community guidelines/img/Mailing list unsubscribe mail form.png)


---

3. Subscribe to the SIG group by following the prompts and returning the message with the verification information and using it in the subject and body of the email, as shown below.

---
![Mailing list unsubscribe to send mail form.png](/Community guidelines/img/Mailing list unsubscribe to send mail form.png)

---

4. After the authentication of the SIG group is successful, you cancel the subscription to the SIG group.

## How to send emails to SIG group members

Any member can send emails to all members of the SIG group. The email address is the IP address of the SIG group. For example, ukui@lists.openkylin.top.

