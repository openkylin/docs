# Transplanting the Hello software to openKylin

This document is an example of packaging and transplanting openKylin software packages.

## I. Introduction to the GNU Hello Project

The GNU Hello project is an example software released by GNU. This project implements the introductory "Hello, world!" program according to the standard GNU specifications. It serves as a reference example for GNU coding standards and maintainers' practices. As the saying goes, small as it is, it is complete with all five internal organs. Although GNU Hello is just a "Hello, world!" program, it includes the following technologies:


- Automake and Autoconf: generate compile configuration scripts
- Gnulib: the basic function library of the program
- Gettext: internationalization support
- getopt: command-line parameter support
- help2man: generate manpages using the program's "--help" option output
- Texinfo: write program documentation


Other more complex software systems in the GNU project have higher business logic complexity, but their organizational structure conforms to the specifications demonstrated by the Hello project.

## II. Packaging the Upstream Source Code

> **Since openKylin uses apt as its package manager, when introducing upstream software into openKylin, the upstream source code needs to be packaged into a source package that conforms to the Debian specifications.**

### 1. Obtaining the Upstream Source Code

The official website of the GNU Hello project is: [https://www.gnu.org/software/hello/](https://www.gnu.org/software/hello/)

Obtain the download link for the latest version (2.12.1) from the introduction on the website: [https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz](https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz)

Then download and extract the source code package:

```shell
$ wget https://ftp.gnu.org/gnu/hello/hello-2.12.1.tar.gz
$ tar -xaf hello-2.12.1.tar.gz
```

### 2. Generating Template Files Using debmake

Enter the source code directory and run `debmake`

```shell
$ cd hello-2.12.1
$ debmake
I: set parameters
I: sanity check of parameters
I: pkg="hello", ver="2.12.1", rev="1"
I: *** start packaging in "hello-2.12.1". ***
I: provide hello_2.12.1.orig.tar.gz for non-native Debian package
I: pwd = "/data2/test/hello"
I: $ ln -sf hello-2.12.1.tar.gz hello_2.12.1.orig.tar.gz
I: pwd = "/data2/test/hello/hello-2.12.1"
I: parse binary package settings: 
I: binary package=hello Type=bin / Arch=any M-A=foreign
I: analyze the source tree
I: build_type = Autotools with autoreconf
I: scan source for copyright+license text and file extensions
I:  43 %, ext = c
I:  27 %, ext = m4
I:  11 %, ext = gmo
I:  11 %, ext = po
I:   1 %, ext = gperf
I:   1 %, ext = mk
I:   1 %, ext = in
I:   1 %, ext = texi
I:   0 %, ext = sin
I:   0 %, ext = sed
I:   0 %, ext = header
I:   0 %, ext = text
I:   0 %, ext = O
I:   0 %, ext = ac
I:   0 %, ext = am
I:   0 %, ext = 1
I:   0 %, ext = pot
I:   0 %, ext = x
I:   0 %, ext = valgrind
I:   0 %, ext = info
I:   0 %, ext = tex
I:   0 %, ext = sub
I:   0 %, ext = rpath
I:   0 %, ext = sh
I:   0 %, ext = guess
I: check_all_licenses
......
I: check_all_licenses completed for 447 files.
I: bunch_all_licenses
I: format_all_licenses
I: make debian/* template files
I: single binary package
I: debmake -x "1" ...
I: creating => debian/control
I: creating => debian/copyright
I: substituting => /usr/share/debmake/extra0/changelog
I: creating => debian/changelog
I: substituting => /usr/share/debmake/extra0/rules
I: creating => debian/rules
I: substituting => /usr/share/debmake/extra1/compat
I: creating => debian/compat
I: substituting => /usr/share/debmake/extra1/watch
I: creating => debian/watch
I: substituting => /usr/share/debmake/extra1/README.Debian
I: creating => debian/README.Debian
I: substituting => /usr/share/debmake/extra1source/local-options
I: creating => debian/source/local-options
I: substituting => /usr/share/debmake/extra1source/format
I: creating => debian/source/format
I: substituting => /usr/share/debmake/extra1patches/series
I: creating => debian/patches/series
I: run "debmake -x2" to get more template files
I: $ wrap-and-sort
```

After running `debmake`, a new debian directory and template files are generated. Check the source tree at this point.

```shell
$ cd ..
$ tree
├── hello-2.12.1
│......
│   ├── debian
│   │   ├── changelog
│   │   ├── compat
│   │   ├── control
│   │   ├── copyright
│   │   ├── patches
│   │   │   └── series
│   │   ├── README.Debian
│   │   ├── rules
│   │   ├── source
│   │   │   ├── format
│   │   │   └── local-options
│   │   └── watch
│......
├── hello_2.12.1.orig.tar.gz -> hello-2.12.1.tar.gz
└── hello-2.12.1.tar.gz
```

### 3. Editing the debian Directory Template Files

#### (1) Check the debian/source folder

##### [1] Ensure that the package format is quilt

`debian/source/format`file defines the format of the source package, which is typically divided into`3.0 (native)`and`3.0 (quilt)`，According to the requirements of the openKylin package building workflow, the source package format must be`3.0 (quilt)`.

```shell
$ cd hello_2.12.1
$ cat debian/source/format 
3.0 (quilt)
```

##### [2] The local-options file is usually not needed and can be removed.

```shell
rm debian/source/local-options
```

#### (2) Edit the debian/changelog file

`debian/changelog`is the update log of the software package, including the package name, version number, version information description, etc.
First, let's check the default initialization content of `debian/changelog`:


```text
hello (2.12.1-1) UNRELEASED; urgency=low

  * Initial release. Closes: #nnnn
    <nnnn is the bug number of your ITP>

 -- your name <your email address>  Wed, 16 Nov 2022 09:40:47 +0800
```

The first line on the left is the package name, and the version number of the package is in parentheses on its right. The package version number format is: `<upstream_version>-<revisions>`, where upstream_version is the upstream version number, which has been automatically identified as 2.12.1 when running `debmake`. revisions is the revision version, which is usually set to 1 by default, but for openKylin packages, it should be set as `ok1` for the first revision.

The UNRELEASED on the right side of the version number needs to be changed to yangtze, and the description in the middle is usually written as "Build for openKylin."

The `your name` in the last line should be your personal name, preferably in English. The `your email address` is your personal email address. Both of these should be filled in and cannot be left blank.

The modified content of `debian/changelog` should be as follows for reference:

```text
hello (2.12.1-ok1) yangtze; urgency=medium

  * Build for openKylin.

 -- zhangsan <zhangsan@email.com>  Tue, 15 Nov 2022 10:13:02 +0800
```

#### (3) Edit the debian/control file

`debian/control`defines the information of the source package and binary package, including compile dependencies, installation dependencies, etc. It is an essential file and can be edited based on the information provided by the upstream software. The edited debian/control file should look like the following.

```text
Source: hello
Section: devel
Priority: optional
Maintainer: openKylin Developers <developers@lists.openkylin.top>
Standards-Version: 4.3.0
Build-Depends: debhelper-compat (= 9)
Homepage: http://www.gnu.org/software/hello/
Rules-Requires-Root: no

Package: hello
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: hello-traditional
Replaces: hello-traditional, hello-debhelper (<< 2.9)
Breaks: hello-debhelper (<< 2.9)
Description: example package based on GNU hello
 The GNU hello program produces a familiar, friendly greeting.  It
 allows non-programmers to use a classic computer science tool which
 would otherwise be unavailable to them.
 .
 Seriously, though: this is an example of how to do a Debian package.
 It is the Debian version of the GNU Project's `hello world' program
 (which is itself an example for the GNU Project).
```

#### (4) Edit the debian/rules file

`debian/rules`is the build script provided by the software package maintainer. This file is actually another Makefile, and the content should be written according to the specific compilation method of the upstream software. During compilation, the Debian-format source package reads the content of the debian/rules file as the compilation rules, so this file is also essential.

Hello is a standard software package built using `Autotools`. The compilation and installation method is:

```shell
$ ./configure 
$ make
$ sudo make install
```

According to the compilation method mentioned above, the content of the debian/rules file can be written as follows:

```Makefile
#!/usr/bin/make -f
%:
    dh $@

override_dh_auto_clean:
    [ ! -f Makefile ] || $(MAKE) distclean

override_dh_installdocs:
    dh_installdocs NEWS
```

#### (5) Handling non-essential files

debian/compat and debian/README.Debian are not essential files and can be removed for this software package.

```shell
$ rm debian/compat
$ rm debian/README.Debian
```

### 4. Pack and test the compilation

Execute `dpkg-source -b .` at the top level of the source directory to package the upstream source code into a Debian-formatted source package. After successful packaging, two new files will be generated with the extensions `.debian.tar.xz` and `.dsc`.

```shell
$ dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building hello using existing ./hello_2.12.1.orig.tar.gz
dpkg-source: info: building hello in hello_2.12.1-ok1.debian.tar.xz
dpkg-source: info: building hello in hello_2.12.1-ok1.dsc

$ tree -L 1 ../
../
├── hello-2.12.1
├── hello_2.12.1-ok1.debian.tar.xz
├── hello_2.12.1-ok1.dsc
├── hello_2.12.1.orig.tar.gz -> hello-2.12.1.tar.gz
└── hello-2.12.1.tar.gz

1 directory, 4 files
```

Before uploading the source package to the Git repository, we can perform a local build test to confirm whether the files such as debian/rules are written correctly. It is recommended to complete the local build test before uploading to the Git repository every time. The Debian-format source package uses `dpkg-buildpackage` tool for compilation. When the compilation is successful, it looks like the following:

```shell
$ dpkg-buildpackage -b -uc
dpkg-buildpackage: info: 源码包 hello
dpkg-buildpackage: info: 源码版本 2.12.1-ok1
dpkg-buildpackage: info: source distribution yangtze
dpkg-buildpackage: info: 源码修改者 your name <your email address>
dpkg-buildpackage: info: 主机架构 amd64
......
dpkg-deb: 正在 '../hello_2.12.1-ok1_amd64.deb' 中构建软件包 'hello'。
dpkg-deb: 正在 'debian/.debhelper/scratch-space/build-hello/hello-dbgsym_2.12.1-ok1_amd64.deb' 中构建软件包 'hello-dbgsym'。
        Renaming hello-dbgsym_2.12.1-ok1_amd64.deb to hello-dbgsym_2.12.1-ok1_amd64.ddeb
 dpkg-genbuildinfo --build=binary
 dpkg-genchanges --build=binary >../hello_2.12.1-ok1_amd64.changes
dpkg-genchanges: info: binary-only upload (no source code included)
 dpkg-source --after-build .
dpkg-buildpackage: info: binary-only upload (no source included)

$ tree -L 1 ../  
../
├── hello-2.12.1
├── hello_2.12.1-ok1_amd64.buildinfo
├── hello_2.12.1-ok1_amd64.changes
├── hello_2.12.1-ok1_amd64.deb
├── hello_2.12.1-ok1.debian.tar.xz
├── hello_2.12.1-ok1.dsc
├── hello_2.12.1.orig.tar.gz -> hello-2.12.1.tar.gz
├── hello-2.12.1.tar.gz
└── hello-dbgsym_2.12.1-ok1_amd64.ddeb

1 directory, 8 files
```

## III. Upload the source package to Gitee platform

### 1. Create the corresponding Git repository

#### (1) Fork the openkylin/community repository to personal repository

To create a new code repository in the openKylin organization, openkylin/community repository needs to be used.
[https://gitee.com/openkylin/community](https://gitee.com/openkylin/community)

Fork the openkylin/community repository to the personal account on the browser web page. A new repository address will be generated: `https://gitee.com/<username>/community` Clone this new repository to the local machine.

```shell
$ git clone git@gitee.com:<username>/community.git
```

The subsequent process, from the perspective of the maintainer role, is divided into two types: **individual package maintainer** and **SIG group maintainer.**


#### (2) As an individual package maintainer

##### [1] Create package maintenance information

Enter the first-level directory packages and create `hello.yaml` with the following content:
(For more examples of filling in, you can browse:[https://gitee.com/openkylin/community/tree/master/packages](https://gitee.com/openkylin/community/tree/master/packages))

```yaml
name: hello
path: hello
maintainers:
- name: 你的Gitee ID
  openkylinid: 你的openkylinid用户名
  displayname: 你的姓名或昵称
  email: 你的联系邮箱
```

##### [2] Push the changes to the personal remote repository and submit a pull request to the openkylin/community repository.

```shell
$ git add packages
$ git commit -m "添加hello软件包维护信息"
$ git push origin master
```

After pushing to the personal repository, initiate `Pull Requests` on the web page, and the corresponding community manager will review and accept the pull request. After that, the openkylin platform will automatically create the corresponding openkylin repository based on the description in the package maintenance information.


For example, after the pull request for `hello` package is merged, a repository named `hello` will be created in openkylin, which can be accessed in the browser. The repository address for `hello` package is:[https://gitee.com/openkylin/hello](https://gitee.com/openkylin/hello)

#### (3) As a SIG group maintainer


##### [1] Create package maintenance information


This step requires adding the source package name to the sig.yaml file of the corresponding SIG group. Taking the Packaging SIG group as an example, the operation is as follows:

```shell
$ echo "- hello" >> sig/packaging/sig.yaml
```

##### [2] Push the changes to the personal remote repository and submit a pull request to the openkylin/community repository.

```shell
$ git add sig/packaging/sig.yaml
$ git commit -m "新增hello软件包"
$ git push origin master
```

After pushing to the personal repository, initiate `Pull Requests` on the web page, and after the corresponding SIG group maintainer approves and accepts the PR, openkylin platform will automatically create the corresponding openkylin repository based on the package maintenance information described in the PR. 



For example, after the PR request for `hello` package is merged, an openkylin repository named `hello` will be created, which can be accessed in a browser. The repository address for `hello` package is：[https://gitee.com/openkylin/hello](https://gitee.com/openkylin/hello)

### 2. Build a local Git repository.

Obtain the building tools from: [https://gitee.com/openkylin/packaging-tools](https://gitee.com/openkylin/packaging-tools)

```shell
$ path/to/source-packing.py import-to-git --dsc-file hello_2.12.1-ok1.dsc --packaging-branch openkylin/yangtze
```

### 3. Push to Remote Git Repository.

```shell
$ cd hello
$ git remote add origin git@gitee.com:openkylin/hello.git
$ git push --all && git push --tags
```

At this point, you can open the Gitee platform's repository link ([https://gitee.com/openkylin/hello](https://gitee.com/openkylin/hello)) in your browser to check if the source code has been pushed successfully.
