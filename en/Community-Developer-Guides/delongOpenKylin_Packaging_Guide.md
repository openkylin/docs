---
title: openKylin Packaging Guide
description:
published: true
date: 2022-06-23T06:51:53.339Z
tags:
editor: markdown
dateCreated: 2022-05-17T07:44:07.346Z
---

# **openKylin Packaging Guide**


### **About this document**


This document is an instructive document that introduces the production of DEB packages. It focuses on guiding you to complete the production of the first DEB package. The detailed rules for the production of DEB packages will be scattered in each specific specification. We will continue to update and improve this document.

In writing this document, the following guidelines were followed:

Provides an overview only, ignoring edge cases. (**Big Picture Principle**)

Keep text short and compact. (**KISS principle**)

Don't reinvent the wheel. (use link to **existing reference**)

Focus on using non-graphical tools and consoles. (using **shell example**)

Be objective. (using [popcon](http://popcon.debian.org/) etc.)

### **1. Software packaging**


A software package under openKylin usually includes a collection of a series of files, which define how applications or library files can be released and deployed through package managers (such as apt, yum, etc.). As a way to replace source code compilation and installation, packaging means making DEB software packages, combining application binary files, configuration documents, man/info help pages and other files into one file, thus making software installation easier stand up. Through the package manager, a series of operations such as acquisition, installation, uninstallation, and query are completed.

**1.1. General Rules**

  openKylin attempts to normalize various open source projects into a coherent system. Therefore, openKylin formulates this packaging guide to standardize the action of making DEB.

- openKylin follows the general [Linux Base Standard (LSB)](https://gitee.com/link?target=http://www.linuxbase.org/). This standard aims to reduce the differences between distributions.

- openKylin also complies with the [Linux Filesystem Hierarchy Standard (FHS)](https://gitee.com/link?target=http://www.pathname.com/fhs/). The standard is about how to manage
     A reference to the Linux filesystem hierarchy.

- In addition to following these general rules that general Linux distributions follow, this document normalizes the practical details of packaging for openKylin Community Edition.

**1.2. Packaging Basics**

Before using this document to create a DEB package, it is recommended that you be familiar with the following knowledge points, which are necessary for how to create a high-quality software package, and provide many detailed details.

|   | skill             |      | links                                                                   |
|---|-------------------|------|-------------------------------------------------------------------------|
| 1 | Debian policy document    | required | https://www.Debian.org/doc/Debian-policy/                               |
| 2 | Debian Maintainer's Guide | required | see the https://www.Debian.org/doc/manuals/debmake-doc/index.zh-cn.html |

**1.3. Associated Documents**

If you plan to introduce software to openKylin official software repository, please refer to
[ openKylin contribution guide](openKylin contribution guide.md).

**1.4. Applicability**

In general, these guidelines apply to all versions of openKylin, including non-lifecycle releases, lifecycle releases, and development releases.

The guidelines also cover all types and delivery scenarios of packages coming into openKylin to some extent. openKylin is a community version, so there is no guarantee that all the rules will remain unchanged. Currently, its core and most important basic principles will not change significantly in the foreseeable future.

**1.5. Technical Reminder**

Here are some technical suggestions, which can make it easier and more effective for you to maintain software packages with other maintainers, so that
  The output of the openKylin project is maximized.

Make your package easy to debug (debug).

Keep your packages simple and understandable.

Don't over-engineer packages.

Make your packages well documented.

Use a readable coding style.

Write comments in the code.

Format your code to make it consistent.

Maintain a git repository for packages.

### **2. Packaging rules**


Each operating system is self-contained, and apart from different technical routes and milestones, the organization of software packages is also different.

The main differences focus on the following aspects:

(1) Different package managers (fedora, openSUSE use rpm, Debian use deb, etc.).

(2) Maintain different package lists, including different software versions.

(3) Separate software package splitting rules.

(4) A software dependency graph that is naturally formed based on different splitting rules.

**2.1. Package Manager**

  openKylin does not intend to reinvent the wheel, using DEB as the base, with apt, dpkg, etc. to manage software packages. DEB format is
Debian system (including Debian and Ubuntu) exclusive installation package format, with apt
software management system, which has become the current Linux
A very popular installation package. Perhaps in the near future, if deb, apt and other tools cannot meet the needs, openKylin will consider launching a new project.

**2.2. Software list, software selection**

  openKylin has its own collection of software lists, and currently has integrated 1000+ software packages, which are still being enriched and improved.

  The software code source of openKylin is directly taken from the stable version of the original software community, and the Debian/\* rule files are written and integrated according to this packaging specification.

  openKylin follows the Upstream First principle.

**2.3. Software split rules**

For upstream source code examples like the one below, we give here some typical
Scenarios and practices of multiarch package splitting:

- a repository source *libfoo-1.0*\*\*.tar.gz\*\*

- A software tool source code *bar-1.0*\*\*.tar.gz\*\*, the software is written in a compiled language

- A software tool source code *baz-1.0*\*\*.tar.gz\*\*, the software is written in an interpreted language

| binary package   | type   | Architecture: | Multi-Arch: | Package Contents                             |
|----------------|--------|---------------|-------------|----------------------------------------|
| lib foo1       | lib \* | any           | same        | Shared library, co-installable                     |
| lib foo -dev   | dev \* | any           | same        | Shared library header files and related development files can be installed together |
| lib foo -tools | bin \* | any           | foreign     | Runtime support program, not co-installable           |
| lib foo -doc   | doc \* | all           | foreign     | Shared library documentation                             |
| bar            | bin \* | any           | foreign     | Compiled program files cannot be co-installed         |
| bar -doc       | doc \* | all           | foreign     | Documentation files that accompany the program                     |
| baz            | script | all           | foreign     | interpreted program file                         |

### **3. Package verification**


(1) You must test your package to see if there are installation problems. The debi command helps you to test all generated binary packages.

(2) Use lintian to check your .changes
document. The lintian command runs a number of test scripts to check for common packaging errors.
Of course, to replace the .changes generated by your own package
The filename of the file. The output of the lintian command is often marked with the following:

- E: stands for error: Definite Debian Policy violation or a definite packaging error.

-W: stands for warning: possible violation of Debian Policy or a possible packaging error.

- I: stands for information: information for a specific packaging class.

- N: stands for comment: detailed information to help you debug.

- O: stands for overridden: a message that is overridden by the lintian-overrides file, but due to the use of
     \--show-overrides option is displayed.

(3) Check whether the binary package after software selection and packaging can use the apt command set install, remove, purge normally
and upgrade. The entire test process should be completed in accordance with the following sequence of operations:

- Install the previous version of the package if possible;

- upgrade packages from the previous version;

- Downgrading the package to the previous version (optional);

- completely remove the package;

- Fresh install the package;

- Uninstall the package;

- Install the package again.

- completely remove the package;

If this is your first package, you should create a test package with a different version number to complete the upgrade test, so as to avoid future problems.

(4) After installation and upgrade, verify: 1) For services, verify start/stop/restart/reload; 2) For commands, at least verify that the basic functions are available.

(5) The source code of the software package comes with tests, which cannot be commented, deleted, or disabled at will. It is necessary to ensure that when the code is submitted, the make in the access control
The check self-test case passed.

(6) Especially after the software is selected and upgraded, it is difficult to independently judge the impact on other software packages, and an integration test is required.

### **4. Packaging specification**


Rules and norms are a process of gradual improvement, and it is necessary to ensure that existing rules are followed.


**4.1. Reliable source**

- Do not embed precompiled binaries or libraries, all binaries or libraries included in the package must be compiled from the source package. Files of the binary firmware class are exempt. If it is necessary to introduce binary, it will be decided after discussion with TC.

- The software package should try to avoid bundling multiple, separated, and upstream projects into one software package, and strive to make one package come from one community.

- Software **should** be open source software, the definition of open source software refers to [https://opensource.org/osd.html](https://gitee.com/link?target=https://opensource.org/ osd.html)
     . If it is not an open source software, it will be decided after discussion with TC.

- Integrate open source software without legal risks, [Open Source License Directory](https://gitee.com/link?target=https://opensource.org/licenses/alphabetical).

- The content of the Debian directory file should be adapted to openKylin and be correct, accurate, clear and concise. If you cite content from other releases, or from the original community, you must explain it at the top.

- Software **must not** be imported if it exists in **Blacklist**.

- Each software introduction decision is taken as a case and a reference for subsequent similar software introduction decisions. technical
     Committ is responsible for the consistency of software introduction principles.

**4.2. Architecture support**

- The package maintainer should try to ensure that it can be successfully compiled on several architectures such as aarch64, x86_64, and MIPS. Later, as openKylin supports other architectures, the requirements for building may be increased.

**4.3. Software Splitting**

- The splitting of the software needs to be implemented in accordance with the openKylin software splitting rules.

- Create corresponding binary package entries in the **Debian/control** file for all binary packages.

- list all file paths in the corresponding **Debian/binarypackagename.install** file

**4.4. Naming rules**

- The name of the package consists of **package name-package version number**. If the fetched upstream source code is of the form
     **hello-0.9.12.tar.gz**, you can use **hello** as the upstream source code name and put
     **0.9.12** as the upstream version number.

- Compose openKylin
     There are certain restrictions on character selection for package names. The most obvious restriction should be the prohibition of uppercase letters in package names. Here is a summary of the rules in regular expression form:

     Upstream package name (**-p**): [-+.a-z0-9]{2,}

     Binary package name (**-b**): [-+.a-z0-9]{2,}

     Upstream version number (**-u**): [0-9][-+.:\~a-z0-9A-Z]\*

      openKylin revision (**-r**): [0-9][+.\~a-z0-9A-Z]\*

- The rules for version comparison can be summarized as follows:

     Strings are compared in order from start to end.

     Characters are larger than numbers.

     Numbers are compared in integer order.

     Characters are compared in ASCII-encoded order.

**4.5. Format specification**

use **debmake**
The necessary template files conforming to the deb package specification can be created for package maintainers. In the generated file, comment lines starting with
**\#**
Get started, which contains some tutorial text. You must remove or modify such comment lines before uploading the package to the openKylin repository.

Among the generated template files, the following files are very important:

- **Debian/changelog**
     The file records the history of the package and defines the version of the upstream package and openKylin on its first line
     revision. All changes should be documented in a clear, formal and concise language style.

- **debian/control**
     The file describes the compiled binary package, compilation dependencies and installation dependencies of the current software package.

- **debian/rules** file describing how to build this package

**4.6. Dependencies**

- Make sure that the compilation and installation dependencies of the package already exist in the openKylin repository. If not, it needs to be packaged and imported together. Compilation dependencies and installation dependencies need to be confirmed by yourself to ensure completeness. Try to avoid circular dependencies.

- **Depends**

This field describes the collection of all packages that this package depends on, and it can be installed only after all the packages it depends on have been installed. Please specify all the software packages that your software package must depend on here. If the dependent software packages are not installed, the software package may not work properly.

- **Recommends**

The packages in this item are not strictly required to be installed to ensure that the program runs. When a user installs a package, all front-end package management tools will ask if they want to install these recommended packages. **aptitude**
and **apt-get**
Will automatically install recommended packages when installing your package (users can disable this default behavior). **dpkg**
This is ignored.

- **Suggests**

Packages in this entry may work better with this program, but are not required. All frontends may not ask to install suggested packages when the user installs the program. **aptitude**
Can be configured to automatically install suggested packages when installing software, but this is not the default. **dpkg** and
**apt-get** will ignore this.

- **Pre-Depends**

Dependencies in this item are stronger than **Depends** items. Packages are only installed if pre-dependencies are installed and *configured correctly*
Then it can be installed normally. This should be used *very cautiously*, only in TC
Only available after discussion. Remember: don't use this at all. :-)

- **Conflicts**

This package can only be installed if all conflicting packages have been removed. Use this when the program does not run at all or has serious problems with the presence of certain packages.

- **Breaks**

Packages listed will be corrupted after this package is installed. Usually **Breaks**
It should be accompanied by a description of "how much the version number is less than". In this way, the package management tool will choose to upgrade the specific version of the package that is broken as a solution.

- **Provides**

Some types of packages define multiple alternate virtual names. Use this if your program provides the functionality of an existing virtual package.

- **Replaces**

When your program replaces some files in other packages, or completely replaces another package (with
**Conflicts** together). Some files in the listed packages will be overwritten by files in your package.

**4.7. Compile and Build**

- **Debian/rules**
     The script repackages the upstream build system to compile the source code files and install the files into
     \*\*\$(DESTDIR), \*\* and store the generated files in each **deb** binary package file.

- by adding the appropriate **override_dh\_**\*
     Target and write the corresponding rules, which can realize the flexible customization of **debian/rules** scripts.

- if some specific \*\*dh_\*\**foo* needs to be invoked in the **dh** command
     command to take some special action, any action that is automatically executed can be disabled by **debian/rules**
     Overridden by additional Makefile targets like \*\*override_dh_\*\**foo* added in .

- Do not embed timestamps based on system time.

- Use "**dh \$@**" in **Debian/rules** to apply the latest **debhelper** features.

- Export the environment variable "**LC_ALL=C.UTF-8**" in the build environment.

- For timestamps used in upstream source code, use environment variables provided by debhelper
     The value of **\$SOURCE_DATE_EPOCH**.

### **5. An example**

**5.1. Overall process**

Build a single non-native Debian from the upstream source tarball **debhello-0.0.tar.gz**
The general flow of the software package can be summarized as follows:

- The maintainer takes the upstream source tarball **debhello-0.0.tar.gz** and extracts its contents to
     **debhello-0.0** directory.

- The **debmake** command makes debian
     debianize, specifically, creating a **debian**
     directory and add various template files only to that directory.

- A symbolic link named **debhello_0.0.orig.tar.gz** is created and points to
     **debhello-0.0.tar.gz** file.

- Maintainers must edit and modify template files by themselves.

- The **debuild** command builds binary packages based on debianized source trees.

- **debhello-0.0-1.debian.tar.xz** will be created, which contains the **debian** directory.

**Approximate flow of software package construction.**

\$ tar -xzmf debhello-0.0.tar.gz

\$ cd debhello-0.0

\$ debmake

... manual customization

\$ debuild

...

**5.2. Template files**

Among them, debmake is used to generate debianized template files, and the specific structure is as follows:

**The source tree after running the basic debmake command. **

\$ tree

├── debhello-0.0

│ ├── LICENSE

│ ├── Makefile

│ ├── debian

│ │ ├── README. Debian

│ │ ├── changelog

│ │ ├── control

│ │ ├── copyright

│ │ ├── patches

│ │ │ └── series

│ │ ├── rules

│ │ ├── source

│ │ │ ├── control

│ │ │ ├── format

│ │ │ ├── local-options

│ │ │ ├── options

│ │ │ └── patch-header

│ │ ├── tests

│ │ │ └── control

│ │ ├── upstream

│ │ │ └── metadata

│ │ └── watch

│ └── src

│ └── hello.c

├── debhello-0.0.tar.gz

└── debhello_0.0.orig.tar.gz -\> debhello-0.0.tar.gz

7 directories, 19 files

The **Debian/rules** files here are build scripts that should be provided by the package maintainer. At this point the file is created by
Template files generated by the **debmake** command.

**5.3. Edit template file**

As a maintainer, making a proper Debian package will of course require some manual tweaking of the template contents.

In order to make the installation file part of the system file, **\$(prefix)** default in **Makefile**
The value of **/usr/local** needs to be overridden to **/usr**. To do this, follow the method given below, in the
Add the file named **override_dh_auto_install** to the **Debian/rules** file
target, set "**prefix=/usr**" in it.

**debian/rules (maintainer version): .**


```
$ vim debhello-0.0/debian/rules
  ... hack, hack, hack, ...
$ cat debhello-0.0/debian/rules
#!/usr/bin/make -f
export DH_VERBOSE = 1
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic
export DEB_LDFLAGS_MAINT_APPEND = -Wl, --as-needed

%:
         dh $@

override_dh_auto_install:
         dh_auto_install --prefix=/usr
```


Exporting the =**DH_VERBOSE** environment variable in the **Debian/rules** file as above can force **debhelper**
The tool outputs fine-grained build reports.

Export **DEB_BUILD_MAINT_OPTION** variable as above can be as **dpkg-buildflags**
In the man page "FEATURE
AREAS/ENVIRONMENT" section, set the hardening options.

Exporting **DEB_CFLAGS_MAINT_APPEND** as above can force the C compiler to give all types of warnings.

Export **DEB_LDFLAGS_MAINT_APPEND** as above
It is possible to force the linker to only link against the libraries that are really needed.

For Makefile-based build systems, **dh_auto_install**
What the command does is basically "**\$(MAKE) install DESTDIR=Debian/debhello**". created here
The **override_dh_auto_install** target modifies its behavior to "**\$(MAKE) install
DESTDIR=Debian/debhello prefix=/usr**".

Here are the maintainer versions of the **Debian/control** and **Debian/copyright** files.

**Debian/control (maintainer version): .**


```
$ vim debhello-0.0/debian/control
  ... hack, hack, hack, ...
$ cat debhello-0.0/debian/control
Source: debhello
Section: devel
Priority: optional
Maintainer: Osamu Aoki <osamu@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.1
Homepage: https://salsa.debian.org/debian/debmake-doc
Rules-Requires-Root: no

Package: debhello
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Simple packaging example for debmake
  This Debian binary package is an example package.
  (This is an example only)
```


in **Debian/**
There are some other template files in the directory. Depending on the scenario, they also need to be updated. The package can then be built.

**5.4. Building packages with debuild**

You can use **debuild** or an equivalent command tool to build a non-native Debian inside this source tree
package.

**Debhello version 0.0 uses the debuild command to generate files: .**


```
$ cd ..
$ tree -FL 1
.
├── debhello-0.0/
├── debhello-0.0.tar.gz
├── debhello-dbgsym_0.0-1_amd64.deb
├── debhello_0.0-1.debian.tar.xz
├── debhello_0.0-1.dsc
├── debhello_0.0-1_amd64.build
├── debhello_0.0-1_amd64.buildinfo
├── debhello_0.0-1_amd64.changes
├── debhello_0.0-1_amd64.deb
└── debhello_0.0.orig.tar.gz -> debhello-0.0.tar.gz
1 directory, 9 files
```


You can see all generated files.

- **debhello_0.0.orig.tar.gz** is a symlink to the upstream source tarball.

- **debhello_0.0-1.debian.tar.xz** contains content generated by the maintainer.

- **debhello_0.0-1.dsc** is the metadata file for Debian source packages.

- **debhello_0.0-1_amd64.deb** is a Debian binary package.

- **debhello-dbgsym_0.0-1_amd64.deb** is the Debian debug symbols binary package.

- **debhello_0.0-1_amd64.build** is the build log file.

- **debhello_0.0-1_amd64.buildinfo** is **dpkg-genbuildinfo**
     Generated metadata file.

- **debhello_0.0-1_amd64.changes** is the metadata file for Debian binary packages.







