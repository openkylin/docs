__openKylin OS Input Method Adaptation Guide__

# 1\. __Background__.

This document provides the introduction of fcitx5 input method framework built in openKyLin OS and the details of the work needed to adapt the input method engine to fcitx5.

# 2\. __fcitx5 input method framework__

## 2\.1 __Introduction__

### 2\.1\.1 __Overall design architecture

fcitx5 input method framework adopts plug-in design. In this design, it contains one fcitx5 input method framework service and several plug-in dynamic libraries. The main modules are introduced below.

#### 2\.1\.1\.1\.1 __ fcitx5 input method framework service__

fcitx5 input method framework service is a service provided by fcitx5 executable program. This service is responsible for loading each plugin dynamic library and communicating with other modules.

#### 2\.1\.1\.1\.2 __front-end plug-in module__

fcitx5 input method framework has several front-end plug-in modules, namely dbusfrontend, fcitx4frontend, ibusfrontend, waylandim, xim. used to be compatible with different system environments and applications.

#### 2\.1\.1\.1\.2\.1 __dbusfrontend__

dbus frontend plugin is implemented by fcitx5's own protocol, Qt/GTK input method module use this frontend plugin to communicate with fcitx5.

#### 2\.1\.1\.2\.2 __fcitx4frontend__

#### fcitx4 frontend plug-in provides support for applications of input method module that use fcitx4 dbus protocol.

#### 2\.1\.1\.2\.3 __ibusfrontend__

ibus frontend plugin implements ibus protocol.

#### 2\.1\.1\.2\.4 __waylandim__

waylandim implements Wayland's server-side protocol for providing input method protocols.

#### 2\.1\.1\.1\.2\.5 __xim__

xim implements the server-side protocol for X11's input method protocol.

#### 2\.1\.1\.1\.3 __UI plugin module__

fcitx5 input method framework provides three UI plug-ins, which are classic UI plug-in, kimpanel UI plug-in, and virtualkeyboard UI plug-in.

#### 2\.1\.1\.1\.3\.1 __classic UI__

UI plugins used by default by fcitx5 and already integrated in the input method framework.

#### 2\.1\.1\.1\.3\.2 __kimpanel UI__

UI proxy plugin for the physical keyboard input window used by fcitx5. The adaptation requires the implementation of the kimpanel server. The plugin uses the DBus interface to communicate with the server.

#### 2\.1\.1\.1\.3\.3 __virtualkeyboard UI__

Virtual keyboard UI proxy plugin used by fcitx5. The adaptation requires the implementation of a virtual keyboard server. The plugin uses the DBus interface to communicate with the server.

#### 2\.1\.1\.1\.4 __Input Method Engine Plugin Module__

The fcitx5 input method framework defines the interface used by the input method engine plug-in. If any input method engine implements this interface and export the input method engine object in certain format, then fcitx5 input method framework can load and use this input method engine correctly.

### 2\.1\.2 __basic working principle__

Multiple front-end plug-in modules of fcitx5 input method framework are implemented to support multiple input method protocols and GUI frameworks, including XIM, Wayland, ibus and other input method protocols and Qt, GTK and other GUI frameworks.

Among them, the dbus frontend module is responsible for dbus communication with the input method plugins of Qt and GTK GUI frameworks.

Take Qt5 application as an example, here is a brief introduction of Qt5 application's interaction with input method engine through fcitx5 input method framework. The basic principle schematic is shown as follows.

![inputmethod.png](./assets/system_input_method_adaptation_guide/inputmethod.png)

#### 2\.1\.2\.1 __Qt5 input method module__

fcitx5\-qt project implements Qt5 defined input method interface and interface to communicate with dbus front-end plug-in of fcitx5 input method framework, so that events in Qt5 program can be sent to fcitx5 input method framework.

#### 2\.1\.2\.2 __dbus front-end plugin__

Receive dbus signals sent from Qt or GTK program through dbus or call

#### 2\.1\.2\.3 __fcitx5 input method framework__

dbus front-end plug-in will do some processing of event or function call and then call the function in fcitx5 input method framework, usually call the method of InputContext object to send an event to fcitx5 input method framework

#### 2\.1\.2\.4 __UI proxy plugin__

fcitx5 input method framework supports multiple UI display methods, including classic UI plugin using X11 API and kimpanel plugin using remote display service. The UI proxy plugin in the schematic says kimpanel plugin, while impanel is the corresponding remote display service. This service can display the input window of the physical keyboard input method.

#### 2\.1\.2\.5 __Input Method UI Service__

If the input method framework uses a UI proxy plugin, the UI proxy plugin communicates with the corresponding input method UI service.

#### 2\.1\.2\.6 __Input Method Engine Plugin__

After fcitx5 input method framework receives keystroke event, after some processing, it will call the function of input method engine plug-in which implemented InputMethodEngine API defined by fcitx5. Communication with input method engine service is realized through functions in plug-in

#### 2\.1\.2\.7 __InputMethodEngine__

The input method engine implements the real logic of the input method. The input method engine can either be wrapped directly in the input method engine plug-in, or the communication between the input method engine and the input method engine plug-in can be implemented by means of IPC communication.

## 2\.2 __ Support for physical keyboard input method engine

### 2\.2\.1 __UI support__

fcitx5 provides physical keyboard input method UI proxy plugin based on DBus protocol: kimpanel. the module is located in fcitx5/src/ui/kimpanel directory.

#### 2\.2\.1\.1 __DBus service__

#### 2\.2\.1\.1\.1 __DBus service name__

org\.kde\.kimpanel\.inputmethod

##### 2\.2\.1\.1\.2 __DBus object path__

/kimpanel

##### 2\.2\.1\.1\.1\.3 __DBus interface name__

org\.kde\.kimpanel\.inputmethod

#### 2\.2\.1\.2 __DBus signal__

The dbus signals provided by kimpanel can be called by kimpanel to enable communication from kimpanel to impanel. The impanel UI service is responsible for responding to these signals.

#### 2\.2\.1\.2\.1 __ExecDialog__

Show dialog box

#### 2\.2\.1\.2\.2\.2 __ExecMenu__

Show menu

#### 2\.2\.1\.2\.3 __RegisterProperties__

Register menu items

#### 2\.2\.1\.2\.4 __UpdateProperty__

Update a menu item

#### 2\.2\.1\.2\.5 __RemoveProperty__

Remove a menu item

#### 2\.2\.1\.2\.6 __ShowAux__

Whether to show Aux prompt text

#### 2\.2\.1\.2\.7 __ShowPreedit__

Whether to show the pre-edit string

#### 2\.2\.1\.2\.8 __ShowLookupTable__

Whether to show candidate results

#### 2\.2\.1\.2\.9 __UpdateLookupTableCursor__

Update the input position cursor

#### 2\.2\.1\.2\.10 __updatePreeditCaret__

Update the pre-edit string cursor

#### 2\.2\.1\.2\.11 __UpdatePreeditText__

Update the pre-edit text

#### 2\.2\.1\.2\.2\.12 __UpdateAux__

Update the aut auxiliary text

#### 2\.2\.1\.2\.2\.13 __UpdateSpotLocation__

#### 2\.2\.1\.2\.14 __UpdateScreen__

#### 2\.2\.1\.2\.15 __Enable__

Enables or disables the input window

#### 2\.2\.1\.3 __DBus method__

The dbus method provided by kimpanel can be called by the impanel UI service to enable communication from impanel to kimpanel

#### 2\.2\.1\.3\.1 __Exit__

Exit fcitx5 input method framework service

#### 2\.2\.1\.3\.2 __ReloadConfig__

Restart input method framework service on fcitx5

#### 2\.2\.1\.3\.3 __Configure__

Start fcitx5 configuration tool

#### 2\.2\.1\.3\.4 __LookupTablePageUp__

Candidate result page up

#### 2\.2\.1\.3\.5 __LookupTablePageDown__

LookupTablePageDown

#### 2\.2\.1\.3\.6 __SelectCandidate__

Selects the specified candidate result

#### 2\.2\.1\.3\.7 __PanelCreated__

Physical keyboard input method input window created successfully

#### 2\.2\.1\.3\.8 __PanelCreated2__

The physical keyboard input window was created successfully.

#### 2\.2\.2 __Input Method Engine Support__

fcitx5 provides input method engine APIs: InputMethodEngine, InputMethodEngineV2, InputMethodEngineV3 and InputMethodEngineV4. Among them, InputMethodEngineV4 adds support for virtual keyboard input method keypress events.

#### 2\.2\.2\.2\.1 __input_engine_API__

#### 2\.2\.2\.1\.1 __keyEvent__

Function: The main function of the input method to handle key events.

Parameters: const InputMethodEntry &entry,KeyEvent &keyEvent

Return value type: void

#### 2\.2\.2\.2\.1\.2 __activate__

Function: Activate the engine

Parameters

Return value type

#### 2\.2\.2\.2\.1\.3 __deactivate__

Function: Deactivate the engine

Parameter

Return value type

#### 2\.2\.2\.2\.1\.4 __reset__

Function: Reset

Parameter

Return value type

#### 2\.2\.2\.1\.5 __filterKey__

Function: Process key events \ (key events not processed elsewhere \)

Parameters: input method entry, key event

Return value type: void

Implementing the simplest physical input method only requires overriding the keyEvent function.

## 2\\.3 __ Support for virtual keyboard input method engine

### 2\.3\.1 __UI support__

fcitx5 provides virtual keyboard UI plugin based on DBus protocol: virtualkeyboard UI in fcitx5/src/ui/virtualkeyboard. The information and interface of this plugin are shown below.

#### 2\.3\.1\.1 __DBus service__

#### 2\.3\.1\.1\.1 __DBus service name__

org\.fcitx\.Fcitx5\.VirtualKeyboardBackend

#### 2\.3\.1\.1\.2 __DBus object path__

/virtualkeyboard

#### 2\.3\.1\.1\.3 __DBus interface name__

org\.fcitx\.Fcitx5\.VirtualKeyboardBackend1

#### 2\.3\.1\.2 __DBus signal__

#### 2\.3\.1\.2\.1 __ShowVirtualKeyboard__

Function: The display signal sent to the virtual keyboard.

Parameters: None.

#### 2\.3\.1\.2\.2 __HideVirtualKeyboard__

Function: The hidden signal sent to the virtual keyboard.

Parameters: None.

#### 2\.3\.1\.2\.3 __UpdatePreeditCaret__

Function: Signal sent to the virtual keyboard to set the pre-edit text cursor position.

Parameters.

int preeditCursor, the number of the cursor location.

#### 2\.3\.1\.2\.4 __UpdatePreeditArea__

Function: Signal sent to the virtual keyboard to set the pre-edit text.

Parameters.

string preeditText, the pre-edit text.

#### 2\.3\.1\.2\.5 __UpdateCandidateArea__

Function: Signal sent to the virtual keyboard to set the candidate words

Parameters.

vector<std::string> &candidateTextList, the set of candidate words for the current page, each element is a candidate word.

bool hasPrev, if or not the previous page candidate exists.

bool hasNext, if or not the next page candidate exists.

int pageIndex, the page number of the current candidate page.

#### 2\.3\.1\.2\.6 __NotifyIMActivated__

Function: Send the current input method activation signal to the virtual keyboard.

Parameters.

string uniqueName, the name of the current input method.

#### 2\.3\.1\.2\.7 __NotifyIMDeactivated__

Function: Send the current input method deactivated signal to the virtual keyboard.

Parameters

string uniqueName, the name of the current input method.

#### 2\.3\.1\.2\.8 __NotifyIMListChanged__

Function: Send a notification signal to the virtual keyboard that the current input method list has changed.

Parameters: None.

#### 2\.3\.1\.1\.3 __DBus method__

#### 2\.3\.1\.3\.1 __ProcessKeyEvent__

Function: Receives key events sent by the virtual keyboard and forwards them to the input method framework.

Parameters.

uint32\_t keyval, the value represented by the current key under the current keyboard layout, in English it is the ASCII code corresponding to the key.

uint32\_t keycode, the unique tag value of the current key in all keyboard layouts, defined in Linux.

uint32\_t state, modifier state of key, such as Shift, Caps Lock, etc. Details can be found in fcitx5/src/lib/fcitx\-utils/keysym\.h.

bool isRelease, if or not the key is pressed, true is lift, false is pressed.

uint32\_t time, timestamp when key is pressed, default is 0.

#### 2\.3\.1\.3\.2 __ProcessVisibilityEvent__

Function: Receive the virtual keyboard visibility event sent by the virtual keyboard and update the visibility information in the backend module.

Parameters

bool visible, the visibility of the virtual keyboard, true is showing, false is hidden

#### 2\.3\.1\.3\.3 __SelectCandidate__

Function: Receive the serial number of the candidate word selected by the virtual keyboard, and up-screen the candidate word.

Parameters

int index, the serial number of the candidate word selected by user.

#### 2\.3\.1\.3\.4 __PrevPage__

Function: Receive the candidate word page request sent by virtual keyboard, switch to the previous page of the candidate word.

Parameters: None.

#### 2\.3\.1\.3\.5 __NextPage__

Function: Receive the page flip request from the virtual keyboard, and switch to the next page of candidates.

Parameters: None. 

### 2\.3\.2 __Input method engine support__

InputMethodEngineV4 of fcitx5 input method engine adds virtualKeyboardEventImpl function to provide support for virtual keyboard input method key event, the related code is in fcitx5/src/lib/fcitx/inputmethodengine.

If input method engine wants to provide support for virtual keyboard keystroke events, it needs to implement InputMethodEngineV4 interface. Otherwise, just consider to implement other three interfaces for physical keyboard input method engine.

# 3\. __Adapt fcitx5 input method framework__

## 3\.1 __Physical keyboard input method engine adaptation__

### 3\.1\.1 __UI adaptation

fcitx5 provides default physical keyboard input method input window UI, which can be simply divided into two kinds without considering special client side input method UI: classic UI and kimpanel UI.

#### 3\.1\.1\.1\.1 __classic UI__

This UI is the input window UI provided by fcitx5 using underlying API\(X11, wayland\).

#### 3\.1\.1\.1\.2 __kimpanel UI__

This UI is an input window UI proxy provided by fcitx5, which defines several dbus APIs to communicate with remote physical keyboard input UI service. This module is originally provided to solve the problem that input window is not visible due to global search interface overlay on Ubuntu.

To simplify the adaptation, third-party input methods can consider directly adapting the dbus interface defined by this UI proxy module.

If you want to provide a more comprehensive customization, you should also consider providing a UI proxy module similar to kimpanel.

### 3\.1\.2 __Input Method Engine Adaptation__

#### 3\.1\.2\.1 __Input method configuration file writing__

The configuration files are divided into plugin configuration files and input method configuration files, and the fields in each configuration file have different meanings.

Addon configuration file: addon configuration file is installed in addon directory /share/fcitx5/addon/.

Example of Pinyin input method addon configuration file.
```c++
[Addon]
Name[ca]=Pinyin
Name[da]=Pinyin
Name[de]=Pinyin
Name[he]=פיניין:
Name[ko]=병음
Name[ru]=Пиньинь
Name[zh_CN]=Pinyin
Name=Pinyin
Category=InputMethod
Version=5.0.11
//The corresponding pinyin input method so is libpinyin.so
Library=libpinyin
Type=SharedLibrary
OnDemand=True
Configurable=True

//Addon dependencies
[Addon/Dependencies]
0=punctuation
```

Input method configuration file: input method configuration file is saved in input method directory /share/fcitx5/inputmethod/.

For example, Pinyin input method engine configuration file.

```c++
[InputMethod]
Name[ca]=Pinyin
Name[da]=Pinyin
Name[de]=Pinyin
Name[he]=פיניין:
Name[ko]=병음
Name[ru]=Пиньинь
Name[zh_CN]=Pinyin
Name=Pinyin
//Icon name for Pinyin input method
Icon=fcitx-pinyin
Label=Pinyin
LangCode=zh_CN
//Input method corresponding to the plug-in
Addon=pinyin
//Whether to support customization
Configurable=True
ðŸ™' ðŸ™'
#### 3\.1\.2\.2 __Project CMakeLists file compilation__

The project file CMakeLists file defines the compilation behavior of the input method source code file. It includes the libraries to be linked during compilation and the configuration file installation path, etc. Take the pinyin input method as an example.

```sh
#pinyin
add_library(pinyin SHARED pinyin.cpp)
target_link_libraries(pinyin PRIVATE Fcitx5::Core)
set_target_properties(pinyin PROPERTIES PREFIX "")
install(TARGETS pinyin DESTINATION "${FCITX_INSTALL_LIBDIR}/fcitx5")

#pinyin-addon configuration file conversion and installation
#export pinyin-addon.conf.in to pinyin-addon.conf
configure_file(pinyin-addon.conf.in pinyin-addon.conf) 
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/pinyin-addon.conf" RENAME quwei.conf DESTINATION "${FCITX_INSTALL_PKGDATADIR}/addon")

# Defines the installation path of the pinyin engine configuration file
# Input Method registration file
install(FILES "pinyin.conf" DESTINATION "${FCITX_INSTALL_PKGDATADIR}/inputmethod")
```
#### 3\.1\.2\.3 __Implementation of physical input method engine interface__

The input method engine part of the adaptation that is to rewrite the defined engine related interfaces, the main one is InputMethodEngine::keyEvent\(\). Take the tinyin input method as an example.

Inherit the AddonFactory plugin factory class and create a pinyin input method engine plugin class

```c++  
class PinyinEngineFactory : public AddonFactory {
public:
    AddonInstance *create(AddonManager *manager) override {
        registerDomain("fcitx5-chinese-addons", FCITX_INSTALL_LOCALEDIR);
        return new PinyinEngine(manager->instance());
    }
};
```
Pinyin Engine plugin class inherits InputMethodEngineV3 and implements the relevant interface functions. Such as activate\(\), deactive\(\), keyEvent\(\) and reset\(\) interfaces

```c++
class PinyinEngine final : public InputMethodEngineV3 {
public :
    PinyinEngine(Instance *instance);
    ~PinyinEngine();
    Instance *instance() { return instance_; }
    void activate(const InputMethodEntry &entry,
                  InputContextEvent &event) override;
    void deactivate(const InputMethodEntry &entry,
                    InputContextEvent &event) override;
    void keyEvent(const InputMethodEntry &entry, KeyEvent &keyEvent) override;
    void reset(const InputMethodEntry &entry,
               InputContextEvent &event) override;
   ...
  };
```
## 3\.2 __Virtual keyboard input method engine adaptation__

### 3\.2\.1 __UI adaptation

fcitx5 provides a default UI proxy module for virtual keyboard input method input window: virtualkeyboard UI.

To simplify the adaptation, third-party input methods can consider directly adapting the dbus interface defined by this UI proxy module.

If you want to provide more comprehensive customization, you should also consider providing a UI proxy module similar to virtualkeyboard.

### 3\.2\.2 __Input Method Engine Adaptation__

The virtual keyboard engine adaptation is basically similar to the physical keyboard keyboard engine adaptation, the only difference is that the virtual keyboard engine needs to implement the virtualKeyboardEventImpl virtual function in the InputMethodEngineV4 class.

# __Reference Documentation__

Input method engine related code documentation: https://codedocs.xyz/fcitx/fcitx5/classfcitx_1_1InputMethodEngine.html

Implementing a simplest input method: https://fcitx-im.org/wiki/Develop_an_simple_input_method












