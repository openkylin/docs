# openKylin source package git workflow

# 1. git branch settings

| Branch            | Description                                                         | Example                                                         |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Upstream branch        | A branch that does not contain the debian directory, holding the upstream source code of the package. Only one upstream branch is needed to modify the package, which is uniformly named upstream. For self-developed packages, multiple versions may be developed in parallel, so there may be multiple upstream branches. In order to avoid confusion with the packaging branch, "/" cannot be used in the name of the upstream branch | upstream master 1.0 2.0                                      |
| native packaging branch  | A branch for package maintainers to submit changes, including the debian directory. Modifications relative to the upstream branch can be submitted directly to this branch.  | The format of the name is: release code name/series code name The development branch of Galaxy Kirin V10 SP1 is named: yhkylin/v101openKylin The development branch of the Yangtze River version is named: openkylin/yangtze |
| quilt packaging branch   |  The modified package requires the use of the quilt branch for packaging. Under this branch, the changes made by the package maintainer relative to the upstream branch are stored in the debian/patches directory in the form of a patch, which is convenient for choosing when updating the upstream version. The self-developed package can be directly packaged with the native branch or quilt. The CI platform automatically completes the synchronization from the native branch to the quilt branch. | he quilt packaging branch corresponding to openkylin/yangtze, add packaging/ in front of the branch name: packaging/openkylin/yangtze |
| patch-queue branch | The temporary branch used in the CI build environment will not be submitted to the source code management platform, and assists in generating a patch from the modified content of the native packaging branch and saving it to the quilt packaging branch. | patch-queue/packaging/openkylin/yangtze                      |



# 2. source package import

For quilt format packages, you need to convert the package branch to native format, so that developers can manage the file modification history with git.


The git_init.sh script describes the specific steps to do this.

```
#! /bin/bash

set -e

[ ! -d $1 ] && echo "should specify source path" exit 1;

cd $1

# Switch to the package branch
git checkout openkylin/yangtze 
git checkout -b packaging/openkylin/yangtze

# Create a temporary integration patch branch, which will automatically be named patch-queue/openkylin/yangtze
gbp pq import 

# fix patches date format
gbp pq export

git add debian
git commit -m "format patches" || true

git checkout openkylin/yangtze 

# Integrate patch in the package branch
git merge patch-queue/packaging/openkylin/yangtze -m "apply patches"

# Delete temporary branches
git branch -D patch-queue/packaging/openkylin/yangtze

# Delete the debian/patches directory
rm -rf debian/patches

# Change the format of the format
echo "3.0 (native)" > debian/source/format

# Commit the changes
git add debian
git commit -m "changed debian/source/format to native"

```
## 2.1 Importing with tools

Importing can be done using the `source-packing` tool.

There are three main steps.

1. Repacking of the source code.

``Bash
. /source-packing rebuild-source --dsc-file sqlite3_3.31.1-4kylin0.3.dsc --new-revisions ok1
```

The new dsc file `sqlite3_3.31.1-ok1.dsc` will be generated

1. Reverse build out the git repository.

```Bash
. /source-packing import-to-git --dsc-file sqlite3_3.31.1-ok1.dsc --packaging-branch openkylin/yangtze
```

1. associate the remote repository and push to the remote repository (if the remote repository is not created, then you need to create the remote repository first)

```Bash
# Go to the source code repository built in step 2
cd sqlite3
# Associate the remote repository and push to it
git remote add origin git@gitee.com:openkylin/sqlte3.git
git push --tags
git push --all
```

The details of how to use this are described in the project documentation: [source reverse build tool-source-packing](https://gitee.com/openkylin/packaging-tools#源码反向构建工具-source-packing)

Note: You can use the manual way to import the source code, you can refer to section 3.2, 3.3 (if you use 3.1 tool reverse build successfully, 3.2, 3.3 section can be skipped)

## 2.2 Importing source code

```Bash
gbp import-dsc --pristine-tar --debian-branch=openkylin/yangtze --upstream-branch=upstream live-build_3.0~a57-1kylin38.20.04.2.dsc live- build
```

Parameter meaning

| --pristine-tar | Record the data of the original orig tarball to avoid changes in the tarball contents after repackaging with gbp |
| ------------------ | ------------------------------------------------------------ |
| --debian-branch= | Specify the name of the branch to be packaged, refer to https://docs.qq.com/doc/DQ2tlZ1BYUHBIVGp4 |
| --upstream-branch= | Specify the name of the upstream branch, use upstream for modified packages and master for self-developed packages |

In addition to the two branches we specified, the gbp import-dsc command will automatically create two tags.

- debian/3.0_a57-1kylin38.20.04.2 Package branch status in version 3.0~a57-1kylin38.20.04.2
- upstream/3.0_a57 The status of this imported upstream version 3.0~a57, we need to refer to this tag when packaging the quilt format source packages

### 2.2.1 Special case handling when there are multiple orig packages

3.0 (quilt)-formatted source packages support multiple orig packages, and some packages will package the upstream source's main directory and individual subdirectories separately

https://manpages.debian.org/testing/git-buildpackage/gbp-buildpackage.1.en.html#git~6

For example, sqlite3, version 3.31.1-4 has two orig packages: sqlite3_3.31.1.orig.tar.xz and sqlite3_3.31.1.orig-www.tar.xz, which will be compiled by first unpacking sqlite3_3.31.1.orig.tar.xz, and then sqlite3_3.31.1.orig-www.tar.xz解压到www子目录.

The package in this case is recognized by gbp when it is imported, but you need to specify --git-component=www when executing gbp buildpackage to successfully package it.

You can create debian/gbp.conf and fix the --git-component option

``Prolog
[buildpackage]
component=www
# If you have more than one, write component=['ft2docs', 'ft2demos']
```

Different packages have different components, so you need to create separate debian/gbp.conf files

Another pitfall is that if the compression of the orig and orig-component packages is different, one is xz and the other is gz, gbp buildpackage will report the following error.



```Plain%20Text
gbp:warning: Components specified, pristine-tar-commit not yet supported - disabling it.
gbp:info: Creating /home/xiewei/git/perl_5.30.0.orig.tar.xz
gbp:info: Creating /home/xiewei/git/perl_5.30.0.orig-regen-configure.tar.xz
gbp:error: Error creating perl_5.30.0.orig-regen-configure.tar.xz: Pristine-tar couldn't checkout "perl_5.30.0.orig-regen-configure.tar. xz": fatal: Path 'perl_5.30.0.orig-regen-configure.tar.xz.delta' does not exist in 'refs/heads/pristine-tar'
pristine-tar: git show refs/heads/pristine-tar:perl_5.30.0.orig-regen-configure.tar.xz.delta failed
```

Fixes.

1. unify the compression format, repackage perl_5.30.0.orig-regen-configure.tar.gz as perl_5.30.0.orig-regen-configure.tar.xz

```Apache
zcat perl_5.30.0.orig-regen-configure.tar.gz | xz -c - > perl_5.30.0.orig-regen-configure.tar.xz
```

1. Import pristine-tar information

```Shell
gbp pristine-tar commit --component=regen-configure path-to/perl_5.30.0.orig-regen-configure.tar.xz
```

## 2.3 Create native package branch, integrate patch (convert package branch to native branch for developers to compile for local testing)

```Bash
git checkout openkylin/yangtze # switch to package branch
gbp pq import # create a temporary integrated patch branch, which will be automatically named patch-queue/openkylin/yangtze
gbp pq export # Automatically format the patch to go back to the openkylin/yangtze branch
git add debian
git commit -m 'format patch'
git branch packaging/openkylin/yangtze # Create a branch of quilt for official packaging
git merge patch-queue/openkylin/yangtze -m "apply patches" # Integrate patches in the packaging branch
git branch -D patch-queue/openkylin/yangtze # Delete the temporary branch
Then delete the debian/patches directory and change debian/source/format to "3.0 (native)"
rm -rf debian/pathes
echo "3.0 (native)" > debian/source/format
Committing changes
```



# 3. development maintainers modify packages (maintain under native package branches like openkylin/yangtze)

## 3.1. production line code changes

Development maintainers clone the official version of the git repository to their personal space, then modify the source files and debian/changelog directly on the package branch.

```Bash
git checkout openkylin/yangtze
# Modify the source files
vim ...
# Modify the changelog, modify the version number according to the package version specification, for version numbers with the '-' symbol, you cannot modify the part before the '-'
dch -R
git add .
git commit
```

Then push it to your personal gitee or gitlab repository and submit the PR to maintainer for review.

Local debugging can be done directly with debuild for compilation testing.

## 3.2. Committing patch upstream after package branch changes

To commit changes upstream, you need to export the patch first

First run git log and find the commit numbers before and after this change

``Bash
git log

commit 5ba63fabc689b225cd7823a81655e531f54eed46 (HEAD -> openkylin/yangtze)
Author: Xie Wei <xiewei@kylinos.cn>
Date: Mon Apr 18 16:41:59 2022 +0800

    edit version and copying

commit 5c066ae094a7788c6507413891dab66db36cd07b
Author: Xie Wei <xiewei@kylinos.cn>
Date: Mon Apr 18 16:40:56 2022 +0800

    edit COPYING

commit e5a25c0fba1b1b68e023062878962abbebf1f8ea
Author: Xie Wei <xiewei@kylinos.cn>
Date: Mon Apr 18 16:40:31 2022 +0800

    edit version

commit 8c8ad046e88f2692bef8b193b517666d82ca6957
Author: Xie Wei <xiewei@kylinos.cn>
Date: Mon Apr 18 16:35:37 2022 +0800

    native
```

As shown above, assuming that you want to commit the versioning and copying changes as a complete patch, you should compare the changes between the penultimate commit and the penultimate commit.

```Apache
# Ignore the debian directory when generating patches, older commits go first, newer commits go second
git diff --binary -r 8c8ad046e88f2692bef8b193b517666d82ca6957 -r 5ba63fabc689b225cd7823a81655e531f54eed46 ':!debian' > /tmp/live-build- change-version-copying.patch
```

After generating patch, it depends on what kind of submission method the upstream community accepts, directly sending patch is not introduced in this article, assuming it supports PR submission in gitee or github, then we first fork our own repository in the corresponding hosting platform, then clone to local, then import patch

```Apache
# clone your own forked repository in the hosting platform
git clone git@gitee.com:xiewei/live-build.git live-build-upstream
cd live-build-upstream
# Switch to the branch you need to commit to, depending on the version the patch is targeting and the upstream community's branching rules
git checkout master
git apply /tmp/live-build-change-version-copying.patch
git add .
# Commit the changes
git commit
# push to your own fork repository
git push
```

Then it's a matter of operating on the hosting platform and committing a PR from your  repository to the upstream community.

If there are updates upstream, you have to sync the upstream changes before committing the PR, resolve the conflicts, and then commit the PR

``Bash
# First add the address of the upstream repository to remote, clone down and only need to do it once because we only need to read and add the address of the https protocol on the line.
git remote add upstream https://gitee.com/upstream/live-build.git
# Sync upstream updates, the last argument is the name of the branch to sync
git pull upstream master
# If there are conflicts, follow the prompts to resolve them, if there are no conflicts, you can push to your fork repository
git push
git push



# 4. upload the build platform (CI platform is implemented automatically)

When uploading the build platform, you need to try to upload in quilt format to fully reuse the orig.tar file and save bandwidth and server space.

The action of converting native format source packages into quilt format will be done automatically through CI platform.


## 4.1 Packaging in quilt format

In the packaging environment clone git repository, create a temporary quilt packaging branch for packaging

native into quilt, the main difference is to form a patch relative to the upstream code changes, rather than directly on the source file, we use gbp pq to generate the patch.

```Bash
# switch to the quilt packaging branch
git checkout packaging/openkylin/yangtze
# Create a temporary patch-queue to automatically switch to patch-queue/packaging/openkylin/yangtze
gbp pq import
# Commit the latest changes to the native packaging branch in the patch-queue
git diff --binary -r openkylin/yangtze -- ':!debian' | git apply -R
git add . -f
# Commit the latest commits from the native package branch to the patch-queue branch for exporting patches
git log -r openkylin/yangtze -1 --pretty=format:"%s" | git commit -F -
# Export patch, automatically switch to packaging/openkylin/yangtze
gbp pq export
# Synchronize changes to the debian directory from the native packaging branch, ignoring patches and source/format
git diff --binary -r openkylin/yangtze -- debian ':!debian/patches' ':!debian/source/format' | git apply -R

git add debian
# Commit the latest commits from the native package branch to the quilt package branch, no manual input required
git log -r openkylin/yangtze -1 --pretty=format:"%s" | git commit -F -
# Pack the source code, because gbp is packaged in the master branch by default, so you need to add the --git-ignore-branch argument, and the --git-tag argument to automatically create a tag after a successful package
gbp buildpackage --git-ignore-branch --git-tag --git-pristine-tar --git-pristine-tar-commit -S -nc
# packaging/openkylin/yangtze branch needs to be automatically saved to gitee or gitlab
git push
git push --tags
```

If the native branch involves binary changes, the generated patch will also have binary information, and the buildpackage will prompt ``detected 1 unwanted binary file (add it in debian/source/include-binaries to allow its inclusion)`

Debian recommends putting the binaries you want to replace in the debian directory, then modifying the install step in debian/*.install or debian/rules to overwrite the upstream binaries at build time. Debian includes binaries, whether they are images or patches, that need to be added to debian/source/include-binaries, or just add a line to debian/source/options to include-binaries, allowing any binary to be included.


https://www.debian.org/doc/manuals/debmake-doc/ch08.en.html

> If you wish to replace upstream provided PNG file **data/hello.png** with maintainer provided one **debian/hello.png**, editing **debian/install* * isn't enough. * When you add **debian/hello.png**, you need to add a line "`include-binaries`" to **debian/source/options** since PNG is See `dpkg-source`(1).

Method to get upstream tags based on version number: (compare on top of existing patches, no more direct comparison of upstream)

```Python
from gbp.deb.git import DebianGitRepository
version = '3.0~a57-1kylin38.20.04.4.4'
upstream_version = version.split('-')[0]
# '3.0~a57'
upstream_tag = DebianGitRepository.version_to_tag('upstream/%(version)s', upstream_version)
# 'upstream/3.0_a57'
```

## 4.2 Packaging in native format

The native branch allows you to package the source code directly in the current branch and upload it to the build platform.

```Bash
## Switch to the native packaging branch
git checkout openkylin/yangtze
# Pack the source code, since gbp is packaged in the master branch by default, so add the --git-ignore-branch argument, and the --git-tag argument to automatically create a tag after successful packaging
gbp buildpackage --git-ignore-branch --git-tag -S
# Synchronize the tags created during packaging to gitee or gitlab
git push --tags
```



# 5. update upstream version (needs to be handled manually by maintainer)

When a package needs to update its upstream version number, it has to import the upstream source code and add the appropriate tags first. This step needs to be operated by maintainer in the official version git repository.

The developer can then rebase the code from the package branch to the new version.

## 5.1 Importing source code for a new upstream version (modified package)

When the upstream version of a package is updated, a new import of the orig package is required

```Bash
gbp import-orig . /live-build_4.0.orig.tar.gz --no-merge --pristine-tar --upstream-branch=upstream
git checkout upstream
git push
git push --tags
```

Parameter Meaning

| --pristine-tar | Record data from the original orig tarball to avoid changes to the tarball after repackaging with gbp |
| ------------------ | ------------------------------------------------------------ |
| --no-merge | Do not merge the upstream version of the code into the package branch, because our package branch is in native format and the source code is modified based on upstream, so there is no way to merge it automatically.
| --upstream-branch= | Specify the name of the upstream branch, and modify the package to use upstream |

In the above example, a new tag will be created: upstream/4.0

## 5.2 Creating a tag for a new version (self-published package)

The maintainer creates the tag upstream/<new version number> based on the upstream branch (or some other upstream stable version branch) as the upstream for new version packaging.

This can be done on gitlab and gitee.

## 5.3 Packaging a branch rebase to a new version

### 5.3.1 Determining if there is a patch on an old version

``Bash
git checkout packaging/openkylin/yangtze
wc debian/patches/series
# Check out the number of patches
```

If there is no patch, press 5.3.2, 5.3.4.

If there is patch, operate as 5.3.3, 5.3.4.

### 5.3.2 rebase without patch

Update the packaging branch

``Bash
git checkout packaging/openkylin/yangtze
### Suppose the original version number is 2:4.11.6-ok1 and the new upstream version number is 2:4.13.17
git merge upstream/4.13.17
```

Update the native branch

```Bash
git checkout openkylin/yangtze
# Assume the original version number is 2:4.11.6-ok1 and the new upstream version number is 2:4.13.17
git merge upstream/4.13.17
```

Change the package version number in the native branch


```Bash
# At this point, you can debug the code based on the upstream upstream in the native branch, such as:
# 1. Modify the code until the binary package can be compiled and installed normally
vim xxx
# 2. Perform debuild under the native branch to compile and test
build
# 3. Commit changes
git add xxx
# 4. Modify debian/changelog, the version number is based on the new upstream version.
dch -R
git add debian/changelog
git commit -m 'merge upstream 4.13.17'
```

### 5.3.3 Rebase with patch

#### 5.3.3.1 Get the commit id corresponding to the old version patch

```Bash
# Create a temporary branch based on the old version
git checkout packaging/openkylin/yangtze
git checkout -b tmp
# Import patch to form git commit record
# Before importing, you can also filter the patch list, modify debian/patches/series, delete unnecessary patches, and then submit
gbp pq import
# Calculate the number of patches
wc debian/patches/series # 14 14 511 debian/patches/series
# Get commit id, redirect to file save
git log --max-count=14 > /tmp/patch-list.txt
```

#### 5.3.3.2 Update the packaging branch

```Bash
git checkout packaging/openkylin/yangtze
# Assume the original version number is 2:4.11.6-ok1, and the new upstream version number is 2:4.13.17
git merge upstream/4.13.17
```

#### 5.3.3.3 Filter the patches that need to be transplanted, respectively cherry-pick

```Bash
# Switch to the packaging branch
git checkout packaging/openkylin/yangtze
# Clear the patch list
git rm debian/patches/series
git commit -m 'prepare for new patch list'
# Create a temporary branch and transplant the lower version patch
gbp pq import
# Read the patch-queue commit list file /tmp/patch-list.txt from the back to the front, determine whether to integrate the patch according to the submission information and the patch file name, and perform cherry-pick on the patch that needs to be integrated
git cherry-pick b2cf5dd7923c50c4b4e1809564a7da63a3e38312 f32df40245f054363698225f44158c2673a06978 2853828b649d5c826bda93c9a11a9720954b915c
# Export patch list
gbp pq export
git add debian/patches
# Then remember to modify debian/changelog, the version number is based on the new upstream version.
dch -R
git add debian/changelog
git commit -m 'Apply patches on new upstream 4.13.17'
git branch --delete --force tmp # delete tmp branch
```

If there is a conflict, merge the conflict according to actual needs (git mergetool; git cherry-pick --continue) or skip this patch (git cherry-pick --skip).

> git cherry-pick basic usage

> Pick a commit-id to merge

> ```
> git cherry-pick commit-id
> ```

-> Note: The merged commit-id will be changed, and a new commit-id will be generated, which is not the same as the original one.

> Select multiple commit-ids to merge

> ```
> git cherry-pick commit-idA commit-idB
> ```

> Select consecutive multiple commit-ids to merge

> ```
> git cherry-pick commit-idA..commit-idB
> ```

-> This instruction is to merge all the commit-id submission records from commit-idA to commit-idB. It should be noted that commit-idA must be submitted earlier than commit-idB, that is to say, after being selected On the branch, there is commit-idA first, and then there is commit-idB

#### 5.3.3.4 Reverse sync to native packaging branch

```Bash
git checkout openkylin/yangtze
git diff --binary -r patch-queue/packaging/openkylin/yangtze -- ':!debian/source/format' | git apply -R
git add .
git commit -m 'merge upstream 4.13.17'
```

### 5.3.4 Submit to the git server

Submit the native packaging branch and packaging branch, do not submit other temporary branches

```
!Push, first push the packaging branch, and then push the native branch.
```

```Bash
git checkout packaging/openkylin/yangtze
git push
git checkout openkylin/yangtze
git push
```



# 6. Change the self-developed package to quilt format

(1) Unzip the source package.

```Apache
dpkg-source -x kylin-camera_1.2kord01rc2.22.dsc
cd kylin-camera-1.2kord01rc2.22
```

(2) Modify the changelog and change the version number to {upstream_version}-{revision}.

```
dch -R
# In the editor, change the version number from 1.2kord01rc2.22 to 1.2kord01rc2.22-0k1
```

(3) Modify the debian/source/format file to 3.0 (quilt)

```Bash
mkdir -p debian/source
echo "3.0 (quilt)" > debian/source/format
```

(4) Package orig.tar

```Bash
debmake -t
```

(5) Generate a new dsc

```Bash
dpkg-buildpackage -S -nc
```

(6) import to git

```Apache
cd ../
gbp import-dsc --pristine-tar --debian-branch=openkylin/yangtze --upstream-branch=upstream kylin-camera_1.2kord01rc2.22-0k1.dsc kylin-camera
```



# 7. Modify the upstream version number without changing the code of the self-developed package

For example, when ukui is on a certain node, to uniformly upgrade the upstream version number of the currently imported packages, you need to operate as follows,

(1) Switch to a packaging branch (such as openkylin/yangtze).

```Shell
git checkout openkylin/yangtze
```

(2) Read debian/changelog to confirm the current upstream version.

​ For example, the version number in the changelog is 3.10.0-0k0

(3) Modify the changelog version number, and change the upstream version to a new version number. Suppose it is to be set to 3.13.0, then the package version number in the changelog can be changed to 3.13.0-0k0. Then commit the changes.

(4) Add an upstream tag corresponding to the new version number.

```Shell
git tag upstream/3.13.0 upstream/3.10.0
```

(5) Changes to the openkylin/yangtze branch and the new label upstream/3.13.0 are pushed to the git server.

```Shell
git push
git push --tags
```

(6) Generate a new version orig.tar package (completed by CI platform).

```Shell
# Synchronize the changelog changes to the quilt packaging branch
git diff --binary -r openkylin/yangtze debian ':!debian/patches' ':!debian/source/format' | git apply -R
git add debian
git log -r openkylin/yangtze -1 --pretty=format:"%s" | git commit -F -
# Pack and automatically save orig.tar
gbp buildpackage --git-ignore-branch --git-tag --git-pristine-tar --git-pristine-tar-commit -S -nc
# After packaging the new upstream version, the pristine-tar branch will be updated and needs to be uploaded to the git server
git push
git push origin pristine-tar
```



# 8. Overwrite the openkylin community warehouse with the modified source code of Kylin

The premise is that the upstream version has not changed, and it is in quilt format, otherwise you need to clear the warehouse and import it or go through the "update upstream version" process.

(1) Cover the debian directory in the quilt packaging branch

```Shell
# Switch to the quilt packaging branch

# Delete the existing debian directory, ready to be overwritten with the Kylin version
# Unzip the *.debian.tar file of the Kylin version
```

(2) Modify the version number and series code in the changelog

Add a modification record, the version number is <upstream version>-ok<N>, N is a number, which is larger than the version number in openkylin before overwriting.

(3) Override the native branch



# 9. Build the packaging branch from scratch

This section describes how to create a packaging branch when there are only branches such as upstream that do not contain debian configuration files

That is, create openkylin/yangtze and packaging/openkylin/yangtze branches from upstream.

## 9.1 First clone a clean code from kylinos-src on gitlab

```Bash
git clone git@gitlab2.kylin.com:kylinos-src/<package name>
cd <package name>
```

## 9.2 Add the upstream label to the upstream branch

We need to select a commit corresponding to the upstream version and label it upstream.

It can be achieved in the following ways, assuming that the upstream version number is set to 1.2.3

(1) Package with the latest version of the upstream branch

```Bash
git tag upstream/v1.2.3 upstream
```

(2) Package with an existing tag, assuming there is already a tag named v1.2.3

```Bash
git tag upstream/1.2.3 v1.2.3
```

(3) Package based on a certain commit of the upstream branch

```Bash
git log upstream # Find the commit id you want to use
git tag upstream/1.2.3 031eae9e49ef7b523286410d45ee44ba57e29f79
```

## 9.3 Create a packaging branch based on the upstream label, and add the debian packaging configuration file

```Bash
git checkout -b packaging/openkylin/yangtze upstream/1.2.3
# Copy the prepared debian packaging configuration file
cp -r /path-to/debian ./
# Modify the changelog to ensure that the version number starts with "1.2.3-" and conforms to the version number specification
dch -R
# Make sure the source package is in quilt format
mkdir -p debian/source
echo "3.0 (quilt)" > debian/source/format
# Test packaging and save pristine-tar data
gbp buildpackage --git-ignore-branch --git-tag --git-pristine-tar --git-pristine-tar-commit -S -nc
```

## 9.4 Create the corresponding development branch, which is convenient for developers to test locally after modifying the code

```Bash
# Create a temporary integration patch branch, which will be automatically named patch-queue/packaging/openkylin/yangtze1
gbp pq import
# Rename to standard development branch name
git checkout -b openkylin/yangtze
# delete temporary branch
git branch -D patch-queue/packaging/openkylin/yangtze
# Then delete the debian/patches directory and change debian/source/format to "3.0 (native)"
rm -rf debian/pathes
echo "3.0 (native)" > debian/source/format
git add debian
git commit -m 'change format to native'
```

## 9.5 Push to gitlab server

```Bash
# Because the new packaging branch involves the modification of 3 branches, use push --all to simplify the operation when pushing, which is why it is necessary to clone a clean code to operate
git push origin --tags
git push origin --all
```

## 9.6 Create a branch of a new production line based on an existing packaging branch

Assuming that the package branch of openkylin/yangtze already exists, it is necessary to create a new bug maintenance branch for the yangtze-2209 production line (the branch name is agreed to be openkylin/yangtze-2209), and the package maintainer will operate as follows on gitee:

1. Access the branch list of the warehouse

![img](https://gitee.com/openkylin/docs/raw/master/%E5%BC%80%E5%A7%8B%E8%B4%A1%E7%8C%AE/assets/openKylin% E6%BA%90%E7%A0%81%E5%8C%85git%E5%B7%A5%E4%BD%9C%E6%B5%81/%E5%88%9B%E5%BB%BA%E6 %96%B0%E4%BA%A7%E7%BA%BF%E7%9A%84%E5%88%86%E6%94%AF%E7%A4%BA%E4%BE%8B1.png)

2. Click "New Branch"

![img](https://gitee.com/openkylin/docs/raw/master/%E5%BC%80%E5%A7%8B%E8%B4%A1%E7%8C%AE/assets/openKylin% E6%BA%90%E7%A0%81%E5%8C%85git%E5%B7%A5%E4%BD%9C%E6%B5%81/%E5%88%9B%E5%BB%BA%E6 %96%B0%E4%BA%A7%E7%BA%BF%E7%9A%84%E5%88%86%E6%94%AF%E7%A4%BA%E4%BE%8B2.png)

3. Enter the branch name and create two new branches respectively:

yopenkylin/yangtze-2209 based on openkylin/yangtze

​ packaging/openkylin/yangtze-2209 based on packaging/openkylin/yangtze



# 10. There are branches in native format, converted to quilt format

## 10.1 Mainline branch conversion

(1) First create an upstream branch and tag based on the current version

```Bash
# Suppose the original branch name is master, the version number in the changelog is 1.2.3, and the packaging branch to be created is openkylin/yangtze
# create upstream branch
git checkout -b upstream master
# The upstream branch should not have a debian directory
git rm -rf debian
git commit -m 'import upstream 1.2.3'
# Create an upstream tag, the version number cannot contain the - symbol
git tag upstream/1.2.3
```

(2) Create a packaging branch

```Bash
# Create quilt packaging branch
git checkout -b packaging/openkylin/yangtze master
# Change the version number and source package format to quilt
dch -v 1.2.3-0k1 "quilt format" -D v101 --force-distribution
echo '3.0 (quilt)' > debian/source/format
git add debian
git commit -m 'to quilt format'
# Create a new native packaging branch based on the quilt branch
git checkout openkylin/yangtze
echo "3.0 (native)" > debian/source/format
git commit -m 'to native format'
# Test packaging and save pristine-tar information
git checkout packaging/openkylin/yangtze
gbp buildpackage --git-pristine-tar --git-pristine-tar-commit --git-ignore-branch -S -nc
```

## 10.2 Custom branch conversion

The custom branch should try to keep the upstream version consistent with the main line, but for packages that have been customized in native format, the version number may already be in the form of 1.2.3hw1. If it is changed to 1.2.3-0k1hw1, the version number will be higher than the original version number. Small, you can only continue to use the customized upstream version number.

(1) Create custom upstream branches and tags

```Bash
git checkout -b upstream-hw hw
git rm -rf debian
git commit -m 'import hw upstream'
git tag upstream/1.2.3hw1
```

(2) Create a packaging branch, which is the same as the mainline branch, but the branch name is different.



# Attachment: Individual problem handling

**1.**

gbp:error: Error creating libwacom_1.3.orig.tar.xz: Pristine-tar couldn't checkout "libwacom_1.3.orig.tar.xz": fatal: Path 'libwacom_1.3.orig.tar.xz.delta ' does not exist in 'refs/remotes/origin/pristine-tar'

The reason is that the initially imported compressed package is in gz format, but debian/gbp.conf specifies the compressed format as xz.

The solution is to modify debian/gbp.conf in the openkylin/yangtze branch, delete compression=xz, and submit.

**2.**

gbp:error: Error creating kbd_2.0.4.orig.tar.xz: Pristine-tar couldn't checkout "kbd_2.0.4.orig.tar.xz": xdelta3: target window checksum mismatch: XD3_INVALID_INPUT

The compression format is the same, but the checksum is different. It may be caused by file corruption during import or a different version of the environment and ci environment. You need to re-import

**3.**

dpkg-source: error: detected 1 unwanted binary file (add it in debian/source/include-binaries to allow its inclusion).

If the native branch involves binary file modification, the generated patch will also have binary information, and the buildpackage will prompt `detected 1 unwanted binary file (add it in debian/source/include-binaries to allow its inclusion)`

Debian recommends to put the binary files to be replaced in the debian directory, and then modify the install steps of debian/*.install or debian/rules to overwrite the upstream binary files during compilation. Debian contains binary files, whether it is a picture or a patch, you need to add the file path to debian/source/include-binaries, or directly add a line of include-binaries to debian/source/options to allow any binary file to be included.

https://www.debian.org/doc/manuals/debmake-doc/ch08.en.html

> If you wish to replace upstream provided PNG file **data/hello.png** with maintainer provided one **debian/hello.png**, editing **debian/install** isn't enough. When you add * *debian/hello.png**, you need to add a line "`include-binaries`" to **debian/source/options** since PNG is a binary file. See `dpkg-source`(1).






