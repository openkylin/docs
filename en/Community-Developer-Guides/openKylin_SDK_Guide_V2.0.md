![Enter a picture description](assets/system_input_method_adaptation_guide/image3.png)

# openSDK V2.0 ------ Development Guide

![Enter a picture description](assets/system_input_method_adaptation_guide/image1.png)

![Enter a picture description](assets/system_input_method_adaptation_guide/image2.png)

openSDK SIG


Table of contents


```
1 Overview
2 Deployment methods
3 System Capability SDK
   3.1 System Information
     3.1.1 System time reporting
     3.1.2 Get hardware information
     3.1.3 Get package list information
     3.1.4 Obtain resource information
     3.1.5 Obtain basic operating system information
     3.1.6 Get network information
     3.1.7 Get system runtime information
     3.1.8 Get current geographic information
   3.2 Power Management
     3.2.1 Lock Screen Settings
   3.3 File Management
   3.4 AI capabilities
     3.4.1 OCR character recognition function
   3.5 Printer Management
4 Application Support SDK
   4.1 QT self-developed control module
     4.1.1 Form module
     4.1.2 Dialog Module
     4.1.3 Input box module
     4.1.4 Button module
     4.1.5 Bar module
     4.1.6 Slider Module
     4.1.7 Message Prompt Module
     4.1.8 Breadcrumbs KBreadCrumb
     4.1.9 KCommentPanel
     4.1.10 KListView
     4.1.11 KTags
     4.1.12 KTranslucentFloor
   4.2 Wayland-helper
     4.2.1 WindowManager
     4.2.2 WindowInfo
     4.2.3 UkuiStyleHelper
   4.3 Apply general function modules
     4.3.1 Log module
     4.3.2 System related modules
     4.3.3 d-bus module------will be obsolete soon
     4.3.4 System information module
5 Basic Development SDK
   5.1 log
   5.2 Timers
   5.3 C language string extension
   5.4 Linked list module
   5.5 Configuration file operation
   5.6 Unit conversion
```



# 1 Overview


  The openKylin self-developed developer kit (hereinafter referred to as openSDK) is based on the openKylin community operating system, providing a safe, reliable, fast, and stable developer interface for ecological construction and software development. Compared with other developer kits or frameworks in the community, openSDK focuses more on solving the problems of compatibility, adaptation, transplantation, optimization and other aspects of the open Kylin desktop operating system. openSDK currently focuses on three major modules, including application support SDK, system capability SDK, and basic development SDK, while fully considering the compatibility of openSDK.

  This document aims to provide developers with an efficient way to refer to the declaration and usage of openSDK interfaces when developing applications on the openKylin system. Reduce the learning cost for developers when using openSDK.

  The overall modules of the SDK are introduced as follows:

| module | describe |
|---|---|
| Application support SDK | Focusing on the application display layer, it provides developers with Kirin self-developed controls, provides graphical development functions for graphical applications, and can use a unified UI framework to manage application windows and interact with the system, reducing application development and application migration. study cost; |
| System Capability SDK | Focus on providing developers with more system capabilities, developers can quickly obtain basic system, hardware information, current runtime information, etc., improve development efficiency, and help development focus more on actual business content;|
| Basic Development SDK | Focus on the application development process, providing developers with log management, package string processing and other capabilities to improve development efficiency; |


# 2 Deployment method


  When downloading and using the openKylin community self-developed developer kit, you need to add the openSDK source address in advance. The installation steps are as follows:

  (1) Software source:


```
deb http://archive.build.openKylin.top/openkylin/ yangtze main cross pty

deb http://archive.build.openKylin.top/openkylin/ yangtze-security main cross pty

deb http://archive.build.openKylin.top/openkylin/ yangtze-updates main cross pty
```
                                                     

(2) Update source after configuration:


```
$ sudo apt update
```
# 3 System Capability SDK

  Each software package in the system capabilities SDK expresses one or a type of system capabilities, such as operating system information, hardware management, system built-in security policies, network management, etc.;

  Install command:

```
$ sudo apt-get install libkysdk-system libkysdk-system-dev
```

## 3.1 System information

  The design of this layer mainly provides applications with functional interfaces related to the operating system, and re-realizes OS-related functions (such as file systems, hardware information, communication, etc.) from the perspective of functions in three ways: self-development, combination, and packaging; Development complexity and debugging difficulty brought about by system differences and platform differences.

### 3.1.1 System time reporting

  The com.kylin.kysdk.TimeServer Dbus service will send out a time signal when the system time is at an integral minute or when the system time is modified.


```
$ sudo apt-get install libdbus-1-dev libdbus-glib-1-dev libkysdk-systime libkysdk-systime-dev
```

  (1) dbus service name: com.kylin.kysdk.TimeServer

  (2) Path name: /com/kylin/kysdk/Timer

  (3) Interfaces: com.kylin.kysdk.TimeInterface

  (4) Signal:

  System time modification signal: TimeChangeSignal

  Timed timing signal: TimeSignal

  System time modification time reporting

<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="72">submodule</td><td colSpan="1" rowSpan= "1" width="85">Programming language</td><td colSpan="2" rowSpan="1" width="266">Statement</td><td colSpan="1" rowSpan="1" width="152">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="72">System time reporting</td><td colSpan="1" rowSpan="3" width="85">C</td><td colSpan="2" rowSpan="1" width="266">Void TimeChangeSignal(const char* t)</td><td colSpan= "1" rowSpan="1" width="152">When the system time is modified, the com.kylin.kysdk.TimeServerDbus service will send out a time signal</td></tr><tr><td colSpan="1 " rowSpan="1" width="132">parameter description</td><td colSpan="1" rowSpan="1" width="133">return value description</td><td colSpan="1" rowSpan="1" width="152"></td></tr><tr><td colSpan="1" rowSpan="1" width="132">t: current time such as: "2021/09 /26 21:13:28"</td><td colSpan="1" rowSpan="1" width="133">none</td><td colSpan="1" rowSpan="1" width=" 152"></td></tr></tbody></table>




  system time full minutes


<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="71">Submodule</td><td colSpan="1" rowSpan= "1" width="86">Programming language</td><td colSpan="2" rowSpan="1" width="263">Statement</td><td colSpan="1" rowSpan="1" width="154">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="71">System time reporting</td><td colSpan="1" rowSpan="3" width="86">C</td><td colSpan="2" rowSpan="1" width="263">void TimeSignal(const char* t)</td><td colSpan= "1" rowSpan="1" width="154">When the system time is in full minutes, the com.kylin.kysdk.TimeServerDbus service will send out a time signal</td></tr><tr><td colSpan=" 1" rowSpan="1" width="132">parameter description</td><td colSpan="1" rowSpan="1" width="131">return value description</td><td colSpan="1 " rowSpan="1" width="154"></td></tr><tr><td colSpan="1" rowSpan="1" width="132">t: current time such as: "2021/ 09/26 21:07:00"</td><td colSpan="1" rowSpan="1" width="131">none</td><td colSpan="1" rowSpan="1" width= "154"></td></tr></tbody></table>


### **3.1.2** Get hardware information

```

$ sudo apt-get install libkysdk-hardware libkysdk-hardware-dev
```

#### **3.1.2.1** Get CPU information

  Encapsulate C interface to obtain CPU information

  (1) CMakeLists.txt build project
  ```

  aux_source_directory(. SOURCESCODE)

  include_directories(.)

  add_library(kyhw SHARED \${SOURCESCODE})

  add_executable(kycpu-test test/kycpu-test.c)

  target_link_libraries(kyhw kylog kyconf pthread systemd udev)

  target_link_libraries(kycpu-test kyhw)
  ```

  (2) To obtain hardware CPU information, a header file needs to be added:
```
#include "kysdk/kysdk-system/libkycpu.h"
```

  Get CPU architecture


<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="282.36">Statement</td><td colSpan="1" rowSpan="1" width="104">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get CPU information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="306">extern const char* kdk_cpu_get_arch()</td><td colSpan=" 1" rowSpan="1" width="104">Get CPU Architecture</td></tr><tr><td colSpan="1" rowSpan="1" width="127">Parameter Description</td ><td colSpan="1" rowSpan="1" width="178">Return value description</td><td colSpan="1" rowSpan="1" width="104"></td></ tr><tr><td colSpan="1" rowSpan="1" width="127">None</td><td colSpan="1" rowSpan="1" width="178">const char* schema Information, such as "x86_64"; the return is const char*, not free</td><td colSpan="1" rowSpan="1" width="104"></td></tr></tbody> </table>



  Get CPU Manufacturer

| submodule     | Programming language | statement                                      | describe                                                         |
|---------|------|-----------------------------------------|------------------------------------------------------------|
| Get CPU information | C    | extern const char* kdk_cpu_get_vendor() | Get the CPU manufacturer                                                   |
|         |      | Parameter Description                                    | Return value description                                                      |   |
|         |      | none                                       | const char* manufacturer string, such as "GenuineIntel"; return const char*, do not free |



 Get CPU model


| submodule     | Programming language | statement                                     | describe                                                                                     |
|---------|------|----------------------------------------|----------------------------------------------------------------------------------------|
| Get CPU information | C    | extern const char* kdk_cpu_get_model() | Get the CPU model                                                                               |
|         |      | Parameter Description                                   | Return value description                                                                                  |   |
|         |      | none                                      | const char* CPU model name, such as "Intel(R) Core(TM) i7-4790K CPU @ 4.00GHz"; return const char*, do not free |





 Get the rated CPU frequency


| submodule     | Programming language | statement                                        | describe                                                            |
|---------|------|-------------------------------------------|---------------------------------------------------------------|
| Get CPU information | C    | extern const char* kdk_cpu_get_freq_MHz() | Obtain CPU rated main frequency                                                     |
|         |      | Parameter Description                                      | Return value description                                                         |   |
|         |      | none                                         | const char* rated main frequency hertz number, such as "1794.742", the unit is MHz; return const char*, do not free |




 Get the number of CPU cores


| submodule     | Programming language | statement                                         | describe                        |
|---------|------|--------------------------------------------|---------------------------|
| Get CPU information | C    | extern unsigned int kdk_cpu_get_corenums() | Get the number of CPU cores                 |
|         |      | Parameter Description                                       | Return value description                     |   |
|         |      | none                                          | unsigned int number of all available CPU cores |



 Get CPU support for virtualization


| submodule     | Programming language | statement                             | describe                                                                       |
|---------|------|--------------------------------|--------------------------------------------------------------------------|
| Get CPU information | C    | const char* kdk_cpu_get_virt() | Get CPU support for virtualization                                                             |
|         |      | Parameter Description                           | Return value description                                                                    |   |
|         |      | none                              | const char* If the CPU supports virtualization, return virtualization technology, such as "vmx"; if not, return NULL; return const char*, do not free |



#### **3.1.2.2** Get network card information

  Encapsulate the C interface to obtain the hardware information of the network card.

  (1) CMakeLists.txt build project
  ```
 aux_source_directory(. SOURCESCODE)

 include_directories(.)

 add_library(kyhw SHARED \${SOURCESCODE})

 add_executable(kync-test test/kync-test.c)

 target_link_libraries(kyhw kylog kyconf pthread systemd udev)

 target_link_libraries(kync-test kyhw)
 ```

 （2) To obtain network card information, you need to add a header file:

```
  #include "kysdk/kysdk-system/libkync.h"
```
  Get all network cards in the system


| submodule    | Programming language | statement                                  | describe                                                                    |
|--------|------|-------------------------------------|-----------------------------------------------------------------------|
| Get network card information | C    | extern char** kdk_nc_get_cardlist() | Get all network cards in the system                                                            |
|        |      | Parameter Description                                | Return value description                                                                 |   |
|        |      | none                                   | char** A list of network card names, ending with a NULL string; generated by alloc and needs to be recycled by kdk_nc_freeall; if an error occurs, return NULL; |


 Check whether the specified network card is in UP state


| submodule    | Programming language | statement                                      | describe                |
|--------|------|-----------------------------------------|-------------------|
| Get network card information | C    | extern int kdk_nc_is_up(const char *nc) | Detect whether the specified network card is in the UP state    |
|        |      | Parameter Description                                    | Return value description             |   |
|        |      | nc：Network card name, such as eno1                           | int Up returns 1, Down returns 0 |



 Get the list of network cards currently in link up state in the system

| submodule    | Programming language | statement                                 | describe                                                                    |
|--------|------|------------------------------------|-----------------------------------------------------------------------|
| Get network card information | C    | extern char** kdk_nc_get_upcards() | Get the list of network cards currently in link up state in the system                                             |
|        |      | Parameter Description                               | Return value description                                                                 |   |
|        |      | none                                  | char** A list of network card names, ending with a NULL string; generated by alloc and needs to be recycled by kdk_nc_freeall; if an error occurs, return NULL; |



 Get the physical MAC address of the specified network card

| submodule    | Programming language | statement                                             | describe                                              |
|--------|------|------------------------------------------------|-------------------------------------------------|
| Get network card information | C    | extern char* kdk_nc_get_phymac(const char *nc) | Obtain the physical MAC address of the specified network card                                  |
|        |      | Parameter Description                                           | Return value description                                           |   |
|        |      | nc：Network card name, such as eno1                                  | char* Physical MAC address, generated by alloc, should be free; if the specified network card does not exist, return NULL |



 Get the first IPv4 address of the specified network card


| submodule    | Programming language | statement                                                   | describe                          |
|--------|------|------------------------------------------------------|-----------------------------|
| Get network card information | C    | extern char* kdk_nc_get_private_ipv4(const char *nc) | Get the first IPv4 address of the specified network card            |
|        |      | Parameter Description                                                 | Return value description                       |   |
|        |      | nc：Network card name, such as eno1                                        | char* IPv4 address, get an error or return NULL if there is no IP |



 Get all IPv4 addresses of the specified network card


| submodule    | Programming language | statement                                            | describe                                                                   |
|--------|------|-----------------------------------------------|----------------------------------------------------------------------|
| Get network card information | C    | extern char** kdk_nc_get_ipv4(const char* nc) | Get all IPv4 addresses of the specified network card                                                      |
|        |      | Parameter Description                                          | Return value description                                                                |   |
|        |      | nc：Network card name, such as eno1                                 | char** IPv4 address list, ending with NULL, generated by alloc, needs to be reclaimed by kdk_nc_freeall; if there is an error in obtaining, return NULL; |



 Get the first IPv6 address of the specified network card


| submodule    | Programming language | statement                                                   | describe                          |
|--------|------|------------------------------------------------------|-----------------------------|
| Get network card information | C    | extern char* kdk_nc_get_private_ipv6(const char *nc) | Get the first IPv6 address of the specified network card            |
|        |      | Parameter Description                                                 | Return value description                       |   |
|        |      | nc：Network card name, such as eno1                                        | char* IPv6 address, get an error or return NULL if there is no IP |


 Get all IPv6 addresses of the specified network card

| submodule    | Programming language | statement                                            | describe                                                                   |
|--------|------|-----------------------------------------------|----------------------------------------------------------------------|
| Get network card information | C    | extern char** kdk_nc_get_ipv6(const char *nc) | Get all IPv6 addresses of the specified network card                                                      |
|        |      | Parameter Description                                          | Return value description                                                                |   |
|        |      | nc：Network card name, such as eno1                                 | char** IPv6 address list, ending with NULL, generated by alloc, needs to be reclaimed by kdk_nc_freeall; if there is an error in obtaining, return NULL; |



 Get the wired/wireless type of the specified network card


| submodule    | Programming language | statement                                             | describe             |
|--------|------|------------------------------------------------|----------------|
| Get network card information | C    | extern int kdk_nc_is_wireless(const char *nc); | Get the wired/wireless type of the specified network card |
|        |      | Parameter Description                                           | Return value description          |   |
|        |      | nc：Network card name, such as eno1                                  | 0 : Wired<br/>1 : Wireless        |




 Get the manufacturer name and device model of the specified network card

<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan="1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="312">statement</td><td colSpan="1" rowSpan="1" width="98">describe</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get network card information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="312">extern int kdk_nc_get_vendor_and_product(const char *nc, char *vendor, char *product);</td><td colSpan="1" rowSpan="1" width="98">Get the manufacturer name and device model of the specified network card</td></tr><tr><td colSpan="1" rowSpan="1" width="127">Parameter Description</td><td colSpan="1" rowSpan="1" width="184">Return value description</td><td colSpan="1" rowSpan="1" width="98"></td></tr><tr><td colSpan="1" rowSpan="1" width="127">nc：Network card name, such as eno1vendor：Buffer accepting vendor name product: Buffer accepting device model</td><td colSpan="1" rowSpan="1" width="184">0 : success 1 : failure</td><td colSpan="1" rowSpan="1" width="98"></td></tr></tbody></table>


 List of recycled strings

| submodule    | Programming language | statement                                            | describe      |
|--------|------|-----------------------------------------------|---------|
| Get network card information | C    | extern inline void kdk_nc_freeall(char **ptr) | list of recycled strings |
|        |      | Parameter Description                                          | Return value description   |   |
|        |      | ptr：list of strings                                     | none       |



#### **3.1.2.3** get disk information

 Encapsulate the C interface to obtain the hardware information of the network card.

 （1）CMakeLists.txt build project
```
 aux_source_directory(. SOURCESCODE)
add_library(kydiskinfo SHARED ${SOURCESCODE})
set_target_properties(kydiskinfo PROPERTIES VERSION 1.2.0 SOVERSION 1)
add_executable(test-getdiskinfo test/getdiskinfo.c)
add_executable(test-getdisklist test/getdisklist.c)
find_library(UDEV_LIB udev)
target_link_libraries(kydiskinfo blkid kylog pthread systemd kyconf ${UDEV_LIB})
target_link_libraries(test-getdiskinfo kydiskinfo)
target_link_libraries(test-getdisklist kydiskinfo)
```
 （2）Get network card information，Need to add header file：
```
 #include "kysdk/kysdk-system/libkydiskinfo.h"
```
 Get a list of all disks in the system


| submodule    | Programming language | statement                               | describe                                      |
|--------|------|----------------------------------|-----------------------------------------|
| get disk information | C    | extern char** kdk_get_disklist() | Get a list of all disks in the system                            |
|        |      | Parameter Description                             | Return value description                                   |   |
|        |      | none                                | char**， Each string represents the absolute path of a disk, ending with a NULL character |


 Free the list of disks returned by kdk_get_disklist

| submodule    | Programming language | statement                                             | describe                         |
|--------|------|------------------------------------------------|----------------------------|
| get disk information | C    | extern void kdk_free_disklist(char** disklist) | Free the list of disks returned by kdk_get_disklist |
|        |      | Parameter Description                                           | Return value description                      |   |
|        |      | disklist ：String pointer returned by kdk_get_disk_list           | none                          |



 Obtain the disk information of the specified disk in the system


| submodule    | Programming language | statement                                                          | describe                            |
|--------|------|-------------------------------------------------------------|-------------------------------|
| get disk information | C    | extern kdk_diskinfo *kdk_get_diskinfo(const char *diskname) | Get the disk information of the specified disk                   |
|        |      | Parameter Description                                                        | Return value description                         |   |
|        |      | diskname：Specifies the disk name, which should be an absolute path such as /dev/sda, or an element in disklist       | kdk_diskinfo* The detailed information of the disk, the specific information is self-obtained |



 Free the disk information structure returned by kdk_get_diskinfo

| submodule    | Programming language | statement                                                 | describe                            |
|--------|------|----------------------------------------------------|-------------------------------|
| get disk information | C    | extern void kdk_free_diskinfo(kdk_diskinfo *disk); | Release the disk information structure returned by kdk_get_diskinfo |
|        |      | Parameter Description                                               | Return value description                         |   |
|        |      | disk：Structure pointer returned by kdk_get_diskinfo                     | none                             |



#### 3.1.2.4 Get bios information

  Encapsulate C interface to obtain bios hardware information.

  (1) CMakeLists.txt build project
```
aux_source_directory(. SOURCESCODE)
include_directories(.)
add_library(kyhw SHARED ${SOURCESCODE})
add_executable(kybios-test test/kycpu-test.c)
target_link_libraries(kyhw kylog kyconf pthread systemd udev)
target_link_libraries(kybios-test kyhw)
```
 （2）To get bios information, you need to add a header file：
```
 #include "kysdk/kysdk-system/libkybiosinfo.h"
```
 Get bios vendor information


| submodule    | Programming language | statement                                        | describe                  |
|--------|------|-------------------------------------------|---------------------|
| get disk information | C    | extern const char *kdk_bios_get_vendor(); | Obtain bios vendor information          |
|        |      | Parameter Description                                      | Return value description               |   |
|        |      | none                                         | Successfully returns the bios manufacturer, fails and returns null |



 Get bios version information

| submodule    | Programming language | statement                                         | describe                  |
|--------|------|--------------------------------------------|---------------------|
| get disk information | C    | extern const char *kdk_bios_get_version(); | Get bios version information          |
|        |      | Parameter Description                                       | Return value description               |   |
|        |      | none                                          | Successfully returns the bios version, fails to return null |



 Free memory


| submodule    | Programming language | statement                                     | describe            |
|--------|------|----------------------------------------|---------------|
| get disk information | C    | extern void kdk_bios_free(char* info); | Release the memory requested by the information acquisition interface |
|        |      | Parameter Description                                   | Return value description         |   |
|        |      | Get the pointer returned by the information interface                            | none             |



#### 3.1.2.5 Obtain motherboard information

  The package C interface gets the motherboard hardware information.

  (1) CMakeLists.txt build project
```
aux_source_directory(. SOURCESCODE)
include_directories(.)
add_library(kyhw SHARED ${SOURCESCODE})
add_executable(kyboard-test test/kycpu-test.c)
target_link_libraries(kyhw kylog kyconf pthread systemd udev)
target_link_libraries(kyboard-test kyhw)
```
 （2）To obtain motherboard information, you need to add header files：
```
 #include "kysdk/kysdk-system/libkyboardinfo.h"
```
 Obtain motherboard product name information

| submodule    | Programming language | statement                                       | describe                  |
|--------|------|------------------------------------------|---------------------|
| get disk information | C    | extern const char *kdk_board_get_name(); | Get motherboard product name            |
|        |      | Parameter Description                                     | Return value description               |   |
|        |      | none                                        | Successfully returns the product name of the mainboard, and fails to return null |



 Get the motherboard production date


| submodule    | Programming language | statement                                       | describe                  |
|--------|------|------------------------------------------|---------------------|
| get disk information | C    | extern const char *kdk_board_get_date(); | Get the motherboard production date            |
|        |      | Parameter Description                                     | Return value description               |   |
|        |      | none                                        | If it succeeds, it will return the production date of the motherboard, if it fails, it will return null |


 Get the motherboard serial number

| submodule    | Programming language | statement                                         | describe                 |
|--------|------|--------------------------------------------|--------------------|
| get disk information | C    | extern const char *kdk_board_get_serial(); | Get the motherboard serial number            |
|        |      | Parameter Description                                       | Return value description              |   |
|        |      | none                                          | Returns the serial number of the motherboard successfully, and returns null if it fails |


 Obtain motherboard manufacturer information


| submodule    | Programming language | statement                                         | describe                  |
|--------|------|--------------------------------------------|---------------------|
| get disk information | C    | extern const char *kdk_board_get_vendor(); | Obtain motherboard manufacturer information            |
|        |      | Parameter Description                                       | Return value description               |   |
|        |      | none                                          | Successfully returns motherboard manufacturer information, fails to return null |



 Free memory


| submodule    | Programming language | statement                                      | describe            |
|--------|------|-----------------------------------------|---------------|
| get disk information | C    | extern void kdk_board_free(char* info); | Release the memory requested by the information acquisition interface |
|        |      | Parameter Description                                    | Return value description         |   |
|        |      | Get the pointer returned by the information interface                             | none             |



#### 3.1.2.6 Get usb device information

  Encapsulate the C interface to obtain the hardware information of the usb device.

  (1) CMakeLists.txt build project
```
aux_source_directory(. SOURCESCODE)
include_directories(.)
add_library(kyhw SHARED ${SOURCESCODE})
add_executable(kyusb-test test/kyusb-test.c)
target_link_libraries(kyhw kylog kyconf pthread systemd udev)
target_link_libraries(kyusb-test kyhw)
```
 （2）To obtain usb device information, you need to add header files:

```
 #include "kysdk/kysdk-system/libkyusbinfo.h"
```
 Get all usb device information


| submodule    | Programming language | statement                                         | describe                                     |
|--------|------|--------------------------------------------|----------------------------------------|
| get disk information | C    | extern pDriverInfoList kdk_usb_get_list(); | Get usb device information                              |
|        |      | Parameter Description                                       | Return value description                                  |   |
|        |      | none                                          | pDriverInfoList : The structure pointer of the usb device information, the specific information is self-obtained |

 free memory


| submodule    | Programming language | statement                                              | describe           |
|--------|------|-------------------------------------------------|--------------|
| get disk information | C    | extern void kdk_usb_free(pDriverInfoList list); | Release the memory allocated when obtaining information |
|        |      | Parameter Description                                            | Return value description        |   |
|        |      | list:kdk_usb_get_list()return pointer                    | none            |



### **3.1.3** Get package list information

  Encapsulates the C interface to obtain all package information in the system.


```
$ sudo apt-get install libkysdk-package libkysdk-package-dev
```

 （1）CMakeLists.txt build project

```
aux_source_directory(. SOURCESCODE)
include_directories(.)
add_library(kypackage SHARED ${SOURCESCODE})
add_executable(kypackage-test test/kypackage-test.c)
target_link_libraries(kypackage-test kypackage)
```
 （2）Get package list information, you need to add header files：
```
 #include "kysdk/kysdk-system/libkypackages.h"
```
 Get a list of all packages in the system


| submodule     | Programming language | statement                                                     | describe                                                                   |
|---------|------|--------------------------------------------------------|----------------------------------------------------------------------|
| Get package list information | C    | extern kdk_package_list* kdk_package_get_packagelist() | Get a list of all packages in the system                                                           |
|         |      | Parameter Description                                                   | Return value description                                                                |   |
|         |      | none                                                      | kdk_package_list* package describe structure list; the returned structure needs to be recycled by kdk_package_free_packagelist() |



 Get the version number of the specified package in the system


| submodule     | Programming language | statement                                                     | describe                                          |
|---------|------|--------------------------------------------------------|---------------------------------------------|
| Get package list information | C    | extern char* kdk_package_get_version(const char *name) | Get the version number of the specified package in the system                                |
|         |      | Parameter Description                                                   | Return value description                                       |   |
|         |      | name：package name                                              | char* version number, generated by alloc, needs to be free; if the given package does not exist, return NULL |



 Check whether the package with the specified package name is installed correctly


<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan="1" width="81">Programming language</td><td colSpan="2" rowSpan="1" width="313">statement</td><td colSpan="1" rowSpan="1" width="94">describe</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get package list information</td><td colSpan="1" rowSpan="3" width="81">C</td><td colSpan="2" rowSpan="1" width="313">extern int kdk_package_is_installed(const char *name, const char *version)</td><td colSpan="1" rowSpan="1" width="94">Check whether the package with the specified package name is installed correctly</td></tr><tr><td colSpan="1" rowSpan="1" width="137">Parameter Description</td><td colSpan="1" rowSpan="1" width="176">Return value description</td><td colSpan="1" rowSpan="1" width="94"></td></tr><tr><td colSpan="1" rowSpan="1" width="137">name：Package name version: version number, NULL in most cases, reserved for snap</td><td colSpan="1" rowSpan="1" width="176">int returns 0 on success, returns an error code on failure</td><td colSpan="1" rowSpan="1" width="94"></td></tr></tbody></table>


### **3.1.4** Get resource information

 Encapsulate the C interface to obtain the usage rate of memory, swap partition, and cpu.


```
$ sudo apt-get install libkysdk-proc libkysdk-proc-dev 

```


 （1）CMakeLists.txt build project
```
aux_source_directory(. SOURCESCODE)
include_directories(.)
add_library(kyrtinfo SHARED ${SOURCESCODE})
add_executable(kyrtinfo-test test/kyrtinfo-test.c)
target_link_libraries(kyrtinfo-test kyrtinfo)
```
 （2）Get package list information, you need to add header files：
```
 #include "kysdk/kysdk-system/libkyrtinfo.h"
```
 Get the total size of physical memory in the system

| submodule    | Programming language | statement                                                   | describe                          |
|--------|------|------------------------------------------------------|-----------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_res_total_KiB() | Get the total size of physical memory in the system                |
|        |      | Parameter Description                                                 | Return value description                       |   |
|        |      | none                                                    | unsigned long Physical memory size, in KiB |



 Get physical memory usage

| submodule    | Programming language | statement                                               | describe            |
|--------|------|--------------------------------------------------|---------------|
| Get resource information | C    | extern float kdk_rti_get_mem_res_usage_percent() | Get physical memory usage     |
|        |      | Parameter Description                                             | Return value description         |   |
|        |      | none                                                | float physical memory usage |



 Get the physical memory usage size


| submodule    | Programming language | statement                                                   | describe                                 |
|--------|------|------------------------------------------------------|------------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_res_usage_KiB() | Get the size of physical memory usage, note that Buffer/Cache is calculated as used memory |
|        |      | Parameter Description                                                 | Return value description                              |   |
|        |      | none                                                    | unsigned long Physical memory usage size, in KiB      |



 Get the actual available physical memory size


| submodule    | Programming language | statement                                                       | describe                                        |
|--------|------|----------------------------------------------------------|-------------------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_res_available_KiB() | Get the actual available physical memory size，This value is approximately equal to Free + Buffer + Cache |
|        |      | Parameter Description                                                     | Return value description                                     |   |
|        |      | none                                                        | unsigned long Available physical memory size, in KiB             |



 Get the actual free physical memory size

| submodule    | Programming language | statement                                                  | describe                                    |
|--------|------|-----------------------------------------------------|---------------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_res_free_KiB() | Get the actual free physical memory size，Note that Buffer/Cache is counted as used memory |
|        |      | Parameter Description                                                | Return value description                                 |   |
|        |      | none                                                   | unsigned long Free physical memory size, in KiB        |


 Get the total amount of virtual memory requested by all applications

| submodule    | Programming language | statement                                                    | describe                            |
|--------|------|-------------------------------------------------------|-------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_virt_alloc_KiB() | Get the total amount of virtual memory requested by all applications               |
|        |      | Parameter Description                                                  | Return value description                         |   |
|        |      | none                                                     | unsigned long The total application amount of virtual memory, in KiB |


 Get the total size of the Swap partition in the system


| submodule    | Programming language | statement                                                    | describe                            |
|--------|------|-------------------------------------------------------|-------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_swap_total_KiB() | Get the total size of the Swap partition in the system                |
|        |      | Parameter Description                                                  | Return value description                         |   |
|        |      | none                                                     | unsigned long SwapPartition size, in KiB |



 Get Swap partition usage

| submodule    | Programming language | statement                                                | describe              |
|--------|------|---------------------------------------------------|-----------------|
| Get resource information | C    | extern float kdk_rti_get_mem_swap_usage_percent() | Get Swap partition usage     |
|        |      | Parameter Description                                              | Return value description           |   |
|        |      | none                                                 | float Swap Partition usage |



 Get Swap partition usage

| submodule    | Programming language | statement                                                    | describe                             |
|--------|------|-------------------------------------------------------|--------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_swap_usage_KiB() | Get Swap partition usage                    |
|        |      | Parameter Description                                                  | Return value description                          |   |
|        |      | none                                                     | unsigned long Swap partition usage, in KiB |



 Get free size of Swap partition


| submodule    | Programming language | statement                                                   | describe                              |
|--------|------|------------------------------------------------------|---------------------------------|
| Get resource information | C    | extern unsigned long kdk_rti_get_mem_swap_free_KiB() | Get free size of Swap partition                    |
|        |      | Parameter Description                                                 | Return value description                           |   |
|        |      | none                                                    | unsigned long Swap partition free size, in KiB |



 Get the instantaneous CPU usage


| submodule    | Programming language | statement                                           | describe                       |
|--------|------|----------------------------------------------|--------------------------|
| Get resource information | C    | extern float kdk_rti_get_cpu_current_usage() | Get the instantaneous CPU usage               |
|        |      | Parameter Description                                         | Return value description                    |   |
|        |      | none                                            | float Instantaneous CPU usage, the value < 1.00 |


 Get the operating system boot time


| submodule                | Programming language           | statement                                                                                                         | describe         |
|--------------------|----------------|------------------------------------------------------------------------------------------------------------|------------|
| Get resource information             | C              | extern int kdk_rti_get_uptime(unsigned int *day, unsigned int *hour, unsigned int *min, unsigned int *sec) | Get the operating system boot time |
|                    |                | Parameter Description                                                                                                       | Return value description      |   |
|                    |                | day：The number of working days<br> hour, the value must be < 24<br> min: the number of minutes, the value must be < 60<br> sec: the number of seconds, the value must be < 60 | int Return 0 for success, return non-zero for failure | 


 Get the CPU usage of the specified process


| submodule    | Programming language | statement                                                           | describe            |
|--------|------|--------------------------------------------------------------|---------------|
| Get resource information | C    | extern float kdk_get_process_cpu_usage_percent(int proc_num) | Get the CPU usage of the specified process |
|        |      | Parameter Description                                                         | Return value description         |   |
|        |      | proc_num：process number                                                 | float CPU usage  |



 Get the memory usage of the specified process


| submodule    | Programming language | statement                                                           | describe           |
|--------|------|--------------------------------------------------------------|--------------|
| Get resource information | C    | extern float kdk_get_process_mem_usage_percent(int proc_num) | Get the memory usage of the specified process |
|        |      | Parameter Description                                                         | Return value description        |   |
|        |      | proc_num：process number                                                 | float memory usage  |



 Get the process status of the specified process


| submodule    | Programming language | statement                                                | describe          |
|--------|------|---------------------------------------------------|-------------|
| Get resource information | C    | extern char* kdk_get_process_status(int proc_num) | Get the process status of the specified process |
|        |      | Parameter Description                                              | Return value description       |   |
|        |      | proc_num：process number                                      | char* process status  |



 Get the port number occupied by the specified process

| submodule    | Programming language | statement                                            | describe           |
|--------|------|-----------------------------------------------|--------------|
| Get resource information | C    | extern int kdk_get_process_port(int proc_num) | Get the port number occupied by the specified process |
|        |      | Parameter Description                                          | Return value description        |   |
|        |      | proc_num：process number                                  | char* port number used |



 Get the startup time of the specified process

| submodule    | Programming language | statement                                                    | describe          |
|--------|------|-------------------------------------------------------|-------------|
| Get resource information | C    | extern char* kdk_get_process_start_time(int proc_num) | Get the startup time of the specified process |
|        |      | Parameter Description                                                  | Return value description       |   |
|        |      | proc_num：process number                                          | char* Start Time  |

 Get the running time of the specified process


| submodule    | Programming language | statement                                                    | describe          |
|--------|------|-------------------------------------------------------|-------------|
| Get resource information | C    | extern char* kdk_get_process_start_time(int proc_num) | Get the running time of the specified process |
|        |      | Parameter Description                                                  | Return value description       |   |
|        |      | proc_num：process number                                          | char* running time  |



 Get the cpu time of the specified process

| submodule    | Programming language | statement                                                  | describe           |
|--------|------|-----------------------------------------------------|--------------|
| Get resource information | C    | extern char* kdk_get_process_cpu_time(int proc_num) | Get the cpu time of the specified process |
|        |      | Parameter Description                                                | Return value description        |   |
|        |      | proc_num：process number                                        | char* cpu时间  |



 Get the Command of the specified process


| submodule    | Programming language | statement                                                 | describe             |
|--------|------|----------------------------------------------------|----------------|
| Get resource information | C    | extern char* kdk_get_process_command(int proc_num) | Get the Command of the specified process |
|        |      | Parameter Description                                               | Return value description          |   |
|        |      | proc_num：process number                                       | char* Command  |



Get the owner of the specified process


<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="308">Statement</td><td colSpan="1" rowSpan="1" width="100">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get resource information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="308">extern char* kdk_get_process_user(int proc_num)</td><td colSpan= "1" rowSpan="1" width="100">Get the owner of the specified process</td></tr><tr><td colSpan="1" rowSpan="1" width="127">Parameters Description</td><td colSpan="1" rowSpan="1" width="181">Return value description</td><td colSpan="1" rowSpan="1" width="100"></ td></tr><tr><td colSpan="1" rowSpan="1" width="127">proc_num: process number</td><td colSpan="1" rowSpan="1" width=" 181">char* owner</td><td colSpan="1" rowSpan="1" width="100"></td></tr></tbody></table>


Get the CPU usage of the specified process
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="308">Statement</td><td colSpan="1" rowSpan="1" width="100">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get resource information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="308">extern float kdk_procname_get_process_cpu_usage_percent(char *proc_name)</td><td colSpan= "1" rowSpan="1" width="100">Get the CPU usage of the specified process</td></tr><tr><td colSpan="1" rowSpan="1" width="127"> Parameter description</td><td colSpan="1" rowSpan="1" width="181">Return value description</td><td colSpan="1" rowSpan="1" width="100">< /td></tr><tr><td colSpan="1" rowSpan="1" width="127">proc_name process name</td><td colSpan="1" rowSpan="1" width=" 181">float CPU usage</td><td colSpan="1" rowSpan="1" width="100"></td></tr></tbody></table>

Get information about the specified process
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="308">Statement</td><td colSpan="1" rowSpan="1" width="100">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get resource information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="308">extern char** kdk_procname_get_process_infomation(char *proc_name)</td><td colSpan="1" rowSpan="1" width="100">Get the information of the specified process</td></tr><tr><td colSpan="1" rowSpan="1" width="127"> Parameter description</td><td colSpan="1" rowSpan="1" width="181">Return value description</td><td colSpan="1" rowSpan="1" width="100">< /td></tr><tr><td colSpan="1" rowSpan="1" width="127">proc_name process name</td><td colSpan="1" rowSpan="1" width=" 181">char** process information list, ending with NULL, generated by alloc, needs to be reclaimed by kdk_proc_freeall; if there is an error in obtaining, return NULL;</td><td colSpan="1" rowSpan="1" width= "100"></td></tr></tbody></table>

  Get all information about the process
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="308">Statement</td><td colSpan="1" rowSpan="1" width="100">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get resource information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="308">char** kdk_get_process_all_information()</td><td colSpan="1 " rowSpan="1" width="100">Get all process information</td></tr><tr><td colSpan="1" rowSpan="1" width="127">Parameter Description</td ><td colSpan="1" rowSpan="1" width="181">Return value description</td><td colSpan="1" rowSpan="1" width="100"></td></ tr><tr><td colSpan="1" rowSpan="1" width="127">None</td><td colSpan="1" rowSpan="1" width="181">char** process All information lists, end with NULL, are generated by alloc and need to be recycled by kdk_proc_freeall; if there is an error in obtaining, return NULL;</td><td colSpan="1" rowSpan="1" width="100"></ td></tr></tbody></table>

List of recycled strings
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="312">Statement</td><td colSpan="1" rowSpan="1" width="98">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get resource information</td><td colSpan="1" rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="312">extern inline void kdk_proc_freeall(char **ptr)</td><td colSpan="1" rowSpan="1" width="98">recycle string list</td></tr><tr><td colSpan="1" rowSpan="1" width="128">parameters Description</td><td colSpan="1" rowSpan="1" width="183">Return value description</td><td colSpan="1" rowSpan="1" width="98"></ td></tr><tr><td colSpan="1" rowSpan="1" width="128">ptr: string list</td><td colSpan="1" rowSpan="1" width= "183">none</td><td colSpan="1" rowSpan="1" width="98"></td></tr></tbody></table>


### **3.1.5** Obtain the basic information of the operating system

  Encapsulate C interface to obtain system name, version number, activation information, etc.

```
$ sudo apt-get install libkysdk-sysinfo libkysdk-sysinfo-dev 

```


 （1）CMakeLists.txt build project

 
```
aux_source_directory(. SOURCESCODE)

include_directories(.)

find_library(GLIBC_LIB glib-2.0)

find_library(DBUS_LIB dbus-1)

find_library(DBUS_GLIB_LIB dbus-glib-1)

add_library(kysysinfo SHARED ${SOURCESCODE})

add_executable(kysysinfo-test test/kysysinfo_test.c)

target_link_libraries(kysysinfo kylin-activation kylog systemd kyconf

${GLIBC_LIB} ${DBUS_LIB} ${DBUS_GLIB_LIB})

target_link_libraries(kysysinfo-test kysysinfo)
```


 （2）To obtain operating system information, you need to add header files:
```
 #include "kysdk/kysdk-system/libkysysinfo.h"
```
 Get operating system architecture information

| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|------|------------------------------------------------------|----------------------------------------------------------------------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_architecture()           | Get system architecture information                                                                   |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* system architecture, for example: x86_64                                                        |       |

 Get OS name                                                                                                                                                        
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_systemName()             | Get OS name                                                                   |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* system name, for example: Kylin                                                         |       |

 Get the operating system version number                                                                                                                                                       
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_version(bool verbose)    | Get the system version number                                                                    |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | verbose: 0 for abbreviated version number, 1 for detailed version number                            | char* System version number, for example: system brief version: xxxx desktop operating system; system detailed version: Desktop-V10-Professional-Release-Build1-210203 |

 Get the kernel version number                                                                                                                                                         
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_kernelVersion()          | Get the kernel version number                                                                    |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char*                                                                      | kernel version number |   |

 Get the username of the currently logged in user                                                                                                                                                    
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_eUser()                  | Get the username of the currently logged in user（Effect User）                                                  |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* returns a string on success, and NULL on failure. The returned string needs to be freed                                   |       |

 Get operating system item number name                                                                                                                                                     
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_projectName()            | Get operating system item number name                                                                |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* returns a string on success, and NULL on failure. The returned string needs to be freed                                   |       |

 Get OS project subnumber name                                                                                                                                                    
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char*  kdk_system_get_projectSubName()        | Get OS project subnumber name                                                               |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* returns a string on success, and NULL on failure. The returned string needs to be freed                                   |       |

 Obtain the operating system product identification code                                                                                                                                                     
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern unsigned int kdk_system_get_productFeatures() | Obtain the operating system product identification code                                                                |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | unsigned int return flag code <br> 0000: abnormal information <br> 0001: only PC features <br> 0010: only tablet features <br> 0011: support tablet and PC features |

 Obtain the virtual machine type of the operating system host                                                                                                                                                 
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_hostVirtType()           | Obtain the virtual machine type of the operating system host                                                            |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char* returns NULL if the acquisition fails, and returns a string if the acquisition succeeds. The content of the string is as follows: <br> [none, qemu, kvm, zvm, vmware,hyper-v, orcale virtualbox, xen, bochs, uml,parallels,bhyve,qnx,arcn,openvz,lxc,lxc-libvirt,systemd-nspawn,docker,podman,rkt,wsl] |

 Obtain the cloud platform type of the operating system host                                                                                                                                                 
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char* kdk_system_get_hostCloudPlatform()      | Obtain the cloud platform type of the operating system host                                                            |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    |  char* returns NULL if the acquisition fails, and returns a string if the acquisition succeeds. The content of the string is as follows: <br> [none, huawei] |

 Determine whether the current operating system is a dedicated machine system                                                                                                                                                
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern bool kdk_system_is_zyj(void)                  | Determine whether the current mirroring system is a dedicated machine system                                                            |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    |  bool <br> true means yes <br> false means no  |

 Get system resolution information                                                                                                                                                       
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern char** kdk_system_get_resolving_power()       | Get system resolution information                                                                  |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | none                                                    | char** Resolution information list, ending with NULL, generated by alloc, needs to be reclaimed by kdk_resolving_freeall; if there is an error in obtaining, return NULL； |       |

 list of recycled strings                                                                                                                                                         
| submodule                                                                                                                                                             | Programming language | statement                                                   | describe                                                                         |
|--------|------|----------------------------------------------------|----------------|
| Get basic system information                                                                                                                                                        | C    | extern inline void kdk_resolving_freeal(char **ptr)  | list of recycled strings                                                                    |
|                                                                                                                                                                 |      | Parameter Description                                                 | Return value description                                                                      |       |
|                                                                                                                                                                 |      | ptr: list of strings                                            | none                                                                          |




### **3.1.6** Get network information

  Get network connection status


| submodule                                                                                                                                                                                                 | Programming language      | statement                                                                  | describe                                 |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|---------------------------------------------------------------------|------------------------------------|
| Get basic system information                                                                                                                                                                                            | dbus interface    | bus: system bus                                                     |
| name : org.freedesktop.NetworkManager                                                                                                                                                               |
| path: org.freedesktop.NetworkManager                                                                                                                                                                |
| method: state()->int arg0;                                                                                                                                                                          | Get network connection status。 |
|                                                                                                                                                                                                     |
|                                                                                                                                                                                                     |           | Parameter Description                                                                | Return value description                              |   |
|                                                                                                                                                                                                     |           | none                                                                   | int network connection status                         |   |
|                                                                                                                                                                                                     |

Get network port status

  <table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get basic system information</td><td colSpan="1 " rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="319">extern int kdk_net_get_port_stat(int port);</td><td colSpan ="1" rowSpan="1" width="91">Get port status</td></tr><tr><td colSpan="1" rowSpan="1" width="161">Parameter description< /td><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"></td> </tr><tr><td colSpan="1" rowSpan="1" width="161">port port number</td><td colSpan="1" rowSpan="1" width="158"> int port status FREE: 0TCP_ESTABLISHED:1 TCP_SYN_SENT:2 TCP_SYN_RECV:3 TCP_FIN_WAIT1:4 TCP_FIN_WAIT2:5 TCP_TIME_WAIT:6 TCP_CLOSE:7 TCP_CLOSE_WAIT:8 TCP_LAST_ACL:9 TCP_LISTEN:10 TCP_CLOSING:11</td>cpanS="1 ="1" width="91"></td></tr></tbody></table>

  Get the status of multiple network ports
  <table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get system basic information</td><td colSpan="1 " rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="319">int kdk_net_get_multiple_port_stat(int start, int end, int *result)</ td><td colSpan="1" rowSpan="1" width="91">Get [start, end] port status</td></tr><tr><td colSpan="1" rowSpan=" 1" width="161">parameter description</td><td colSpan="1" rowSpan="1" width="158">return value description</td><td colSpan="1" rowSpan="1 " width="91"></td></tr><tr><td colSpan="1" rowSpan="1" width="161">start start port number end end port number result int with enough space array. Accept port status</td><td colSpan="1" rowSpan="1" width="158">Success returns 0 Failure returns port status</td><td colSpan="1" rowSpan="1" width= "91"></td></tr></tbody></table>

get default gateway
  <table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get basic system information</td><td colSpan="1 " rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="319">extern prouteMapList kdk_net_get_route();</td><td colSpan=" 1" rowSpan="1" width="91">Get Default Gateway</td></tr><tr><td colSpan="1" rowSpan="1" width="161">Parameter Description</td ><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"></td></ tr><tr><td colSpan="1" rowSpan="1" width="161">None</td><td colSpan="1" rowSpan="1" width="158">prouteMapList network management information, The specific information is fetched by yourself, and NULL is returned if it fails</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>

Get firewall status
 
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get system basic information</td><td colSpan="1 " rowSpan="3" width="82">C</td><td colSpan="2" rowSpan="1" width="319">extern pChain kdk_net_get_iptable_rules()</td><td colSpan="1 " rowSpan="1" width="91">Get Firewall Information</td></tr><tr><td colSpan="1" rowSpan="1" width="161">Parameter Description</td> <td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"></td></tr ><tr><td colSpan="1" rowSpan="1" width="161">None</td><td colSpan="1" rowSpan="1" width="158">pChain firewall information, specific Information self-fetching, failure returns NULL</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>



### **3.1.7** Obtain system runtime information

  Encapsulate C++ class to obtain network speed, cpu temperature, hard disk temperature, etc.


```
$ sudo apt-get install libkyrealtimeinfo libkyrealtimeinfo-dev
```


 (1) CMakeLists.txt build project

```
include_directories("smartctl/")
aux_source_directory(./smartctl CODE)
add_library(kyrealtime SHARED libkyrealtimeinfo.cpp libkyrealtimeinfo.h ${CODE})
set_target_properties(kyrealtime PROPERTIES VERSION 1.2.0 SOVERSION 1)

add_executable(realtimetest test/getrealtimeinfo.cpp)
target_link_libraries(realtimetest kyrealtime)
```

(2) To obtain the runtime information of the operating system, a header file needs to be added:

```
#include "kysdk/kysdk-system/libkyrealtimeinfo.h"
```

 Get Instantaneous Internet Speed

<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get basic system information</td><td colSpan="1 " rowSpan="3" width="82">C++</td><td colSpan="2" rowSpan="1" width="319">RealTimeInfo::float kdk_real_get_net_speed();</td><td colSpan ="1" rowSpan="1" width="91">Get Instantaneous Internet Speed</td></tr><tr><td colSpan="1" rowSpan="1" width="161">Parameter Description </td><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"></td ></tr><tr><td colSpan="1" rowSpan="1" width="161">none</td><td colSpan="1" rowSpan="1" width="158">float Instantaneous network speed</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>


  Get cpu temperature

<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get basic system information</td><td colSpan="1 " rowSpan="3" width="82">C++</td><td colSpan="2" rowSpan="1" width="319">RealTimeInfo::float kdk_real_get_cpu_temperatu();</td><td colSpan ="1" rowSpan="1" width="91">Get instantaneous cpu temperature</td></tr><tr><td colSpan="1" rowSpan="1" width="161">Parameter description </td><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"></td ></tr><tr><td colSpan="1" rowSpan="1" width="161">none</td><td colSpan="1" rowSpan="1" width="158">floatcpu Instantaneous temperature</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>

  Get HDD temperature
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="67">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="67">Get basic system information</td><td colSpan="1 " rowSpan="3" width="82">C++</td><td colSpan="2" rowSpan="1" width="319">RealTimeInfo::float kdk_real_get_disk_temperatu(const char * name);</td ><td colSpan="1" rowSpan="1" width="91">Get the instantaneous hard disk temperature</td></tr><tr><td colSpan="1" rowSpan="1" width="161 ">Parameter description</td><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91" ></td></tr><tr><td colSpan="1" rowSpan="1" width="161">name: the absolute path of the hard disk</td><td colSpan="1" rowSpan="1 " width="158">float specifies the instantaneous temperature of the hard disk</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>

  Get disk speed
<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="1" width="68">submodule</td><td colSpan="1" rowSpan= "1" width="82">Programming language</td><td colSpan="2" rowSpan="1" width="319">Statement</td><td colSpan="1" rowSpan="1" width="91">Description</td></tr><tr><td colSpan="1" rowSpan="3" width="68">Get system basic information</td><td colSpan="1 " rowSpan="3" width="82">C++</td><td colSpan="2" rowSpan="1" width="319">RealTimeInfo::int kdk_real_get_disk_rate(const char *name);</td ><td colSpan="1" rowSpan="1" width="91">Get disk speed</td></tr><tr><td colSpan="1" rowSpan="1" width="161" >Parameter description</td><td colSpan="1" rowSpan="1" width="158">Return value description</td><td colSpan="1" rowSpan="1" width="91"> </td></tr><tr><td colSpan="1" rowSpan="1" width="161">name: the absolute path of the hard disk</td><td colSpan="1" rowSpan="1" width="158">int specifies the disk speed</td><td colSpan="1" rowSpan="1" width="91"></td></tr></tbody></table>



### **3.1.8** Get current geographic information

  Encapsulates the C interface to obtain the geographic information of the current ip address.

```
$ sudo apt-get install libkylocation libkylocation-dev

```

 (1) CMakeLists.txt build project
```
include_directories("/usr/include/python3.8")
add_library(kylocation SHARED libkylocation.c)
set_target_properties(kylocation PROPERTIES VERSION 1.2.0 SOVERSION 1)
target_link_libraries(kylocation -lpython3.8)
add_executable(locationtest test/getlocation.c)
target_link_libraries(locationtest kylocation)
```

(2) To obtain the geographical information of the IP address, a header file needs to be added:

```
#include "kysdk/kysdk-system/libkylocation.h"
```

 Get local location


| submodule      | Programming language | statement                               | describe             |
|----------|------|----------------------------------|----------------|
| Get basic system information | C    | extern char *kdk_loaction_get(); | Get the location of this machine      |
|          |      | Parameter Description                             | Return value description          |   |
|          |      | none                                | Describe the json string of the geographic location |



## **3.2** Power Management

  The design of this layer is mainly for the application power management interface.

### **3.2.1** Lock Screen Settings

  Encapsulate the C++ interface to provide the ability to set the lock screen.

  Set to prohibit lock screen


| submodule           | Programming language  | statement                                                                             | describe     |
|---------------|-------|--------------------------------------------------------------------------------|--------|
| power management module        | C++   | uint32_t kdk_set_inhibit_lockscreen(const char *appName , const char *reason) | Set to prohibit lock screen |
|               |
|               |       | Parameter Description                                | Return value description |
|               |       | appName：Application name <br> reason: Reason for prohibiting lock screen | 0: Failed <br> >0: Successful (greater than 0)       |



 Unban lock screen

| submodule             | Programming language  | statement                                            | describe     |
|-----------------|-------|-----------------------------------------------|--------|
| power management module          | C++   | int kdk_un_inhibit_lockscreen(uint32_t flag) | Unban lock screen |
|                 |
|                 |       | Parameter Description                                          |Return value description |
|                 |       | flag：The return value of prohibiting the lock screen interface | 0: success <br> -1: failure          |


## **3.3** File Management

  The module design is mainly for developers to monitor file changes in the file system and provide interface definitions; reduce

  Differences between system versions due to changes in some file systems;

  Installation method:

```
 $ sudo apt install libkysdk-filesystem libkysdk-filesystem-dev
```


| statement                                                                                                                                                                                 | describe                           |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|
| QStringList addWatchTargetRecursive(QString url, FileWatcherType type = PERIODIC, int attr = OPEN`|`CLOSE`|`MODIFY`|`DELETE, int maxdepth = 5, int recurType = DIR`|`REGULAR);             | Add the file path that needs to be monitored. For directories, recursive monitoring of subdirectories is enabled by default |
| QStringList addWatchTargetListRecursive(QStringList urlList, FileWatcherType type = PERIODIC, int attr = OPEN`|`CLOSE`|`MODIFY`|`DELETE, int maxdepth = 5, int recurType = DIR`|`REGULAR); | Increase the list of files that need to be monitored                  |
| QStringList removeWatchTargetRecursive(QString url, int maxdepth = 5, int recurType = ALLFILE);                                                                                    | Remove the specified file from the listen list                |
| void clearWatchList();                                                                                                                                                             | clear watch list                       |
| void pauseWatcher();                                                                                                                                                               | Pause file monitoring                       |
| void restartWatcher();                                                                                                                                                             | resume file monitoring                       |




## **3.4** AI Capabilities

  The design of this layer mainly provides the AI recognition function interface for the application, and adds OCR to the OS.
  Text recognition function; shielding needs

  The development complexity and debugging difficulty brought by the introduction of AI functions.

### **3.4.1** OCR text recognition function

  Encapsulate C++ interface to provide OCR text recognition function.

  Get text box


| submodule                | Programming language | statement                                                                 |                           | describe                               |
|--------------------|------|-----------------------------------------------------------------------------|------------------|----------------------------------|
| OCR text recognition function          | C++  | Std::vector<std::vector<std::vector<int>>> kdk::kdkOCR::getRect(const std::string &imagePath) || get text box                            |
|                    |      | Parameter Description                                                                                          | Return value description                            |   |
|                    |      | String Image file path                                                                                 | Vector<vector<vector<vector<int> <br> The coordinate matrix of the text box points, and the number of text boxes |



 Get text content

| submodule          | Programming language | statement                                                               || describe     |
|----------------|---------|-------------------------------------------------------------------|-|--------|
| OCR text recognition function  | C++     | Std::vector<std::vector<getCls>>>getCls(const std::string &imagePath,int nums) || get text content |
|                |      | Parameter Description                                        | Return value description  |   |
|                |      | String Image file path<br>Vector<string>Text string in the image                | Int nums Number of literal stacks to process simultaneously |  |


## **3.5** Printer Management

  This layer mainly provides a printer management interface for the application, which can realize the delivery of printing tasks, printing methods, and printing tasks.

  cancellation of service.

  Get a list of system printers


| submodule     | Programming language | statement                                  | describe                   |
|------------|----------|---------------------------------------|------------------------|
| printer management | C        | extern char** kdk_printer_get_list(); | Get a list of printers         |
|            |          | Parameter Description                              | Return value description             |
|            |          | none                                    | char ** System Printer List |

 Get a list of available printers on the system                                    

| submodule     | Programming language | statement                                            | describe                   |
|------------|----------|-------------------------------------------------|------------------------|
| printer management | C        | extern char** kdk_printer_get _availablelist(); | Get a list of available printers     |
|            |          | Parameter Description                                        | Return value description             |
|            |          | none                                              | char ** System Printer List |

Set print parameters

| submodule     | Programming language | statement                                                                                                                                                                                                     | describe         |
|------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|
| printer management | C        | extern void kdk_printer_set_options(int number_up,const char *media,const char *number_up_layout,const char *sides);                                                                                     | Set print parameters |
|            |          | Parameter Description                                                                                                                                                                                                 | Return value description   |
|            |          | number_up：Print several pages on one paper<br>media: paper type<br>number_up_layout: cups attribute, such as lrtb<br>sides: single-sided one-sided, double-sided (long side flip): two_sided_long_dege<br>double-sided (short flip side)：<br> two_sided_short_dege | none           |

 print documents                                           
| submodule     | Programming language | statement                                                                              | describe             |
|------------|----------|-----------------------------------------------------------------------------------|------------------|
| printer management | C        | extern int kdk_printer_print_local_file(const char *printername, char *filepath); | print documents         |
|            |          | Parameter Description                                                                          | Return value description       |
|            |          | printername: printer name <br> filepath：Print the absolute path of the file                             | int id of the print job |

 Cancel print job                                         
| submodule                                            | Programming language        | statement                                                                                                                   | describe              |
|-----------------|-------|-----------------------------------------------|--------|
| printer management                                          | C           | extern int kdk_printer_cancel_all_jobs(const char *printername);                                                     | cancel print job          |
|                                                |             | Parameter Description         | Return value description           |   |
|                                                |             | printername：printer name  | int <br> Success: 0 <br> Failed:-1   |

 Get printer status                                        
| submodule     | Programming language | statement                                                        | describe           |
|------------|----------|-------------------------------------------------------------|----------------|
| printer management | C        | extern int kdk_printer_get_status(const char *printername); | Get printer status |
|            |          | Parameter Description                                                    | Return value description     |
|            |          | printername，printer name                                       | printer status code   |


 Parse the downloaded filename from the url                                  
| submodule     | Programming language | statement                                                                   | describe                  |
|------------|----------|------------------------------------------------------------------------|-----------------------|
| printer management | C        | extern void kdk_printer_get_filename(const char *url, char *filename); | Parse downloaded filename from url |
|            |          | Parameter Description                                                               | Return value description            |
|            |          | url，download link                                                          |                       |
|            |          | filename，parsed filename                                             |                       |


 Get current print job status                                     

| submodule     | Programming language | statement                                                                       | describe                 |
|------------|----------|----------------------------------------------------------------------------|----------------------|
| printer management | C        | extern int kdk_printer_get_job_status(const char *printername, int jobid); | Get current print job status |
|            |          | Parameter Description                                                                   | Return value description           |
|            |          | printername，printer name                                                      | print task status code       |


 Download network files to local                                     
| submodule     | Programming language | statement                                                                                      | describe               |
|------------|----------|-------------------------------------------------------------------------------------------|--------------------|
| printer management | C        | extern int kdk_printer_print_download_remote_file(const char *url, const char *filepath); | Download network files to local |
|            |          | Parameter Description                                                                                  | Return value description         |
|            |          | url，network file                                                                             | download status code         |
|            |          | filepath，The file path to save, user-defined                                                    |                    |




# **4** Application support SDK


  In the openSDK application support layer, all packages provide graphical development functions for graphical applications, such as graphical controls, theme styles, etc.; this layer is usually bound to a specific development framework, such as QT, GTK; When installing the application support layer SDK, two virtual packages need to be installed: libkysdk-application, libkysdk-applications-dev, and other packages generate multiple real packages according to functional classification, such as: QT control class: libkysdk-qtwidgets, wayland display protocol compatible interface libkysdk -waylandhelper, apply common module libkysdk-kabase etc.

  Install command:


```
$ sudo apt install libkysdk-applications libkysdk-applications-dev

```

## **4.1** QT self-developed control module

  The QT self-developed control module kysdk-qtwidgets is a submodule of kysdk-application, and the installation method is as follows:


```
sudo apt install libkysdk-qtwidgets libkysdk-qtwidgets-dev
```


  According to different project types, you can refer to the following demos:

  (1) .pro file build project

  Added to the qt project .pro file:

```
CONFIG += link_pkgconfig
PKGCONFIG += kysdk-qtwidgets

```

 (2) CMakeLists.txt build project


```
cmake_minimum_required(VERSION 3.5)
find_package(Qt5 COMPONENTS Widgets REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(KYSDKQTWIDGETS_PKG kysdk-qtwidgets)
target_include_directories(demo PRIVATE ${KYSDKQTWIDGETS_PKG_INCLUDE_DIRS})
target_link_directories(demo PRIVATE ${KYSDKQTWIDGETS_PKG_LIBRARY_DIRS})
target_link_libraries(demo Qt5::Widgets ${KYSDKQTWIDGETS_PKG_LIBRARIES})
```


 In a specific project, you need to introduce the corresponding header file and namespace in the code, such as:


```
#include "kwidget.h"
using namespace kdk;
```


Some controls use translation files, you can load translation files in the main() function, currently supported

  Chinese, Tibetan, and English, the translation files have been compiled into the dynamic library, and the loading method is as follows:


```
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator trans;
    QString locale = QLocale::system().name();
    if(locale == "zh_CN")
    {
        if(trans.load(":/translations/gui_zh_CN.qm"))
        {
            a.installTranslator(&trans);
        }
    }
    if(locale == "bo_CN")
    {
        if(trans.load(":/translations/gui_bo_CN.qm"))
        {
            a.installTranslator(&trans);
        }
    }
    Widget w;
    w.show();
    return a.exec();
}
```



### **4.1.1** Form Module
#### **4.1.1.1** Basic form


  Function description: KWidget, inherited from QWidget, supports responsive theme background switching, responsive icon main

Title switching, the title color responds to the window activation status, and the window button style conforms to the design style of ukui3.1.

  Divided into four components iconBar, windowButtonBar, sideBar, baseBar.

  iconBar, windowButtonBar default height is 40px, sideBar default width is 200px.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image4.png)
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image5.png)


| enumerated type | LayoutType { VerticalType, HorizontalType, MixedType } |
|------|--------------------------------------------------------|


> VerticalType：upper and lower structure
>
> HorizontalType：left and right structure
>
> MixedType：transitional structure


| statement                                             | describe                                               |   |   |
|------------------------------------------------|--------------------------------------------------|---|---|
| QWidget* sideBar();                            | Get the left column widget, and add custom content through setlayout.                  |   |   |
| QWidget* baseBar();                            | Get the widget in the main content area, and add custom content through setlayout.                 |   |   |
| void setIcon(const QIcon& icon);               | Add form icon via icon                                     |   |   |
| void setIcon(const QString& iconName);         | Set the window icon, iconName needs to directly specify the icon name in the system directory, such as "kylin-music". |   |   |
| void setWidgetName(const QString& widgetName); | Set the form name.                                          |   |   |
| KWindowButtonBar* windowButtonBar();           | Gets the window triplet control to control whether to display the maximize, minimize, and drop-down menu buttons.              |   |   |
| KIconBar* iconBar();                           | Get window title, icon combination controls to control related styles.                           |   |   |
| void setLayoutType(LayoutType type);           | Sets the layout structure type.                                        |   |   |



#### **4.1.1.2** KBubbleWidget
>
> Function description: KBubbleWidget, inherited from QWidget, is a window with a bubble tail

Body, you can specify the size, display direction and display position of the bubble tail, and you can also set the circle of the form

> Corners, frosted glass and transparency. Enabled since version 2.0.0.0.

<table style="width: auto;"><tbody><tr><td colSpan="1" rowSpan="2" width="75">enumeration type</td><td colSpan="1" rowSpan ="1" width="492">enum TailDirection{ TopDirection, LeftDirection, BottomDirection, RightDirection, None };</td></tr><tr><td colSpan="1" rowSpan="1" width=" 492">enum TailLocation{ LeftLocation, MiddleLocation, RightLocation };</td></tr></tbody></table>


> TailDirection: The display direction of the tail of the bubble
>
> TailLocation: Specify the display position of the tail of the bubble


| statement                                                                                | describe                   |
|-----------------------------------------------------------------------------------|----------------------|
| void setTailSize(const QSize& size);                                              | Sets the bubble tail size             |
| QSize tailSize();                                                                 | Get the bubble tail size             |
| void setTailPosition(TailDirection dirType, TailLocation locType=MiddleLocation); | Set the display position of the bubble tail           |
| TailDirection tailDirection();                                                    | Get the display direction of the bubble tail (left, up, right, down)  |
| TailLocation tailLocation();                                                      | Get the display position of the bubble tail (left, center, right) |
| void setBorderRadius(int bottomLeft,int topLeft,int topRight,int bottomRight);    | Set the form corner radius             |
| void setBorderRadius(int radius);                                                 | Set the form corner radius             |
| void setEnableBlur(bool flag);                                                    | Set whether to enable the frosted glass effect          |
| bool enableBlur();                                                                | Get whether the frosted glass effect is enabled         |
| void setOpacity(qreal opacity);                                                   | set transparency                |
| qreal opacity();                                                                  | get transparency                |


### **4.1.2** Dialog Module

#### **4.1.2.1** Basic Dialog

> Function description: KDialog, inherited from QDialog, supports switching of responsive theme and background, corresponding icon main

Title switching, the window button style conforms to the design style of ukui3.1, and the title color responds to the activation state of the window.


| statement                                           | describe                                                                    |
|----------------------------------------------|-----------------------------------------------------------------------|
| void setWindowIcon(const QIcon &icon);       | Settings dialog icon                                                               |
| void setWindowIcon(const QString& iconName); | Set the window icon directly according to the icon name, for example:dialog->setWindowIcon("kylin-music");               |
| void setWindowTitle(const QString &);        | Set the dialog title name                                                             |
| QPushButton* maximumButton();                | Get the maximize button                                                               |
| QPushButton* minimumButton();                | Get the minimize button                                                               |
| QPushButton* closeButton();                  | get the close button                                                                |
| KMenuButton* menuButton();                   | Get the drop-down menu button, which is hidden by default and not displayed。                                                  |
| QWidget* mainWidget();                       | Get the main content area and add content through setLayout(). For example:dialog->mainWidget()->setLayout(hLayout); |



#### **4.1.2.2** About Dialog
>
> Function description: KAboutDialog, the main contents include: application icon, application name, version

ID, team mailbox and specific application description, note that the default application description is not displayed. can pass

> Control whether it needs to be displayed through setBodyTextVisible(bool).
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image6.png)
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image7.png)
| statement                                             | describe                        |
|------------------------------------------------|---------------------------|
| void setAppIcon(const QIcon& icon);            | set application icon                  |
| void setAppName(const QString& appName);       | set application name                  |
| QString appName();                             | get application name                  |
| void setAppVersion(const QString& appVersion); | Set the application version number                 |
| QString appVersion();                          | Get application version number                 |
| void setBodyText(const QString& bodyText);     | Set application-specific description content             |
| QString bodyText();                            | Get application-specific description content             |
| void setAppSupport(const QString& appSupport); | Set service and support mailboxes, with default defaults           |
| QString appSupport();                          | Get Service and Support Email                 |
| void setBodyTextVisiable(bool flag);           | Set whether to display the description content                |
| void setAppPrivacyLabelVisible(bool flag);     | Set whether the privacy button is visible (enabled since version 1.2.0.10) |
| bool AppPrivacyLabelIsVisible();               | Returns whether the privacy button is visible (enabled since version 1.2.0.10) |



#### **4.1.2.3** Input Dialog
>
> Function description: input dialog KInputDialog, inherited from QDialog, refer to QInputDialog
>
> Source code, adjusted the layout and style of sub-controls, the function is the same as QInputDialog.
>
> (1)QString KInputDialog::getText(QWidget \*parent, const QString
> &label,
>
> QLineEdit::EchoMode mode, const QString &text, bool \*ok,
>
> Qt::WindowFlags flags, Qt::InputMethodHints inputMethodHints)
>
> Text input dialog, same as QInputDialog

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image8.png)

> (2)QString KInputDialog::getMultiLineText(QWidget \*parent,const
> QString&label,
>
> const QString &text, bool \*ok, Qt::WindowFlags flags,
>
> Qt::InputMethodHints inputMethodHints)
>
> Multi-line text input box, same as QInputDialog

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image9.png)

> (3)
```
int KInputDialog::getInt(QWidget *parent,const QString &label, int value,
int min, int max, int step, bool *ok, Qt::WindowFlags flags)
```
> Integer number input dialog, same as QInputDialog
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image10.png)
>
> (3)

```
double KInputDialog::getDouble(QWidget *parent,const QString &label,
double value, double minValue, double maxValue, 
int decimals, bool *ok, Qt::WindowFlags flags)
```

> Floating-point number input dialog, same as QInputDialog
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image11.png)
>
> enumerated type

| enum  | InputDialogOption { NoButtons, UseListViewForComboBoxItems, UsePlainTextEditForTextInput } |
|-------|--------------------------------------------------------------------------------------------|
| flags | InputDialogOptions                                                                         |
| enum  | InputMode { TextInput, IntInput, DoubleInput }                                             |



| constant                                         | describe                                                         |
|--------------------------------------------|------------------------------------------------------------|
| QInputDialog::NoButtons                    | Don't show OK and Cancel buttons                                                 |
| QInputDialog::UseListViewForComboBoxItems  | Use QListView instead of non-editable QComboxItems to display items set with SetComboxItems(). |
| QInputDialog::UsePlainTextEditForTextInput | Use QPlainTextEdit for multi-line text input. In this value was introduced in 5.2.                    |



| constant                        | describe            |
|---------------------------|---------------|
| QInputDialog::TextInput   | Used to enter text strings.    |
| QInputDialog::IntInput    | Used to enter integers.       |
| QInputDialog::DoubleInput | For floating-point numbers entered in double precision. |



| statement                                                                                                                                                                                                                                                          | describe                                        |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| void setInputMode(InputMode mode);                                                                                                                                                                                                                          | set input mode                                    |
| InputMode inputMode() const;                                                                                                                                                                                                                                | get input mode                                    |
| void setLabelText(const QString &text);                                                                                                                                                                                                                     | set label text                                    |
| QString labelText() const;                                                                                                                                                                                                                                  | get label text                                    |
| void setOption(InputDialogOption option, bool on = true);                                                                                                                                                                                                   | Setting Options                                      |
| bool testOption(InputDialogOption option) const;                                                                                                                                                                                                            | test options                                      |
| void setOptions(InputDialogOptions options);                                                                                                                                                                                                                | Setting Options                                      |
| InputDialogOptions options() const;                                                                                                                                                                                                                         | Get input box options                                   |
| void setTextValue(const QString &text);                                                                                                                                                                                                                     | set text value                                     |
| QString textValue() const;                                                                                                                                                                                                                                  | get text value                                     |
| void setTextEchoMode(QLineEdit::EchoMode mode);                                                                                                                                                                                                             | set text mode                                    |
| QLineEdit::EchoMode textEchoMode() const;                                                                                                                                                                                                                   | get text mode                                    |
| void setComboBoxEditable(bool editable);                                                                                                                                                                                                                    | Set the combo box to be editable                                  |
| bool isComboBoxEditable() const;                                                                                                                                                                                                                            | Return whether the combo box can be edited                               |
| void setComboBoxItems(const QStringList &items);                                                                                                                                                                                                            | Set combo box item                                   |
| QStringList comboBoxItems() const;                                                                                                                                                                                                                          | Return combo box list                                   |
| void setIntValue(int value);                                                                                                                                                                                                                                | Set the value of type int                                 |
| int intValue() const;                                                                                                                                                                                                                                       | Get the value of type int                                 |
| void setIntMinimum(int min);                                                                                                                                                                                                                                | Set the lowest value of type int                               |
| int intMinimum() const;                                                                                                                                                                                                                                     | Get the lowest value of int                                  |
| void setIntMaximum(int max);                                                                                                                                                                                                                                | Set the maximum value of int type                                |
| int intMaximum() const;                                                                                                                                                                                                                                     | Get the maximum value of int                                  |
| void setIntRange(int min, int max);                                                                                                                                                                                                                         | set int range                                   |
| void setIntStep(int step);                                                                                                                                                                                                                                  | set int steps                                   |
| int intStep() const;                                                                                                                                                                                                                                        | Get int steps                                   |
| void setDoubleValue(double value);                                                                                                                                                                                                                          | set double value                                 |
| double doubleValue() const;                                                                                                                                                                                                                                 | get double value                                 |
| void setDoubleMinimum(double min);                                                                                                                                                                                                                          | Set the double minimum value                               |
| double doubleMinimum() const;                                                                                                                                                                                                                               | Get the minimum value of double                               |
| void setDoubleMaximum(double max);                                                                                                                                                                                                                          | Set the maximum value of double                               |
| double doubleMaximum() const;                                                                                                                                                                                                                               | Get the maximum value of double                               |
| void setDoubleRange(double min, double max);                                                                                                                                                                                                                | set double range                                |
| void setDoubleDecimals(int decimals);                                                                                                                                                                                                                       | set two decimals                                    |
| int doubleDecimals() const;                                                                                                                                                                                                                                 | returns the number of decimal places                                    |
| void setOkButtonText(const QString &text);                                                                                                                                                                                                                  | Set confirm button text                                  |
| QString okButtonText() const;                                                                                                                                                                                                                               | Get the confirm button text                                  |
| void setCancelButtonText(const QString &text);                                                                                                                                                                                                              | Set cancel button text                                  |
| QString cancelButtonText() const;                                                                                                                                                                                                                           | get cancel button text                                  |
| void open(QObject *receiver, const char *member);                                                                                                                                                                                                           | This function connects one of its signals to the receiver and the slot specified by the member. The specific signal depends on the parameters specified in the member. |
| void setVisible(bool visible) override;                                                                                                                                                                                                                     | Whether the setting is visible                                    |
| QSize minimumSizeHint() const override;                                                                                                                                                                                                                     | Min Size Tips                                    |
| QSize sizeHint() const override;                                                                                                                                                                                                                            | size tips                                      |
| static QString getText(QWidget *parent,const QString &label,QLineEdit::EchoMode echo = QLineEdit::Normal,const QString &text = String(), bool *ok = nullptr,Qt::WindowFlags flags = Qt::WindowFlags(),Qt::InputMethodHints inputMethodHints = Qt::ImhNone); | get text                                      |
| static QString getMultiLineText(QWidget *parent,const QString &label,const QString &text = QString(), bool *ok = nullptr,Qt::WindowFlags flags = Qt::WindowFlags(),Qt::InputMethodHints inputMethodHints = Qt::ImhNone);                                    | get multiline text                                    |
| static QString getItem(QWidget *parent,const QString &label,const QStringList &items, int current = 0, bool editable = true,bool *ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags(),Qt::InputMethodHints inputMethodHints = Qt::ImhNone);             | get item                                      |
| static int getInt(QWidget *parent,const QString &label, int value = 0,int minValue = -2147483647, int maxValue = 2147483647,int step = 1, bool *ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags())                                                    | get int type text                                  |
| static double getDouble(QWidget *parent,const QString &label,double value = 0, double minValue = -2147483647,double maxValue = 2147483647, int decimals = 1, bool *ok = nullptr,Qt::WindowFlags flags = Qt::WindowFlags());                                 | Get double type text                              |
| void setDoubleStep(double step);                                                                                                                                                                                                                            | Set the number of double steps                                |
| double doubleStep() const;                                                                                                                                                                                                                                  | Get the number of double steps                                |
| void setPlaceholderText(const QString &);                                                                                                                                                                                                                   | Set the text content of PlaceholderText (enabled since 1.2.0.12)       |
| QString placeholderText() const; | Get the text content of PlaceholderText (enabled since 1.2.0.12) |



#### **4.1.2.4** Progress dialog
>
> Function description: KProgressDialog, inherited from QDialog, refer to QProgressDialog source code,
The style of the child control ProgressBar has been adjusted. You can set whether specific download information needs to be displayed, set the suffix of the progress value, etc.


```
KProgressDialog *progress2 = new KProgressDialog(tr("Download"),tr("Cancel"),0,100,this);
progress2->setSubContent("Downloading...");
progress2->setSuffix("MB");
progress2->setWindowTitle("Progress Dialog");
progress2->setWindowIcon("kylin-music");
progress2->setValue(50);
progress2->setShowDetail(false);
```


![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image12.png)
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image13.png)




| statement                                            | describe                                                                                                      |
|-----------------------------------------------|---------------------------------------------------------------------------------------------------------|
| void setLabel(QLabel *label);                 | Set the label to label. The progress dialog resizes to fit. The label becomes the ownership of the progress dialog and will be deleted if necessary, so don't pass the address of the object on the stack.                                        |
| void setCancelButton(QPushButton *button);    | Set the cancel button to the button, cancelButton. The progress dialog takes ownership of the button, and the button will be deleted if necessary, so instead of passing the address of an object on the stack, use new() to create the button. If nullptr is passed, the cancel button will not be displayed. |
| void setBar(QProgressBar *bar);               | Set the progress bar widget to bar. The progress dialog resizes to fit. The progress bar dialog takes ownership of the progress bar, which will be deleted if necessary, so don't use a progress bar allocated on the stack.                               |
| void setSuffix(const QString& suffix);        | Set the suffix of detail                                                                                             |
| void setShowDetail(bool flag);                | Set whether to display detailed information                                                                                              |
| int minimum() const;                          | return minimum                                                                                                   |
| int maximum() const;                          | return the maximum value                                                                                                   |
| int value() const                             | return the current value                                                                                                   |
| QString labelText() const;                    | Return to prompt content                                                                                                  |
| void setAutoReset(bool reset);                | Sets whether the progress dialog calls reset() immediately when value() equals maximum(), defaults to true.                                                       |
| bool autoReset() const;                       | Returns whether to reset automatically                                                                                                |
| void setAutoClose(bool close);                | Set whether to automatically close the dialog box                                                                                             |
| bool autoClose() const;                       | Returns whether to automatically close the dialog                                                                                             |
| QProgressBar* progressBar();                  | get progress bar                                                                                                   |
| void cancel()；                                | Cancel the progress bar.                                                                                                  |
| void reset()；                                 | Reset the progress bar.                                                                                                  |
| void setMaximum(int maximum)                  | Set the highest value represented by the progress bar, the default value is 100.                                                                                   |
| void setMinimum(int minimum)                  | Set the minimum value represented by the progress bar, the default value is 0.                                                                                     |
| void setRange(int minimum, int maximum)       | Set the range of the progress bar. If the maximum value is smaller than the minimum value, the minimum value becomes the only legal value. If the current value is outside the new range, reset the progress box with reset().                                            |
| void setValue(int progress)                   | Set the current progress value.                                                                                                |
| void setLabelText(const QString &text)        | Set the hint label text.                                                                                               |
| void setCancelButtonText(const QString &text) | Set the cancel button text.                                                                                               |
| void setSubContent(const QString &text)       | Set secondary content.                                                                                                 |





#### **4.1.2.5** Program uninstall dialog
>
> Function description: KUninstallDialog, the code is integrated from the Kylin installer, and supports displaying application diagrams

Logo, application name, package name, version number and other information, excluding the specific uninstallation behavior. Just pass the

> Enter the package name and the last two parameters of the version.


```
KUninstallDialog *uninstallDialog = new KUninstallDialog("browser360-cn-stable","104",this);
```


![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image14.png)


| statement                               | describe               |
|----------------------------------|------------------|
| QLabel* debAppNameLabel();       | Get the label of the application name     |
| QLabel* debNameLabel();          | Get the label of the package name       |
| QLabel* debIconLabel();          | Get the label of the application icon     |
| QLabel* debVersionLabel();       | Get the label of the package version      |
| QPushButton* uninstallButtton(); | Get the uninstall button pushbutton |




### **4.1.3** Input box module

#### **4.1.3.1** Password input box


> Function description: KPasswordEdit, supports switching between plain text/dark text of the input content, supports settings

Password verification result (blue border for normal input, green border for correct password, and green border for wrong password

> red border), supports loading status, and supports setting whether to enable clearButton.
>
![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image15.png)
>
> enumerated type


|       |                                           |
|-------|-----------------------------------------------|
| enum  | LoginState{Ordinary,LoginSuccess,LoginFailed} |

| statement                                       | describe                     |
|------------------------------------------|------------------------|
| void setState(LoginState state);         | set login status                 |
| LoginState state();                      | Return to login status                 |
| void setLoading(bool flag);              | Set whether to enable the loading state.            |
| bool isLoading();                        | Determine whether it is in the loading state             |
| QString placeholderText();               | Return the text content of placeholderText |
| void setPlaceholderText(QString&text);   | Set the text content of PlaceholderText |
| void setClearButtonEnabled(bool enable); | Set whether to enable ClearButton      |
| bool isClearButtonEnabled() const;       | Returns whether ClearButton is enabled     |
| void setEchoModeBtnVisible(bool enable); | Set whether EchoModeBtn is visible      |
| bool echoModeBtnVisible();               | Return whether EchoModeBtn is visible      |
| void setClearBtnVisible(bool enable);    | Set whether ClearBtn is visible         |
| bool clearBtnVisible();                  | Return whether ClearBtn is visible         |
| void setEnabled(bool);                   | Set whether KLineEdit is available        |


#### **4.1.3.2** Search input box
>
> Function description: You can set the text content and alignment of placeHolder, and the alignment of input text
>
> mode, whether to enable the clear button, etc.
>

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image16.png)



| statement                                                | describe                        |
|---------------------------------------------------|---------------------------|
| void setEnabled(bool);                            | Is the setting available                    |
| bool isEnabled();                                 | return is available                    |
| void setClearButtonEnabled(bool enable);          | Set whether to display the clear button                |
| bool isClearButtonEnabled()  const;               | Returns whether to display the clear button                |
| QStringplaceholderText() const;                   | return placeholder             |
| void setPlaceholderText(const QString &);         | Set the text of the placeholder          |
| Qt::Alignment placeholderAlignment() const;       | Returns the alignment of the placeholder        |
| void setPlaceholderAlignment(Qt::Alignment flag); | Set the alignment of the placeholder        |
| Qt::Alignment alignment() const;                  | Returns the alignment of the input text              |
| void setAlignment(Qt::Alignment flag)；            | Sets the alignment of the input text.              |
| void clear()；                                     | Clear the contents of the search box.                  |
| void setTranslucent(bool flag);                   | Set whether to enable translucent effect (enabled since 1.2.0.10)  |
| bool isTranslucent();                             | Get whether translucency is enabled (enabled since 1.2.0.10)  |



### **4.1.4** Button module

#### **4.1.4.1** Bordered buttons

> Function description: KBoderButton, inherited from QPushButton, is encapsulated in style. able to pass
>
> setPalette() to further adjust the style.
>
> Such as:


```
QPalette palette = m_pBtn1->palette();                 
palette.setColor(QPalette::ButtonText,QColor(255,0,0)); 
m_pBtn1->setPalette(palette);                          
```

> All interfaces in QPushButton are applicable and support four construction methods.


| statement                                                                                | describe                |
|-----------------------------------------------------------------------------------|-------------------|
| KBorderButton(QWidget* parent = nullptr);                                         | only one button         |
| KBorderButton(const QString &text, QWidget *parent = nullptr);                    | Construct a button with text     |
| KBorderButton(const QIcon &icon, const QString &text, QWidget *parent = nullptr); | Constructs a button with text and icon |
| KBorderButton(const QIcon &icon, QWidget *parent = nullptr);                      | Construct a button with an icon    |
| void setIcon(const QIcon &icon);                                                  | Settings button icon            |


#### **4.1.4.2** Borderless buttons
>
> Function description: KboderlessButton, inherited from QPushButton, is encapsulated in style. same
>
> You can further adjust the style through setPalette().

![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image17.png)

> All interfaces in QPushButton are applicable and support four construction methods.


| statement                        | describe           |
|---------------------------------------------------------------------------------------|-------------------|
| KBorderlessButton(QWidget* parent = nullptr);                                         | only one button         |
| KBorderlessButton(const QString &text, QWidget *parent = nullptr);                    | Construct a button with text     |
| KBorderlessButton(const QIcon &icon, const QString &text, QWidget *parent = nullptr); | Constructs a button with text and icon |
| KBorderlessButton(const QIcon &icon, QWidget *parent = nullptr);                      | Construct a button with an icon    |
| void setIcon(const QIcon &icon)                                                       | Set borderless button icons.        |



#### **4.1.4.3** Drop-down menu button
>
> Function description: KMenuButton, inherited from QPushButon, the default ICon is "open-menu-symbolic", the first-level menu contains 5 options, namely: "Settings", "Theme", "Help", "About", " Exit", the theme includes 3 secondary options, namely: "Follow Theme", "Light Theme", and "Dark Theme".

![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image18.png)

> Each interface of QPushButton applies.


| statement                        | describe           |
|---------------------------|--------------|
| QMenu* menu();            | get main menu        |
| QMenu* themeMenu();       | Get theme menu       |
| QAction* settingAction(); | Get settings action   |
| QAction* themeAction();   | Get theme Action   |
| QAction* assistAction();  | Get HelpAction   |
| QAction* aboutAction()    | Get About Action   |
| QAction* quitAction();    | Get leave Action   |
| QAction* autoAction();    | Get the following topic Action |
| QAction* lightAction();   | Get light theme Action |
| QAction* darkAction();    | Get dark theme Action |


#### **4.1.4.4** switch button
>
> Function description: KSwitchButton, inherited from QPushButton, redraws the button to indicate the on/off state.
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image19.png)

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image20.png)


| statement                              | describe                       |
|---------------------------------|--------------------------|
| void setCheckable(bool);        | Whether the setting is optional                  |
| bool isCheckable() const;       | Returns whether it is selectable                  |
| bool isChecked() const;         | Return whether selected                   |
| void setChecked(bool);          | Is the setting selected                   |
| void setTranslucent(bool flag); | Set whether to enable translucent effect (enabled since 1.2.0.10) |
| bool isTranslucent()            | Get whether translucency is enabled (enabled since 1.2.0.10) |



#### **4.1.4.5** Tool Buttons
>
> Function description: KToolButton, inherited from QToolButton, supports three styles, supports loading status, and supports whether to display the drop-down button.
>
![Enter image description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image21.png)
>
> enumerated type

|       |                                           |
|-------|-------------------------------------------|
| enum  | KToolButtonType{Flat,SemiFlat,Background} |


| statement                                                                             | describe                   |
|---------------------------------------|---------------------------------|
| KToolButtonType type();               | return type                            |
| void setType(KToolButtonType type);   | setting type                            |
| void setIcon(const QIcon& icon);      | Set Icon                          |
| void setLoading(bool flag);           | Set the loading state, which is only supported by toolbuttuons without arrows |
| bool isLoading();                     | Returns whether it is loading                        |
| QIcon icon();                         | getIcon                          |
| void setArrow(bool flag);             | Set whether to display the down arrow, the default is not displayed                |
| bool hasArrow() const;                | Returns whether to display the arrow                        |
| void setPixmapSize(const QSize&size); | Set pixmap size                      |
| QSize pixmapSize() const;             | Get pixmap size                      |


#### **4.1.4.6** KPushButton
>
> Function description: KPushButton, inherited from QPushButton, provides a translucent effect on the basis of QPushButton. You can set the button rounded corners, background color, whether the icon follows the system highlight, and you can also set the button type, which is divided into normal type and round shape (the size needs to be set to achieve a perfect circle). Enabled since version 1.2.0.10.

![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image22.png)

> enumerated type


|       |                                           |
|-------|----------------------------------------|
| enum  | ButtonType { NormalType, CircleType }; |

| statement                                                                             | describe                   |
|--------------------------------------------------------------------------------|-------------------------------------|
| void setBorderRadius(int radius);                                              | Set button rounded corners by corner radius (same for each corner)                |
| void setBorderRadius(int bottomLeft,int topLeft,int topRight,int bottomRight); | Set rounded corners by four points                          |
| int borderRadius();                                                            | Get button rounded corners                              |
| void setBackgroundColor(QColor color);                                         | Set button background color                             |
| QColor backgroundColor();                                                      | Get button background color                             |
| void setButtonType(ButtonType type);                                           | Set the type of KPushButton                    |
| ButtonType buttonType();                                                       | Get the type of KPushButton                    |
| void setTranslucent(bool flag);                                                | Set whether the KPushButton is translucent                 |
| bool isTranslucent();                                                          | Determine whether KPushButton is translucent                 |
| void setIconHighlight(bool flag);                                              | Set whether the icon follows the system highlight color or not by default                 |
| bool isIconHighlight();                                                        | Determine whether the icon follows the system highlight color                       |
| void setIconColor(QColor color);                                               | Set the color of the button add icon (enabled since 1.2.0.13)            |
| QColor IconColor();                                                            | Get the color of the button add icon (enabled since 1.2.0.13)            |
| void setBackgroundColorHighlight(bool flag);                                   | Set whether the background color of the button follows the system highlight color or not by default (enabled since 1.2.0.13) |
| bool isBackgroundColorHighlight();                                             | Determine whether the background color of the button follows the system highlight color (enabled since 1.2.0.13)       |


#### **4.1.4.7** KPressButton
>
> Function description: KPressButton, inherited from QPushButton, provides translucent effect on the basis of QPushButton, can set rounded corners, whether it is selectable, whether it is selected, and can also set the type of button and whether to enable the loading state. Enabled since version 1.2.0.10.
>
![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image23.png)
>
> enumerated type


|       |                                           |
|-------|----------------------------------------|
| enum  | ButtonType { NormalType, CircleType }; |

| statement                                                                             | describe                   |
|--------------------------------------------------------------------------------|----------------------|
| void setBorderRadius(int radius);                                              | Set button rounded corners by corner radius (same for each corner) |
| void setBorderRadius(int bottomLeft,int topLeft,int topRight,int bottomRight); | Set rounded corners by four points           |
| void setCheckable(bool);                                                       | Whether the setting is optional              |
| bool isCheckable() const;                                                      | Returns whether it is selectable              |
| void setChecked(bool);                                                         | Is the setting selected               |
| bool isChecked() const;                                                        | Return whether selected               |
| void setButtonType(ButtonType type);                                           | Set the button type           |
| ButtonType buttonType();                                                       | Get the type of KPushButton     |
| void setTranslucent(bool flag);                                                | Set whether the KPushButton is translucent  |
| bool isTranslucent();                                                          | Determine whether KPushButton is translucent  |
| void setLoaingStatus(bool flag);                                               | Set whether to enable the loading state      |
| bool isLoading();                                                              | Return whether loading status is enabled      |




### **4.1.5** Bar module
#### **4.1.5.1** KIconBar

> Function description: A component of KWidget and KDialog, used to display icons and window names.
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image24.png)



| statement                                                    | describe                  |
|-----------------------------------------------|-----------|
| void setIcon(const QString& iconName);        | set icon name    |
| void setIcon(const QIcon& icon);              | settings icon      |
| void setWidgetName(const QString& widgetName) | set title      |
| QLabel* nameLabel();                          | Get title label |
| QLabel* iconLabel();                          | Get icon label |



#### **4.1.5.2** KWindowButtonBar
>
> Function description: A component of KWidget and KDialog, used for drop-down menu, minimize, maximize, and close buttons.
>
> enumerated type


|       |                                           |
|-------|-------------------------------------|
| enum  | MaximumButtonState{Maximum,Restore} |



![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image25.png)


| statement                                                    | describe                  |
|-------------------------------------------------------|---------------------|
| QPushButton* minimumButton();                         | Get the minimize button             |
| QPushButton* maximumButton();                         | Get the maximize button             |
| QPushButton* closeButton();                           | get the close button              |
| KMenuButton* menuButton();                            | get menu button              |
| MaximumButtonState maximumButtonState();              | Get the maximize the state of the button (maximize/restore)  |
| void setMaximumButtonState(MaximumButtonState state); | Set maximize button icon state (maximize/restore) |



#### **4.1.5.3** Progress bar
>
> Function description: KProgressBar, inherited from QProgressBar, supports three states: normal, completed and failed, supports whether to display the progress value, supports horizontal and vertical.
>
![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image26.png)
>
> enumerated type


|       |                                           |
|-------|-----------------------------------------------------------------|
| enum  | ProgressBarState{NormalProgress,FailedProgress,SuccessProgress} |

| statement                                     | describe   |
|----------------------------------------|------|
| ProgressBarState state();              | get status |
| void setState(ProgressBarState state); | set state |
| QString text() const override;         | get text |
| void setOrientation(Qt::Orientation);  | set direction |


#### **4.1.5.4** KProgressCircle
>
> The circular progress bar supports three states: normal, completed and failed, and supports whether to display the progress value.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image27.png)


| statement                                    | describe              |
|---------------------------------------|-----------------|
| int minimum() const                   | Get the minimum value of the circular progress bar.    |
| int maximum() const                   | Get the maximum value of the circular progress bar.    |
| int value() const                     | Get the value of the circular progress bar.      |
| QString text() const                  | Get the text of the circular progress bar.     |
| void setTextVisible(bool visible)     | Sets whether the text of the circular progress bar is visible. |
| bool isTextVisible() const            | Returns whether the text of the circular progress bar is visible. |
| ProgressBarState state()              | Get the status of the circular progress bar.     |
| void setState(ProgressBarState state) | Set the state of the circular progress bar.     |



#### **4.1.5.5** KTabBar
>
> Function description: Inherited from QTabBar
>
![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image28.png)
>
> enumerated type

|       |                                           |
|-------|------------------------------------------------|
| enum  | KTabBarStyle{SegmentDark,SegmentLight,Sliding} |

| statement                                                                 | describe                   |
|----------------------------------------------|---------------------------------------|
| void setTabBarStyle(KTabBarStyle barStyle);  | Set the TabBar style                            |
| KTabBarStyle barStyle();                     | Return TabBar style                            |
| void setBorderRadius(int radius);            | Set the corner radius, only valid for SegmentDark and SegmentLight styles |
| int borderRadius();                          | Get the corner radius                                |
| void setBackgroundColor(const QColor &color) | Set KTabBar background color.                         |


#### **4.1.5.6** Navigation Bar
>
> Functional description: KNavigationBar supports displaying three styles of items, the item with icon indicates the first-level navigation item, the item without icon indicates the second-level navigation item, and the item in gray font indicating the group.

| statement                                                                 | describe                   |
|--------------------------------------------------------------------|----------------------|
| void addItem(QStandardItem*item);                                  | Add regular Item             |
| void addSubItem(QStandardItem*subItem);                            | Add secondary Item             |
| void addGroupItems(QList<QStandardItem*>items,const QString& tag); | Add Items in groups, and tags will be displayed in the navigation bar |
| void addTag(const QString& tag);                                   | add tag                |
| QStandardItemModel* model();                                       | get model              |
| QListView* listview();                                             | get listview          |


#### **4.1.5.7** KPixmapContainer
>
> Function description: It is used to indicate the message prompt information on the upper right of the avatar. You can set the value, font size, background color, and picture size of the prompt information.
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image29.png)


| statement                                     | describe       |
|----------------------------------------|----------|
| int value() const;                     | get value      |
| void setValue(int value);              | Settings      |
| void setValueVisiable(bool flag);      | Whether the setting value is visible  |
| bool isValueVisiable() const;          | Gets whether the value is visible  |
| void setPixmap(const QPixmap& pixmap); | set pixmap |
| QPixmap pixmap()const;                 | get pixmap |
| void clearValue();                     | clear value      |
| QColor color();                        | return background color    |
| void setColor(const QColor& color);    | set background color    |
| int fontSize();                        | return font size   |
| void setFontSize(int size);            | set font size   |


### **4.1.6** Slider module
#### **4.1.6.1** Slider

> Function description:
>
> Non-step relationship: the number of steps is 1, you can click and drag anywhere;
>
> Step relationship: the number of steps is a fixed value, which can be clicked and dragged according to the step value;
>
> Node relationship: the number of steps is the node interval, which can be clicked and dragged according to the node interval
>
![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image30.png)
>
> enumerated type


|       |                                           |
|-------|--------------------------------------------------------------------|
| enum  | KSliderType{SmoothSlider,StepSlider,NodeSlider,SingleSelectSlider} |

| statement                                    | describe                       |
|---------------------------------------|--------------------------|
| void setTickInterval(int interval);   | set node interval                   |
| void setSliderType(KSliderType type); | Set the slider type                  |
| KSliderType sliderType();             | Get the slider type                  |
| int                                   | tickInterval() const;    | Get node interval                |
| void setValue(int);                   | Settings                      |
| void setNodeVisible(bool flag);       | Set whether to display nodes                 |
| bool nodeVisible();                   | Get whether to display the node                 |
| void setToolTip(const QString&);      | Set tooltip (enabled since 1.2.0.7)    |
| QString                               | toolTip() const;         | Get toolTip (enabled since 1.2.0.7) |
| void setTranslucent(bool flag);       | Set whether to enable translucent effect (enabled since 1.2.0.10) |
| bool isTranslucent();                 | Get whether translucency is enabled (enabled since 1.2.0.10) |



### **4.1.7** Message prompt module
#### **4.1.7.1** KBadge

> Function description: message reminder bubble. You can set the prompt information value, font size, background color.
>
![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image31.png)


| statement                                                                | describe                  |
|-------------------------------------|-----------------------------|
| int value();                        | return value                         |
| void setValue(int value);           | Setting value, the maximum display value is 999, if it is greater than 999, it will be displayed"..." |
| void setValueVisiable(bool flag);   | Whether the setting value is visible                     |
| bool isValueVisiable() const;       | Gets whether the value is visible                     |
| QColor color();                     | get background color                       |
| void setColor(const QColor& color); | set background color                       |
| int fontSize();                     | get font size                      |
| void setFontSize(int size);         | set font size                      |



#### **4.1.7.2** KBallonTip
>
> Function description: message prompt box, supports four background colors and corresponding prompt icons.

![Enter picture description](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image32.png)

> enumerated type


|       |                                           |
|-------|--------------------------------------------|
| enum  | TipType{Nothing,Normal,Info,Warning,Error} |

| statement                                                                | describe                  |
|-------------------------------------------------------------------|---------------------|
| void showInfo();                                                  | Bubbles disappear automatically after a certain period of time after displaying    |
| void setTipType(const TipType& type);                             | setting type                |
| TipType tipType();                                                | return type                |
| void setText(const QString& text);                                | set text content              |
| QString text();                                                   | return text content              |
| void setContentsMargins(int left, int top, int right, int bottom) | Set the content margin by up, down, left, and right values.     |
| void setContentsMargins(const QMargins &margins);                 | Set the content margins through the QMargins object. |
| void setTipTime(int my_time);                                     | set duration              |



#### **4.1.7.3** KSecurityLevelBar
>
> Function description: password strength prompt bar, used to indicate the password strength level. It is divided into three levels: low, medium and high, and the specific division strategy of the level is specified by the application program.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image33.png)

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image34.png)

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image35.png)

> enumerated type

|       |                                           |
|-------|--------------------------------|
| enum  | SecurityLevel{Low,Medium,High} |

| statement                                          | describe     |
|---------------------------------------------|--------|
| void setSecurityLevel(SecurityLevel level); | set security level |
| SecurityLevel securityLevel();              | get security level |


### **4.1.8** Breadcrumbs KBreadCrumb
>
> Borderless tab bar, you can add text and icons, support click and hover highlight.
>
> Function diagram:

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image36.png)

> enumerated type


|       |                                           |
|-------|----------------------------------------------------|
| enum  | KBreadCrumbType { FlatBreadCrumb, CubeBreadCrumb } |

| statement                               | describe                      |
|----------------------------------|-------------------------|
| void setIcon(const QIcon &icon); | settings icon                    |
| QIcon icon() const;              | back icon                    |
| bool isFlat() const;             | Returns whether KBreadCrumb is of flat type. |
| void  setFlat(bool flat);        | Set whether KBreadCrumb is flat type. |



### **4.1.9** KCommentPanel


|       |                                           |
|-------|-------------------------------------------------------------------------------|
| enum  | StarLevel {LevelZero =0, LevelOne, LevelTwo, LevelThree, LevelFour,LevelFive} |



> 枚举类型


| statement                              | describe     |
|---------------------------------|--------|
| void setIcon(const QIcon&);     | settings icon   |
| void setTime(const QString&);   | set comment time |
| void setName(const QString&);   | set comment name |
| void setText(const QString&);   | Set comment content |
| void setGrade(StarLevel level); | set comment level |


### **4.1.10** KListView
>
> Provide the display effect of a picture with two lines of text. If there is only one line of text, it will be displayed in the center of the icon.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image37.png)

### **4.1.11** KTag
>
> Labels are divided into default and closeable.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image38.png)

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image39.png)

> enumerated type

|       |                                           |
|-------|-----------------------------------------------------------|
| enum  | TagStyle { HighlightTag, BoderTag, BaseBoderTag, GrayTag} |

| statement                                | describe          |
|-----------------------------------|-------------|
| void setClosable(bool flag)       | Sets whether the label can be closed. |
| bool closable()                   | Returns whether the tab can be closed. |
| void setText(const QString &text) | Set the text of the label.    |
| void setTagStyle(TagStyle style)  | Set the style of the label.    |
| TagStyle tagStyle()               | Get the label's style.    |
| QString text()                    | Get the label's text.    |



### **4.1.12** KTranslucentFloor
>
> KTranslucentFloor, inherited from QFrame, provides a frosted glass bottom plate, which can set rounded corners and whether to add shadow effects. Enabled since version 1.2.0.12.

![输入图片说明](assets/openKylin%20SDK%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97V2.0/image40.png)


| statement                                  | describe                       |
|-------------------------------------|--------------------------|
| void setBorderRadious(int radious); | set corner radius                   |
| int borderRadius();                 | Returns the corner radius                   |
| void setShadow(bool flag);          | Set whether to show shadows                 |
| bool shadow();                      | Return whether to show shadow                 |
| void setEnableBlur(bool flag);      | Set whether to enable frosted glass effect (enabled since 2.0.0.0)  |
| bool enableBlur();                  | Get whether the frosted glass effect is enabled (enabled since 2.0.0.0) |
| void setOpacity(qreal opacity);     | Set transparency (enabled since 2.0.0.0)        |
| qreal opacity();                    | Get transparency (enabled since 2.0.0.0)        |


## 4.2 Wayland-helper
>
> This module is mainly responsible for providing a compatible interface that can take effect under both the X platform and the Wayland platform, so that the application does not need to consider the difference of the display platform. The module is installed as follows:
```
sudo apt install libkysdk-waylandhelper libkysdk-waylandhelper-dev
```
> According to different project types, you can refer to the following demos to build projects:
>
> (1) .pro file build project:
>
> Add in the qt project .pro file:


```
CONFIG += link_pkgconfig         
PKGCONFIG += kysdk-waylandhelper 
```


> （2） CMakeLists.txt build project


```
cmake_minimum_required(VERSION 3.5)
find_package(Qt5 COMPONENTS Widgets REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(KYSDKWAYLANDHELPER_PKG kysdk-waylandhelper)
target_include_directories(demo PRIVATE ${KYSDKWAYLANDHELPER_PKG_INCLUDE_DIRS})
target_link_directories(demo PRIVATE ${KYSDKWAYLANDHELPER_PKG_LIBRARY_DIRS})
target_link_libraries(demo Qt5::Widgets ${KYSDKWAYLANDHELPER_PKG_LIBRARIES})
```


### **4.2.1** WindowManager


| statement                                                              | describe                                              |
|-----------------------------------------------------------------|-------------------------------------------------|
| static WindowManager* self();                                   | Get the singleton object                                          |
| static WindowInfo getwindowInfo(const WindowId& windowId);      | Get window information                                          |
| static WindowId currentActiveWindow();                          | Get the current active window                                        |
| static void keepWindowAbove(const WindowId& windowId);          | top window                                            |
| static QString getWindowTitle(const WindowId& windowId);        | Get window title                                          |
| static QIcon getWindowIcon(const WindowId& windowId);           | Get window icon                                          |
| static QString getWindowGroup(const WindowId& windowId);        | Get the group name of the group where the window is located                                      |
| static void closeWindow(const WindowId& windowId);              | close the window                                            |
| static void activateWindow(const WindowId& windowId);           | activate window                                            |
| static void maximizeWindow(const WindowId& windowId);           | maximize window                                           |
| static void minimizeWindow(const WindowId& windowId);           | minimize window                                           |
| static quint32 getPid(const WindowId& windowId);                | Get the window process pid                                       |
| static void showDesktop();                                      | show current desktop                                          |
| static void hideDesktop();                                      | undisplay current desktop                                        |
| static QString currentDesktop();                                | Get the name of the current desktop                                       |
| static QList<WindowId> windows();                               | Get current window list                                        |
| static NET::WindowType getWindowType(const WindowId& windowId); | Get the window type, only applicable in X environment, return normal in wayland              |
| static void                                                     | setGeometry(QWindow *window,const QRect &rect); | set window position |
| static void setSkipTaskBar(QWindow *window,bool skip);          | Set whether to skip the taskbar (enabled since 2.0.0.0)                           |
| static  void setSkipSwitcher(QWindow *window,bool skip);        | Set whether to skip window selection (enabled since 2.0.0.0)                          |
| static bool skipTaskBar(const WindowId& windowId);              | Determine whether the form skips the taskbar (enabled since 2.0.0.0)                         |
| static bool skipSwitcher(const WindowId& windowId);             | Determine whether the form skips window selection (enabled since 2.0.0.0)                        |
| static bool isShowingDesktop();                                 | Determine whether the desktop is displayed (enabled since 2.0.0.0)                        |
| static void setOnAllDesktops(const WindowId &windowId);         | Settings window is shown in all desktops (enabled since 2.0.0.0)                        |
| static bool isOnAllDesktops(const WindowId &windowId);          | Determine whether the window is displayed on all desktops (enabled since 2.0.0.0)                      |



### **4.2.2** WindowInfo


| statement                                             | describe             |
|------------------------------------------------|----------------|
| inline bool isMaximized() const;               | Returns whether the window is maximized   |
| inline bool isMinimized() const;               | Returns whether the window is minimized   |
| inline bool isMaximizable() const;             | Returns whether the window can be maximized    |
| inline bool isMinimizable() const;             | Returns whether the window can be minimized    |
| inline bool isKeepAbove() const;               | Returns whether the window is in the top state    |
| inline bool hasSkipTaskbar() const;            | Returns whether the window skips the taskbar    |
| inline bool isFullscreen() const;              | Returns whether the window is full screen    |
| inline bool isOnAllDesktops() const； noexcept; | Returns whether the window is shown on all desktops |
| inline bool isValid() const;                   | Determine whether the window id is valid     |
| inline bool isActive() const noexcept;         | Returns whether the window is active    |



### **4.2.3** UkuiStyleHelper


| statement                                  | describe      |
|-------------------------------------|---------|
| static UkuiStyleHelper *self();     | Get the singleton object  |
| void removeHeader(QWidget* widget); | Remove window title bar |



## 4.3 Application general function module

### 4.3.1 Log module


| submodule  | Programming language | statement                                       | describe         |
|------|------|--------------------------------------------------------------------------------------------------|--------------------|
| log module | C++  | static void logOutput(QtMsgType type , const QMessageLogContext &context , const QString &msg); | Used by Qt to register logging functions and should not be called alone. |
|      |      | Parameter Description                                                                                             | Return value description              |
|      |      | type:Log type context: call print log interface file information msg: log information                                                        | none                  |



### 4.3.2 System related modules

| submodule  | Programming language | statement                                       | describe         |
|------|------|---------------------------------------------|------------|
| window tube module | C++  | static bool setWindowMotifHint(int winId); | Add window protocol .   |
|      |      | Parameter Description                                        | Return value description      |
|      |      | winId : window id                               | success : true  |
|      |      |                                             | fail : false |



| submodule  | Programming language | statement                                       | describe         |
|------|------|------------------------------------------|------------|
| window tube module | C++  | static bool setScalingProperties(void); | Set window scaling properties   |
|      |      | Parameter Description                                     | Return value description      |
|      |      | none                                        | success : true  |
|      |      |                                          | fail : false |

| submodule  | Programming language | statement                                       | describe         |
|-----------|------|-----------------------------------------------------------------|---------------|
| session module | C++  | quint32 setInhibitLockScreen(AppName appName , QString reason); | Prohibit system lock screen .      |
|           |      | Parameter Description                                                            | Return value description         |
|           |      | appName : application name                                                   | success : a positive integer other than 0 |
|           |      | reason : Reasons why the lock screen is prohibited                                                | fail : 0        |



| submodule  | Programming language | statement                                       | describe         |
|-----------|------|----------------------------------------|------------|
| session module | C++  | bool unInhibitLockScreen(quint32 flag) | Unblock system lock screen . |
|           |      | Parameter Description                                   | Return value description      |
|           |      | flag : Prohibition of lock screen logo                          | success : true  |
|           |      |                                        | fail : false |



### 4.3.3 d-bus Module ------ will be obsolete soon


| submodule    | Programming language | statement                                                                                                                                                                                              | describe       |
|-----------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| d-bus module | C++      | static QList<QVariant> callMethod(QString serviceName, QString objectPath, QString interfaceName,                                  QString methodName, QList<QVariant> args = QList<QVariant>()); | 函数调用   |
|           |          | Parameter Description                                                                                                                                                                                          | Return value description |
|           |          | serviceName : Service Name                                                                                                                                                                              | 函数返回值 |
|           |          | objectPath : object path                                                                                                                                                                             |            |
|           |          | interfaceName : interface name                                                                                                                                                                            |            |
|           |          | methodName : Function name                                                                                                                                                                               |            |
|           |          | args : parameter list                                                                                                                                                                                   |            |



### 4.3.4 System Information Module


| submodule    | Programming language | statement                                              | describe                            |
|--------|------|-------------------------------------------------|-------------------------------|
| System Information Module | C++  | QString getLsbReleaseInformation(QString key); | Obtain information according to the key value of the lsb-release file |
|        |      | Parameter Description                                            | Return value description                         |
|        |      | key : key value                                        | The obtained information, if it is empty, it may be indeed empty or it may fail       |




| submodule    | Programming language | statement                                             | describe                           |
|--------|------|------------------------------------------------|------------------------------|
| System Information Module | C++  | QString getOsReleaseInformation(QString key); | Get information according to the key value of the os-release file |
|        |      | Parameter Description                                           | Return value description                        |
|        |      | key : key value                                       | The obtained information, if it is empty, it may be indeed empty or it may fail      |


| submodule    | Programming language | statement                                | describe                       |
|--------|------|-----------------------------------|--------------------------|
| System Information Module | C++  | QString getProjectCodeName(void) | Get the value of the PROJECT_CODENAME field |
|        |      | Parameter Description                              | Return value description                    |
|        |      | none                                 | empty : failed                   |
|        |      |                                   | non-empty: the obtained value               |




| submodule    | Programming language | statement                             | describe         |
|--------|------|--------------------------------|------------|
| System Information Module | C++  | QString getCpuModelName(void) | Get CPU model  |
|        |      | Parameter Description                           | Return value description      |
|        |      | none                              | empty : failed     |
|        |      |                                | non-empty: the obtained value |




| submodule    | Programming language | statement                            | describe         |
|--------|------|-------------------------------|------------|
| System Information Module | C++  | QString getHdPlatform(void); | Obtain hardware platform information   |
|        |      | Parameter Description                          | Return value description      |
|        |      | none                             | empty : failed     |
|        |      |                               | non-empty: the obtained value |



# 5 Basic Development SDK
>
> The design of this layer mainly provides an operating system-independent, highly versatile, and basic function set for application development. Reduce the difference and complexity of different applications in realizing the same function. The design of this layer mainly provides application development with a set of functions that are independent of the operating system, highly versatile, and fundamental. Reduce the difference and complexity of different applications in realizing the same function.
>
> Install command:


```
$ sudo apt install libkysdk-base libkysdk-base-dev 
```


## **5.1** log
>
> The C language log module provides eight levels of logging interfaces, and non-root programs are recorded in \~/.log
>
> directory, the root program is recorded under /var/log


```
$ sudo apt-get install libkysdk-log llibkysdk-log-dev 
```


> (1) CMakeLists.txt build project
>
```
aux_source_directory(. SOURCESCODE)
add_library(kylog SHARED ${SOURCESCODE})
add_executable(kylog-testlog test/test-log.c)
add_executable(kylog-testsetdir test/test-setdir.c)
add_executable(kylog-testpressure test/test-pressure.c)
add_executable(kylog-testautowrap test/test-autowrap.c)
find_library(SYSTEMD_LIB systemd)
target_link_libraries(kylog kyconf pthread ${SYSTEMD_LIB})
target_link_libraries(kylog-testlog kylog)
target_link_libraries(kylog-testsetdir kylog)
target_link_libraries(kylog-testautowrap kylog)
target_link_libraries(kylog-testpressure kylog)
```
> (2) To call the log module, you need to add a header file:
>
```
#include "kysdk/kysdk-base/libkylog.h"
```
>
> output log


| type            | statement                              |
|---------------|---------------------------------|
| Output debug level log   | #define klog_debug(fmt , ...)   |
| Output info level log    | #define klog_info(fmt , ...)    |
| Output notice level log  | #define klog_notice(fmt , ...)  |
| Output warning level log | #define klog_warning(fmt , ...) |
| Output error level log   | #define klog_err(fmt , ...)     |
| Output crit level log    | #define klog_crit(fmt , ...)    |
| Output alert level log   | #define klog_alert(fmt , ...)   |
| Output emerg level log   | #define klog_emerg(fmt , ...)   |



> Initialize logging

| submodule | Programming language | statement          | describe                             |
|-----|------|-----------------------------------|----------------------------------|
| log  | C    | extern int                        | kdk_logger_init(const char *ini) | Initialize the log record, or directly use the above log record macro without calling this function, if run in this way, the program will use the default log configuration file |
|     |      | Parameter Description                              | Return value description                            |                                                       |
|     |      | ini：Log configuration file path, if NULL is passed in, the default log configuration file will be used | int，0 means success, non-zero means failure                 |


> Buffer log write

| submodule | Programming language | statement          | describe                             |
|-----|------|-------------|--------------------------------|
| log  | C    | extern void | kdk_logger_flush() DESTRUCTOR; | In the asynchronous writing mode, you can call this function to manually write the logs in the buffer to the file |
|     |      | Parameter Description        | Return value description                          |                                  |
|     |      | none           | none                              |



> Set the log storage directory

| submodule | Programming language | statement                                                | describe         |
|-----|------|-------------------------------------------------|---------------------------------------------------------|
| log  | C    | extern int kdk_logger_setdir(const char* dpath) | Set the log storage directory, if not set, non-root programs will be recorded in ~/.log, root programs will be recorded in /var/log |
|     |      | Parameter Description                                            | Return value description                                                   |              |
|     |      | dpath: log storage path                                    | int                                                     | Returns 0 on success, -1 on failure |



> Set log output to wrap automatically


| submodule | Programming language | statement                                                | describe         |
|-----|------|---------------------------------------------------|------------|
| log  | C    | extern void kdk_logger_set_autowrap(int autowarp) | Set log output to wrap automatically |
|     |      | Parameter Description                                              | Return value description      |   |
|     |      | autowarp：1 means start line wrapping is enabled, 0 means automatic line wrapping is disabled                      | none          |



## **5.2** Timer
>
> C language timer module, providing timer function interface for C/C++ program.


```
$ sudo apt-get install libkysdk-timer libkysdk-timer-dev 
```


> （1）CMakeLists.txt build project
>
```
aux_source_directory(. SOURCESCODE)
add_library(kytimer SHARED ${SOURCESCODE})
add_executable(test-kytimer test/test-kytimer.c)
target_link_libraries(kytimer pthread)
target_link_libraries(test-kytimer kytimer)
```

> （2）To call the timer module, you need to add a header file：
>
```
#include "kysdk/kysdk-base/libkytimer.h"
```
>
> Initialize timer

| submodule | Programming language | statement                       | describe    |
|-----|------|----------------------|------------------|
| timer | C    | int kdk_timer_init() | Initialize the timer core component       |
|     |      | Parameter Description                 | Return value description            |   |
|     |      | none                    | int，0 means success, non-zero means failure |


> start timer


| submodule | Programming language | statement                                                                                           | describe                                                                                                                                       |
|-----|------|----------------------|------------------|
| timer | C    | size_t                                                                                       | kdk_timer_start(unsigned int intervalms, time_handler callback, KTimerAttribute attr, KTimerType type, void* userdata, int freeOnDelete) |
|     |      | Parameter Description                                                                                         | Return value description                                                                                                                                    |
|     |      | intervalms：timer time in milliseconds                                                                      | The ID of the size_t timer                                                                                                                             |
|     |      | callback：The callback function pointer triggered after the timer expires                                                                     |                                                                                                                                          |
|     |      | attr：Timer attribute, KTIMER_SINGLESHOT indicates a one-time timer; KTIMER_PERIODIC indicates a periodic timer; KTIMER_NEVER indicates a timer that will not be triggered         |                                                                                                                                          |
|     |      | type：Timer type, KTIMER_ABSOLUTE indicates an absolute time timer, modifying the system time will not affect the timer time; KTIMER_RELATIVE indicates a relative time timer, modifying the system time will affect the timer time |                                                                                                                                          |
|     |      | userdata：pointer to user data                                                                           |                                                                                                                                          |
|     |      | freeOnDelete： [Not Enabled]                                                                          |                                                                                                                                          |



> stop the given timer


| submodule | Programming language | statement                       | describe    |
|-----|------|-------------------------------------|----------|
| timer | C    | void kdk_timer_stop(size_t timerfd) | stop the given timer |
|     |      | Parameter Description                                | Return value description    |   |
|     |      | timerfd：Timer ID returned by kdk_timer_start:   | none        |



> destroy timer


| submodule | Programming language | statement                       | describe    |
|-----|------|--------------------------|-------|
| timer | C    | void kdk_timer_destroy() | destroy timer |
|     |      | Parameter Description                     | Return value description |   |
|     |      | none                        | none     |



> reset timer


| submodule | Programming language | statement                                                             | describe    |
|-----|------|----------------------------------------------------------------|-------|
| timer | C    | void kdk_timer_reset(size_t timerfd , unsigned int intervalms) | reset timer |
|     |      | Parameter Description                                                           | Return value description |
|     |      | timerfd：Timer ID returned by kdk_timer_start                               | none     |
|     |      | intervalms：The time interval that needs to be adjusted, in ms                                    |       |



## **5.3** C language string extension
>
> This module encapsulates the C interface for string operations, including string cropping, splitting, case conversion, and searching for specific characters.

```
$ sudo apt-get install libkysdk-utils libkysdk-utils-dev 

```

> （1）To call the C language string extension module, a header file needs to be added：
>
```
#include "kysdk/kysdk-base/cstring-extension.h"
```
>
> split string


| submodule      | Programming language | statement                                                   | describe                                                                                                                          |
|----------|------|-----------------------------------------|--------------------|
| C language string extension | C    | static inline char** strsplit(char *str, char delim) | Split the original string with the given delimiter, note that this function will modify the original string                                                                                               |
|          |      | Parameter Description                                                 | Return value description                                                                                                                       |
|          |      | str:the string to split                                         | char** List of split strings, NULL terminated. The string list storing all the split strings itself is memory requested by alloc, so it should be freed after use; and the split strings are not the requested memory, but point to the specific memory in the original string. position, so they do not need to be freed separately |
|          |      | delim:delimiter                                            |                                                                                                                             |



> lowercase string

| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|-----------------------------------------|--------------------|
| C language string extension | C    | static inline void str2lower(char *str) | Converts all uppercase letters in a string to lowercase |
|          |      | Parameter Description                                    | Return value description              |   |
|          |      | str:A pointer to the string to operate on                          | none                  |



> uppercase string


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|-----------------------------------------|---------------------|
| C language string extension | C    | static inline void str2upper(char *str) | Convert all lowercase letters in a string to uppercase |
|          |      | Parameter Description                                    | Return value description               |   |
|          |      | str:A pointer to the string to operate on                          | none                   |



> count occurrences

| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|----------------------------------------------------------|------------------|
| C language string extension | C    | static inline size_t strcounts(const char *str, char ch) | Counts the number of occurrences of a given character in a string |
|          |      | Parameter Description                                                     | Return value description            |
|          |      | str:original string                                                 | size_t:the number of occurrences of the character   |
|          |      | ch:characters to be counted                                               |                  |


> Suffix judgment


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|--------------------------------------------------------------------|-------------------------------|
| C language string extension | C    | static inline int strendwith(const char *str, const char *postfix) | It is case-sensitive to determine whether str ends with postfix      |
|          |      | Parameter Description                                                               | Return value description                         |
|          |      | str:original string                                                           | int Returns 0 if str ends with postfix; otherwise returns 1 |
|          |      | postfix:The string suffix to match                                                 |                               |



> Find the first occurrence of a substring


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|--------------------------------------------------------|-------------------------------|
| C language string extension | C    | static inline int strfirstof(const char* str, char ch) | Finds the position of the first occurrence of the given character in the given string; counting starts at 0 |
|          |      | Parameter Description                                                   | Return value description                         |
|          |      | str:original string                                               | position of the first occurrence of int, or -1 if the given character is not found  |
|          |      | ch:character to look for                                             |                               |



> Find the last occurrence of a substring


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|-------------------------------------------------------|--------------------------------|
| C language string extension | C    | static inline int strlastof(const char* str, char ch) | Finds the position of the last occurrence of the given character in the given string; counting starts at 0 |
|          |      | Parameter Description                                                  | Return value description                          |
|          |      | str:original string                                              | position of the last occurrence of int, or -1 if the given character is not found  |
|          |      | ch:character to look for                                            |                                |



> remove spaces and tabs


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|---------------------------------------------|--------------------------------------|
| C language string extension | C    | static inline void strstripblank(char *str) | Delete spaces and horizontal tabs (tab) before and after the given string, note that this operation will modify the original string |
|          |      | Parameter Description                                        | Return value description                                |   |
|          |      | str:String pointer to modify                              | none                                    |



> Skip all leading spaces, tabs, newlines


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|-------------------------------------------------------|---------------------------------|
| C language string extension | C    | static inline const char *strskipspace(const char *p) | Skip all spaces, tabs, and newlines before the string; this operation does not modify the original string |
|          |      | Parameter Description                                                  | Return value description                           |   |
|          |      | p:pointer to the original string                                           | const char* Pointer to the character string after skipping the space    |



> Prefix judgment (case sensitive)


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|----------------------------------------------------------------------|------------------------------|
| C language string extension | C    | static inline int strstartswith(const char *str, const char *prefix) | Determine whether str starts with prefix, this function is case sensitive    |
|          |      | Parameter Description                                                                 | Return value description                        |
|          |      | str：original string                                                             | int Returns 0 if str begins with prefix; otherwise returns 1 |
|          |      | prefix：string prefix to match                                                    |                              |



> Prefix judgment (case insensitive)


| submodule      | Programming language | statement                                                                          | describe                           |
|----------|------|-----------------------------------------------------------------------------|------------------------------|
| C language string extension | C    | static inline int strstartswith_nocase(const char *str, const char *prefix) | Determine whether str starts with prefix; case insensitive      |
|          |      | Parameter Description                                                                        | Return value description                        |
|          |      | str:original string                                                                    | int Returns 0 if str begins with prefix; otherwise returns 1 |
|          |      | prefix:string prefix to match                                                           |                              |


> Delete the specified characters before and after the string


| submodule      | Programming language | statement                                              | describe                                         |
|----------|------|-------------------------------------------------|--------------------------------------------|
| C language string extension | C    | static inline void strstrip(char *str, char ch) | Perform a strip operation on a given string, and delete the specified characters before and after the string; note that this operation will modify the original string |
|          |      | Parameter Description                                            | Return value description                                      |
|          |      | str:The pointer to the string that needs to be stripped                             | none                                          |
|          |      | ch:characters to delete                                      |                                            |



> Skip all leading spaces, horizontal tabs


| submodule      | Programming language | statement                                                    | describe                                 |
|----------|------|-------------------------------------------------------|------------------------------------|
| C language string extension | C    | static inline const char *strskipblank(const char *p) | Skip all spaces and horizontal tabs before the string; this operation does not modify the original string |
|          |      | Parameter Description                                                  | Return value description                              |   |
|          |      | p: pointer to the original string                                           | const char* Pointer to the character string after skipping the space       |



> Delete leading and trailing spaces, tabs, newlines


| submodule      | Programming language | statement                                          | describe                                |
|----------|------|---------------------------------------------|-----------------------------------|
| C language string extension | C    | static inline void strstripspace(char *str) | Delete spaces, tabs, and newlines before and after the given string. Note that this operation will modify the original string |
|          |      | Parameter Description                                        | Return value description                             |   |
|          |      | str：A pointer to a string that needs to be stripped                       | none                                 |


## **5.4** Linked list module
>
> This module implements the related interface of linked list operation in C language.

```
$ sudo apt-get install libkysdk-utils libkysdk-utils-dev
```


> (1) CMakeLists.txt build project
>
```
aux_source_directory(linklist/skip_linklist SOURCESCODE)
add_library(kydatastruct SHARED ${SOURCESCODE})
add_subdirectory(linklist)
include_directories(linklist)
include_directories(linklist/skip_linklist)
add_executable(test-delete linklist/skip_linklist/test/delete_test.c)
add_executable(test-insert linklist/skip_linklist/test/insert_test.c)
add_executable(test-search linklist/skip_linklist/test/search_test.c)
target_link_libraries(test-delete kydatastruct)
target_link_libraries(test-insert kydatastruct)
target_link_libraries(test-search kydatastruct)
```
>
> （2）To call the timer module, you need to add a header file：
>
```
#include "kysdk/kysdk-base/skip_linklist.h"
```
>
> Create skip list


|     |      |                                                |                     |
|-----|------|------------------------------------------------|---------------------|
| submodule | Programming language | statement                                             | describe                  |
| linked list  | C    | extern kysdk_skiplist* kysdk_create_skiplist() | Create skip list                |
|     |      | Parameter Description                                           | Return value description               |   |
|     |      | none                                              | kysdk_skiplist*  jump table |


> destroy jump list


|     |      |                                                          |                                           |
|-----|------|----------------------------------------------------------|-------------------------------------------|
| submodule | Programming language | statement                                                       | describe                                        |
| linked list  | C    | extern void kysdk_destroy_skiplist(kysdk_skiplist *list) | Destroy the jump list and reclaim all allocated memory; note that the memory pointed to by data.ptr (if it exists) will not be released |
|     |      | Parameter Description                                                     | Return value description                                     |   |
|     |      | list：jump table                                                  | none                                         |



> Delete the node corresponding to the key value


|     |      |                                                                 |                     |
|-----|------|-----------------------------------------------------------------|---------------------|
| submodule | Programming language | statement                                                              | describe                  |
| linked list  | C    | extern int kysdk_skiplist_delete(kysdk_skiplist *list, int key) | Delete the node corresponding to the key value         |
|     |      | Parameter Description                                                            | Return value description               |
|     |      | list：linked list to be manipulated                                                    | int, Successful deletion returns 0, failure returns -1 |
|     |      | key：key                                                           |                     |



> insert node


|     |      |                                                                                      |                    |
|-----|------|--------------------------------------------------------------------------------------|--------------------|
| submodule | Programming language | statement                                                                                   | describe                 |
| linked list  | C    | extern int kysdk_skiplist_insert(kysdk_skiplist *list, int key, kysdk_listdata data) | insert node               |
|     |      | Parameter Description                                                                                 | Return value description              |
|     |      | list：linked list to be manipulated                                                                         | int，Successful insertion returns 0, failure returns -1 |
|     |      | key：key                                                                                |                    |
|     |      | data：data                                                                              |                    |


> Search the content of data according to the given key


|     |      |                                                                            |                                         |
|-----|------|----------------------------------------------------------------------------|-----------------------------------------|
| submodule | Programming language | statement                                                                         | describe                                      |
| linked list  | C    | extern kysdk_listdata kysdk_skiplist_search(kysdk_skiplist *list, int key) | Search data content according to the given key                        |
|     |      | Parameter Description                                                                       | Return value description                                   |
|     |      | list：linked list to be manipulated                                                               | kysdk_listdata，When the searched key does not exist, the value of data.num is -1 |
|     |      | key：key                                                                      |                                         |



> Set the highest level of skip table


|     |      |                                                                                      |                        |
|-----|------|--------------------------------------------------------------------------------------|------------------------|
| submodule | Programming language | statement                                                                                   | describe                     |
| linked list  | C    | extern int kysdk_skiplist_setmaxlevels(kysdk_skiplist *list, unsigned int maxlevels) | Set the highest level of the skip list, this option must be used when the skip list is empty |
|     |      | Parameter Description                                                                                 | Return value description                  |
|     |      | list：Jump table that needs to be modified                                                                         | int，Returns 0 on success, -1 on failure       |
|     |      | maxlevels：layers                                                                         |                        |



## **5.5** Configuration file operation
>
> KYSDK configuration file processing library, supports standard format, XML (not implemented), JSON (not implemented)
>
> Configuration file processing, including reading and writing operations of configuration files.
>
> sudo apt-get install libkysdk-config libkysdk-config-dev
>
> (1) CMakeLists.txt build project
>
```
aux_source_directory(. SOURCESCODE)
add_library(kyconf SHARED ${SOURCESCODE})
add_executable(kyconf-test-struct test/test_structlist.c)
target_link_libraries(kyconf-test-struct kyconf kylog pthread)
```
>
> (2) To call the configuration file module, a header file needs to be added:
>
```
#include "kysdk/kysdk-base/libkyconf.h"
```
>
> Initialize configuration file


| submodule    | Programming language | statement                                             | describe                           |
|--------|------|------------------------------------------------|------------------------------|
| Profile Operations | C    | extern int kdk_conf_init(const char* confpath) | Initialize the configuration file                      |
|        |      | Parameter Description                                           | Return value description                        |   |
|        |      | confpath：The path to the configuration file                               | int Successfully returns the handle number (non-negative value), fails to return an error code (negative value) |



> Destroy the specified configuration file handle


| submodule    | Programming language | statement                                   | describe          |
|--------|------|--------------------------------------|-------------|
| Profile Operations | C    | extern void kdk_conf_destroy(int id) | Destroys the specified profile handle |
|        |      | Parameter Description                                 | Return value description       |   |
|        |      | id：The configuration file handle returned by kdk_conf_init           | none           |



> reload configuration file


| submodule    | Programming language | statement                                 | describe                |
|--------|------|------------------------------------|-------------------|
| Profile Operations | C    | extern int kdk_conf_reload(int id) | reload configuration file          |
|        |      | Parameter Description                               | Return value description             |   |
|        |      | id：The configuration file handle returned by kdk_conf_init         | int Returns 0 on success, returns an error code on failure |



> Get the value of the specified configuration item


| submodule    | Programming language | statement                                                                                | describe                                     |
|--------|------|-----------------------------------------------------------------------------------|----------------------------------------|
| Profile Operations | C    | extern const char* kdk_conf_get_value(int id, const char* group, const char* key) | Get the value of the specified configuration item                              |
|        |      | Parameter Description                                                                              | Return value description                                  |
|        |      | id：The configuration file handle returned by kdk_conf_init                                                        | const char* The value owned by the configuration item, if the key does not exist, an empty string will be returned |
|        |      | group：The group name of the key                                                                   |                                        |
|        |      | key：Configuration item name                                                                         |                                        |



> enumeration key value


| submodule       | Programming language | statement                                                             | describe                                                                                                                                      |
|--------------|----------|------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| Profile Operations | C        | extern char** const kdk_conf_list_key(int id, const char* group) | List the key values under the specified Group of the configuration file corresponding to the id, and end with a NULL pointer                                                                                |
|              |          | Parameter Description                                                         | Return value description                                                                                                                                |
|              |          | id：Handle value returned by kdk_conf_init                                  | const char** const is a NULL-terminated string list, each string is a key name, and the string list itself is memory allocated by alloc and needs to be released by free; the string does not need to be released |
|              |          | group：Group name to be listed                                       |                                                                                                                                           |



> Enumerate the Group of the configuration file


| submodule    | Programming language | statement                                              | describe                                                                                   |
|--------|------|-------------------------------------------------|--------------------------------------------------------------------------------------|
| Profile Operations | C    | extern char** const kdk_conf_list_group(int id) | List all the groups whose id corresponds to the configuration file, and end with a NULL pointer                                                       |
|        |      | Parameter Description                                            | Return value description                                                                                |   |
|        |      | id：Handle value returned by kdk_conf_init                         | const char** const NULL-terminated string list, each string is a group name, the string list itself is memory allocated by alloc and needs to be released by free; the string does not need to be released |



## **5.6** Unit conversion


```
$ sudo apt-get install libkysdk-utils libkysdk-utils-dev
```


> (1) To call the C language string extension module, a header file needs to be added:
>
```
#include "kysdk/kysdk-base/cstring-extension.h"
```
>
> Character format unit conversion

| submodule    | Programming language | statement                                                                                                                   | describe              |
|--------|------|----------------------------------------------------------------------------------------------------------------------|-----------------|
| Profile Operations | C    | extern int kdkVolumeBaseCharacterConvert(const char* origin_data, KDKVolumeBaseType result_base, char* result_data); | character format unit conversion      |
|        |      | Parameter Description                                                                                                                 | Return value description           |
|        |      | Entry parameters:                                                                                                                  | Int type value, whether the interface execution is successful |
|        |      | origin_data:Data of the original character type, ending with a specific base unit, if not, the default is 'B'                                                                          | 0 - no exception           |
|        |      | result_base:desired result unit                                                                                                | 1- Illegal base          |
|        |      | Out of parameters:                                                                                                                  | 2- Illegal parameter          |
|        |      | result_data:Character data converted into base system, with base unit                                                                                         | 3- Illegal data format        |
|        |      |                                                                                                                      | 4-Unknown error caused by abnormal system operation |


> Digital format unit conversion


| submodule    | Programming language                          | statement                                                                                                                                                         | describe         |
|--------|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| Profile Operations | C                             | extern int kdkVolumeBaseNumericalConvert(double origin_numerical, KDKVolumeBaseType origin_base, KDKVolumeBaseType result_base, double* result_numerical); | Digital format unit conversion |
|        | Parameter Description                          | Return value description                                                                                                                                                      |            |
|        | Entry parameters:                           | Int type value, whether the interface execution is successful                                                                                                                                            |            |
|        | origin_numerical：raw numeric data     | 0 - no exception                                                                                                                                                      |            |
|        | origin_base：original base unit           | 1- Illegal base                                                                                                                                                     |            |
|        | Out of parameters:                           | 2- Illegal parameter                                                                                                                                                     |            |
|        | result_base：desired base unit           | 3- Illegal data format                                                                                                                                                   |            |
|        | result_numerical：Number type data in the desired base | 4-Unknown error caused by abnormal system operation                                                                                                                                            |            |