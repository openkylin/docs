---
title: openKylin Package Copyright Agreement Supplementary Guide
date: 2022-09-16 14:36:45
tags:
---

# openKylin package copyright agreement supplementary guide

openKylin is very strict about package copyright and license information. All packages in openKylin should contain the corresponding copyright information, generally including the following three steps: 1.
1. place the appropriate copyright notice file in the header comments of each source file.
2. create a file named "LICENSE" or "COPYING" in the package's first-level directory and put the license information for the entire project.
3. provide a summary of the copyright information of all source files of the project in the Debian/copyright file.

 **It is recommended that new open source projects use the [Mulan PSL v2 protocol](http://license.coscl.org.cn/index.html).** Mulan PSL v2 is similar to BSD-type licenses and has good compatibility: BSD and MIT-type loose licenses are compatible with the Mulan PSL v2 protocol. The Mulan PSL v2 protocol is compatible with Apache License v2.0, L/GPLv2, L/GPLv3, and other licenses. That is, code licensed under BSD, MIT class licenses can contribute to projects using the Mulan PSL v2 protocol; code using the Mulan PSL v2 protocol can contribute to projects such as Apache License v2.0, L/GPLv2 or L/GPLv3.

## Header Comments

For source files such as .c/.cpp/.h/.py/.java within a project, add the appropriate copyright notice to the header comments, the corresponding notice can be found in the license text message, or you can refer to projects following the same license at

Take the project ukui-foo, license "Mulan PSL v2", copyright subject Zhang Three, for example, you need to insert in the header comment of each source file.

```
Copyright (c) 2022 Zhang Three
ukui-foo is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
This SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
```

Take the project ukui-foo, license GPL-3.0+, copyright subject Zhang Three as an example:
```
Copyright (C) 2022 Zhang Three

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
This program is distributed in the hope that it will not be used for any other purpose.

This program is distributed in the hope that it will be useful,
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
```

> Enterprise users use the enterprise license

## Project license file
Depending on which license the project follows, you need to copy the complete contents of the corresponding license (openKylin from the /usr/share/common-license directory, Mulan PSL v2 from the [official website](http://license.coscl.org.cn/index.html)) Copy it to the first-level directory of the project and rename it to "LICENSE" or "COPYING".
Take the project ukui-foo, license GPL-3 as an example.
```
cd ukui-foo
cp /usr/share/common-license/GPL-3 COPYING
```
## debian/copyright file
### File syntax

The Debian/copyright file must convey all required upstream information, copyright notices, and license details in a file consisting of two or more paragraphs. At a minimum, the file must include a title paragraph and a document paragraph.

### Paragraphs

There are three types of paragraphs. The first paragraph in the file is called the Header paragraph. Each of the other paragraphs is a Files paragraph or a separate License paragraph.

#### Header Paragraphs

The following fields can appear in the header field

  - Format: mandatory
  - Upstream-Name: Optional. (recommended)
  - Upstream-Contact: Optional. (Recommended)
  - Source: Optional. (Recommended)
  - Disclaimer: Optional.
  - Comment: Optional. (Recommended)
  - License: Optional.
  - Copyright: Optional.


#### Files paragraph (repeatable)

The copyright and license for a file are declared in one or more paragraphs. In the simplest case, a single paragraph can be used which applies to all files and lists all applicable copyrights and licenses.

The following fields can appear in a single document paragraph.

  - Files: Required
  - Copyright: Required
  - License: Required
  - Comment: Optional.

#### Separate License paragraph (optional, repeatable).

When a group of files is multi-licensed, or when the same license appears multiple times, you can use single-line license fields and separate license paragraphs to expand the license abbreviation.

The following fields can appear in separate license paragraphs.

  - License: mandatory
  - Comment: optional

### Fields

The following fields are defined for use in Debian/copyright.

#### Format

Type: single line value.

The file format follows the canonical address, e.g.

- Format: https://www.Debian.org/doc/packaging-manuals/copyright-format/1.0/.

#### Upstream-Name

Type: single-line value.

Name of the software used upstream

#### Upstream-Contact

Type: row-based list;

The preferred address to reach the upstream project. This may be free-form text, but by convention it is usually written as a list of RFC5322 addresses or URIs.

#### Source
Type: formatted text, no summary.

An explanation of the upstream source. Normally this would be a URL, but it may be a free-form interpretation. If the upstream source has been modified to remove non-free parts, this should be specified in this field.

#### Disclaimer

Type: formatted text, no summary.

This field may be used in the case of non-free and contributed packages (non-free)

#### Comment

Type: formatted text, without a summary.

This field can provide additional information.

#### License

Type: formatted text with a brief description.

In the Header segment, this field gives the license information for the entire package, which may differ from all combinations of the per-file license information or a simplification of them. In the Files paragraph, this field gives the license terms for the files listed in the Files field of the paragraph. In a separate license paragraph, it gives the license terms for those paragraphs that reference it.

First line: the abbreviated name of the license, or the expression that provides an alternative. [standard abbreviation](https://dep-team.pages.debian.net/deps/dep5/#license-short-name).

Remaining lines: If left blank here, the file must include a separate license paragraph that matches the short name of each license listed in the first line. Otherwise, this field should include the full text of the license, or include a pointer to the license file under /usr/share/comm-licenses. This field should include all the text needed to satisfy the Debian Policy requirements for including software distribution licenses, as well as any licensing requirements for including warranty disclaimers or other statements in binary packages.

#### Copyright

Type: formatted text, without a summary.

One or more free-form copyright notices. Any format is allowed.
See the following example to see how to organize this field to make it easier to read. In the Title paragraph, this field gives the copyright information for the entire package, which may differ from or be simplified by the combination of copyright information for all individual files. In the Files segment, it gives the copyright information that applies to the files matched by the file schema.

The Copyright field collects all relevant copyright notices for the files in this segment. Not all copyright notices apply to each individual file; the year of publication of a copyright holder may be collected together. For example, if Document A has

 - Copyright 2008 John Smith
 - Copyright 2009 Angela Watts

and Document B has

 - Copyright 2010 Angela Watts

the copyright fields covering both Document A and Document B need only contain.

 - Copyright 2008 John Smith
 - Copyright 2009, 2010 Angela Watts

The copyright field can contain an exact copy of the original copyright notice (including the word " Copyright") or the text can be shortened, as long as the information is not sacrificed.

#### Files

Type: a space-separated list.

Indicates the schema list of documents covered by the licenses and copyrights specified in this paragraph.

The filename patterns in the file field are specified using the simplified shell glob syntax. Patterns are separated by spaces.

  - Only the wildcard characters * and ? apply; the former matches any number of characters (including none), the latter matches a single character. Both match the slash (/).

  - Matches pathnames starting from the source code. Thus, "Makefile.in " only matches files in the root modal stream of the source code, but "\*/Makefile.in" matches files at all directory levels.


### License specification
#### syntax

License names are case-insensitive and cannot contain spaces.

In the case of multiple licenses, when the user can choose between different licenses, and when the use of the work must comply with the terms of multiple licenses at the same time, the short names of the licenses are separated by "or".

For example, here is a simple "GPL version 2 or higher" field.

License: GPL-2+

This is a dual-licensed GPL/Artistic work, e.g. Perl.

License: GPL-1+ or Artistic

This applies to files that contain both GPL and classic BSD code: License: GPL-2+ and Artistic

License: GPL-2+ and BSD

For the most complex cases, commas are used to eliminate the priority of ors and ands, and take precedence over ors, unless preceded by a comma. Example.

A or B and C means A or (B and C).

A or B, and C means (A or B), and C.

This is a file containing Perl code and classic BSD code.

License: GPL-2+ or Artistic-2.0, and BSD

A GPL-2+ work with an OpenSSL exception is actually a dual-licensed work that can be redistributed both under GPL-2+ and under both GPL-2+ and OpenSSL exceptions. As such, it is expressed as

License: GPL-2+ with OpenSSL exception

Debian/copyright can be found in [example](https://gitee.com/openKylin/ukui-session-manager/blob/openKylin/yangtze/debian/copyright "example")

## How to check copyright

There are already programs upstream in the community that can help us check the copyright writeup, and these programs currently follow the DEP-5 rules.

#### licensecheck 

The maintainer can check the source file headers with the licensecheck command

 - $ licensecheck * -r

#### cme

Use the cme command to automatically update the debian/copyright file based on the source file headers

 - $ cme update dpkg-copyright


