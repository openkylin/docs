---
title: install openKylin on arm
description: 
published: true
date: 2023-01-12T02:42:26.148Z
tags: 
editor: markdown
dateCreated: 2023-01-12T02:36:15.135Z
---

# 1 Install openKylin on coolpi

## 1.1 Download the image

Download via the following link:

https://www.openkylin.top/downloads

Right click to extract the `.img` file directly after download.



## 1.2 Burning image

Install the burner: https://www.raspberrypi.com/software/

![arm64_installation_address_of_the_burner.png](./assets/arm64-system-installation/arm64_installation_address_of_the_burner.png)

Insert the SD card, open rpi-imager, select custom image, then select the installed `.img` file.

![arm64_burner_image_selection.png](./assets/arm64-system-installation/arm64_burner_image_selection.png)


Select the SD card, click WRITE, and wait for the procedure to be completed.

![arm64_sd_card_selection_for_burner.png](./assets/arm64-system-installation/arm64_sd_card_selection_for_burner.png)



## 1.3 Launch openKylin on coolpi

Plug in the power cable and monitor cable of the coolpi development board, connect the keyboard and mouse, and insert the SD card.

![arm64_coolpi_wiring_method.png](./assets/arm64-system-installation/arm64_coolpi_wiring_method.png)
During launch, the screen may remain black, or it may suddenly enter a faulty kylin OS (this is due to an error on the coolpi board when loading the SD card). To solve this problem, unplug and re-plug the power cable, then reboot.

Go to the login interface, enter your username and password to login to your desktop (password: kk123123).

![arm64_login_interface.png](./assets/arm64-system-installation/arm64_login_interface.png)
![arm64_desktop.png](./assets/arm64-system-installation/arm64_desktop.png)



## 1.4 Other problems
### 1.4.1 System lag

This problem is caused by the high CPU usage of the window manager. Go to the control panel and turn off the performance mode to alleviate the lag.

![arm64_turn_off_the_performance_mode.png](./assets/arm64-system-installation/arm64_turn_off_the_performance_mode.png)


### 1.4.2 Bluetooth service is not started

This problem is caused by an error during Bluetooth self-boot. Press `win+t` to open a terminal, enter `sudo systemctl start bluetooth` to start the Bluetooth service. Then you will see the Bluetooth module in the control panel.

![arm64_enable_bluetooth_service.png](./assets/arm64-system-installation/arm64_enable_bluetooth_service.png)


### 1.4.3 Screen remains black when waking up after turning off the display or sleep

To avoid this problem, it is recommended that you set the monitor to never turn off and the system to never sleep in the control panel.

![arm64_cancel_sleep_and_cancel_turning_off_the_display.png](./assets/arm64-system-installation/arm64_cancel_sleep_and_cancel_turning_off_the_display.png)

# 2 Install openKylin on Raspberry PI

## 2.1 Download the image

Download via the following link:

https://www.openkylin.top/downloads

Right click to extract the `.img` file directly after download.


## 2.2 Burning image

Install the burner: https://www.raspberrypi.com/software/

![arm64_installation_address_of_the_burner.png](./assets/arm64-system-installation/arm64_installation_address_of_the_burner.png)

Insert the SD card, open rpi-imager, select custom image, then select the installed `.img` file.

![arm64_burner_image_selection.png](./assets/arm64-system-installation/arm64_burner_image_selection.png)


Select the SD card, click WRITE, and wait for the procedure to be completed.

![arm64_sd_card_selection_for_burner.png](./assets/arm64-system-installation/arm64_sd_card_selection_for_burner.png)




## 2.3 Launch openKylin on Raspberry PI

Plug in the power cable, monitor cable, connect the keyboard and mouse, plug in the SD card.

![arm64 raspberry pi wiring method.png](./assets/arm64-system-installation/arm64_raspberry_pi_wiring_method.png)During launch, the screen may remain black. To solve this problem, unplug and re-plug the power cable, then reboot.

Enter the login interface, select ukui-Wayland at the bottom right, and then enter the user name and password, you can login to the desktop (password: kk123123). (Because this image lacks X11-related packages, kwin-x11 cannot be used to enter the desktop).

![arm64_select_to_login_on_wayland.png](./assets/arm64-system-installation/arm64_select_to_login_on_wayland.png)
![arm64_login_interface.png](./assets/arm64-system-installation/arm64_login_interface.png)
![arm64_desktop.png](./assets/arm64-system-installation/arm64_desktop.png)



## 2.4 Other problems
### 2.4.1 System lag

This problem is caused by the high CPU usage of the window manager. Go to the control panel and turn off the performance mode to alleviate the lag.

![arm64_turn_off_the_performance_mode.png](./assets/arm64-system-installation/arm64_turn_off_the_performance_mode.png)


### 2.4.2 Screen remains black when waking up after turning off the display or sleep

To avoid this problem, it is recommended that you set the monitor to never turn off and the system to never sleep in the control panel.

![arm64_cancel_sleep_and_cancel_turning_off_the_display.png](./assets/arm64-system-installation/arm64_cancel_sleep_and_cancel_turning_off_the_display.png)