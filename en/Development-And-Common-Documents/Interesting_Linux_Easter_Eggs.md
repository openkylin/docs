# <center>Funny Linux eggs - Insult Users with Sudo</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>

There are some strange options available in some common programs that you may wish to check out.

First, open a terminal and type the following command:

**sudo visudo**

It is highly recommended to use visudo to edit the /etc/sudoers configuration file. Visudo is used for two reasons: it prevents two users from modifying it at the same time, and it helps to verify that the changes are correct. visudo will not save a configuration file with syntax errors without permission, it will prompt you for problems and ask what to do about them. visudo opens configuration files in vi by default, while in Ubuntu Kylin visudo opens /etc/sudoers in nano by default.

Then, at the top of the file, add this line:

**Defaults insults**

As shown in the image:　　

![](https://www.ubuntukylin.com/upload/201602/1456191668463393.jpg)

Next, save and close the file. If you are using nano, press Ctrl+X to exit and it will prompt you if you want to save the changes. Please press Y to save the changes.

Then, type sudo -k in the terminal to clear the password cache. Finally, enter the wrong password in the sudo command:

![](https://www.ubuntukylin.com/upload/201602/1456192456272768.jpg)

Look, it's still impatient with me. Now you can also cry to your friends about your own sudo bullying you ^_^.
