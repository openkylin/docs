# <center>commonly used combination commands</center>
#### <center>Author: Stranger</center>
#### <center>2022-04-23 00:16:10</center>

There is not much content, if there are useful commands collected, it will be updated here as soon as possible

1. Install header files

```
sudo apt install linux-headers-`uname -r`
```

2. Update boot

```
sudo update-grub
```

3. Check if a package is installed

```
dpkg -l|grep package name or some characters contained in the package name
```

4. Dual system time synchronization
```
sudo timedatectl set-local-rtc 1
sudo hwclock --localtime --systohc
```

5. Merge two files (merge file1.txt and file2.txt into file.txt)

```
cat file1.txt file2.txt > file.txt
```