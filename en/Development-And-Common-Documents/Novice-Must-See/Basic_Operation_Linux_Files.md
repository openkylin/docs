# <center>Linux file basic operations</center>
#### Author: winifred
#### Time: 2023.3.15

## 1. File directory

### 1.1 ls command
The `ls` command is used to print the current directory list, and you can view file permissions (including directories, folders, and file permissions) to view directory information, etc.

Command format: `ls [option] [directory name]`

Common parameters:
- -a: list all files, including implied files starting with .
- -l: List file permissions, owner, file size and other information
- -t: Sort by file modification time

### 1.2 cd command
The `cd` command is used to change the current directory to the specified directory.

Command format: `cd [directory name]`

Common examples:
- `cd /`: change to the root directory
- `cd ..`: switch to the previous directory
- `cd ~`: switch to user home directory

### 1.3 pwd command
The `pwd` command is used to determine the exact location of the current directory within the file system.

Command format: `pwd [option]`

Common parameters:
- -P: Display the actual physical path
- -L: When the directory is a connection path, display the connection path

## 2. Basic file operations

### 2.1 Create a new file
Use the `touch` or `vi` command to create a blank file.

Common examples:
```linux
touch test.md
vi test.md
```

### 2.2 Create a new directory
Use the `mkdir` command to create an empty directory, and you can also specify the permission attribute to create the directory.

Common examples:
- Create an empty directory named mydir: `mkdir mydir`
- Create multi-level directories: `mkdir -p A/B/C`

### 2.3 Delete files/directories
Use the `rm` command to delete a file, use the -f parameter to force deletion, and use the -r parameter to delete a directory.

Common examples:
- Delete the test.md file: `rm test.md`
- Force delete test.md file: `rm -f test.md`
- Delete the A folder: `rm -r A`

### 2.4 Copy files/directories
Use the `cp` command to copy a file to the specified directory, and use the -r parameter to copy the directory.

Command format:
- Copy files: `cp [filename] [directory]`
- Copy directory: `cp -r [filename] [directory]`

Common examples:
- Copy test.md file to C folder: `cp test.md /home/user/A/B/C`
> You need to enter the directory where the test.md file is located
- Copy A to the alphabet folder: `cp -r A alphabet`

### 2.5 Moving and renaming files
Use the `mv` command to move files (cut) or rename files

Command format:
- Move files: `mv [source directory file] [destination directory]`
- Rename file: `mv [original filename] [new filename]`

Common examples:
- Move the file file to the filedir directory: `mv file filedir`
- Rename file file1 to file2: `mv file1 file2`

### 2.6 View files
#### 2.6.1 cat, tac command
The `cat` and `tac` commands are used to print the contents of the file to the standard output (terminal), where cat displays the number of lines in positive order, and tac displays the number of lines in reverse order.

Common examples:
- View the /etc/passwd file: `cat /etc/passwd`
- Show line numbers: `cat -n /etc/passwd`

#### 2.6.2 more command
`more` is used to "read" the contents of a file. You can use Enter to scroll down, Space to print all, and q to quit.

#### 2.6.3 head, tail command
View the first few lines and the last few lines of the file, the -n parameter is used to indicate the number of lines to view.

### 2.7 grep command
The `grep` command is used to find matching text in a file, and can accept regular expressions and wildcards, and multiple `grep` command options can be used to generate output in various formats.

Command format: `grep [option] pattern [file]`

Common examples:
- Extract the line where root appears in the /etc/passwd file: `grep "root" /etc/passwd`
- Extract the line that does not appear root in the /etc/passwd file: `grep -v "root" /etc/passwd`