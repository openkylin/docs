# <center>Tune openKylin OS through New Bing/chatGPT</center>
#### <center>Author: Gary</center>
#### <center>2023-03-10 13:36:00</center>



### What is openKylin OS?

![Image](./assets/chatGPT_openKylin/了解OPenKylin.png)

### Install openKylin OS
![image](./assets/chatGPT_openKylin/安装OpenKylin.png)

### Create USB drive for openKylin OS
![Image](./assets/chatGPT_openKylin/USB驱动安装.png)


### Features of openKylin OS


![Image](./assets/chatGPT_openKylin/WorkWithKylin.png)


### Coding with openKylin OS

![image](./assets/chatGPT_openKylin/coding.png)

### Install pip on openKylin OS

![image](./assets/chatGPT_openKylin/安装py.png)