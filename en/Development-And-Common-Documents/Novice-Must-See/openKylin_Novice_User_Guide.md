**Author: secretly**

  **Time: February 28, 2023**

# OpenKylin Novice User Guide

### 1. System update

We use the shortcut key <kbd> Ctrl </kbd> + <kbd> Alt </kbd> + <kbd> T </kbd> to open the terminal and enter the following command to complete the system update.

```shell
sudo apt update
sudo apt upgrade
```
### 2. The pointer on the touchpad moves slowly, how to adjust it?

We use the shortcut key <kbd> Win </kbd> + <kbd> I </kbd> to open the center, and find the `touchpad` function in `devices`.

Click the pointer speed status bar in the right panel, and we can keep trying the moving speed until the speed is adjusted to suit our habits.

![Enter image description](./assets/OpenKylin_user_guide/mouse_speed_set.png)

### 3. Commonly used shortcut keys in the system

![Enter picture description](./assets/OpenKylin_user_guide/systemhotkey.png)

### 4. Screenshot shortcut keys

![Enter picture description](./assets/OpenKylin_user_guide/jietu_hotkey.png)

### 5. Enable ssh login

- **Install openssh-server**

```shell
sudo apt install openssh-server -y
```
- **Modify configuration file `ssh_config`**

```shell
sudo vi /etc/ssh/ssh_config
```

>Find `# PasswordAuthentication yes`, delete the # in front, save and exit

![Enter picture description](./assets/OpenKylin_user_guide/vi_ssh_config.png)

- **restart ssh service**

```shell
sudo service sshd restart
```

- **view ip address**

```shell
ifconfig
or
ip address
```

![ifconfig view ip address](./assets/OpenKylin_user_guide/ifconfig01.png)

- **ssh remote login**

We access through the ssh login command in the windows terminal

```cmd
# Example ssh account name@IP address
ssh anan@192.168.124.82
```

![Enter picture description](./assets/OpenKylin_user_guide/ssh_link_openkylin.png)

### 6. Apt software installation command

The execution of the apt command requires super administrator privileges (root), so you need to add the `sudo` command in front of it

```shell
# install software
sudo apt install package
# Uninstall software
sudo apt remove package
# update software
sudo apt upgrade
# For example
sudo apt install openssh-server
sudo apt install putty
```
- **Detailed command parameters**

```shell
apt[]
       list - list packages by name
       search - search package descriptions
       show - show package details
       install - Install a package
       reinstall - reinstall a package
       remove - remove a package
       autoremove - remove all automatically installed packages that are no longer in use
       update - update the list of available packages
       upgrade - update the system by installing/upgrading software
       full-upgrade - Update system by uninstall/install/upgrade
       edit-sources - Edit software source information files
       satisfy - Satisfy the system with dependency strings
```

- **Detailed help can be viewed by command**

```shell
apt -h
```

### 7. wget download command

wget is a free tool for automatically downloading files from the Internet, and supports downloading through the three most common TCP/IP protocols: HTTP, HTTPS, and FTP.

For programs or packages that have provided download paths on the Internet, we can download them through the wget command, which is most commonly used for downloading deb packages.

- **Usage:**

```shell
wget [option arguments]... [Url].....
```

- **Example:**

```shell
example
wget http://www.xxx.com/download/putty.deb
```

- **Detailed help can be viewed by command**

```shell
wget -h
```

### 8. dpkg installation command

Software installation command dpkg

dpkg is a debian package management tool, which can install, uninstall, obtain information and other operations on packages in deb format.

- **Usage:**

```shell
dpkg -i package_file
dpkg --install package_file
```
- **Example:**

```shell
# Connect to the putty.deb file downloaded from the previous example
dpkg -i putty.deb
```

- **Detailed help can be viewed by command**

```shell
dpkg --help
```