# <center>Linux novice gift package: those things you must know to learn Linux</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>
the
Welcome to join the Linux family! For you, this may still be an unfamiliar field, but I believe that with a gradual and in-depth understanding, you will like Linux and open source! First of all, let's take a look at what Linux novice should pay attention to and understand!

### 1. Know a few big cows

Linus Torvalds
Born on December 28, 1969 in Helsinki, Finland, he has American citizenship. He is the earliest author of the Linux kernel, then initiated this open source project, served as the chief architect and project coordinator of the Linux kernel, and is one of the most famous computer programmers and hackers in the world today. He also initiated the Git open source project and is the main developer.

Richard Matthew Stallman
Referred to as RMS, born on March 16, 1953, the spiritual leader of the American Free Software Movement, the founder of the GNU Project and the Free Software Foundation. As a well-known hacker, his main achievements include Emacs and later GNU Emacs, GNU C compiler and GDB debugger. The GNU General Public License he wrote is the most widely used free software license in the world, opening up a new path for the Copyleft concept.

Eric Steven Raymond
Born on December 4, 1957, programmer, author of "Cathedral and Bazaar", maintainer of "New Hacker Dictionary" ("Jargon File"), and famous hacker. As the main editor and maintainer of "The New Hacker's Dictionary", Raymond has long been regarded as a historian and anthropologist of hacker culture. But after 1997, Raymond was widely recognized as one of the main leaders of the open source movement and the most widely known (and most controversial) hacker.

### 2. Get to know the star members of the Linux family
**Red Hat Enterprise Linux**: Red Hat Enterprise Linux is the Red Hat Corporation's Linux distribution for the commercial market, including mainframes. Red Hat provides 10-year support for each version of the enterprise version of LINUX starting from Red Hat Enterprise Linux 5. Red Hat Enterprise Linux is often abbreviated as RHEL. Red Hat Enterprise Linux releases a new version about every 3 years.

**Fedora Linux**: Fedora Linux is one of the better known Linux distributions. It is a free operating system with complete functions and fast updates. For the sponsor, Red Hat, it is a test bed for many new technologies that are considered available and will eventually be added to Red Hat Enterprise Linux.

**Centos**: The full name of CentOS is "Community Enterprise Operating System", which is one of the Linux distributions. It is compiled from the source code released by Red Hat Enterprise Linux in accordance with the open source code regulations. become. Because it comes from the same source code, some servers that require high stability use CentOS instead of the commercial version of Red Hat Enterprise Linux. The difference between the two is that CentOS does not contain closed source software. CentOS' major modification to the upstream code is to unload trademarks that are not free to use.

**Ubuntu**: Ubuntu is a GNU/Linux operating system mainly based on desktop applications. Its name comes from the word "Ubuntu" in Zulu or Hausa in southern Africa, which means "humanity". Ubuntu is based on the Debian distribution, which differs from Debian in that it releases a new version every 6 months.

**SUSE Linux**: SUSE is one of the distributions of the Linux operating system and a distribution in Germany. SUSE Linux is currently focused on the enterprise market.

**openSUSE**: openSUSE is an open community project, known as "the most beautiful Linux distribution".

**Debian**: Debian is a free operating system, full name Debian GNU/Linux, maintained by the Debian Project (Debian Project), Debian is an operating environment purely composed of free software.

**Archlinux**: Arch Linux (or Arch) is a Linux distribution designed with lightweight and concise design. Its development team adheres to the design tenet of simplicity, elegance, correctness and minimal code. The Arch Linux project was inspired by CRUX and started in 2002 by Judd Vinet.

For more Linux versions, please see here: [Linux system family tree](https://code.csdn.net/groups/7587/discources/935281)

### 3. Linux basic books that beginners must read
To learn Linux well, you need at least: a good introductory textbook, a linux instruction reference manual, a linux system management manual, and a book explaining the principles of linux systems. Here are a few well-recognized books we recommend:
* ["Brother Bird's Linux Private Kitchen Basic Learning Chapter"](https://book.douban.com/subject/4889838/) by Brother Bird; People's Posts and Telecommunications Press
* ["Brother Bird's Linux Private Kitchen Server Setup"](https://book.douban.com/subject/10794788/) by Brother Bird; Machinery Industry Press
* ["Linux Commands, Editors and Shell Programming"](https://book.douban.com/subject/25750712/) [US] Sobell M.G.; Tsinghua University Press
* ["Linux Device Drivers"](https://book.douban.com/subject/1723151/) by Coppert; China Electric Power Publishing House
* ["In-depth understanding of the Linux kernel"](https://book.douban.com/subject/2287506/) (USA) Bowei, Xister; China Electric Power Publishing House
* ["Advanced Programming in UNIX Environment"](https://book.douban.com/subject/1788421/) W.Richard Stevens / Stephen A.Rago; People's Posts and Telecommunications Press

### 4. Familiar with Linux common commands

  * man: Whenever you feel unsure about a command line, you can enter "man + command" to find out exactly what the command can do.
  * ls: List directory contents.
  * pwd : Display the full path of the current working directory in the terminal.
  * cd: To change the directory you are currently in.
  * mkdir: Create a new directory.
  * cp: Copy files/rename files.
  * mv: Move files.
  * find and locate: Search for files.
  * kill: Quickly close a process.
  * passwd: Change password.
  * md5sum: calculate and verify MD5 information signature
  * history : Query history commands.
  * sudo: (super user do) command allows authorized users to execute commands of super users or other users.
  * touch : Create a new file, or update the file's access and modification time to the current time.
  * chmod: Modify the access permissions of the file.
  * chown: Change the file owner and user group.
  * apt: APT is an advanced package manager developed for Debian series systems (Ubuntu, Kubuntu, etc.). On Gnu/Linux systems, it will automatically and intelligently search, install, upgrade and resolve dependency issues for packages.

### 5. Beware of several mistakes that novices often make
**DO NOT LOGIN AS ROOT**: It's a Unix convention, don't run anything as root unless you have to.

**File naming confusion**: Avoid using special characters such as dollar signs ($), brackets, and percent signs (%), which have special meaning to the shell and may cause conflicts. Avoid spaces, don't use invalid characters, "/" is special for the root directory.

**All files are mixed together**: Put the Home directory on a separate partition, so that you can reinstall the system or even upgrade your entire version without losing your data and personal settings.

**Trying to click to run .exe files**: Unless you have WINE installed, double-clicking those .exe files does nothing. New users need to know that both Linux and Windows will only run applications developed for their own systems.

**Send OpenOffice documents to Microsoft Office users in the default format**: Microsoft products are not very friendly to other operating systems and other applications. Many new Linux users often have trouble sharing files with friends because the other party It is not possible to read the file format they share, so new Linux users should pay attention to the format in which the files are stored to ensure that they can be opened by similar applications from Microsoft.

**Ignore Updates**: New updates may patch some new vulnerabilities. Maintaining updates can create the divide between a vulnerable system and a secure one. Linux security comes from constant maintenance.

The above are some errors in operating habits, as well as some technical errors, you can check "Avoid Common Errors in UNIX and Linux"

### 6. Often visit some Linux communities and websites
Domestic professional Linux website
[ChinaUnix](https://www.chinaunix.net/): Founded in 2001, it is an open source technology community mainly discussing Linux/Unix-like operating system technology, software development technology, database technology and network application technology website.
 
[LinuxCN](https://linux.cn/): Linux China is a community focusing on Chinese Linux technology and information, where you can get first-hand Linux information and technical knowledge.

Famous foreign Linux website
[Linux Online](https://www.linux.org/): The most authoritative Linux website, with articles and discussions covering everything, including software and hardware.

[Linux International](https://li.org/): Has an extensive list of Linux resources.

[Linux](https://www.linux.com/): The best website to learn Linux, and also the gathering place for Linux experience.

[Linuxforums](https://www.linuxforums.org/): A comprehensive website that provides Linux software resources, Linux forums, information on Linux server distributions, and LINUX articles and tutorials.