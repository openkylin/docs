# <center>Linux commands and 20 interesting things about Linux terminals</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>

Playing Linux is a lot of fun! Ha ha. Do not believe. Mark my words, by the end of this article you will be convinced that Linux is really fun.
![](https://www.ubuntukylin.com/upload/images/tl0.png)
### Command: sl (steam locomotive)
You probably know about the 'ls' command and use it a lot to view the contents of folders. However, there are times when you might spell it 'sl', how are we supposed to have some fun and not see "command not found"?
install sl
![](https://www.ubuntukylin.com/upload/images/tl1.png)
output
![](https://www.ubuntukylin.com/upload/images/tl2.png)
![](https://www.ubuntukylin.com/upload/images/t13.png)
This command also works when you type 'LS' instead of 'ls'.

### Command: telnet

No! No! ! It's not as complicated as it usually is. You are probably familiar with telnet. Telnet is a textual two-way network protocol. Nothing needs to be installed here. All you need is a Linux system and a connected network.
![](https://www.ubuntukylin.com/upload/images/t14.png)
![](https://www.ubuntukylin.com/upload/images/tl5.png)

### Command: fortune

Try your unknown luck, there are sometimes fun things in the terminal.

install fortune
![](https://www.ubuntukylin.com/upload/images/tl6.png)

### Command: rev (reverse)

It will reverse every string passed to it, isn't it fun.
![](https://www.ubuntukylin.com/upload/images/tl7.png)

### Command: factor

Time to talk a bit about Mathematics, this command outputs all the factors of a given number.

![](https://www.ubuntukylin.com/upload/images/tl8.png)

### Command: script

Ok, this is not a command, but a script, a very interesting script.

![](https://www.ubuntukylin.com/upload/images/tl9.png)

### Command: Cowsay

A little cow made up of ASCII codes in the terminal, this little cow will say what you want it to say.

Install Cowsay
![](https://www.ubuntukylin.com/upload/images/tl10.png)

output

![](https://www.ubuntukylin.com/upload/images/tl11.png)

What if you piped the 'fortune command' command to cowsay?

root@tecmint:~# fortune | cowsay

![](https://www.ubuntukylin.com/upload/images/tl12.png)

Tip: '|' is the pipeline command character. Usually it takes the output of one command as input to the next command. In the above example the output of 'fortune' is used as the output of the 'cowsay' command. Pipeline commands are often used in scripting and programming.

xcowsay is a graphical interface program. It is similar to cowsay but expressed in a graphic way, so it can be said to be the X version of cowsay.

![](https://www.ubuntukylin.com/upload/images/tl13.png)

output

![](https://www.ubuntukylin.com/upload/images/tl14.png)

![](https://www.ubuntukylin.com/upload/images/tl15.png)

cowthink is another command. Run "cowthink Linux is sooo funny" to see how it differs from cowsay.
![](https://www.ubuntukylin.com/upload/images/tl16.png)

output

![](https://www.ubuntukylin.com/upload/images/tl17.png)

### command: yes

yes is a very interesting and useful command, especially for scripters and system administrators, which can automatically generate a predefined response or pass it to the terminal.

![](https://www.ubuntukylin.com/upload/images/tl18.png)

Tip: (doesn't stop until you press ctrl+c)

### command: toilet

What? Are you kidding me! Of course not, but sure the name of this command is crazy and I don't know where the name of this command came from.

install toilet

![](https://www.ubuntukylin.com/upload/images/tl19.png)

output

![](https://www.ubuntukylin.com/upload/images/tl20.png)

This command even offers some color and font formatting.

![](https://www.ubuntukylin.com/upload/images/tl21.png)

Tip: Figlet is another command that produces similar effects to toilet.

### Command: cmatrix

You may have watched too much of the Hollywood movie 'The Matrix' and reveled in the ability given to Neo to see anything in the Matrix, or you may have imagined a vivid image of a desktop similar to 'Hacker'.

install cmatrix

![](https://www.ubuntukylin.com/upload/images/tl23.png)

output

![](https://www.ubuntukylin.com/upload/images/tl24.png)

### Command: oneko

Maybe you firmly believe that the Linux mouse pointer is always the same black or white and not vivid at all, then you are wrong. "oneko" is a software package that makes a "Jerry" your mouse pointer attach to your mouse.

install oneko

![](https://www.ubuntukylin.com/upload/images/tl25(1).png)

output

![](https://www.ubuntukylin.com/upload/images/tl26.png)

![](https://www.ubuntukylin.com/upload/images/tl27.png)

Tip: When the terminal running oneko is closed, Jerry will also disappear, and when the terminal is restarted, the project will not appear again. You can add this program to your startup options and continue using it.

### Fork Bomb

This is a very under-drawn piece of code. You are responsible for the consequences of running this command. This command is actually a fork bomb, it will multiply itself exponentially until all system resources are used or the system hangs (you can try this command once if you want to see the power of this command, but you will do so at your own risk, remember Close and save all other programs and files before running it).

![](https://www.ubuntukylin.com/upload/images/tl28.png)

### Command: while

The "while" command below is a script that will give you the date and files in color until you hit the interrupt key (ctrl+c). Copy and paste this command into your terminal.

![](https://www.ubuntukylin.com/upload/images/tl29.png)

![](https://www.ubuntukylin.com/upload/images/tl30.png)

Tip: The above script with the following modification will produce similar output but a bit different, try it in your terminal.

![](https://www.ubuntukylin.com/upload/images/tl31.png)

### Command: espeak

Turn up the volume of your multimedia speakers to the maximum, and then copy this command to your terminal to see your reaction when you hear the voice of God.

install espeak

![](https://www.ubuntukylin.com/upload/images/tl32.png)

output

![](https://www.ubuntukylin.com/upload/images/tl33.png)

### Command: aafire

How about setting a fire on your terminal. Type this "aafire" into your terminal, no quotes needed to see the magic. Press any key to middle point to the program.

install aafire

![](https://www.ubuntukylin.com/upload/images/tl34.png)

output

![](https://www.ubuntukylin.com/upload/images/tl35.png)


![](https://www.ubuntukylin.com/upload/images/tl36.png)
### Command: bb

First install "apt-get install bb", then type "bb" and see what happens.

![](https://www.ubuntukylin.com/upload/images/tl37.png)

![](https://www.ubuntukylin.com/upload/images/tl38.png)

### command: url

Wouldn't it be cool to use the command line to change your twitter status in front of your friends? Just replace username, password and "your status message" with your username password and your desired status respectively.

![](https://www.ubuntukylin.com/upload/images/tl39.png)

###ASCIIquarium

What should I do if I want to make an aquarium in the terminal?

![](https://www.ubuntukylin.com/upload/images/tl40.png)

### Install ASCIIquarium

Download and install ASCIIquarium.

![](https://www.ubuntukylin.com/upload/images/tl41.png)

Finally, run "asciiquarium" or "/usr/local/bin/asciiquarium" in the terminal, remember not to add quotation marks, and a magical scene will be displayed in front of your eyes.

![](https://www.ubuntukylin.com/upload/images/tl42.png)

![](https://www.ubuntukylin.com/upload/images/tl43.png)
### Command: funny manpages
First install "apt-get install funny-manpages" and then run the man page of the following command. some of them

![](https://www.ubuntukylin.com/upload/images/tl44.png)

### Linux Tweaks
Time to do some optimization

![](https://www.ubuntukylin.com/upload/images/tl45.png)

Linux is always sexy: who | grep -i blonde | date; cd ~; unzip; touch; strip; finger; mount; gasp; yes; uptime; umount; sleep (sweat, if you know what I mean!)

There are some other commands, but these commands do not work on all systems, so this article does not cover them. For example, dog, filter, banner

Happy using, you can say thank you to me later :)









