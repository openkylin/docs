# <center>In Linux, you must master the basic network commands!</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>


Whether you are an aspiring Linux system administrator or a Linux fanatic, you must understand these basic and important Linux network commands!

In the process of learning Linux, everyone will pay great attention to the use of the command line, and must have read a lot of books to learn. In today's article, I (Note: The author is Abhishek Prakash) will summarize the network command set that helped me get high scores in the course of computer network engineering. Everyone is welcome to take out the cheat sheet and jot it down, I hope it will be useful to you too.

### Network Connectivity
Ping: Send an ICMP echo request message to the host, which lasts until you press Ctrl+C. Ping means that a packet is sent from your machine via ICMP, and then gets a response at the IP layer. Ping can detect whether you are connected to another host.
Telnet host: Interact with the host on the specified port. The telnet port is 23 by default. Other commonly used ports are loopback port 7, which is used to send mail, SMTP 25, and user query 79. Use Ctrl+] to exit telnet.

### ARP
ARP is used to translate IP addresses into Ethernet addresses. Root users can add/delete ARP entries. Among them, ARP entries are cached in the kernel, and will be automatically deleted after 20 minutes. But the root user can create permanent ARP entries.
arp -a: print ARP table
arp -s[pub]: Add entry
arp -a -d: delete all entries

### Routing
netstat -r: Print the routing table. The routing table is stored in the kernel, and ip uses it to send packets to the external network.
routed: A BSD daemon that performs dynamic routing. Implement the RIP routing protocol. Can only be used with root privileges.
gated: gated is another routing daemon that implements RIP. Use OSPF/EGP/RIP at the same time. Can only be used with root privileges.
traceroute: It can be used to trace the routing information of IP packets.
netstat -rnf inet: Displays the routing table for IPv4.
sysctl net.inet.ip.forwarding=1: Make packets forward (turn a host into a route).
route: The route command is used to set a static route in the routing table. All information from PC to IP/SubNet goes through the specified gateway IP. This command can also be used to set the default route.
route add|delete [-net|-host]: Add/delete static routes (eg: route add 192.168.20.0/24 192.168.30.4).
route flush: delete all routes.
route add -net 0.0.0.0 192.168.10.2: Add a default route.

### Important Documents
/etc/hosts: IP address and name
/etc/networks: IP address and network name
/etc/protocols: protocol number and protocol name
/etc/services: the port number corresponding to the tcp/udp service name

### Tools and network performance analysis
ifconfig[up]: open the interface
ifconfig[down|delete]: stop interface
tcpdump -i -vvv: A tool for grabbing and analyzing packets
netstat -w [seconds] -l [interface]: display network settings and data

### other
nslookup: Converts IPs to names, or names to IPs, by querying a DNS server. For example, nslookup ubuntukylin.com will get the IP address of ubuntukylin.com.
ftp: transfer files between local host and remote host.
rlogin: log in to the host remotely.