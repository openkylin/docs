# <center>Interesting Linux easter eggs - using sudo to offend users</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>

One can configure sudo (which is used to authorize commands) to offend users who mistype their passwords. Let's take a look together!

First, open a terminal and enter the following command:

**sudo visudo**

It is highly recommended to use visudo to edit the /etc/sudoers configuration file. There are two reasons why visudo is used, one is that it can prevent two users from modifying it at the same time; the other is that it can help verify whether the modification is correct. Visudo will not save configuration files with grammatical errors without authorization, it will prompt you about the problems and ask how to deal with them. By default, visudo opens the configuration file in vi, while in Ubuntu Kylin, visudo opens /etc/sudoers in nano by default.

Then, at the top of the file, add this line:

**Defaults insults**

as the picture shows:  

![](https://www.ubuntukylin.com/upload/201602/1456191668463393.jpg)

Next, save and close the file. If you are using nano, press Ctrl+X to exit and it will prompt you if you want to save the changes. Please press Y to save changes.

Then, enter sudo -k in the terminal to clear the password cache. Finally, enter the wrong password in the sudo command:

![](https://www.ubuntukylin.com/upload/201602/1456192456272768.jpg)

Look, it's still impatient with me. Now you can also cry to your friends that your sudo bullies you^_^.