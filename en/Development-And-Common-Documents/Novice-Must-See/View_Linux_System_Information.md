# <center>Check the summary of commands related to linux system</center>
#### <center>Author: Hua Tsai</center>
#### <center>2023-3-10 10:39:00</center>

The linux command to view system information is the basic knowledge necessary for linux beginners. Usually before carrying out the development work, you need to know the system information, so it is necessary to know the relevant command information in linux.
The following are classified according to different functions and provide the commonly used commands for querying system information in each Linux distribution. You can refer to them.

## 1. Kernel related commands

### (1) Check the Linux kernel version command
1. cat /proc/version
2. uname -a

### (2) List loaded kernel modules
lsmod


## 2. System version related commands

### (1) Check the Linux system version command
1. lsb_release -a
2. cat /etc/redhat-release (Note: only suitable for Redhat Linux),
3. cat /etc/issue

### (2) Check the operating system version
  head -n 1 /etc/issue
  ### (3) View CPU information
  cat /proc/cpuinfo
  ### (4) View system running time, number of users, load
  uptime
 
  ## 3. View process and service related commands
### (1) View all processes
ps -ef
### (2) List all system services
chkconfig –list
### (3) List all started system service programs
chkconfig –list | grep on
### (4) Real-time display process status user
top
### (5) Query process information according to PID
ps -f -p <process PID>
### (6) View port occupation process
lsof -i:<PORT_ID>
 
 
## Four, view system user related commands
   ### (1) View the specified user information
id <username>
### (2) View user login log
  last
### (3) View all users of the system
cut -d: -f1 /etc/passwd
### (4) View all groups in the system
cut -d: -f1 /etc/group


## 5. Query external device commands

### (1) List all PCI devices
lspci -tv

### (2) List all USB devices
lsusb -tv
### (3) View the IDE device detection status network at startup
  dmesg | grep IDE
### (4) Check the computer name
  hostname
  ### (5) View hardware information
  1. lshw
  2. lshw -short
  3. dmidecode


## Six, system disk related query commands
### (1) View memory usage and swap area usage
  free -m
### (2) View the usage of each partition
  df -h
### (3) View the size of the specified directory
du -sh <directory name>
### (4) View the total amount of memory
grep MemTotal /proc/meminfo
### (5) View the amount of free memory
  grep MemFree /proc/meminfo
### (6) View system load disks and partitions
cat /proc/loadavg
### (7) View the mounted partition status
  mount | column -t
### (8) View all partitions
  fdisk -l
### (9) View all swap partitions
  swapon -s
### (10) View disk parameters (only for IDE devices)
hdparm -i /dev/hda

## Seven, query network related information

### (1) View the properties of all network interfaces
ifconfig
### (2) View firewall settings
  iptables -L
### (3) Check the routing table
  route-n
### (4) View all listening ports
  netstat -lntp
### (5) View all established connections
netstat -antp
### (6) Check the process of network statistics
netstat -s

## 8. Use scp to copy files between different machines

### (1) Remotely copy a single file from the local to the target host
scp local_file remote_username@remote_ip:remote_folder
### (2) Remotely copy the folder from the local to the target host
scp -r local_folder remote_username@remote_ip:remote_folder
### (3) Copy a single file from the remote host to the local
scp remote_username@remote_ip:remote_folder/remote_file local_folder
### (4) Copy the folder from the remote host to the local
scp -r remote_username@remote_ip:remote_folder local_folder