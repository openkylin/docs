# <center>Linux Newbie Bundle: What You Must Know About Learning Linux</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>
　
　　Welcome to the Linux family! It may still be unfamiliar territory for you, but I'm sure with progressive insight you'll come to love Linux and love open source! First, let's look at what newbies to Linux should be aware of and know!

### 1. Get to know a few maestro

Linus Torvalds
Linus Torvalds was born in Helsinki, Finland, on December 28, 1969, and has American citizenship. He was the first author of the Linux kernel, then launched the open source project, served as the chief architect and project coordinator of the Linux kernel, and is one of the world's most famous computer programmers and hackers today. He also started the Git open source project and is the lead developer.

Richard Matthew Stallman
RMS, born March 16, 1953, is the spiritual leader of the American free software movement and founder of the GNU Project and the Free Software Foundation. A well-known hacker, his major achievements include Emacs and later GNU Emacs, the GNU C compiler and the GDB debugger. He wrote the GNU General Public License, the world's most widely adopted free software license, which blazed a new trail for the Copyleft concept.

Eric Steven Raymond
Born December 4, 1957, programmer, author of Cathedrals and Bazaars, maintainer of The New Hacker's Dictionary ("Jargon File"), and noted hacker. As the primary compiler and maintainer of the New Hacker Dictionary, Raymond has long been considered a historian and anthropologist of hacker culture. But after 1997, Raymond is widely recognized as one of the main leaders of the open source movement and the most widely known (and controversial) hacker.

### 2. Learn about the star members of the Linux family
**Red Hat Enterprise Linux**: Red Hat Enterprise Linux is Red Hat's Linux distribution for the commercial market, including mainframes. Red Hat Enterprise Linux is often abbreviated as RHEL. Red Hat Enterprise Linux releases a new version approximately once every three years.

**Fedora Linux**: Fedora Linux is one of the more widely known Linux distributions. It is a full-featured, rapidly updated, free operating system. For its sponsor, Red Hat, it is a test bed for many new technologies that are considered usable and will eventually be added to Red Hat Enterprise Linux.

**Centos**: CentOS, known as the Community Enterprise Operating System, is one of the Linux distributions that comes from Red Hat Enterprise Linux. It is compiled from the source code distributed by Red Hat Enterprise Linux in accordance with open source regulations. Because it comes from the same source code, some servers that require a high degree of stability use CentOS instead of the commercial version of Red Hat Enterprise Linux. The difference between the two is that CentOS does not contain closed source software, and the main modification to the upstream code is to uninstall trademarks that are not freely available.

**Ubuntu**: Ubuntu is a desktop application-based GNU/Linux operating system whose name comes from the Zulu or Hausa word for "Ubuntu" in southern Africa, meaning "humanity". Ubuntu is based on the Debian distribution, and differs from Debian in that it releases a new version every six months.

**SUSE Linux**: SUSE is one of the distributions of the Linux operating system and one of the German distributions.SUSE Linux is currently focused on the enterprise market.

**openSUSE**: openSUSE is an open community initiative that claims to be the "most beautiful Linux distribution".

**Debian**: Debian is a free operating system called Debian GNU/Linux, maintained by the Debian Project, which is an operating environment made up of purely free software.

**Archlinux**: Arch Linux (or Arch) is a Linux distribution designed with lightness and simplicity in mind. Its development team adheres to the design tenets of simplicity, elegance, correctness and code minimization.The Arch Linux project was inspired by CRUX and started by Judd Vinet in 2002.

For more Linux versions see here: [Linux System Family Genealogy](https://code.csdn.net/groups/7587/discources/935281)

### 3. Must-read basic Linux books for beginners　　
To learn Linux well, you need at least: a good introductory textbook, a linux command reference manual, a linux system administration manual, and a book explaining the principles of the linux system. Here we recommend a few recognized good books:
* [《鸟哥的Linux私房菜 基础学习篇》](https://book.douban.com/subject/4889838/) Bird by Bird; People's Post and Telecommunications Publishing House
* ["Bird's Linux Private Room Server Setup"](https://book.douban.com/subject/10794788/) by Bird; Machinery Industry Press
* [Linux Commands, Editors, and Shell Programming](https://book.douban.com/subject/25750712/) [US] Sobell-M.G.; Tsinghua University Press
* [Linux Device Drivers](https://book.douban.com/subject/1723151/) by Kopot; China Electric Power Press
* [In-depth Understanding of Linux Kernel](https://book.douban.com/subject/2287506/) (US) by Bovee, Cister; China Electric Power Press
* [Advanced Programming for UNIX Environments](https://book.douban.com/subject/1788421/) by W. Richard Stevens / Stephen A. Rago; People's Post and Telecommunications Publishing House

### 4. Familiarize yourself with common Linux commands

 * man : Any time you are not sure about a command line, you can type "man + command" to find out exactly what the command does.
 * ls : List the contents of a directory.
 * pwd : Displays the full path of the current working directory in the terminal.
 * cd : To change the directory you are currently in.
 * mkdir : Create a new directory.
 * cp : Copy files/rename files.
 * mv : Move files.
 * find and locate : Search for files.
 * kill : Shut down a process quickly.
 * passwd : Change password.
 * md5sum : Calculate and verify MD5 information signatures.
 * history : Query history command.
 * sudo : (super user do) command allows authorized users to execute super user or other user commands.
 * touch : Create a new file, or update the access and modification time of a file to the current time.
 * chmod : Modifies the access rights of a file.
 * chown : Change the owner and user group of a file.
 * apt : APT is an advanced package manager developed for the Debian family of systems (Ubuntu, Kubuntu, etc.). On Gnu/Linux systems, it automatically and intelligently searches, installs, upgrades, and resolves dependencies for packages.

### 5. Beware of a few common mistakes made by newbies　
**Don't log in as the root user**: This is a convention with Unix, unless you have to then don't run anything easily under the root user.

**Confusing file naming**: Avoid using special characters such as dollar signs ($), parentheses and percent signs (%), which have a special meaning for the shell and may cause conflicts. Avoid using spaces, don't use invalid characters, and "/" is specific to the root directory.

**All files are mixed together**: Putting the Home directory on a separate partition will allow you to reinstall your system or even upgrade your entire version without losing your data and personal settings.

**Trying to click to run .exe files**: Unless you have WINE installed, double clicking on those .exe files is useless. New users need to know that both Linux and Windows will only run applications developed for their own systems.

**Send OpenOffice documents in default format to Microsoft Office users**: Microsoft products are not very friendly to other operating systems and other applications, many new Linux users often have trouble sharing files with friends because the other person cannot read the format of the files they are sharing, so new Linux users need to be careful about the format in which they store files and make sure they can be opened by similar Microsoft applications.

**Neglect updates**: New updates can patch some new vulnerabilities. Maintaining updates can construct the watershed between a vulnerable system and a secure one. security in Linux comes from constant maintenance.

These are some of the mistakes in operating habits, and there are some technical types of mistakes that you can see in Avoiding Common Mistakes in UNIX and Linux

### 6. frequent some Linux communities and websites
Domestic professional Linux websites
[ChinaUnix](https://www.chinaunix.net/): Founded in 2001, it is an open source technology community website that focuses on discussing Linux/Unix-like operating system technology, software development technology, database technology and network application technology.
 
[LinuxCN](https://linux.cn/): Linux China is a community focusing on Chinese Linux technology and information, where you can get first-hand Linux information and technical knowledge.

Foreign famous Linux website
[Linux Online](https://www.linux.org/): The most authoritative Linux website, articles and discussions are all inclusive, software and hardware are all available.

[Linux International](https://li.org/): A large list of Linux resources.

[Linux](https://www.linux.com/): The best site to learn Linux, and a place to gather experience in using Linux.

[Linuxforums](https://www.linuxforums.org/): A comprehensive website that provides information on Linux software resources, Linux forums, information on Linux server distributions, and LINUX articles and tutorials.
