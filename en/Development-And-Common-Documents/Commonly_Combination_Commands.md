# <center>Common combination commands</center
#### <center>Author: stranger</center
#### <center>2022-04-23 00:16:10</center

Not much content, if there is a collection of useful commands will be updated here as soon as possible
1. Install the header file

```
sudo apt install linux-headers-`uname -r`

```

2. Update the boot

```
sudo update-grub

```

3. Check if a package is installed

```
dpkg -l|grep 包名或者是包名里包含的部分字符

```

4. Synchronize the time of dual systems

```
sudo timedatectl set-local-rtc 1
sudo hwclock --localtime --systohc

```


5. Merge two files (merge file1.txt and file2.txt into file.txt)

```
cat file1.txt file2.txt > file.txt

```