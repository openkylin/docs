# <center>How to add boot parameters to grub</center>
#### <center>Author: OK</center>
#### <center>2022-04-22 23:36:00</center>



## How to add boot parameters to grub
For grub2, ubuntu gives an official configuration file /etc/default/grub. Most of the grub2 settings can be done in this file, and the structure of this file is relatively simple and easy to modify. There is no need to change /boot/grub/grub.cfg or /etc/grub.d/ directly.

Modifying /etc/default/grub requires only one simple command.
sudo vim /etc/default/grub

The following are the system defaults and the most common ways to modify the menu display time and default operating system.
 #If you change this file, run 'update-grub' afterwards to update
 #/boot/grub/grub.cfg.

GRUB_DEFAULT=0 #Changing 0 to saved allows grub to remember which system was selected the last time it booted
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_TIMEOUT="5" #Show the time to start the selection menu
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX=""

After the modification is complete, execute the following command.

$ sudo apt-get install grub2-common  //If you don't have the update-grub command, run this installation command first

$ sudo update-grub  //Generate the grub configuration file

That's all.
