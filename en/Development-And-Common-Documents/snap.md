# <center>the principle is not the same snap</center>
#### <center>Author: OK</center>
#### <center>2022-04-22 23:36:00</center>


**Foreword**

Both snap and flatpak are the next generation of package management technology across Linux distributions. We briefly introduced the principle of flatpak in the last article (Portal), and today we will continue with a brief introduction to the security mechanism of snap.

 
### Introduction

snap is a new generation of Linux package management tool proposed by Canoncial, dedicated to unify the package format on all Linux distributions, so that "once packaged, used everywhere". Currently snap is available on several Linux distributions, including Ubuntu, Fedora, Mint, etc. First, let's understand the various terms related to snap.

#### snap

A new generation of package management technology across Linux distributions, supporting all major Linux distributions, ensuring user data security through the Linux kernel security mechanism, completely solving package dependency-related problems, and greatly simplifying the application software packaging process. snap is also a command-line tool for installing and managing snap packages.
#### snapd

Manage the backend services of the snap package.
#### kernel snap

Kernel packaged in snap format, containing kernel images and kernel modules.
#### OS snap

The rootfs, repackaged using the snap format, contain the basic resources to run and manage snap, and when you first install snap, OS snap is installed first.

#### snapcraft

A collection of packaging tools to package software into snap format.

#### snappy

This name has been deprecated and is now known as Ubuntu Core, which is Ubuntu's full-snap operating system, as opposed to classic Ubuntu, which is traditionally based on deb packages.

 
### Security Policy

snap applications run in a sandbox. The system implements its security features by limiting the application's access to resources through a number of mechanisms, such as sandboxing and snap file system implemented through the configuration of kernel security mechanisms AppArmor, Seccomp, etc. Developers do not need to know much about the details of system security mechanisms. The following is a brief description of some of the security policies used by snap.
#### Sandbox

Linux Sandbox is a method of process access control based on a number of security mechanisms supported in the kernel. A process is usually placed in a strictly restricted state by assigning it a random uid, placing it in a chroot environment, and configuring Capability for the process uid. snap applications use this method to run in the sandbox environment assigned to them by the system.
#### security policy ID

Each snap application command has a unique security policy ID, which the system binds to the command, thus allowing different security policies to be configured for different programs within the same snap package. As a marker for the system to identify the command, when the program is installed and run, the system will allocate resources to it according to its Security Policy ID. Communication control between snap applications running in the sandbox is also configured by this ID. 

The security policy ID of a snap application is named snap.name.command 
For example, the security policy ID of the hello application is snap.hello.hello
#### snap file system

The snap file system is divided into areas with read-only and read-write permissions, and each snap application has its own unique restricted file directory, as shown in the following figure.

![](https://www.ubuntukylin.com/upload/201708/1501752729610536.png)


File access permissions for an application can be viewed as follows.

$ snap install hello
hello 2.10 from 'canonical' installed
$ snap run --shell hello.hello


$ env | grep SNAP
SNAP_USER_COMMON=/home/kylin/snap/hello/common
＃ Writable directory for all versions of the application for a single user

SNAP_LIBRARY_PATH=/var/lib/snapd/lib/gl:/var/lib/snapd/void
＃ Add to the directory of LD_LABRARY_PATH

SNAP_COMMON=/var/snap/hello/common
＃ Writable directory for all versions of applications for all users

SNAP_USER_DATA=/home/kylin/snap/hello/20
＃ Writable directory for single-user specified versions of applications

SNAP_DATA=/var/snap/hello/20
＃ Writeable directory for all user-specified versions of the application

As can be seen, the directories to which a snap application has write access are extremely limited, and each snap application has its separate writable directory. snap file system's configuration of permissions for snap application-related directories illustrates that this approach achieves isolation between application and application, and application and system. 

At the same time, this approach provides good support for upgrade and rollback of snap applications. To upgrade, you only need to copy the relevant directory of the determined version to the corresponding directory of the higher version, and to rollback, only need to delete the directory of the higher version.
#### AppArmor

AppArmor is a mandatory access control system that provides control at the kernel level over the resources that processes can access. When a snap application is installed, the system generates its own AppArmor-specific configuration file for each of these commands. The kernel's Capability limits on executable programs can also be configured through Aparmor. When executing a command in an application, the AppArmor mechanism ensures that the command does not override access rights. As a security mechanism in the kernel, AppArmor is also supported in ubuntu classic. However, unlike the classic system, the snap system has stricter access control on the program, basically "only the minimum privileges required for program execution".
#### Seccomp

Seccomp is a kernel interface access filter through which a snap application accesses the kernel interface. Seccomp has a similar role in the snap system to AppArmor, in that it controls the application's access to system resources.

Snap interface calls

snap applications are strictly limited to the security policy described above, but communication between snap applications is also required, for example, a hardware driver as a snap application must provide interfaces and services for applications using this hardware. The following is a brief explanation of the communication mechanism between snap applications.
#### Default Security Policy

In the absence of special configuration, snap applications use the default security policy, which contains the default directory access control in the snap file system mentioned earlier, as well as the following partial policies.

- Read-only permissions for the snap application installation directory.

- Read and write access to shared memory (ie. /dev/shm/snap.SNAP_NAME.*)

- Permissions for different processes of the same application to send signal to each other
#### Installation mode

snap provides different resource access control through different installation modes。
1) Devmode

devmode is the development mode. Use the following command to install the application in this mode.

$ snap install hello --devmode
$ snap list
Name         Version       Rev   Developer  Notes
core          16-2.26.9     2381  canonical  -
hello          2.10          20    canonical  devmode
pc            16.04-0.8     9     canonical  -
pc-kernel     4.4.0-83.106  68    canonical  -

This mode provides full access to the application, but logs the program's overstepping in the log.

Under devmode, the snap application can only access files under /snap/.
2) Classic

This mode will remove all access restrictions and no overstepping will be recorded in the logs.

In classic mode, the snap application can access the files under '/'.
#### Interfaces

In addition to the resources provided by the default security policy, snap applications do not have access to other system resources. If a snap application needs to use system resources or resources provided by other applications, it needs to configure interfaces through the interfaces mechanism. Interfaces are divided into two types interfaces, slot (service provider) and plug (service user). 

The snap application access to restricted resources is illustrated as follows.

![](https://www.ubuntukylin.com/upload/201708/1501752836565131.png)

Note: The operating system also exists as a snap application in the snap system. 
As shown in the figure, snap applications can be accessed from each other by configuring the plug and slot of the snap application. 

Check for existing bugs and slots on the system：

$ snap interfaces
Slot                                              Plug
:account-control                             -
:alsa                                                -
:autopilot-introspection                 -
:bluetooth-control                          -
:browser-support                            -
:camera                                          -
:classic-support                         classic
:core-support                 core:core-support-plug
.........
The following is an example to illustrate the use of plug and slot.

name: blue
...
apps:
  blue:
    command: bin/blue
    slots: 【bluez】    

The above file can be used as a snap package packaging control file for a Bluetooth device driver. When this application is installed, it is assigned the security policy ID snap.blue.blue and contains the rule: create a bluez slot for blue when it starts. 

To use the functionality provided by this slot in other applications, the package control file looks like this.

name: blue-client
...
apps: 
  blue-client:
    command: bin/blue-client
    plugs: 【bluez】

Similarly, when this application is installed, the system will assign the security policy ID to snap.blue-client.blue-client and include the rule: allow this application to communicate with snap.blue.blue. Also, the security rule providing the slot will be rewritten to read: snap.blue.blue allows snap.blue-client.blue-client to communicate with it. 

The following diagram illustrates the mutual communication between snap applications and applications under strict separation constraints.

![](https://www.ubuntukylin.com/upload/201708/1501752890346136.png)



The snap system consists of snap applications, including the system and kernel, in the form of snap packages. Each snap package provides services to each other through interfaces to accomplish cooperative work, while each application does not lose its independence.

 
### Summary

The snap system provides a robust security system. Compared to traditional Linux distributions, applications in snap systems are more independent and secure, while the configuration of snap application permissions is simpler. In the growing demand of embedded and IoT with the increasingly serious system security situation, snap system shows outstanding advantages over traditional Linux distributions.
