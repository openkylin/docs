# <center>20 interesting facts about Linux commands and Linux terminals</center>
#### <center>author: Little K</center>
#### <center>2022-04-22 23:36:00</center>

Playing Linux its fun! Haha. Don't believe it. Mark my words and by the end of the article you'll be convinced that Linux is indeed fun.

![](https://www.ubuntukylin.com/upload/images/tl0.png)

### Command: sl (steam locomotive)
You might be aware of command ‘ls‘ the list command and use it frequently to view the contents of a folder but because of miss-typing sometimes you would result in ‘sl‘, how about getting a little fun in terminal and not “command not found“.
Install sl 
    
![](https://www.ubuntukylin.com/upload/images/tl1.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl2.png) 
![](https://www.ubuntukylin.com/upload/images/t13.png)

This command works even when you type ‘LS‘ and not ‘ls‘.

### Command: telnet

No! No!! it is not as much complex as it seems. You would be familiar with telnet. Telnet is a text-oriented bidirectional network protocol over network. Here is nothing to be installed. What you should have is a Linux box and a working Internet.

![](https://www.ubuntukylin.com/upload/images/t14.png)
![](https://www.ubuntukylin.com/upload/images/tl5.png)

### Command: fortune

Try your unknown luck, there is sometimes fun in the terminal.

Install fortune

![](https://www.ubuntukylin.com/upload/images/tl6.png)

### Command: rev (flip)

It reverse every string given to it, is not it funny.

![](https://www.ubuntukylin.com/upload/images/tl7.png)

### Command: factor

Time for some Mathematics, this command output all the possible factors of a given number.

![](https://www.ubuntukylin.com/upload/images/tl8.png)

### Command: script

OK fine this is not a command and a script but it is nice.

![](https://www.ubuntukylin.com/upload/images/tl9.png)

### Command: Cowsay

An ASCII cow in terminal that will say what ever you want.

Install Cowsay

![](https://www.ubuntukylin.com/upload/images/tl10.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl11.png)

How about pipelineing ‘fortune command‘, described above with cowsay?

root@tecmint:~# fortune | cowsay

![](https://www.ubuntukylin.com/upload/images/tl12.png)

Note: ‘|‘ is called pipeline instruction and it is used where the output of one command needs to be the input of another command. In the above example the output of ‘fortune‘ command acts as an input of ‘cowsay‘ command. This pipeline instruction is frequently used in scripting and programming.

xcowsay is a graphical program which response similar to cowsay but in a graphical manner, hence it is X of cowsay.

![](https://www.ubuntukylin.com/upload/images/tl13.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl14.png)

![](https://www.ubuntukylin.com/upload/images/tl15.png)

cowthink is another command just run “cowthink Linux is sooo funny” and see the difference in output of cowsay and cowthink.

![](https://www.ubuntukylin.com/upload/images/tl16.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl17.png)

### Command: yes

It is funny but useful as well, specially in scripts and for System Administrators where an automated predefined response can be passed to terminal or generated.

![](https://www.ubuntukylin.com/upload/images/tl18.png)

Tip: (it doesn't stop until you press ctrl+c)

### Command: toilet

what? Are u kidding, huhh no! Definitely not, but for sure this command name itself is too funny, and I don’t know from where this command gets it’s name.
Install toilets

! [](https://www.ubuntukylin.com/upload/images/tl19.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl20.png)

It even offers some kind of color and fonts style.

![](https://www.ubuntukylin.com/upload/images/tl21.png)

Note: Figlet is another command that more or less provide such kind of effect in terminal.

### Command: cmatrix

You might have seen Hollywood movie ‘matrix‘ and would be fascinated with power, Neo was provided with, to see anything and everything in matrix or you might think of an animation that looks alike Hacker‘s desktop.

Install cmatrix

![](https://www.ubuntukylin.com/upload/images/tl23.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl24.png)

### Command: oneko

OK so you believe that mouse pointer of Linux is the same silly black/white pointer where no animation lies then I fear you could be wrong. “oneko“ is a package that will attach a “Jerry“ with you mouse pointer and moves along with you pointer.

Install oneko

![](https://www.ubuntukylin.com/upload/images/tl25(1).png)

Output

![](https://www.ubuntukylin.com/upload/images/tl26.png)

![](https://www.ubuntukylin.com/upload/images/tl27.png)

Note: Once you close the terminal from which oneko was run, jerry will disappear, nor will start at start-up. You can add the application to start up and continue enjoying.

### Fork bomb

This is a very nasty piece of code. Run this at your own risk. This actually is a fork bomb which exponentially multiplies itself till all the system resource is utilized and the system hangs. (To check the power of above code you should try it once, but all at your own risk, close and save all other programs and file before running fork bomb).

![](https://www.ubuntukylin.com/upload/images/tl28.png)

### Command: while

The below “while” command is a script which provides you with colored date and file till you interrupt (ctrl + c). Just copy and paste the below code in terminal.

![](https://www.ubuntukylin.com/upload/images/tl29.png)

![](https://www.ubuntukylin.com/upload/images/tl30.png)

Note: The above script when modified with following command, will gives similar output but with a little difference, check it in your terminal.

![](https://www.ubuntukylin.com/upload/images/tl31.png)

### Command: espeak

Just Turn the Knob of your multimedia speaker to full before pasting this command in your terminal and let us know how you felt listening the god’s voice.

Install espeak

! [](https://www.ubuntukylin.com/upload/images/tl32.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl33.png)

### Command: aafire

How about fire in your terminal. Just type “aafire” in the terminal, without quotes and see the magic. Press any key to interrupt the program.

Install aafire

![](https://www.ubuntukylin.com/upload/images/tl34.png)

Output

![](https://www.ubuntukylin.com/upload/images/tl35.png)


![](https://www.ubuntukylin.com/upload/images/tl36.png)

### Command: bb

First install “apt-get insatll bb” and then, type “bb” in terminal and see what happens.

![](https://www.ubuntukylin.com/upload/images/tl37.png)

![](https://www.ubuntukylin.com/upload/images/tl38.png)

### Command: url

Won’t it be an awesome feeling for you if you can update you twitter status from command line in front of your friend and they seems impressed. OK just replace username, passwordand your status message with your’s username, password and “your status message“.

![](https://www.ubuntukylin.com/upload/images/tl39.png)

### ASCIIquarium

How it will be to get an aquarium in terminal.

![](https://www.ubuntukylin.com/upload/images/tl40.png)

### Install ASCIIquarium

Now Download and Install ASCIIquarium.

![](https://www.ubuntukylin.com/upload/images/tl41.png)

And finally run “asciiquarium” or “/usr/local/bin/asciiquarium“ in terminal without quotes and be a part of magic that will be taking place in front of your eyes.

![](https://www.ubuntukylin.com/upload/images/tl42.png)

![](https://www.ubuntukylin.com/upload/images/tl43.png)

### Command: funny manpages
First install “apt-get install funny-manpages” and then run man pages for the commands below. Some of them may be 18+, run at your own risk, they all are too funny.

![](https://www.ubuntukylin.com/upload/images/tl44.png)

### Linux Tweaks
It is time for you to have some one liner tweaks.

![](https://www.ubuntukylin.com/upload/images/tl45.png)

Linux is sexy: who | grep -i blonde | date; cd ~; unzip; touch; strip; finger; mount; gasp; yes; uptime; umount; sleep (If you know what i mean)

There are certain other but these don’t work on all the system and hence not included in this article. Some of them are man dog , filter, banner, etc.

Have fun, you can say me thanks later :) yup your comment is highly appreciated which encourages us write more. Tell us which command you liked the most. Stay tuned i will be back soon with another article worth reading.