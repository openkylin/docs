# openKylinCommunity-docs sig

openKylin community-docs, dedicated to improve the openKylin community documentation, to help users better and more convenient to use the openKylin system, the main work is to write all kinds of instructions documentation, including but not limited to tutorials, Q&A and other content

# Basic information

## Repository address
Main repository: [docs](https://gitee.com/openkylin/docs)

Sub-repository: [sig-documentation](https://gitee.com/openkylin/sig-documentation.git)

Mobile users see the README file by default because of the adaptive problem of the code cloud, the specific content of the warehouse visit this link: [cell phone repository directory](https://gitee.com/openkylin/sig-documentation/tree/master)

## Panelists

- stranger
- chipo
- AICloudOpser
- delong1998

## Communication method

- Mailing list: <docs@lists.openkylin.top>, [mailing list subscription page](https://mailweb.openkylin.top/postorius/lists/docs.lists.openkylin.top/)

## Work content

- Collect various problems related to openKylin, and write corresponding solutions according to your understanding
- Write all kinds of tutorials that can be used on openKylin, including but not limited to tutorials, Q&A, etc.

# Contribution guidelines
## Content requirements
- No special requirements for content direction, as long as it can be used on openKylin
- The content needs to take care of novice users, and all processes should be as detailed as possible
- The content should have a title, author and creation time, other requirements are not yet available
- No insulting words, no politically sensitive words
- No advertising
- No information that violates laws and regulations
- File names and category folders should be as concise as possible
- The .md suffix is used at the end, and the encoding is a uniform UTF-8 without BOM headers.
- Images and other resources are placed in the same level as the document and classified in the resource directory
- Image height is recommended to be around 640px, width not more than 820px, images are generally in .png format, size not more than 150K
- To avoid property rights infringement and disputes, please use original images or copyright-free images for the document.
- Pictures are recommended to be named according to the content, only using numerical sequences is not conducive to the succession of subsequent pictures
- Forbid any branch forced push
- Forbid pushing to master branch, master branch does not accept any pr request other than dev branch and also does not accept any push
- The English version of the main repository is recommended to be put in the directory corresponding to en, and the sig group members will have someone to review it.
- If there are spaces in the directory, all of them are separated by a short horizontal line, and the file names are separated by underscores.
- If there are no special instructions, the main repository refers to [docs repository](https://gitee.com/openkylin/docs) and the secondary repository refers to [sig-documentation](https://gitee.com/openkylin/sig-documentation)
- In general, tutorials written by enthusiasts will be placed in the secondary repository, while the official documentation will be placed in the main repository, but not absolutely, the sig group will also make corresponding suggestions according to the content written
- Translation class documents, the entire translation does not accept lightweight pr, can only choose fork after the pr, not much change can accept lightweight pr
- It is recommended to use relative paths for jump links within the site, unless the link is outside the repository, and try to avoid absolute paths starting with http


## Participation methods

- There are no more requirements to participate in this work, as long as you are willing to try and work according to the guidelines, and discuss with others when you encounter doubts or problems that you cannot decide, you will definitely be able to produce high-quality results
- Members of the sig group create a branch on the repository in the format dev-name, e.g. dev-moshengren, dev-chibo, and write the corresponding content on their branch.
- Non-sig members can fork the repository on gitee, make changes in the dev branch of their own repository, then add, add or modify content according to [content requirements](#content requirements), and submit it to the main repository by mentioning pr after completion, and then merge it after passing the audit.
- Non-sig members can apply to join sig through the above mailing list <docs@lists.openkylin.top> or send a separate email to the mailing list owner <docs-owner@lists.openkylin.top> to communicate, it is recommended to subscribe first, non-subscribers' emails will be temporarily blocked, mailing list subscription connection ：[docs subscription page](https://mailweb.openkylin.top/postorius/lists/docs.lists.openkylin.top/)
- [sig application to join the process](https://docs.openkylin.top/zh/SIG%E7%AE%A1%E7%90%86%E6%8C%87%E5%8D%97/SIG%E7%BB%84%E7%9A%84%E7%94%B3%E8%AF%B7%E4%B8%) 8E%E6%92%A4%E9%94%80%E6%B5%81%E7%A8%8B)
- Before participating in the contribution can first mention an Issue in the warehouse (only limited to add), explain what you want to write, easy to see if someone is writing what they want to write, if you want to modify some content can be modified later to mention pr
- If you don't know how to write markdown format documents, you can search for markdown document syntax in search engines or communicate through mailing lists and let sig members help you convert.

## About the license

The repository uses CC BY-SA 4.0 license by default, if you have other needs, you can indicate the specific CC license version under your own document.

The following content inserted at the end of the document will automatically become the corresponding license when the document platform generates the page

```
Copyright: This article was originally written by <xxx>, modified by <xxx>, and licensed under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license, please include a link to the source and this statement.

```