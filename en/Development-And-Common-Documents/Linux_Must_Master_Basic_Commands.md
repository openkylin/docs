# <center>The basic networking commands you must master in Linux!</center>
#### <center>author:little K</center>
#### <center>2022-04-22 23:36:00</center>


Whether you are an aspiring Linux system administrator or a Linux fanatic, these are the basic and important Linux networking commands you must know!

In the process of learning Linux, we all pay a lot of attention to the command line, and I'm sure we've read many books to learn it. In today's article, I (note: the author is Abhishek Prakash) give you a summary of the set of network commands that helped me get a high score in my computer network engineering class. Feel free to take out your cheat sheet and write it down, i hope it will be useful for you too.

### Network connectivity
Ping: Sends an ICMP echo request message to the host and continues until you press Ctrl+C. Ping means a packet is sent out from your machine via ICMP and then gets a response at the IP layer. ping can detect if you are connected to another host.
Telnet host: Interacts with the host on the specified port. telnet port defaults to 23. Other more common ports are sendback port 7, used to send mail SMTP 25, and user query 79. use Ctrl+] to exit telnet.

### ARP
ARP is used to convert IP addresses to Ethernet addresses. roots users can add/remove ARP entries. The ARP entries are cached in the kernel and are usually deleted automatically after 20 minutes. However, the root user can create permanent ARP entries.
arp -a: print ARP table
arp -s[pub]: add entries
arp -a -d: delete all entries

### Routing
netstat -r: prints the routing table. The routing table is stored in the kernel, and ip sends packets through it to the outside network.
routed: BSD daemon that performs dynamic routing. Implements the RIP routing protocol. Can only be used under root privileges.
gated: gated is another routing daemon that implements RIP. Uses both OSPF/EGP/RIP. Can only be used under root privileges.
traceroute: Can be used to trace IP packets through routing information.
netstat -rnf inet: Displays the routing table for IPv4.
sysctl net.inet.ip.forwarding=1: Enables packets to continue passing (turns a host into a route).
route: The route command is used to set static routes in the routing table. All messages from the PC to the IP/SubNet go through the specified gateway IP. this command can also be used to set a default route.
route add|delete [-net|-host]: Add/delete static routes (e.g., route add 192.168.20.0/24 192.168.30.4).
route flush: Delete all routes.
route add -net 0.0.0.0 192.168.10.2: Add a default route.

### Important files
/etc/hosts: IP address and name
/etc/networks: IP addresses and network names
/etc/protocols: protocol number and protocol name
/etc/services: tcp/udp service names corresponding to port numbers

### Tools and network performance analysis
ifconfig[up]: turn on the interface
ifconfig[down|delete]: stop the interface
tcpdump -i -vvv: tool to grab and analyze packets
netstat -w [seconds] -l [interface]: show network settings and data

### Other
nslookup: Converts IPs to names or names to IPs by querying DNS servers. e.g., nslookup ubuntukylin.com will get the IP address of ubuntukylin.com.
ftp: Transfer files between the local host and the remote host.
rlogin: Log in to the remote host.
