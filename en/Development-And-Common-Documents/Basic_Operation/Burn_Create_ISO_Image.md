# openkylin Basics Burning Creating ISO images
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

Logo in the lower left corner - Other - Burning

![image](../assets/KVM/image/ok-createiso-1.png)

Add burn data

![image](../assets/KVM/image/ok-createiso-2.png)

create mirror

![image](../assets/KVM/image/ok-createiso-3.png)

Check the created object

![image](../assets/KVM/image/ok-createiso-4.png)

The form of installing files in the ISO can generally be used in: the virtual machine transfers files by mounting the image

&emsp;