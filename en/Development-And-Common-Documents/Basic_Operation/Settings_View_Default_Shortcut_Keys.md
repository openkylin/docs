# openkylin basic settings View system default shortcut keys
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Devices - Shortcuts

![image](../assets/KVM/image/ok-sck-1.png)

hot key

![image](../assets/KVM/image/ok-sck-2.png)

I mainly use "Screenshot" and "Take a screenshot of an area", these two shortcut keys.

&emsp;