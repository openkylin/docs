# openkylin basic terminal three startup methods
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Logo in the lower left corner - MATE terminal

![image](../assets/KVM/image/ok-openterminal-1.png)

![image](../assets/KVM/image/ok-openterminal-2.png)

Right click on desktop - open terminal

![image](../assets/KVM/image/ok-openterminal-3.png)

![image](../assets/KVM/image/ok-openterminal-4.png)

shortcut key ctrl + alt + t

![image](../assets/KVM/image/ok-openterminal-5.png)

&emsp;