# openkylin basic desktop display
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

![image](../assets/KVM/image/ok-desktop-1.png)

Start Menu

![image](../assets/KVM/image/ok-desktop-2.png)

Software Store - Mobile Apps

![image](../assets/KVM/image/ok-ss-1.png)

I am using a virtual machine. To install a mobile application, the mobile operating environment needs to be initialized, and the mobile operating environment does not support virtual machines, so the attempt failed. I will have a chance to find a physical machine and try again later.

![image](../assets/KVM/image/ok-ss-2.png)

power menu

![image](../assets/KVM/image/ok-power-1.png)

Hibernation Sleep Lock Screen Logout Reboot Shutdown

&emsp;