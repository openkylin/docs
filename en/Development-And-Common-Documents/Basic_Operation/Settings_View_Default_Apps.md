# openkylin basic settings view default application
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Apps - Default Apps

![image](../assets/KVM/image/ok-defaultsf-1.png)

In daily use, we install new software, but when we double-click the file to run, we find that the program is still old. At this time, we can solve this problem by modifying the default application to open the file.

&emsp;