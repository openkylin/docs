# openkylin basic settings view hibernation settings
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - System - Power

![image](../assets/KVM/image/ok-screen-sleep-1.png)

You can view the sleep settings in the power interface

&emsp;