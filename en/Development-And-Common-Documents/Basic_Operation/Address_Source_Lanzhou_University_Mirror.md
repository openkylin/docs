# <center>Replace the address of the source server with the mirror image of Lanzhou University</center>
#### <center>Author: lerambo</center>
#### <center>2022-08-04 11:25:00</center>


### 1. Enter the command
```bash
sudo pluma /etc/apt/sources.list
```
### 2. Replace the mirror address of the file content
```bash
#Original mirror address
#deb http://archive.build.openkylin.top/openkylin/ yangtze main cross pty
#deb http://archive.build.openkylin.top/openkylin/ yangtze-security main cross pty
#deb http://archive.build.openkylin.top/openkylin/ yangtze-updates main cross pty


#Lanzhou University mirror address
deb http://mirror.lzu.edu.cn/openkylin/ yangtze main cross pty
deb http://mirror.lzu.edu.cn/openkylin/ yangtze-security main cross pty
deb http://mirror.lzu.edu.cn/openkylin/ yangtze-updates main cross pty
```