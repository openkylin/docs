# openkylin basic properties Check the read, write and execute permissions of the file
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


Right click on the file - Properties

![image](../assets/KVM/image/ok-showfilepri-1.png)

View file permissions

![image](../assets/KVM/image/ok-showfilepri-2.png)

Permissions are read, write, execute

Classification includes user, user's group, other

&emsp;