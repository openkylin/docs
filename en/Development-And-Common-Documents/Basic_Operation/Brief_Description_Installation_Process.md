# Openkylin basics Brief description of the installation process
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Welcome and selection interface

![image](../assets/KVM/image/openkylin-install-1.png)

Check the platter for errors

![image](../assets/KVM/image/openkylin-install-2.png)

test memory

![image](../assets/KVM/image/openkylin-install-3.png)

If you use vmware, set the default disk size allocated to the virtual machine to 50G

Full disk installation and custom installation

![image](../assets/KVM/image/ok-install-dipar-1.png)

![image](../assets/KVM/image/ok-install-dipar-2.png)

Pay attention to the file system of each partition. There are knowledge points here. Interested students can study it in detail.

![image](../assets/KVM/image/ok-install-dipar-3.png)

![image](../assets/KVM/image/ok-install-dipar-4.png)

mount point

``` c
/
/boot
/data
/backup
/home
/tmp
```

&emsp;