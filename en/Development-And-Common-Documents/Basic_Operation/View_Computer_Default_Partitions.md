# openkylin basics View the default partition of the computer
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

computer

![image](../assets/KVM/image/ok-osfirstfiles-1.png)

You can see the file system disk and data disk

View the root directory

![image](../assets/KVM/image/ok-osfirstfiles-2.png)

In the openkylin file system, each folder has its special positioning and function, interested students can learn in depth.

View the contents of the data disk

![image](../assets/KVM/image/ok-osfirstfiles-3.png)

&emsp;