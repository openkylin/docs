# openkylin basic U disk launcher Make system boot U disk
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Use the U disk launcher to make a system boot U disk

![image](../assets/KVM/image/ok-upanos-1.png)

The USB flash drive will be formatted, so the data in the USB flash drive must be copied out in advance.

Now the USB flash drive is cheap, prepare a 3.0 port, the size is 16G or 32G.

&emsp;