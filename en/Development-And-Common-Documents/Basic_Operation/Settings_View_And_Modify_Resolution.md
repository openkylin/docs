# openkylin basic settings view and modify resolution
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Logo in lower left corner - settings

![image](../assets/KVM/image/ok-screen-1.png)

Settings - System

![image](../assets/KVM/image/ok-screen-2.png)

Monitor - Resolution

![image](../assets/KVM/image/ok-screen-3.png)

&emsp;