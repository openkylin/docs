# <center>Quick check if your computer is equipped with USB 3.0</center>
#### <center>Author: Little K</center>
#### <center>2022-04-22 23:36:00</center>
 
More and more computers are coming with USB 3.0 ports, but how do you know if your computer has a 3.0 port and which port is 3.0? Methods as below:
Open a terminal and use the following command:
$ lsusb
This command displays USB-related information in the system. Check the results. If the words "3.0 root hub" are displayed, it means that the system has USB 3.0.


Next, how to identify which port is USB 3.0?
Usually, USB 3.0 ports are marked with SS (short for Super Speed). If the hardware manufacturer doesn't mark SS or USB 3, check the color inside the port, USB 3.0 is usually blue.