# openkylin basic settings View time zone and time synchronization server
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Time Language - Time and Date

![image](../assets/KVM/image/ok-timeconserver-1.png)

Add another time zone

![image](../assets/KVM/image/ok-timeconserver-2.png)

Through the "Add" function, you can view the time in other time zones, but you can't see it on the desktop, you can only view it in the time and date window.

&emsp;