# openkylin basics set global search
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Improve global search efficiency by enabling indexing

![image](../assets/KVM/image/ok-search-v-1.png)

&emsp;