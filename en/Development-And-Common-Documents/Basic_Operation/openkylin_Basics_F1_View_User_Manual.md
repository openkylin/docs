# openkylin basics F1 view user manual
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

I know two ways to bring up the user manual

1. Use the shortcut key F1
2. Through Settings-Help

Mainly talk about the operation steps of the second method.

&emsp;

Logo in lower left corner - settings

![image](../assets/KVM/image/ok-doc-1.png)

Set the three horizontal lines in the upper right corner of the interface

![image](../assets/KVM/image/ok-doc-2.png)

help

![image](../assets/KVM/image/ok-doc-3.png)

user manual

![image](../assets/KVM/image/ok-doc-4.png)

By reading the user manual, some problems can be effectively solved

&emsp;