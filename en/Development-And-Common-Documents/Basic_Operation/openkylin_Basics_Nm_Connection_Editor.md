# nm-connection-editor Open the network connection configuration tool
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


``` bash
nm-connection-editor
```

![image](../assets/KVM/image/ok-bash-nm-connection-editor-1.png)

In this interface, specific network links can be configured

&emsp;