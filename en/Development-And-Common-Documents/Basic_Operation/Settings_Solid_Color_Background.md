# openkylin foundation set solid color background
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Personalization - Background

![image](../assets/KVM/image/ok-screen-color-1.png)

Retro style, like XP.
Some software that detects light leakage from the display also uses a solid color background

&emsp;