# View system information

- View kernel version

```
uname -a
```

- View system version information

```
cat /etc/os-release
lsb_release -a
```

- View CPU information

```
lscpu
```

- View memory information

```
free
```

- View disk information

```
df -h
fdisk -l
```

- View real-time information of system resources

```
top
```

- View hard disk read and write speed
```
iostat -t 1 3
# 1s once, check 3 times
```


> Copyright statement: This article is original by rechie, authorized according to [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license, please attach the source link and this statement for reprinting.