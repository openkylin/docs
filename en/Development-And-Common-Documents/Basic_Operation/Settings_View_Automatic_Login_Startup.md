# openkylin basic settings View account settings and password-free login, automatic login when booting
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Accounts - Account Information

![image](../assets/KVM/image/ok-user-1.png)

New user and related field requirements

![image](../assets/KVM/image/ok-user-2.png)

change Password

![image](../assets/KVM/image/ok-user-3.png)

view usergroup

![image](../assets/KVM/image/ok-user-4.png)

![image](../assets/KVM/image/ok-user-5.png)

&emsp;