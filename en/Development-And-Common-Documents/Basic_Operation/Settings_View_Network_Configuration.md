# openkylin basic settings view network configuration
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Settings - Network - Wired Network

![image](../assets/KVM/image/ok-shownetwork-1.png)

View the network configuration information in use

![image](../assets/KVM/image/ok-shownetwork-2.png)

Network configuration information ipv4

![image](../assets/KVM/image/ok-shownetwork-3.png)

Network configuration information ipv6

![image](../assets/KVM/image/ok-shownetwork-4.png)

Back to the main interface - advanced settings

![image](../assets/KVM/image/ok-shownetwork-5.png)

![image](../assets/KVM/image/ok-shownetwork-6.png)

View general Ethernet 802.1X security and more

![image](../assets/KVM/image/ok-shownetwork-7.png)

&emsp;