# openkylin basic system monitor view process
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

System Monitor - Processes - Applications

![image](../assets/KVM/image/ok-osmonitor-1.png)

System Monitor - Processes - My Processes

![image](../assets/KVM/image/ok-osmonitor-2.png)

system monitor - processes - all in town

![image](../assets/KVM/image/ok-osmonitor-3.png)

System Monitor - Services

![image](../assets/KVM/image/ok-osmonitor-4.png)

System Monitor - Disk

![image](../assets/KVM/image/ok-osmonitor-5.png)

Note that there are rules for disk naming, and interested students can learn about it.

Students who want to learn the operating system can start from the perspective of the process to understand the status of the process, scheduling algorithm and related dependencies.

&emsp;