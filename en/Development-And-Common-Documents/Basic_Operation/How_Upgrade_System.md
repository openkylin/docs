# How to upgrade openKylin

## Download the new installation through the official website

[Download link](https://www.openkylin.top/downloads/)

## Users who have already installed openKylin can upgrade via:

```
sudo apt update
sudo apt full-upgrade
```