# openkylin basics View the software store
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

software store

![image](../assets/KVM/image/ok-showss-1.png)

Easy and safe software installation

![image](../assets/KVM/image/ok-showss-2.png)

Can uninstall installed software

&emsp;