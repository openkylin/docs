# openkylin basic settings View system version information
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Logo in lower left corner - settings

![image](../assets/KVM/image/ok-osinfo-1.png)

Settings - System

![image](../assets/KVM/image/ok-osinfo-2.png)

System - About

![image](../assets/KVM/image/ok-osinfo-3.png)

In this interface, you can view related information such as version name, computer name and kernel.

&emsp;