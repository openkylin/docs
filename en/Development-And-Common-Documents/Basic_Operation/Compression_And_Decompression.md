# openkylin basic archive manager compression and decompression
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


![image](../assets/KVM/image/ok-zip-1.png)

Archive Manager is a compression and decompression tool

![image](../assets/KVM/image/ok-zip-2.png)

Unzip a single archive

![image](../assets/KVM/image/ok-zip-3.png)

Compress multiple files

![image](../assets/KVM/image/ok-zip-4.png)

Set password and split

![image](../assets/KVM/image/ok-zip-5.png)

&emsp;