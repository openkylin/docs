# openKylin Foundation Toolbox View full machine information
#### Author: Shi Wanwu
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

Toolbox View machine information Hardware parameters Hardware monitoring Driver management

Check machine information

![image](../assets/KVM/image/ok-toolshowpc-1.png)

hardware parameters

![image](../assets/KVM/image/ok-toolshowpc-2.png)

hardware monitoring

![image](../assets/KVM/image/ok-toolshowpc-3.png)

driver management

![image](../assets/KVM/image/ok-toolshowpc-4.png)

I installed openkylin on a virtual machine, so all I see on the driver management interface are the driver names of the virtual machine software

&emsp;