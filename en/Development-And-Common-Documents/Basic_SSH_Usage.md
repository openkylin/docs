# <center>Basic ssh usage</center>
#### <center>Author: OK</center>
#### <center>2022-04-22 23:36:00</center>


1. SSH is the abbreviation of Secure Shell, developed by the Network Working Group of IETF; SSH is a security protocol based on the application layer, generally speaking, it is most frequently used to log in to remote hosts, it has a built-in sftp module that uses encrypted transmission, and the port used is 22, which is much more secure than  The security of sftp is much better than that of ftp without encryption. The simplest command format is as follows:
   `ssh   Username@host`

    Of course this is based on the default port, if the sshserver port is not the default, it will look like this:
    `ssh      Username@host   -p      port`
    Then you will be prompted to enter a password, the password is the password of the user name you want to log in, the user must exist, and is allowed to log in only, in general, we install the system when the establishment of the user is no problem, and the default password does not return to show.

    He can also proxy the x11 protocol to transfer the interface locally and actually run it on the remote host, though it requires the remote host to install xauth and start the x11 proxy both locally and remotely to do so. Some applications may not be supported during actual testing. In addition, if the local system is win, it may not work well, if it is linux, the experience is still quite good with sufficient network speed. The downside is that it may not be possible to use Chinese input methods, only copy and paste, although you can configure the input method to start, but I have not succeeded. The other is the role of a port mapping, generally speaking, called ssh tunneling, you can map the remote port to the local, the local port mapping to the remote, or change to double, so used as an intranet penetration, the second is to expose the local service in the remote, but also the remote port into local access, with ssh encryption without worrying about being eavesdropped, etc., in general, the development will be used more In general, the development will be used more often, the database port mapping to a local port, direct access, without opening the corresponding port of the public network to improve security. For example, vnc, mapping the port of vnc to local, although the vncview prompt is not encrypted, but the data transmitted in the middle is encrypted by ssh, do not worry about being eavesdropped, but also can close the firewall of the corresponding port, others can not scan. ssh can disable the password login and only use the key login, or both, as long as there is a public key and private key, and then the public key into a specific You can use the private key for password-free login directly by having a public key and a private key in a specific file.

2. SSh a little trick, if in a period of time does not respond then you can modify the heartbeat time, every certain time to send an empty packet, keep the response, in general I do not recommend setting the time more than 30, if the network environment is almost 30 may be broken, generally set to 15, extreme cases set to 5, here in seconds.
	There is also the fact that if you exit the ssh, then the running program will be killed, you can use the screen command to let the program run in the background, and then Ctrl + a + d to return, so you can exit the ssh without worrying about the program hanging, the recovery command is also available.
    
3. If it's the first time to log in a host will appear a prompt, roughly as follows:

This is the first time you log into a new host to give the prompt, as to why this prompt, because the ssh communication process leads to, is a kind of verification method, specific you can check the Baidu, it should be noted that here only enter yes to continue, simply a y or enter is not allowed, then enter the password to log in, after successful login will display some welcome message, and the last login address.
Then enter the password to log in, after successful login will display some welcome message, and the last login address.

The bottom one looks the same as the top one, but it is actually the result of ssh login, because the terminal is displayed in the format of "username@hostname".
Once you have successfully logged in, you can use the terminal to execute some commands on the remote host

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Gorgeous divider~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is because ssh is divided into two parts, one is the server and the other is the client, the client is the ssh command we used above, the server part is not necessarily available in the desktop environment, the server version of Ubuntu will let the user choose whether to install the server or not during the installation. If not, you can directly install openssh-server, it should be noted that the default server we installed prohibits root user login, so to allow root users to log in remotely you need to modify the configuration file of the server, again without going into detail, a tutorial online to save resources.



The following articles were referenced in the writing of this article.
[https://forum.openkylin.top/forum.php?mod=viewthread&tid=193097](https://forum.openkylin.top/forum.php?mod=viewthread&tid=193097)
[https://forum.openkylin.top/forum.php?mod=viewthread&tid=193211](https://forum.openkylin.top/forum.php?mod=viewthread&tid=193211)





