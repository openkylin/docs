---
title: openKylin Packaging Guidelines
description: 
published: true
date: 2022-09-12T08:45:51.245Z 
tags: 
editor: markdown
dateCreated: 2022-05-17T07:44:07.346Z
---

# **openKylin Packaging Guide**

## About this document
This is an instructive document to show you how to make DEB package, focusing on guiding you to make your first DEB package. The production rules for each specific DEB package will be scattered in each specific sections. We will continue to update and improve this document.

When updating this document, please follow these guidelines:

Provides an overview only, ignoring edge cases. (**Big Picture principle**)

Keep text short and concise. (**KISS principle**)

Don't reinvent the wheel. (Use the link to point to an **existing reference**)

Focus on using non-graphical tools and consoles. (using **shell example**)

Be objective. (using [popcon](https://gitee.com/link?target=http%3A%2F%2Fpopcon.debian.org%2F) etc.)

## **1. Software Packaging**

An openKylin package usually contains a collection of files that define how an application or library file can be distributed and deployed through a package manager (such as apt, yum, etc.). As an alternative to source code compilation and installation, packaging, a.k.a., making a DEB package, will merge and package the application binary files, configuration documents, man/info help pages and other files into one file, thus making the software installation easier. Through packaging manager, complete a series of operations such as obtaining, installing, uninstalling, and querying.

### 1.1. **General Rules**

openKylin is trying to standardize diverse open source projects into a coherent system. Therefore, openKylin developed this packaging guide to standardize the process of making DEB.

- openKylin adheres to the general Linux Foundation Standard (LSB). This standard aims to reduce the differences between distributions.

- openKylin also adheres to the Linux Filesystem Hierarchy Standard (FHS). This standard is a reference on how to manage the Linux file system hierarchy.

- In addition to following these general rules that normal Linux distributions do, this document normalizes the actual details of packaging for the openKylin Community Edition.

### 1.2. **Packaging Basics**

Before using this document to create a DEB package, it is recommended that you familiarize yourself with the following knowledge points, which are necessary to create a high-quality software package and provide many details.

|   Skill       |     |   Links      |                                                    |
|---                |---          |---                 |---                                                              |
|   Debian Policy Documentation          | required         |            https://gitee.com/link?target=https%3A%2F%2Fwww.debian.org%2Fdoc%2Fdebian-policy%2F        |   
|   Debian Maintainer's Guide          | required         |  see the https://www.debian.org/doc/manuals/debmake-doc/index.zh-cn.html                  |

### 1.3. **See Related Documents**
If you plan to introduce the software to the openKylin official software repository, please refer to [the openKylin contribution guide](https://gitee.com/openkylin/docs/blob/master/zh/开始贡献/openKylin贡献攻略).

### 1.4 **Applicability**

In general, these guidelines apply to all versions of openKylin, including non-lifecycle versions, lifecycle versions, and development versions.

The guidelines also cover, to a certain extent, all types of packages and delivery scenarios that go into openKylin. openKylin is a community version, so it cannot guarantee that all the rules will stay the same. As for now, its core and most important basic principles will not have any major changes in the foreseeable future.

### 1.5 **Technical Reminder**

Here are some technical suggestions that can make it easier and more efficient for you to co-maintain packages with other maintainers, thereby maximizing the output of openKylin projects.

- Make your packages easy to debug.

- Keep your packages simple and easy to understand.

- Don't over-engineer the package.

- Maintain a well documented records for your package.

- Use a readable code style.

- Write comments in the code.

- Format the code to make it consistent.

- A git repository for maintaining packages.

## **2. Packaging rules**

Each operating system is self-contained, and in addition to different technical routes and milestones, the organization of software packages is also different.

The main differences are in the following aspects:

(1) Different package managers (fedora, openSUSE uses rpm, debian uses deb, etc.).

(2) Maintain a list of different software packages, including different software versions.

(3) Independent software package splitting rules.

(4) A software dependency graph that is naturally formed based on different splitting rules.

### 2.1. **Package Manager**

openKylin does not intend to reinvent the wheel, using DEB as the base, and attaching apt, dpkg, etc. to manage packages. The DEB format is an exclusive installation package format for the Debian system (including Debian and Ubuntu). With the apt software management system, it has become a very popular installation package under Linux. Maybe in the near future, if deb, apt and other tools can not satisfy the demand, openKylin will consider launching a new project.

### 2.2. **Software List & Software Selection**

openKylin has its own software list collection. Currently, it has integrated 1000+ software packages and we are continuing to enrich and improve it.

The source of openKylin's software code is the stable version directly taken from the software's native community. Meanwhile, the debian/* rule file is written and integrated according to this packaging specification.

openKylin follows the Upstream First principle.

### 2.3. **Software Splitting Rules**

Here we give some typical scenarios and practices of multiarch package splitting when using **debmake** using an upstream source code example like the following,  :

- A software library source code libfoo-1.0**.tar.gz**

- A software tool source code bar-1.0**.tar.gz**, the software is written in a compiled language

- A software tool source code baz-1.0**.tar.gz**, the software is written in an interpreted language

|   Binary Package         |   Type   |   Architecture| Multi-Arch         |   Package Content                                                          |
|---                |---          |---                 |---                                                              |---
|   lib foo1          | lib*         |       any      |same       |  Shared library, can be installed together                                                 |
|   lib foo -dev        | dev*         |  any                  |   same|           Shared library header files and related development files, which can be installed together                                       |
|   lib foo -tools       | bin*         |       any             |   foreign |  Runtime support programs, cannot be installed together                                                |
|   lib foo -doc       | doc*         |all                    |   foreign               |Shared library documentation
|   bar           | bin*         |any|foreign                    |   Compiled program files, cannot be installed together                                                |
|   bar -doc  | doc*         |        all| foreign            |   Program supporting documents files                                           |
|   baz  | scipt         | all| forign               |   Interpreted program file    |

## **3. Packaging verification**

(1) You must test your package for installation problems. The debi command can help you test all generated binary packages.

(2) Use lintian to check your .changes file. The lintian command runs a number of test scripts to check for common packaging errors. Make sure to replace the filename of the .changes file generated by your own package. The output of the lintian command is often marked with the following flags:

- E: stands for error: a determined violation of Debian Policy or a positive packaging error.

- W: stands for warning: possible violation of Debian Policy or a possible packaging error.

- I: stands for information: information for a specific packaging category.

- N: stands for Notes: Detailed information to help you debug.

- O: for overridden: a message that was overridden by the lintian-overrides file, but it was displayed due to using --show-overrides.

(3) Check whether the binary package can use the apt command set to install, remove, purge and upgrade normally after software selection and packaging. The entire testing process should be done in the following sequence of operations:

- If possible, install the previous version of the package;

- upgrade the package from the previous version;

- downgrade the package to the previous version (optional);

- completely remove the package;

- Freshly install the package;

- uninstall the package;

- Install the package again.

- completely remove the package;

If this is your first package, you should create a test package with a different version number to complete the upgrade test to avoid future problems.

(4) After installing and upgrading, verify: 1) For services, verify start/stop/restart/reload; 2) For commands, at least verify that the basic functions are available.

(5) There are built-in tests in the source code of the software package, which cannot be commented, deleted, or disabled at will. It is necessary to ensure that the make check self-test case in the access control passes when the code is submitted.

(6) Especially after software selection and upgrade, it is difficult to independently judge the impact on other software packages, therefore an integration testing is required.

## **4. Packaging Specifications**

Rules and regulations are improved via a gruadual process. It is necessary to ensure that existing rules are followed.

### 4.1. **Reliable sources**

- Do not embed precompiled binary files or libraries, all binary files or libraries included in the package must be compiled from the source package. Binary firmware-like files are exempt. If it is necessary to introduce binary, it will be decided after discussion with Technical Commitee (TC).

- Software packages should try to avoid multiple, separate, upstream projects being bundled into one package, and strive to source one package from one single community.

- Software **should** be open source software. For the definition of open source software, please refer to https://opensource.org/osd.html. If it is not open source software, it will be decided after discussion with TC.

- Integrate open source softwares without legal risk, [open source license directory](https://gitee.com/link?target=https://opensource.org/licenses/alphabetical).

- The content of the debian directory file should be adapted to openKylin, so that it is correct, accurate, clear and concise. If citing other distribution content, or from its native community, it must be stated at the top.

- Software that is on the **blacklist must not be imported**.

- Each software introduction decision is taken as a case and a reference for subsequent similar software introduction decisions. The TC is responsible for the consistency of software introduction principles.

### 4.2. **Architecture Support**

- Package maintainers should try their best to ensure that they can be compiled successfully on aarch64, x86_64, MIPS and other architectures. Construction requirements may increase with the potential future support of openKylin to other architectures.

### 4.3. **Software Split**

- The software splitting needs to be implemented according to the openKylin software splitting rules.

- Create corresponding binary package entries in the **debian/control** file for all binary packages.

- List the paths to all files in the corresponding **debian/binary package name.install** file
 
### 4.4. **Naming Rules**

- The name of the package consists of the **package name - the package version number**. If the upstream source code is obtained in the form of **hello-0.9.12.tar.gz**, you can use **hello** as the upstream source code name and 0.9.12 as the upstream version number.

- There are certain restrictions on characters that can make up the openKylin package name. The most obvious restriction is that no uppercase letters are allowed in package names. Here is a summary of the rules in regular expression form:

    - Upstream package name (**-p**): [-+.a-z0-9]{2,} 

    - Binary package name (**-b**): [-+.a-z0-9]{2,}

    - Upstream version number (**-u**): [0-9][-+.:~a-z0-9A-Z]*

    - openKylin revision (**-r**): [0-9][+.~a-z0-9A-Z]*

- The rules for version comparison can be summarized as follows:

    - Strings are compared in order from start to end.

    - Characters are larger than numbers.

    - Numbers are compared in integer order.

    - Characters are compared in ASCII-encoded order.


### 4.5. **Format Specifications**

**Debmake** can create the necessary template files for package maintainers that conform to the deb package specification. In the generated file, comment lines start with **#** and contain some tutorial text. You must remove or modify such comment lines before uploading the package to the openKylin repository.

In the generated template file, the following files are very important:

- The **debian/changelog** file records the package history and defines the upstream package version and openKylin revision on its first line. All changes should be documented in a clear, formal, and concise language style.

- The **debian/control** file describes the binary packages, compilation dependencies, and installation dependencies of the current package that will be compiled.

- The **debian/rules** file describes how to compile the package

### 4.6. **Dependencies**

Make sure that the compilation and installation dependencies of the package already exist in the openKylin repository. If not, it needs to be packaged and introduced. Compilation dependencies and installation dependencies need to be confirmed by themselves to ensure completeness.Try to avoid circular dependencies.

- #### Depends
This field describes the set of all packages that this package depends on, and can only be installed if all the dependencies on are installed.Please specify all the dependencies on here. If the dependencies are not installed, the package may not work properly.

- #### Recommends
The packages in this category are not strictly necessary to be installed in order for the program to run. When a user installs a package, all front-end package management tools ask if they want to install these recommended packages. **aptitude** and **apt-get** will automatically install recommended packages when installing your packages (users can disable this default behavior). **dpkg**  ignores this.

- #### Suggests
The packages in this category may work better with this program, but are not required. When the user installs the program, all front-end programs may not ask whether to install the suggested package. **aptitude** can be configured to automatically install suggested packages when installing software, but this is not the default. **dpkg** and **apt-get will** ignore this.

- #### Pre-Depends
The dependencies in this category are stronger than the Depends category. Packages will only install properly if the pre-dependent packages have been installed and configured correctly. This should be used with great caution and should only be used after a TC discussion. Remember: don't use this at all. :-)

- #### Conflicts
This package can only be installed after all conflicting packages have been removed. This is used when a program does not run at all or has serious problems in the presence of some specific packages.

- #### Breaks
Listed packages will be corrupted after this package is installed. Usually **Breaks** comes with a description of "the version number is smaller than...". In this way, the package management tool will choose to upgrade the broken specific version of the package as a solution.

- #### Provides
Some types of packages define multiple alternate virtual names. Use this if your program provides the functionality of an existing virtual package.

- #### Replaces
When your program replaces some files in another package, or completely replaces another package (used with **Conflicts** ). Some files in the listed packages will be overwritten by files in your package.

### 4.7. **Compiling the Build**

- The **debian/rules** script repackages the upstream build system for the purpose of compiling the source files, installing the files to **$(DESTDIR)**, and storing the generated files in each **deb** binary package file.

- Flexible customization of **debian/rules** scripts can be achieved by adding appropriate **override_dh_** targets and writing corresponding rules .

- If you need to take some special action when the **dh** command calls some specific **dh_foo** command, then any automatic actions can be overridden by additional Makefile targets like **override_dh_foo** in **debian/rules**

- Do not embed timestamps based on system time.

- Use **dh $@** in **debian/rules** to apply the latest **debhelper** features.

- Export the environment variable "**LC_ALL=C.UTF-8**" in the build environment.

- For timestamps used in upstream source code, use the value of the environment variable **$SOURCE_DATE_EPOCH** provided by debhelper.

## 5. **Example**

### 5.1. **Overall Process**
The general process for building a single non-native Debian package from the upstream source tarball **debhello-0.0.tar.gz** can be summarized as follows:

- The maintainer obtains the upstream source compressed package **debhello-0.0.tar.gz** and decompresses its contents into the **debhello-0.0** directory.

- The **debmake** command debianizes the upstream source tree. Specifically, it creates a **debian** directory and only adds various template files to that directory.

- A symbolic link named **debhello_0.0.orig.tar.gz** is created and points to the **debhello-0.0.tar.gz** file.

- The maintainer must edit and modify the template file by himself.

- The **debuild** command builds binary packages from debianized source trees.

- **debhello-0.0-1.debian.tar.xz** will be created, which contains the **debian** directory.

**The general flow of package building.**

    $ tar -xzmf debhello-0.0.tar.gz
    $ cd debhello-0.0
    $ debmake
    ... manual customization
    $ debuild
    ...

### 5.2. **Template files**

Among them, debmake is used to generate debianized template files, and the specific structure is as follows:

**The source tree after running the basic debmake command.**

$ tree

├── debhello-0.0

│ ├── LICENSE

│ ├── Makefile

│ ├── debian

│ │ ├── README.Debian

│ │ ├── changelog

│ │ ├── control

│ │ ├── copyright

│ │ ├── patches

│ │ │ └── series

│ │ ├── rules

│ │ ├── source

│ │ │ ├── control

│ │ │ ├── format

│ │ │ ├── local-options

│ │ │ ├── options

│ │ │ └── patch-header

│ │ ├── tests

│ │ │ └── control

│ │ ├── upstream

│ │ │ └── metadata

│ │ └── watch

│ └── src

│ └── hello.c

├── debhello-0.0.tar.gz

└── debhello_0.0.orig.tar.gz -> debhello-0.0.tar.gz

7 directories, 19 files

The package maintainer should provide the build script for the **debian/rules** file here. At this point, the file is a template file generated by the **debmake** command.

### 5.3. **Editing template files**

As a maintainer, making a proper Debian package will of course require some manual tweaking of the template contents.

In order to make the installation files part of the system files, the default /**usr**/**local** value of **$(prefix)** in the **Makefile** needs to be overwritten to /**usr**. To do this, add a target named **override_dh_auto_install** in the **debian/rules** file as given below and set "**prefix=/usr**" in it.

**debian/rules (maintainer version):**

    $ vim debhello-0.0/debian/rules 
    ... hack, hack, hack, ... 
    $ cat debhello-0.0/debian/rules 
    #!/usr/bin/make -f 
    export DH_VERBOSE=1 
    export DEB_BUILD_MAINT_OPTIONS=hardening=+ all 
    export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic 
    export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

    %: 
        dh $@

    override_dh_auto_install: 
        dh_auto_install -- prefix=/usr

The export =**DH_VERBOSE** environment variable in the **debian/rules** file as above can force the **debhelper** tool to output fine-grained build reports.

Exporting the **DEB_BUILD_MAINT_OPTION** variable as above can set hardening options as described in the "FEATURE AREAS/ENVIRONMENT" section in the **dpkg-buildflags** man page.

Exporting **DEB_CFLAGS_MAINT_APPEND** as above can force the C compiler to give all types of warnings.

Exporting **DEB_LDFLAGS_MAINT_APPEND** as above can force the linker to only link against really needed libraries.

For Makefile-based build systems, what the **dh_auto_install** command does is basically **"$(MAKE) install DESTDIR=debian/debhello"**. 

The **override_dh_auto_install** target created here modifies its behavior to "**$(MAKE) install DESTDIR=debian/debhello prefix=/usr**".

Here are the **debian/control** and **debian/copyright** files for the maintainer version.

**debian/control (maintainer version):**

    $ vim debhello-0.0/debian/control 
    ... hack, hack, hack, ... 
    $ cat debhello-0.0/debian/control 
    Source: debhello 
    Section: devel 
    Priority: optional 
    Maintainer: Osamu Aoki <osamu@debian.org> 
    Build-Depends: debhelper-compat (= 13) 
    Standards-Version: 4.5.1 
    Homepage: https://salsa.debian.org/debian/debmake-doc 
    Rules-Requires-Root: no

    Package: debhello 
    Architecture: any 
    Multi-Arch: foreign 
    Depends: ${misc:Depends}, ${shlibs:Depends} 
    Description: Simple packaging example for debmake 
    This Debian binary package is an example package. 
    (This is an example only)

There are some other template files in the **debian**/ directory. Depending on the scenario, they also need to be updated. You can then start building the package.

### 5.4. Building packages with debuild

You can use **debuild** or an equivalent command tool to build a non-native Debian package within this source tree.

**The files generated by debhello version 0.0 using the debuild command:**

    $ cd .. 
    $ tree -FL 1 
    . 
    ├── debhello-0.0/ 
    ├── debhello-0.0.tar.gz 
    ├── debhello-dbgsym_0.0-1_amd64.deb 
    ├── debhello_0.0-1.debian. tar.xz 
    ├── debhello_0.0-1.dsc 
    ├── debhello_0.0-1_amd64.build 
    ├── debhello_0.0-1_amd64.buildinfo 
    ├── debhello_0.0-1_amd64.changes 
    ├── debhello_0.0- 1_amd64.deb 
    └── debhello_0.0.orig.tar.gz -> debhello-0.0.tar.gz 

1 directory, 9 files

You can see all the files generated.

- **debhello_0.0.orig.tar.gz** is a symbolic link to the upstream source tarball.

- **debhello_0.0-1.debian.tar.xz** contains maintainer-generated content.
    
- **debhello_0.0-1.dsc** is the metadata file of the Debian source package.
    
- **debhello_0.0-1_amd64.deb** is a Debian binary package.

- **debhello-dbgsym_0.0-1_amd64.deb** is a binary package of debug symbols for Debian.

- **debhello_0.0-1_amd64.build** is the build log file.

- **debhello_0.0-1_amd64.buildinfo** is a metadata file generated by **dpkg-genbuildinfo**.

- **debhello_0.0-1_amd64.changes** is the metadata file for Debian binary packages.

