---
title: About Us
description: about us
published: true
date: 2023-03-08T07:30:23.959Z
tags: 
editor: markdown
dateCreated: 2023-03-08T07:30:23.959Z
---

OpenKylin is an open organization and there are many ways to contact us. This webpage will provide some common methods, but not all. Other contact methods can be found on other parts of our website.

## General Information

Most of the information about OpenKylin can be found on our website: https://openkylin.top/. Therefore, please browse and search our website before contacting us.

Our [FAQ](https://docs.qq.com/doc/DWFRHUVVCS01zeUFj?u=70a0637feb964f6bb9cceeb732675673) can answer many questions you may have about OpenKylin.

Many questions related to OpenKylin can also be addressed through our mailing list:

https://mailweb.openkylin.top/postorius/lists/

If you are certain that our website and documentation cannot solve your problem, we have a community where users and developers can join together to discuss and possibly answer your questions.

All community-related questions can be subscribed to our mailing list:

https://www.openkylin.top/community/index-cn.html , or join the community for submission.

## Promotion

If you want to request our articles or put news on our news webpage, please contact our official email at contact@openkylin.top.

## Events

Please submit invitations for seminars, exhibitions, or other events to:

https://www.openkylin.top/community/community-cn.html

## Assisting OpenKylin 

<!-- If you are willing to assist OpenKylin, please refer to the [Contribution Guidelines](/zh/开始贡献/openKylin贡献攻略) first. 
If you are interested in maintaining an OpenKylin mirror, please refer to the [OpenKylin Mirror](https://www.openKylin.top/downloads/index-cn.html). Any issues with existing mirrors can be reported to contact@openkylin.top -->
If you are willing to assist OpenKylin, please refer to the [Contribution Guidelines](/zh/开始贡献/openKylin贡献攻略) first.
If you are interested in maintaining an OpenKylin mirror, please refer to the [OpenKylin Mirror](https://www.openKylin.top/downloads/index-cn.html). Any issues with existing mirrors can be reported to contact@openkylin.top

## Harassment Issues

OpenKylin is a community that values etiquette and speech. If you are a victim of any behavior or feel harassed, whether it be in a seminar, project-organized collective development, or general project interactions, please contact the community team at the following email address: contact@openkylin.top.
