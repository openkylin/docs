# UKUI
## Introduction
UKUI desktop environment is mainly developed in Qt language, and is now used by default in KyLin Software's UkiLin open source operating system and Galaxy KyLin commercial distribution. A range of plug-ins, applications and other desktop products are integrated, and new UKUI 3.1 components are constantly being developed, which means UKUI will be actively maintained, constantly tested and developed to provide a more enjoyable interactive experience for users.

## Project Address
https://github.com/ukui