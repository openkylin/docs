---
title: UKUI
description: 
published: true
date: 2022-06-23T06:56:45.129Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:17:06.244Z
---

# UKUI
## Project Introduction
The UKUI (Ultimate Kylin User Interface) SIG team is dedicated to the planning, maintenance, and upgrading of desktop environment-related software packages to provide users with a basic graphical operating platform that meets the needs of various devices and users. It mainly includes program launchers (start menus), user configuration, file management, login lock screens, desktops, network tools, quick configurations, etc. The development tools for desktop core components are mainly Qt and C++, with the aim of consistently improving the system's operating experience and providing a desktop environment that integrates stability, aesthetics, smoothness, and convenience.

## Project Address
### Virtual package / Installation package
- [ukui-desktop-environment](https://gitee.com/openkylin/ukui-desktop-environment)

  UKUI desktop environment: This installation package is divided into three parts: UKUI core installation package ukui-desktop-environment-core, UKUI full installation package ukui-desktop-environment, and UKUI extended installation package ukui-desktop-environment-extras. The core installation package installs the necessary basic components for UKUI, providing you with a lightweight desktop environment. The full installation package installs applications such as screenshot, memo, and tablet environment on top of the basic components and recommends installing desktop environment frameworks such as theme and input method frameworks, which greatly enriches your office, life, and entertainment scenarios. The extended installation package will add some additional tools,

### Basic components

- [ukui-greeter](https://gitee.com/openkylin/ukui-greeter)

   ukui-greeter is the login program based on Lightdm for the UKUI desktop environment

- [ukui-session-manager](https://gitee.com/openkylin/ukui-session-manager)

  ukui-session-manager is the session manager for the UKUI desktop environment, which manages how we interact with the Linux shell

- [ukui-control-center](https://gitee.com/openkylin/ukui-control-center)

  UKCC (ukui-control-center) is the control panel for the UKUI desktop environment, where we can configure themes, desktop backgrounds, add users, and change passwords through the interface

- [ukui-screensaver](https://gitee.com/openkylin/ukui-screensaver)

  ukui-screensaver is the lock screen and screen saver for the UKUI desktop environment

- [ukui-menu](https://gitee.com/openkylin/ukui-menu)

  ukui-menu is the launcher for the UKUI desktop environment, where we can conveniently select the applications we want to open

- [ukui-panel](https://gitee.com/openkylin/ukui-panel)

  ukui-panel is the taskbar for the UKUI desktop environment, where we can visually see the status of the applications in use and switch between them quickly

- [ukui-sidebar](https://gitee.com/openkylin/ukui-sidebar)

  ukui-sidebar is the sidebar shortcut tool in the UKUI desktop environment, mainly including the control center and notification center. We can use it to check application messages and configure some switches

- [peony](https://gitee.com/openkylin/peony)

  Peony is the file manager for the UKUI desktop environment, which allows users to browse directories, preview files, and launch associated applications. It is also responsible for handling icons on the UKUI desktop and is compatible with both local and remote file systems

- [ukui-window-switch](https://gitee.com/openkylin/ukui-window-switch)

  ukui-window-switch is the multitasking view in the UKUI desktop environment. In PC mode, the multitasking view provides a convenient and quick interactive interface, allowing users to easily switch and locate desired windows among virtual desktops, thereby improving productivity and efficiency. In tablet mode, it is easy to manage opened windows. Users can choose to wake up or close any windows displayed in the task view, and can also enhance the interactive experience with different animations

### Basic services
- [ukui-settings-daemon](https://gitee.com/openkylin/ukui-settings-daemon)

  ukui-settings-daemon is a low-level daemon in the UKUI desktop environment, responsible for setting various parameters of the UKUI session and running applications

- [ukui-notification-daemon](https://gitee.com/openkylin/ukui-notification-daemon)

  ukui-notification-daemon is the notification service of the UKUI desktop environment, which is implemented based on the freedesktop notification specification. It efficiently manages and displays notifications from both applications and the system

- [ukui-notification](https://gitee.com/openkylin/ukui-notification)

  ukui-notification consists of the notification service (ukui-notification-server) and the notification development interface (libukui-notification) for the UKUI desktop environment

- [kylin-device-daemon](https://gitee.com/openkylin/kylin-device-daemon)

  kylin-device-daemon is the daemon responsible for device management in the UKUI desktop environment

### Tablet features
- [ukui-tablet-desktop](https://gitee.com/openkylin/ukui-tablet-desktop)

  ukui-tablet-desktop is the desktop and taskbar for the UKUI tablet, implemented using Qt/QML

- [kylin-status-manager](https://gitee.com/openkylin/kylin-status-manager)

  kylin-status-manager is the status manager for the UKUI desktop environment. It monitors and manages the rotation, folding, and other states of laptops, tablets, and other devices

- [ukui-app-widget](https://gitee.com/openkylin/ukui-app-widget)

  ukui-app-widget provides a framework for implementing small widgets on the UKUI tablet desktop. Using this framework, developers can create desktop widgets

- [ukui-system-appwidget](https://gitee.com/openkylin/ukui-system-appwidget)

  ukui-system-appwidget is the time widget for the UKUI desktop environment in tablet mode

### Specialized Applications
- [ukui-search](https://gitee.com/openkylin/ukui-search)

  ukui-search is the global search feature in the UKUI desktop environment. It provides aggregated search functionality for local files, text content, applications, settings, notes, and more. Leveraging its file indexing capability, it offers users a fast and accurate search experience. Additionally, it provides a search solution for files and file content, which can be conveniently integrated and used by developers through its API

- [ukui-clock](https://gitee.com/openkylin/ukui-clock) 

  ukui-clock is the alarm clock application in the UKUI desktop environment

- [time-shutdown](https://gitee.com/openkylin/time-shutdown)

  time-shutdown is the timed shutdown and startup application in the UKUI desktop environment

### Short-range management
- [kylin-nm](https://gitee.com/openkylin/kylin-nm)

  kylin-nm, short for Kylin Network Manager, is the network front-end for the UKUI desktop environment. It provides features such as wired and wireless connection management

- [libkylin-nm-base](https://gitee.com/openkylin/libkylin-nm-base)

  libkylin-nm-base is the plugin development package for the kylin-nm component in the UKUI desktop environment
  
- [kylin-nm-plugin](https://gitee.com/openkylin/kylin-nm-plugin)

  kylin-nm-plugin is a plugin for the kylin-nm component in the UKUI desktop environment

- [ukui-bluetooth](https://gitee.com/openkylin/ukui-bluetooth)

  ukui-bluetooth is the Bluetooth tool in the UKUI desktop environment

### Style theme
- [qt5-ukui-platformtheme](https://gitee.com/openkylin/qt5-ukui-platformtheme)

  qt5-ukui-platformtheme is the platform theme in the UKUI desktop environment. The theme is designed to unify and beautify all Qt applications based on the UKUI design

- [ukui-globaltheme](https://gitee.com/openkylin/ukui-globaltheme)

  ukui-globaltheme is the system theme on the UKUI desktop environment, which includes two themes: "Seeking Light" and "He Yin". Users can switch between these themes by selecting them in the theme options

- [ubuntukylin-theme](https://gitee.com/openkylin/ubuntukylin-theme)

  ubuntukylin-theme is the Ubuntu Kylin theme, which includes the default theme for Ubuntu Kylin

- [openkylin-theme](https://gitee.com/openkylin/openkylin-theme)

  openkylin-theme is the Open Kylin theme, which includes the default theme for Open Kylin

- [dmz-cursor-theme](https://gitee.com/openkylin/dmz-cursor-theme)

  dmz-cursor-theme is a style-neutral, scalable cursor theme in the UKUI desktop environment. These themes originated from the Industrial theme developed for the Ximian GNOME desktop and provide scalable black and white cursors in an extensible format

### Biometric recognition
- [biometric-authentication](https://gitee.com/openkylin/biometric-authentication)

  biometric-authentication is the biometric recognition framework in the UKUI desktop environment, which is used for various biometric recognition and authentication

- [ukui-biometric-auth](https://gitee.com/openkylin/ukui-biometric-auth)

  ukui-biometric-auth is the UKUI authentication agent for PolicyKit-1. The ukui-polkit package supports both generic and biometric authentication, and this service is provided by biometric-auth

- [ukui-biometric-manager](https://gitee.com/openkylin/ukui-biometric-manager)

  ukui-biometric-manager is the biometric authentication manager in UKUI desktop environment, which is a tool for managing biometric device drivers and user functions, as well as controlling whether biometric authentication is enabled or not

### Other (to be categorized and supplemented)
- [ukui-interface](https://gitee.com/openkylin/ukui-interface)

  ukui-interface provides various API interfaces for applications at the desktop environment layer, including operating system, user account, personalization, time, language, security management, system updates, network management, hardware management, and more

- [ukui-power-manager](https://gitee.com/openkylin/ukui-power-manager)

  ukui-power-manager is the power management application in the UKUI desktop environment

- [ukui-kwin](https://gitee.com/openkylin/ukui-kwin)

  ukui-kwin is the default window manager for the UKUI desktop environment, derived from KWin

- [ukui-kwin-effects](https://gitee.com/openkylin/ukui-kwin-effects)

  ukui-kwin-effects is the old version of the multitasking view application in the UKUI desktop environment, which is no longer maintained. The new version can be found in [ukui-window-switch](https://gitee.com/openkylin/ukui-window-switch).

- [ukui-system-monitor](https://gitee.com/openkylin/ukui-system-monitor)

  ukui-system-monitor is a system monitoring application in UKUI desktop environment, which allows users to view and manage running processes in a graphical way. It also displays system resources such as CPU and memory usage, as well as file systems.

- [ukui-touch-settings-plugin](https://gitee.com/openkylin/ukui-touch-settings-plugin)

  ukui touch settings plugin is a collection of plugins for touch related devices, including touchscreen settings, touchscreen settings, and touchpad settings, which are loaded by the control panel

- [ukui-input-gather](https://gitee.com/openkylin/ukui-input-gather)

  ukui-input-gather is a background service responsible for event distribution, with two main functions: first, it is used by kwin to handle touchpad gestures; second, it is used by state management to monitor mode switching

- [ubuntukylin-default-settings](https://gitee.com/openkylin/ubuntukylin-default-settings)

  ubuntukylin-default-settings is the default settings for the Ubuntu Kylin desktop. This software package contains the default settings used by Ubuntu Kylin

- [openkylin-default-settings](https://gitee.com/openkylin/openkylin-default-settings)
  
  openkylin-default-settings is the default settings of the Open Kylin desktop. This software package contains the default settings used by Open Kylin

- [ubuntukylin-wallpapers](https://gitee.com/openkylin/ubuntukylin-wallpapers)

  ubuntukylin-wallpapers are outstanding wallpapers selected from the Ubuntu Kylin 13.10 wallpaper competition, which are expected to showcase wonderful Chinese styles

- [kylin-app-manager](https://gitee.com/openkylin/kylin-app-manager)

  Kylin App Manager is an application management component of the UKUI desktop environment, responsible for launching applications.

- [kylin-app-cgroupd](https://gitee.com/openkylin/kylin-app-cgroupd)

   kylin-app-cgroupd is responsible for process management in the UKUI desktop environment and acts as the Kylin system resource observer and process manager

- [kylin-usb-creator](https://gitee.com/openkylin/kylin-usb-creator)

  kylin-usb-creator is a USB startup disk creation tool in the UKUI desktop environment

- [kylin-user-guide](https://gitee.com/openkylin/kylin-user-guide)

  kylin-user-guide is the user guide for the UKUI desktop environment

- [peony-extensions](https://gitee.com/openkylin/peony-extensions)

  peony-extensions is the extension package for the penoy component in the UKUI desktop environment. It adds additional functionality to the peony file manager

- [youker-assistant](https://gitee.com/openkylin/youker-assistant)
  
  youker-assistant is a toolbox application in the UKUI desktop environment, which displays four sections of information to the user: system information, hardware parameters, hardware monitoring, and driver management

- [libpwquality](https://gitee.com/openkylin/libpwquality)

  libpwquality is used for password quality checking and generating random passwords that pass the check

- [qt5-gesture-extensions](https://gitee.com/openkylin/qt5-gesture-extensions)

  qt5-gesture-extensions is a library for gesture extensions in UKUI desktop environment, such as list view scrolling, etc

- [kwin](https://gitee.com/openkylin/kwin)

  KWin is an easy-to-use yet flexible composite window manager designed for the Xorg windowing system on Linux (Wayland, X11). Its main use case is in conjunction with a Desktop Shell, such as the KDE Plasma Desktop.

- [chinese-segmentation](https://gitee.com/openkylin/chinese-segmentation)

  chinese-segmentation is a Chinese word segmentation component in the UKUI desktop environment. It provides Chinese word segmentation, Chinese character to Pinyin conversion, and Chinese traditional-simplified conversion as a global singleton.

- [ukui-file-meta-data](https://gitee.com/openkylin/ukui-file-meta-data)

  ukui-file-meta-data provides file metadata parsing and user-defined data writing functionality, with the reading part of the structure and some code based on KFileMetaData. In order to meet the personalized development needs of UKUI, we have made some modifications and deletions based on KFileMetaData

- [kpipewire](https://gitee.com/openkylin/kpipewire)

  kpipewire provides a convenient set of classes for using PipeWire (https://pipewire.org/) in Qt projects. It is developed in C++ and its main target usage is QML components. kpipewire provides two main components: KPipeWire, which is the main component that connects to the application and renders PipeWire into the application, and KPipeWireRecord, which helps to record PipeWire video streams to files using FFmpeg

- [lightdm](https://gitee.com/openkylin/lightdm)

  lightdm is an X display manager used in the UKUI desktop environment for managing user sessions and logins in Linux systems. It is a customizable login interface that can support multiple users, use different desktop environments and window managers, and add custom themes and backgrounds

- [policykit-1](https://gitee.com/openkylin/policykit-1)

  Polkit is a toolkit used in UKUI desktop environment to define and handle authorizations. It is used to allow non-privileged processes to communicate with privileged processes.



