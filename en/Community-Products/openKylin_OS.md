# openKylin OS
openKylin OS is a root community desktop operating system led by the openKylin community for desktop, notebook, tablet and embedded devices with mainstream architectures such as X86, ARM and RISC-V. It adopts the leading version in terms of kernel, base libraries, and application software, and is the technical upstream of the commercial version of Kirin, which follows the same route and develops in concert with the commercial version. At the same time, openKylin Community Edition will also integrate various innovative technology applications to gather the industry's strength and jointly promote the innovative development of the Linux industry!

## Release Features
The openKylin OS is currently in version 1.0 development stage, and as of March 2023, four phase releases have been released, 0.7, 0.7.5, 0.9 and 0.9.5, incorporating new features and requirements contributed by more than 20 SIG groups from the openKylin community, with more than 750,000 cumulative image downloads on the official website.

## Multi-Architecture Support
The openKylin OS currently supports X86, ARM, and RISC-V architectures for PCs, laptops, and tablet devices, including the Raspberry Pi and COOL Pi development boards for the ARM architecture, and the Hifive unmatched, VisionFive, and RISC-V architectures. The RISC-V architecture has completed the adaptation of Hifive unmatched, Safang VisionFive, and Ali Pinto Trailing Shadow 1520. It can meet the needs of most individual users and developers!

![architecture.png](./assets/openKylin_OS/architecture.png)

## UKUI 4.0 desktop environment
The new generation of UKUI 4.0 desktop environment is installed by default, which deeply optimizes the tablet experience, including the tablet mode desktop, time and date small plug-in, file manager, sidebar, notification center, weather, toolbox, viewing, recording and other self-developed applications of the tablet mode interface and interactive operation display, etc.

![ukui4.png](./assets/openKylin_OS/ukui4.png)

## Rich application ecosystem
In addition to a variety of self-developed applications such as video, music and housekeeping, the openKylin software store also provides users with thousands of three-party mainstream software such as WPS, WeChat, QQ and Kugou music, which meet the needs of users for daily office, life entertainment, Internet access, production activities and other multi-scene applications.

![softwareShop.png](./assets/openKylin_OS/softwareShop.png)

## Tiered freezing mechanism
The "Graded Freeze" is a set of application process lifecycle management mechanism specially designed and developed by the openKylin community to "grade" applications in different states, "freeze" users' unoperated applications in a special way to free up system resources (such as CPU, disk I/O, memory, etc.) and prioritize the allocation of resources for users' currently operating applications, thus significantly improving the user experience!

![gradingFreeze.png](./assets/openKylin_OS/gradingFreeze.png)

## Interconnection
openKylin OS is pre-installed with multi-terminal collaboration tools by default, supporting convenient interconnection between Android devices and system devices, system devices and system devices, screen sharing, file synchronization management, cross-end file search and other functions, adding interconnection capabilities to openKylin OS!

![interconnection.png](./assets/openKylin_OS/interconnection.png)

## KMRE Mobile Compatible Operating Environment
openKylin OS supports KMRE mobile-compatible runtime environment, which can realize the AA (Android+ARM) ecosystem-compatible runtime in openKylin OS!

![kmre.png](./assets/openKylin_OS/kmre.png)

## VirtIO-GPU Hardware Video Acceleration
VirtIO-GPU is a GPU virtualization technology, but because the technology does not support hardware codec, the CPU occupancy is too high when playing HD video in virtual machines, which will lead to video stuttering and frame drops. For this reason, the openKylin Community Virtualization SIG group has pioneered a set of hardware codecs for VirtIO-GPU with hardware video acceleration mechanism using front and back-end architecture, which significantly improves the experience in virtual machine scenarios!

![VirtIO-GPU.png](./assets/openKylin_OS/VirtIO-GPU.png)

## openKylin Self-Developed Developer Kit
The openKylin Self-Developed Developer Kit (hereinafter referred to as OpenSDK) is dedicated to providing a secure, reliable, fast and stable developer interface for ecological construction and software development on the openKylin operating system. openSDK currently focuses on three major modules, including the Application Support SDK, System Capability SDK, and Basic Development SDK, while fully considering At the same time, the compatibility of OpenSDK is fully considered. The OpenSDK SIG group has completed the development of OpenSDK V2.0, which provides a safer, more reliable, faster and more stable development interface for the community developers and greatly improves development efficiency!

![OpenSDK.png](./assets/openKylin_OS/OpenSDK.png)

## openKylin Virtual Keyboard
openKylin virtual keyboard is a good, easy-to-use and stable virtual keyboard in tablet mode, developed by InputMethod SIG group in the lead. During the development period,InputMethod SIG group also worked deeply with Fcitx5 authors to add basic virtual keyboard support mechanism to Fcitx5 input method framework!

![InputMethod.png](./assets/openKylin_OS/InputMethod.png)

