---
title: Disclaimers
description: 
published: true
date: 2023-04-17
tags: 
editor: markdown
dateCreated: 2023-03-08
---

# Disclaimers
**General provisions**

Before accepting the services of the openKylin open-source community (https://openkylin.top/), users must carefully read and agree to this statement.

The acts of users directly or indirectly using the services and data of the openKylin open-source community (such as off-site API references, etc.) shall be deemed as having unconditionally accepted all the contents of this statement; if users have any objections to any terms of this statement, they should stop using all services provided by the openKylin open-source community.

**Article 1**

In the processes of using the services and data of the openKylin open-source community in any way (including but not limited to publishing, promoting, reposting, browsing, and utilizing content published by the openKylin open-source community or its users), users shall not engage in any behavior that violates Chinese laws and social ethics in any way through the openKylin open-source community, either directly or indirectly, and users shall abide by the following commitments:

1.The content published, reposted or provided is in compliance with Chinese laws and social ethics;

2.Users shall not interfere with, damage or infringe upon the legitimate rights and interests of the openKylin open-source community;

3.Users shall abide by the agreements, guidelines, management rules and regulations of the openKylin open-source community and related network services;

4.Users shall not interfere with, damage or infringe upon the legitimate rights and interests of other users of the openKylin community.

**The openKylin open-source community has the right to delete any content that violates the commitments above.**

**Article 2**

1.The openKylin open-source community only provides storage space for the content published by users, and does not provide any form of guarantee for the content published or reposted by users: it does not guarantee that the content meets your requirements, nor does it guarantee that the openKylin open-source community's services will not be interrupted. The openKylin open-source community shall not bear any legal responsibility for any reason that causes users to be unable to use the openKylin open-source community normally, including network conditions, communication lines, third-party websites or requirements from management departments.

2.The content published by users in the openKylin open-source community (including but not limited to the content in various product functions currently available in the openKylin open-source community) only represents their personal positions and views, and does not represent the positions or views of the openKylin open-source community. As the publisher of the content, users shall be responsible for the content they publish. For all disputes caused by the content published, the publisher of the content shall bear all legal and joint liability. The openKylin open-source community shall not bear any legal or joint liability.

3.If users publish content that infringes upon the intellectual property rights or other legitimate rights and interests of others in the openKylin open-source community, the openKylin open-source community has the right to delete it and reserves the right to hand it over to judicial authorities for processing.

**4.Since the content of web pages linked to external links is not actually controlled by the openKylin community, the openKylin community cannot guarantee the accuracy and completeness of external links provided for your convenience.**

**5.Individuals or units who believe that there is content on the openKylin open-source community that infringes upon their legitimate rights and interests should prepare materials with legal effects and contact the openKylin open-source community in a timely manner so that the openKylin open-source community can make prompt decisions.**

**Supplementary Provisions**

OpenKylin Community reserves the right to interpret, modify, and update this disclaimer.

Contact us: contact@openkylin.top