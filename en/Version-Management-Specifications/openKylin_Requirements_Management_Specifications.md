---
title: OpenKylin Requirements Management Specifications
description: 
published: true
date: 2023-03-08T08:22:46.338Z
tags: 
editor: markdown
dateCreated: 2023-08-08T08:22:46.338Z
---

## 1.1 Requirements Collection
When the release planning is clear, the product manager sends out relevant emails to collect requirements through the following two ways, the proposers of these two types of requirements and the specifications for submitting issues about requirements in Gitee are as follows:
- The technical planning requirements for the community release are proposed by the community technical committee, and the product manager enters the requirements issues into the Release-Management repository, selects the issues type as "Requirements", fills in the requirements title and details, marks the release SIG and feature tags, and marks the release milestone.
- The planning requirements of each SIG project for this version are proposed by each SIG group, and the Maintainer of each SIG group will enter the requirements into the repository of this SIG project, select the issues type as "Requirements", fill in the requirements title and details, mark this SIG and feature tag, and mark the version milestone.
- After all requirements are recorded on Gitee, the product manager can filter and summarize all requirements for the entire version via the full issues library of the community.

Explanation:
- Mark all requirements issues with SIG ownership tags to associate them with SIG groups.
- The requirements details must be clearly filled in according to the template: requirements background, requirements description, implementation plan, and acceptance criteria.
- For requirements that need to be protected, check the Content Risk Identification checkbox to prevent external members from accessing the repository.
- When performing data statistics, count requirements according to the issues type, and mark the feature tag to facilitate filtering on the Gitee web pages.

## 1.2 Requirements Review
When the product manager summarizes the requirements, the technical committee is organized to complete the review, and after the review is passed, the review conclusion is notified to all subscribers through the mailing list, and the following operations are performed on the code cloud issues:
- For requirements issues to be completed in this version, verify the correct association with a milestone.
- For requirements issues planned to be completed in future versions, verify the correct association with a milestone.
- For requirements issues with no plans for the time being, do not associate them with a milestone, change the status to "rejected," and wait for new plans to modify the status to "confirmed" and correctly associate them with the milestone.

## 1.3 Requirements Refinement
When the review of requirements is completed, all the requirements issues planned to be completed in this version shall output the Requirements Statement Document within one week, and the document can be submitted through the following ways:
- Requirements document: create a PR, select the target branch, fill in the title as: "[Requirements Issues Title] Requirements Document," describe the PR details in the first line: This document is a detailed description of the requirements (issues number and link), and tag it with PRD, selecting the version milestone.
- After submitting the PR, when the reviewer approves it, merge it.
- Requirements issues: After the requirements document PR is merged, comment on the requirements issues stating the requirements document PR link.

## 1.4 Requirements Scheduling
When Release SIG group specifies the release plan, the product manager can notify all subscribers of the release plan through the mailing list and notify each SIG group to schedule the requirements issues as follows:
- The SIG Maintainer for each SIG group confirms the requirements issues completion plan for the respective SIG group, marks the "start date" and "end date" on Gitee, and can set the "priority."
- After the requirements issues scheduling is completed, the Release SIG team publishing manager checks the requirements issues scheduling situation, and if there are any disputes about the requirements, an evaluation can be organized.

Explanation:
- The evaluation conclusion can be notified to all subscribers via the email list.

## 1.5 Requirements Changes
In the version plan, if there are any changes in the requirements scope and plan, the product manager must organize a technical committee/Release SIG group change review. After the review is completed, the product manager will notify all subscribers of the conclusion via the email list and take the following actions:
- In the comment box of the changed requirements issues, mark the reason for the change and the review conclusion of this requirements.
- For the requirements issues with changes in the milestone, confirm the association with the milestone plan after the change and set the correct scheduling. For requirements issues without plans, do not associate them with a milestone, and change the status to "rejected."
- For requirements issues with changes in scope, the requirements leader needs to output the modified requirements document, and submit it in accordance with section 1.3.

Explanation:
- The evaluation conclusion can be notified to all subscribers via the email list.