---
title: Community Profile
description: 
published: true
date: 2022-05-17T07:16:03.826Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:23.903Z
---

# openKylin Community Introduction
Welcome to the openKylin community!

## Community Vision
 The openKylin Community (hereinafter referred to as "the Community") is an open source community formed by various enterprises, non-profit organizations, community organizations, universities, research institutions and individual developers on the basis of open source, voluntary, equality and collaboration. By implementing transparent and open governance to solidify open source collaborative development, it has become a global open source community.
 The openKylin community welcomes and encourages participation from all.
No matter how you know yourself, or how others perceive you: We all welcome you. We welcome contributions from anyone, as long as the interaction with our community is positive.

## Communication
 The openKylin community has multiple communication channels, please refer to [Community Communication](https://www.openkylin.top/community/community-cn.html).
Everyone is welcome to join the communication and create a good technical atmosphere.

## Community Governance
### Organizational structure
 openKylin has the following official community organization types.
- Community Council
- Secretariat
- Technical Committee 
- Advisory Board
- Ecology Committee
- SIG

### Contact information
 The openKylin community is an open community and there are several ways to contact us, please refer to [Contact](https://www.openkylin.top/community/) for more information.

Translated with www.DeepL.com/Translator (free version)