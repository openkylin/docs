---
title: 03. Integrate
description: Integrate and use CCAI runtime environment
published: true
date: 2022-05-17T07:17:22.071Z
tags: ccai
editor: markdown
dateCreated: 2022-03-11T03:18:20.908Z
---

# How to get CCAI components/images access

You can visit [https://www.ukylin.com/cooperation/intel.html](https://www.ukylin.com/cooperation/intel.html).

# How to install the pre-built runtime and verify it quickly

## Prepare and quick start

> Note: all components versions mentioned below in this document are for example, they will change/update according to new features/new releases in following days, so please replace the specific version with those exact versions/releases depending on what you get.
{.is-info}

This part is targeting only for the Cloud Client AI Service Framework released on 20210201+ with OV 2021.1 as default inference backend. Pre-condition to meet:

**System requirements**:

1. Install ubuntu 20.04 or UKylin release on your host.
   Execute (if need) 
   ```bash
   $> sudo apt update
   $> sudo apt install docker.io libgrpc++1
   ```
2. Configure Kernel to support docker, UKylin kernel (if you would like to use UKylin kernel instead of your own one) must be configured and compiled with the following required configuration options:

   ```
   CONFIG_CGROUP_DEVICE=y 
   CONFIG_BRIDGE=y 
   ```

## Proxy setting

If you are behind a firewall, you may need to set a proxy correctly for pulling our pre-built docker image of CCAI.Please execute the following commands to check the setting:

```bash
$> env | grep proxy
no_proxy=localhost
https_proxy=http://your-proxy-server:your-proxy-port
http_proxy=http://your-proxy-server:your-proxy-port
```

If you configure docker using proxy, please add *nvbox.bj.intel.com* to `no_proxy`.

## Container image preparation

If you want to have a quick try instead of building your own framework image system, you can pull our existing docker image directly. (Otherwise, please build your own docker image as described at following chapters) in the latter part of this document before you install host packages and run any testing.):

```bash
$> docker pull TBD
```

## Download and install service-framework packages/test cases/docker files in host

You can checkout `DEB` resources from *https://www.ukylin.com/cooperation/intel.html*.

```bash
$> sudo dpkg -i service_runtime_debs/*.deb
```

## Start/Stop service-framework

By default service-framework will be started automatically after the installation.You can manually stop / start service-framework by following the instructions:

```bash
# Stop:
$> sudo systemctl stop service-runtime-health-monitor.service
$> sudo  systemctl stop service-runtime.service
# Start:
$> sudo systemctl start service-runtime-health-monitor.service
$> sudo  systemctl start service-runtime.service
```

Now, the service container will use ports 8080 and 8081 for http services and gRPC services respectively, if you’d like to change the ports to fit your requirements, you can change the default ports setting in file */lib/systemd/system/service-runtime.service*. 

Open this file, and find this line:

```
ExecStart=/opt/intel/service_runtime/service_runtime.sh start --port-rest-api 8080 --port-grpc 8081
```

Then change the ports setting according to your needs.

## Verify CCAI functions with samples or test cases

You can execute ‘docker ps | grep service_runtime_container’ to check if the CCAI container is running.

```bash
$ docker ps | grep service_runtime_container
c9372e841a00        574dee124467            "/start.sh"         16 minutes ago      Up 16 minutes   127.0.0.1:8080-8081->8080-8081/tcp, 127.0.0.1:50006-50007->50006-50007/tcp   service_runtime_container
```

You can execute ‘docker exec -it service_runtime_container ps -ef’ to view all processes running in the CCAI container.

```
$ docker exec -it service_runtime_container ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
www-data       1       0  0 02:33 pts/0    00:00:00 /sbin/docker-init -- /start.
www-data       6       1  0 02:33 pts/0    00:00:00 /bin/bash /start.sh
```

If you want to do more tests, please refer to [Test Cases](https://docs.ukylin.com/en/Intel-CCAI-Development-Manual/Testcases).

## How to file bug(s) for CCAI project

Please use this link for bug report/discussion and technical support.

*https://forum.ukylin.com*