---
title: 02. Design
description: How does CCAI work
published: true
date: 2022-05-17T07:17:10.096Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:18:05.283Z
---

# The high level call flow of CCAI

> This section available for `1.1` release.
{.is-warning}

The picture below is showing the basic working model of CCAI as a whole services provider to provide high level APIs to external users of the services container. Basically, there are 2 methods to use those services which are provided in REST/gRPC APIs form. One is calling those APIs directly, the other one is calling simulation lib APIs (we will talk simulation lib later).

![image2.png](./assets/image2.png)

# CCAI (1.1 release) stack architecture

> This section available for `1.1` release.
{.is-warning}

The architecture picture below is showing those modules and stacks in a high level picture, it shows CCAI’s components and their dependencies. It is up to date for CCAI 1.1 release. 

![image1.png](./assets/image1.png)