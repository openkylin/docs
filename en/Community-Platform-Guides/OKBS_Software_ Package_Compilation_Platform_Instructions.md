---
title: OKBS软件包编译平台使用说明
description: 
published: true
date: 2022-07-20T12:42:46.476Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:17:11.341Z
---

# OKBS Software Package Compilation Platform Instructions

## Access address

https://build.openKylin.top/ For new users, please register a personal account. Note that the mailbox prefix will become the user's personal ID.

## Preparation before development

### Login and create PPA

This operation is the same as that of launchpad, and the PPA of the openKylin distribution is created by default.

### Add ssh public key

Use the following command:

> $ ssh-keygen -t rsa

First create the key, then use the following command if necessary:

> $ cat ~/.ssh/id_rsa.pub

Copy the output to the position shown in the picture.

![添加SSH密钥](./assets/add_ssh_key_location.png)

Click <span style="background-color:#448cff; padding:5px;border-radius:4px;color:white">Import Public Key</span> to upload.

### Add PGP public key

Create the public key using the following command:

> $ gpg --full-gen-key

First, supplement relevant information according to the prompts. The RSA type will be selected by default, with a length of 3072 bits and never expires.

Then please fill in your name and common email address.

After the creation is complete, use the following command:

> $ gpg --fingerprint

This command allows you to view the keys currently in the system.

Execute the following command to upload the key to hkp://keyserver.build.openKylin.top:11371:

> $ gpg --keyserver hkp://keyserver.build.openKylin.top:11371 --send-keys <The last 8 digits of keyid>

After the upload is complete, copy the creation key to the position shown in the picture.

![添加PGP密钥](./assets/add_gpg_key_location.png)

Click <span style="background-color:#448cff; padding:5px;border-radius:4px;color:white">import key</span> .

### Configure the dput tool

Modify the local ~/.dput.cf or /etc/dput.cf file and add the following content:

Note: Please change LOGIN to your own okbs account corresponding ID

> [okbs] 

> fqdn=build.openkylin.top:2121 

> method=sftp 

> incoming=%(okbs)s 

> login=LOGIN 

## Upload source code package to ppa

In the source code package changelog, the series code is set to yangtze.

Please ensure that paramiko is installed in the system environment before executing dput.

> $ sudo pip3 install paramiko

Install dependencies with this command.

After generating source.changes, execute the dput command according to the prompt on the ppa page.

For example:

> $ dput okbs:~xiewei/ppa <source.changes> 

After dput is executed successfully, you need to wait for the server to process the uploaded files. You can view the processing results at http://archive.build.openkylin.top/dput-logs/.

## Upload the source code package to openKylin

Please send an application to the technical committee for the new source code package. For details, please see [SIG group members and maintenance package change process](/zh/SIG使用手册/SIG组的成员与维护包变更流程).

Upload the ppa related to openkylin after confirming to obtain the corresponding permissions

> $ dput okbs:openkylin <source.changes> 
