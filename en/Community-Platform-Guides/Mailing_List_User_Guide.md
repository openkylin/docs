---
title: Mailing List User's Guide
description: 
published: true
date: 2022-06-23T07:23:30.999Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:17:23.928Z
---
# Mailing List User's Guide

## Mailing List Introduction

- Access address：<a target="blank" href="https://mailweb.openkylin.top/postorius/lists/">mailweb.openkylin.top</a>
Mail group introduction: Each mail group will have a special mailbox to manage the mail group, take UKUI as an example, as follows
ukui@lists.openkylin.top (mail group mailbox address)
ukui-request@lists.openkylin.top(mail group subscription mailbox address)
ukui-owner@lists.openkylin.top(mail group administrator email address)

## How to subscribe to SIG group

1. After entering the [mailing list homepage](https://mailweb.openkylin.top/postorius/lists/)，as shown below, click the SIG group you want to subscribe to.
---

![mail_homepage.png](./assets/mailing_list_user_guide/mail_homepage.png)

---

2. After entering the corresponding mailing group, as shown below, fill in the email address and name in the input box, and click Subscribe button below to receive the confirmation email of subscribing SIG.

![邮件列表订阅页.png](./assets/mailing_list_user_guide/邮件列表订阅页.png)

3.The subscription mailbox will receive a confirmation message to join, follow the instructions to enter the verification information to join the corresponding SIG group.

4.If the subscription is successful, you will receive a welcome email from the mailing list group, as shown in the following figure.
---

![邮件列表欢迎邮件形式.png](./assets/mailing_list_user_guide/邮件列表欢迎邮件形式.png)

---

<span style="color:red">Note: If all the above steps are normal, but you do not receive the email, please contact contact@openkylin.top the email address to explain the situation</span>


## How to unsubscribe SIG group

1. Take UKUI group as an example, send an email to ukui-request@list.openkylin.top, the content subject and body of the email contains "unsubscribe", as shown in the following picture
---

![邮件列表取消订阅形式.png](./assets/mailing_list_user_guide/邮件列表取消订阅形式.png)

---

2. Next, you will receive an unsubscribe verification email, as shown below

---

![邮件列表退订邮件形式.png](./assets/mailing_list_user_guide/邮件列表退订邮件形式.png)

---

3. According to the prompted information, bring the verification information when you reply, and use it as the subject and body of the email to complete the subscription to the SIG group, as shown in the following figure.

---

![邮件列表退订发送邮件形式.png](./assets/mailing_list_user_guide/邮件列表退订发送邮件形式.png)

---

4. After the SIG group is successfully verified, the SIG group is unsubscribed.

## How to send emails to SIG group members
Any member can send email to all members of SIG group, the email address is the address of SIG group, take UKUI as an example, the email address is ukui@lists.openkylin.top.
