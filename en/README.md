---
title: README
description: 
published: true
date: 2022-05-17T08:23:20.757Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:36.576Z
---

# Docs

#### Introduction to openKylin Community 
Welcome to openKylin community! 

Our Vision for openKylin Community 
 
The openKylin community (hereinafter referred to as "community") is an open source community composed of various enterprises, non-profit organizations, community organizations, colleges and universities, scientific research institutions and individual developers on the basis of open source, voluntary, equality and collaboration. Community. Through the implementation of transparent and open governance, we will consolidate open source collaborative development and become a global open source community. The openKylin community welcomes and encourages participation from all. No matter how you know yourself or how others see you: we welcome you. We welcome contributions from anyone as long as the communication with our community is positive. 

#### General information
Most openKylin information can be found on our website https://openkylin.top/, so please browse and search our website before contacting us.

Our Frequently Asked Questions (FAQ) can answer many of your questions.

You can also contact openKylin via our mailing list with other questions:

https://mailweb.openkylin.top/postorius/lists/

If you are sure that our website and documentation cannot help resolve your issues, we have a community for you to join and discuss, and maybe the users and developers in the community can help answer your questions.

All questions about the community can be reached by subscribing to our mailing list:

https://www.openkylin.top/community/index-cn.html , join the community to submit any questions you have.

#### Publicity
If you would like to request some of our articles, or if you would like to put some news on our news page, please contact our official email contact@openkylin.top. 

#### Activity
Please send invitations to seminars and exhibitions or any other type of gathering activities to:

https://www.openkylin.top/community/community-cn.html

#### Assist the community
If you are willing to help the community, please refer to the contribution guide first. If you would like to maintain an openKylin mirror, please refer to the openKylin mirror. Issues with existing mirrors can be reported to contact@openkylin.top. 

#### Harassment Issues
openKylin is a community that values etiquette and speech. If you are the victim of any conduct, or feel harassed, whether in workshops, group development organized by projects, or in general project interactions, please contact the community team at: contact@openkylin.top. 
