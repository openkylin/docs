# ****openKylin系统安装指南****

OpenKylin System Installation Guide

This document mainly explains the three installation scenarios under the X86 architecture that are most commonly used by community users. There are some differences in the steps required for installation in these three scenarios. Please choose the corresponding chapters based on your actual installation scenario.

In addition, for those who need to install openKylin OS on RISC-V and ARM development boards (advanced players), please jump to the corresponding document via the appendix at the end of this article.

# ****安装前的准备****

# ****Preparation before installation****

1, an x86\_64 bit computer;

2, a USB flash drive that can be formatted with a capacity of no less than 8GB;

tips: 1G = 1024MB, for ease of calculation, can be estimated as 1G = 1000MB

## ****配置要求****

**Configuration Requirements** 

### ****Recommended configuration for smooth user experience****

● CPU: mainstream products after 2018 (6 cores and above)

● Memory: 8GB and above (TIPS: VMs are recommended to allocate 4GB and above)

● Hard disk: 128GB and above free storage space

### ****Minimum configuration for system installation****

* CPU: products from 2015 onwards (dual core)

● Memory: no less than 4GB (tips: virtual machine should allocate at least 2GB)

* Hard disk: no less than 50GB of free storage space

# ****实体机单系统安装****

# ****Physical single-system installation****

## ****Step 1: Download the openKylin image****

Go to the official website to download the x86\_64 image（[https://www.openkylin.top/downloads/628-cn.html](https://www.openkylin.top/downloads/628-cn.html)

![](./assets/OpenKylin_System_Installation_Guide/1714490743.1359885.png)

tips：After downloading the image file, please check whether the MD5 value of the file is the same as that on the official website, if it is not, please download it again

The official website also provides multiple image stations for download（[https://www.openkylin.top/downloads/mirrors-cn.html](https://www.openkylin.top/downloads/mirrors-cn.html)

![](./assets/OpenKylin_System_Installation_Guide/1714490743.136656.png)

## ****第二步：选择一个启动盘制作工具制作启动盘****

## ****Step 2: Choose a boot disk creation tool to create a boot disk****

It is recommended to use rufus or ventoy to make the openKylin system boot disk.

1. rufus

Go to the official website (http://rufus.ie/) to download and install the rufus tool, and then configure it according to the following figure to complete the creation of the boot disk

![3-rufus](./assets/OpenKylin_System_Installation_Guide/1714490743.1379223.png)

2、ventoy

Go to the official website (https://www.ventoy.net/) to download the ventoy tool, follow the steps below to create a boot disk, and then copy the to-be-installed image to a USB flash drive.

![4-ventoy](./assets/OpenKylin_System_Installation_Guide/1714490743.1385467.png)

## ****第三步：通过启动盘（Ventoy为例）启动电脑****

**Step 3: Boot your computer via boot disk (Ventoy for example)**

Turn off the computer, insert the prepared startup disk into the computer USB interface, and then start the computer while continuously pressing the delete, F2, F10, or F12 keys to enter the BIOS interface. 

Tips: Different brands and models of computers may vary. You can search on Baidu to find out which key your computer presses to enter the BIOS

Turn off secure boot and make the USB flash drive the first boot item, then save and reboot.

Tips: Generally, the F10 key is "save and reboot", and the BIOS interface will usually have a prompt.

![5-设置bios](./assets/OpenKylin_System_Installation_Guide/1714490743.139543.jpeg)

You will then be taken to the Ventoy boot disk interface and select the openKylin 1.0 image. As shown in the picture below:

![6-ventoy选择镜像](./assets/OpenKylin_System_Installation_Guide/1714490743.143952.jpeg)

Enter the Grub boot interface, select "Try with installing" to enter the openKylin trial desktop.

![39-安装grub引导项](./assets/OpenKylin_System_Installation_Guide/1714490743.1552382.jpeg)

Tips: The first two options boot the 6.1 kernel; the last two options boot the 5.15 kernel then you will enter the Ventoy boot disk interface, select openKylin 1.0 image. As shown in the figure below:

Enter the Grub boot interface, select "Try with installing" to enter the openKylin trial desktop.

Tips: The first two options start the 6.1 kernel; the last two options start the 5.15 kernel.

## ****第四步：开始安装系统****

Step 4: Start installing the system

On the openKylin trial desktop, click the "Install openKylin" icon on the desktop to start pre-installation configuration.

![7-试用桌面](./assets/OpenKylin_System_Installation_Guide/1714490743.1670055.jpg)

Select Language 

![8-选择语言](./assets/OpenKylin_System_Installation_Guide/1714490743.171299.png)

Select Time Zone 

![40-选择时区](./assets/OpenKylin_System_Installation_Guide/1714490743.172726.png)

Create User 

![9_创建用户](./assets/OpenKylin_System_Installation_Guide/1714490743.1758635.png)

Select the installation method (partition)

1, full disk installation (suitable for novice white)

Tips: Full disk installation will format the hard disk, please make sure that there is no need to retain the files on the hard disk!

![10_选择安装方式](./assets/OpenKylin_System_Installation_Guide/1714490743.176918.png)

2、Custom installation (suitable for advanced players)

Click "Create Partition Table" to empty the current partition (tips: this time will not really erase)

![11-清空分区表](./assets/OpenKylin_System_Installation_Guide/1714490743.1781569.png)

Then create partitions according to your needs. Generally at least need to create: root partition (for "ext4", mount point "/", space allocation greater than 15GB):

![21-创建根分区](./assets/OpenKylin_System_Installation_Guide/1714490743.179449.png)

efi partition (for "efi", space allocation 256MB~2GB).

![13-创建efi分区](./assets/OpenKylin_System_Installation_Guide/1714490743.1805716.png)

Click OK and confirm the partitioning strategy you have chosen.

Tips: It's not too late to back out, after clicking "Start Installation" the drive will be formatted\* and you will lose the original contents of the drive\*.

![14-确认分区策略](./assets/OpenKylin_System_Installation_Guide/1714490743.1815867.png)

Start the installation process

![15-开始安装进程](./assets/OpenKylin_System_Installation_Guide/1714490743.1827707.png)

## ****第五步：完成安装****

**Step 5: Complete the installation** 

When the installation progress is finished, it will indicate that the installation is complete.

![16-安装完成](./assets/OpenKylin_System_Installation_Guide/1714490743.1841571.jpeg)

Click "Reboot Now", follow the prompts to pull out the USB flash drive and press the "enter" button, the system will automatically reboot into the system, successfully installed!

![17-拔掉优盘重启](./assets/OpenKylin_System_Installation_Guide/1714490743.193047.jpeg)

# ****实体机双系统安装****

## ****第一步：下载openKylin镜像****

## ****Step 1: Download the openKylin image****

Go to the official website to download the x86\_64 image

[https://www.openkylin.top/downloads/628-cn.html](https://www.openkylin.top/downloads/628-cn.html)

tips：After downloading the image file, please check whether the MD5 value of the file is the same as that on the official website, if it is not, please download it again

![](./assets/OpenKylin_System_Installation_Guide/1714490743.2035794.png)

The official website also provides multiple image stations for download
[https://www.openkylin.top/downloads/mirrors-cn.html](https://www.openkylin.top/downloads/mirrors-cn.html)

![](./assets/OpenKylin_System_Installation_Guide/1714490743.2041817.png)

## ****第二步：选择一个启动盘制作工具制作启动盘****

## ****Step 2: Choose a boot disk creation tool to create a boot disk****

It is recommended to use rufus or ventoy to make the openKylin system boot disk.

[http://rufus.ie/](http://rufus.ie/)

1、 rufus

Go to the official website [http://rufus.ie/](http://rufus.ie/) to download and install the rufus tool, and then configure it according to the following figure to complete the creation of the boot disk

![3-rufus](./assets/OpenKylin_System_Installation_Guide/1714490743.2052565.png)

2、ventoy

Go to the official website [https://www.ventoy.net/](https://www.ventoy.net/) to download the ventoy tool, follow the steps below to create a boot disk, and then copy the to-be-installed image to a USB flash drive.

![4-ventoy](./assets/OpenKylin_System_Installation_Guide/1714490743.2058573.png)

**第三步**

**Step 3: Partition the computer disk** 

The default machine has windows installed, open the disk management tool, select the partitioned disk space, right-click on the disk, select "Compressed Volume".

![18-磁盘分区-1](./assets/OpenKylin_System_Installation_Guide/1714490743.2067277.png)

At this point, a compression window will pop up, enter the amount of compressed space size, this display is about 135GB (tips: allocated space of not less than 50GB). Confirm the amount of compressed space and click "Compress".

![19-磁盘分区-2](./assets/OpenKylin_System_Installation_Guide/1714490743.2081542.png)

After the compression is finished, there will be an extra "unallocated" free space, which will be used for installing the openKylin OS.

![20-磁盘分区-3](./assets/OpenKylin_System_Installation_Guide/1714490743.208475.png)

## ****第四步：通过启动盘（Ventoy为例）启动电脑****

Step 3: Boot your computer via boot disk (Ventoy for example)

Turn off the computer, insert the prepared startup disk into the computer USB interface, and then start the computer while continuously pressing the delete, F2, F10, or F12 keys to enter the BIOS interface. 

Tips: Different brands and models of computers may vary. You can search on Baidu to find out which key your computer presses to enter the BIOS

Turn off secure boot and make the USB flash drive the first boot item, then save and reboot.

Tips: Generally, the F10 key is "save and reboot", and the BIOS interface will usually have a prompt.

![5-设置bios](./assets/OpenKylin_System_Installation_Guide/1714490743.2091281.jpeg)

You will then be taken to the Ventoy boot disk interface and select the openKylin 1.0 image. As shown in the picture below:

![6-ventoy选择镜像](./assets/OpenKylin_System_Installation_Guide/1714490743.2132347.jpeg)

Enter the Grub boot interface, select "Try with installing" to enter the openKylin trial desktop.

![39-安装grub引导项](./assets/OpenKylin_System_Installation_Guide/1714490743.2240124.jpeg)

Tips: The first two options start the 6.1 kernel; the last two options start the 5.15 kernel.

## ****第五步：开始安装系统****

## ****Step 5: Start installing the system****

On the openKylin trial desktop, click the "Install openKylin" icon on the desktop to start pre-installation configuration.

![7-试用桌面](./assets/OpenKylin_System_Installation_Guide/1714490743.2361035.jpg)

Select Language 

![8-选择语言](./assets/OpenKylin_System_Installation_Guide/1714490743.240342.png)

Select Time Zone 

![40-选择时区](./assets/OpenKylin_System_Installation_Guide/1714490743.2416027.png)

Create User 

![9_创建用户](./assets/OpenKylin_System_Installation_Guide/1714490743.2446675.png)

Select the installation method (partition)

Tips: Full disk installation will format the hard disk, please make sure that there is no need to retain the files on the hard disk!（图21）

![21-创建根分区](./assets/OpenKylin_System_Installation_Guide/1714490743.2457573.png)

Custom installation (suitable for advanced players)

Click "Create Partition Table" to empty the current partition (tips: this time will not really erase)

Then create partitions according to your needs. Generally at least need to create: root partition (for "ext4", mount point "/", space allocation greater than 15GB):

![21-创建根分区](./assets/OpenKylin_System_Installation_Guide/1714490743.2470722.png)

efi partition (for "efi", space allocation 256MB~2GB).

![13-创建efi分区](./assets/OpenKylin_System_Installation_Guide/1714490743.2481968.png)

Click OK and confirm the partitioning strategy you have chosen.

Tips: It's not too late to back out, after clicking "Start Installation" the drive will be formatted\* and you will lose the original contents of the drive\*.

![14-确认分区策略](./assets/OpenKylin_System_Installation_Guide/1714490743.2491891.png)

Start the installation process

![15-开始安装进程](./assets/OpenKylin_System_Installation_Guide/1714490743.250388.png)

## ****第六步：完成安装****

Step 6: Complete the installation 

When the installation progress is finished, it will indicate that the installation is complete.

![16-安装完成 (1)](./assets/OpenKylin_System_Installation_Guide/1714490743.2517395.jpeg)

Click "Reboot Now", then follow the prompts to pull out the USB flash drive and press "enter", the system will automatically reboot

![17-拔掉优盘重启 (1)](./assets/OpenKylin_System_Installation_Guide/1714490743.260534.jpeg)

# ****虚拟机安装****

Virtual Machine Installation

## ****第一步：下载openKylin镜像****

Step 1: Download the openKylin image

Go to the official website to download the x86\_64 image（[https://www.openkylin.top/downloads/628-cn.html](https://www.openkylin.top/downloads/628-cn.html)

![](./assets/OpenKylin_System_Installation_Guide/1714490743.2709756.png)

tips：After downloading the image file, please check whether the MD5 value of the file is the same as that on the official website, if it is not, please download it again

The official website also provides multiple image stations for download（[https://www.openkylin.top/downloads/mirrors-cn.html](https://www.openkylin.top/downloads/mirrors-cn.html)

![](./assets/OpenKylin_System_Installation_Guide/1714490743.271572.png)

## ****第二步：创建虚拟机****

## ****Step 2: Create a virtual machine****

Install the virtual machine software on the original system, this document uses VMware Workstation as an example, click "Create a new virtual machine".

![24-打开虚拟机软件](./assets/OpenKylin_System_Installation_Guide/1714490743.2726429.png)

Default configuration is sufficient, click Next

![25-虚拟机配置](./assets/OpenKylin_System_Installation_Guide/1714490743.273354.png)

Check "Installer CD image file", click "Browse", select the openKylin operating system image file you have downloaded, click Next.

![26-选择镜像](./assets/OpenKylin_System_Installation_Guide/1714490743.2737787.png)

Default configuration is sufficient, click Next

![27-选择系统类型](./assets/OpenKylin_System_Installation_Guide/1714490743.27412.png)

Edit the virtual machine name, select the installation location, click Next.

![28-编辑虚拟机名称](./assets/OpenKylin_System_Installation_Guide/1714490743.2744422.png)

Adjust the disk size according to the prompts, and select the default configuration for the virtual disk, click Next.

Tips: Allocate disk space of not less than 50GB.

![29-设置虚拟机磁盘大小](./assets/OpenKylin_System_Installation_Guide/1714490743.2748158.png)

Confirm the virtual machine configuration, if you need to customize the adjustment, click "Customize hardware". After confirming the hardware configuration, click "Finish".

\*tips:\*\* In order to ensure that the virtual machine experience is as smooth as possible, it is recommended that the memory is not less than 4GB; CPU cores are not less than 4!

![30-确认硬件配置](./assets/OpenKylin_System_Installation_Guide/1714490743.2751987.png)

Finish creating the virtual machine. Click "Open this virtual machine".

![31-完成虚拟机创建](./assets/OpenKylin_System_Installation_Guide/1714490743.2755191.png)

Select "Try openKylin without installing (T)" to enter the openKylin trial desktop.

![32-开启虚拟机](./assets/OpenKylin_System_Installation_Guide/1714490743.276321.png)

## ****第三步：开始安装系统****

## ****Step 3: Start installing the system****

On the openKylin trial desktop, click the "Install openKylin" icon on the desktop to start pre-installation configuration.

![7-试用桌面](./assets/OpenKylin_System_Installation_Guide/1714490743.2773428.jpg)

Select Language 

![8-选择语言](./assets/OpenKylin_System_Installation_Guide/1714490743.2816374.png)

Select Time Zone 

![40-选择时区](./assets/OpenKylin_System_Installation_Guide/1714490743.282896.png)

Create User 

![9_创建用户](./assets/OpenKylin_System_Installation_Guide/1714490743.2863557.png)

Select the installation method (partition)

1, full disk installation (suitable for novice white)

Tips: Full disk installation will format the hard disk, please make sure that there is no need to retain the files on the hard disk!

![10_选择安装方式](./assets/OpenKylin_System_Installation_Guide/1714490743.287424.png)

2、Custom installation (suitable for advanced players)

Click "Create Partition Table" to empty the current partition (tips: this time will not really erase)

![11-清空分区表](./assets/OpenKylin_System_Installation_Guide/1714490743.2887242.png)

Then create partitions according to your needs. Generally at least need to create: root partition (for "ext4", mount point "/", space allocation greater than 15GB):

![21-创建根分区](./assets/OpenKylin_System_Installation_Guide/1714490743.289995.png)

efi partition (for "efi", space allocation 256MB~2GB).

![13-创建efi分区](./assets/OpenKylin_System_Installation_Guide/1714490743.2910326.png)

Click OK and confirm the partitioning strategy you have chosen.

Tips: It's not too late to back out, after clicking "Start Installation" the drive will be formatted\* and you will lose the original contents of the drive\*.

![14-确认分区策略](./assets/OpenKylin_System_Installation_Guide/1714490743.2920907.png)

Start the installation process

![15-开始安装进程](./assets/OpenKylin_System_Installation_Guide/1714490743.2933087.png)

## ****第四步：完成安装****

**Step 4: Complete the installation** 

When the installation progress is finished, it will indicate that the installation is complete.

# ****系统安装FAQ****

# ****System Installation FAQ****

Q: What should I do when the system installation is stuck at 96%?

A: When the installation progress reaches 96%, you generally need to wait a little longer (the exact time depends on the performance of the hardware). If the waiting time is too long (more than half an hour), please make sure that the space allocated to the efi partition is not enough, and reallocate the space and try again

Q：When installing with USB boot disk, "try ubuntu kylin without installation" or "install ubuntu kylin" appears, Enter to select "Install". What should I do if the monitor is black without any display after "try ubuntu kylin without installation" or "install ubuntu kylin", enter and select "install"?

A: Method 1: If the display is black, there may be a problem with the graphics card display support, try to fix it manually.

![35-安装grub界面](./assets/OpenKylin_System_Installation_Guide/1714490743.2949994.jpeg)

Move the cursor to "install ubuntu kylin" , press "e" to enter edit mode, enter command line mode.

Find ''quote splash'', add "nomodeset" after it, and press F10 to install.

![36-安装grub界面-2](./assets/OpenKylin_System_Installation_Guide/1714490743.305436.jpeg)

![37-安装grub界面-3](./assets/OpenKylin_System_Installation_Guide/1714490743.3061576.jpg)

Tips: Add different driver options according to different graphics cards, for example, if you are using Nvidia graphics card, add nomodeset.

Q：Select the boot disk to boot, can not successfully enter the grub interface

A：Please check whether the BIOS setting has turned off the security check (secure boot).

Q：How to solve the problem if there is no boot item selection interface after the dual system installation is completed?

A: There may be problems with the boot items, you can download and install EasyBCD software to repair the boot items.

Q：What should I do if there is no root partition or efi partition after customizing the installation and configuring the partitions?

A: The root partition corresponds to "/", and the efi partition needs to change the "for" type to efi when partitioning, these two partitions must be created. In addition, data backup partition (corresponding to "/data"), backup restore partition (corresponding to "/backup") and swap partition (linux-swap), please confirm whether to create these partitions according to your needs.

Q：What should I do if I am prompted "Only one efi partition can exist" after configuring the partitions?

A: There should be an efi partition in the existing Windows system, if it is a dual-system installation, we need to delete the efi partition we added. If it is a single system installation (windows system will be overwritten), delete the existing efi partition and re-add the efi partition.

Q: How to check whether my computer's BIOS is UEFI or Legacy?

A: Take windows system as an example, press "win+r" shortcut key to confirm, type "msinfo32", enter, the system information interface will appear, you can check the BIOS mode, as shown in the figure below:

![38-biso查看](./assets/OpenKylin_System_Installation_Guide/1714490743.3122053.png)
