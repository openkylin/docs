---
title: A Collection of Translation Tasks
description:
published: true
date: 2022-05-17T07:16:20.488Z
tags:
editor: markdown
dateCreated: 2022-03-11T03:16:58.824Z
---

# Translation task collection

Note: Please place the entire translated file in the openKylin/docs/en directory, and the subsequent sig group will classify it separately