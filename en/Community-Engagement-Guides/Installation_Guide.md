---
title: Installation Guide
description: 
published: true
date: 2022-07-18T09:21:33.933Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:53.878Z
---

# Installation Guide (For MacOS)

This article is for Mac notebooks with Intel and Apple Silicon chips, and details the installation tutorial for installing openKylin open source operating system in macOS via virtual machine. （For Windows and Linux installation tutorials, please move to [openKylin系统安装指南](./openKylin系统安装指南.md) Guide/openKylin download address and installation method ）

# Preparation 
The first step to install and use openKylin open source operating system is to get the openKylin open source operating system image file, we can directly download it from the openKylin official website.

Download link: https://www.openKylin.top/downloads Select the appropriate version (64-bit x86 architecture as an example)

![download-x86.png](./assets/installation-guide/download-x86.png)
# I. Installing VMware Fusion

VMware Fusion provides the best way to run Windows, Linux, and other operating systems on Apple Macs without rebooting.

Fusion 13 supports Intel and Apple Silicon Macs running macOS 12 and later and offers features for developers, IT administrators and everyday users.

Use the following link to get started with a free, fully functional 30-day trial (no registration required)

Download link: https://www.vmware.com/cn/products/fusion/fusion-evaluation.html

![VMware-Fusion-download.png](./assets/installation-guide/VMware-Fusion-download.png)

# II. Install the openKylin open source operating system

Open the virtual machine software VMware Fusion and click on New in the File menu.

! [%E6%96%B0%E5%BB%BA%E8%99%9A%E6%8B%9F%E6%9C%BA.png] (./assets/installation-guide/%E6%96%B0%E5%BB%BA%E8%99%9A%E6%8B%9F%E6%9C%BA.png)

Create the virtual machine by dragging the downloaded ISO file from the preparation into the window.

![%E5%AE%89%E8%A3%85%E8%99%9A%E6%8B%9F%E6%9C%BA.png](./assets/installation-guide/%E5%AE%89%E8%A3%85%E8%99%9A%E6%8B%9F%E6%9C%BA.png)

Once you enter the virtual machine boot screen, follow the default options and click Continue.

Select "Install the operating system later" and click Next.

Select "Linux" for the client operating system and Ubuntu 64-bit for the version, and click Next.

Enter the name of the virtual machine and choose the path to install it, click Next.

Then you can make a series of settings for memory, processor, etc. in the hardware configuration interface, click the Close button at the bottom right corner after the settings are completed, go back to the New Virtual Machine guide and click Finish. At this point, you will automatically jump to the boot screen of the virtual machine, click "Open this virtual machine" to start the system installation.

Click "Open this virtual machine" to start the system installation. 
![%E5%AE%8C%E6%88%90%E8%99%9A%E6%8B%9F%E6%9C%BA.png](./assets/installation-guide/%E5%AE%8C%E6%88%90%E8%99%9A%E6%8B%9F%E6%9C%BA.png)


Select the language setting "Chinese (Simplified)" and click Next.


Select "Shanghai" as the time zone, click next.


After the user information is set, click Next.


Wait for the installation to finish, click the "Reboot Now" button, and you can use openKylin open source OS on the virtual machine.
