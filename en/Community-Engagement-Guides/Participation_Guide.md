## openKylin Community Participation Guide

On June 24, 2022, with the theme of "Create the momentum of open source and enjoy a borderless future", the launch of openKylin, China's first root community for desktop operating systems, was successfully held online, and the openKylin open source community was officially released. The conference attracted countless developers, and was once again featured in CCTV news and top searches on many social media platforms. During the period after the release of openKylin, we have received a lot of questions from our partners, among which the most concerned are how to participate in the openKylin community correctly and how to submit personal contributions in the community correctly.

### 1. Sign the CLA

The full name of CLA is `Contributor License Agreement`, which translates to Contributor License Agreement. In order to avoid some unnecessary troubles or disputes, no matter you are an enterprise, employee or individual developer, you need to sign the CLA with the corresponding identity before adding bricks to the openKylin community. openKylin community CLA signing The address is [https://cla.openkylin.top](https://cla.openkylin.top), the following is a brief introduction to the three forms of CLA signing for the openKylin community and the corresponding signing process.

#### 1.1 Enterprise CLA Signing

The process of enterprise signing is more complicated, and the enterprise CLA signers who pass the openKylin community audit can join the community membership system.

- Apply for candidacy to the Council, enjoy guidance on community development direction, development planning and implementation guidance;
- Apply for [create](https://www.openkylin.top/join/application-cn.html) SIG group, and can assign SIG group Owner to run for the technical committee;
- Apply to [join](https://www.openkylin.top/join/application-cn.html) existing [SIG groups](https://www.openkylin.top/join/sig-cn.html) and participate in technical code and other development discussions and contributions;
- Apply to run for the Eco Committee to participate in community operation activities, co-branding promotion, gift exchange and other rights and interests.

The process for companies to sign the CLA is shown in the following diagram


![企业CLA签署流程.png](./assets/participation-guide/企业CLA签署流程.png)


1. First, submit the basic information for enterprise signing in the [enterprise signing](https://cla.openkylin.top/cla/sign/corporation_cla/openKylin-f46f289e01bc11edb8990242ac110005) page, where `enterprise`, ` contact person`, `position`, `email` and `validation code` are required fields, other information can be left out;
2. after clicking the sign button, you will receive an email in the email address you filled in the previous step, which includes the basic information filled in the previous step and three attached files, follow the instructions in the email to complete the subsequent steps;
3. openKylin community administrator will review the information filled in after receiving the mailed items, and after the review is approved, the attachments will be uploaded on the CLA platform and the enterprise administrator account of your enterprise will be automatically added, which will be sent to the email address filled in the first step;
4. log in to the enterprise administrator account on the CLA platform and change the password, at this point the signing process of enterprise CLA is completed (this step is not required, but it is recommended to complete this step in order to prevent forgetting or deleting the enterprise administrator email by mistake);

> **TIPS!**
>
> After finishing enterprise signing, the account received in enterprise mailbox is enterprise administrator account, which is used to review the signing information of associated enterprise employees. If the enterprise administrator also needs to submit contributions on gitee, he/she also needs to sign the employee CLA using the mailbox suffix that matches the enterprise to bind the gitee id, so as to pass the CI's CLA signing check!

#### 1.2 Employee CLA Signing

After your enterprise has finished signing the enterprise CLA, you can use the mailbox that matches your enterprise mailbox suffix to sign the employee CLA. The process of employee signing is shown below.

The process of employee signing is shown below. 

![员工CLA签署流程.png](./assets/participation-guide/员工CLA签署流程.png)


1. Open the [Employee Signing](https://cla.openkylin.top/cla/sign/employee_cla/openKylin-f46f289e01bc11edb8990242ac110005) page, fill in the basic information, click the Send Captcha button, and enter the email verification code into the verification code box, check the box **I have read the agreement and agree to sign**, click the sign button, a pop-up prompt employee sign has been submitted;
2. contact the enterprise administrator, who will activate the employee signing request submitted by you on the employee management page;
3. after the enterprise administrator activates your signing request, the email you applied for will receive an alert email that the CLA signing is complete, supporting that the employee CLA signing is all complete;

#### 1.3 Personal CLA Signing

If you are an individual developer or your company does not have an enterprise CLA, you can submit your contribution to the openKylin community by signing a personal CLA, the personal CLA signing process is similar to the employee CLA signing process, as shown in the figure below.


![个人签署CLA流程.png](./assets/participation-guide/个人签署CLA流程.png)


1. Open the [Personal Signing](https://cla.openkylin.top/cla/sign/person_cla/openKylin-f46f289e01bc11edb8990242ac110005) page, fill in your basic personal information, click the Send Captcha button, put the verification code from your email address Enter the verification code into the verification code box, check **I have read the agreement and agree to sign**, click the sign button, and a pop-up will appear to indicate successful signing;
2. open the personal signing mailbox, you can receive a personal CLA signing audit approval email;

> **TIPS!**
> - Each mailbox can only be signed once.
> - Each mailbox can only sign CLA once
> - The `gitee id` in the employee signing and personal signing should be filled in correctly, if it is filled in incorrectly, then the PR submitted on gitee will not pass the CI's CLA check, and your submitted PR will be tagged with `openkylin-cla/no`.

#### 1.4 Differences between the three types of signatures

Speaking of which, some of you may ask, "Why do we need to make these three forms of CLA signature? Don't worry, let me see the following picture


![三种CLA签署形式区别.png](./assets/participation-guide/三种CLA签署形式区别.png)


Employee signing is mandatory for enterprises. If the enterprise where the developer works signs the enterprise CLA, then all employees of that enterprise can sign the employee CLA belonging to that enterprise, so **contributions submitted by employees in the openKylin community (including but not limited to PR, ISSUE, etc.) are attributed to the enterprise's contribution to the openKylin community**.

If an individual developer or a developer's company does not sign a corporate CLA, then they can sign a CLA in the form of individual signing. Therefore, contributions submitted by developers** in the openKylin community through individual CLA signing form belong to the individual developer**.
### 2. Submitting Contributions

After the first step of CLA signing, you can start to submit your contributions to the openKylin community. Here is a simple repository to demonstrate how to submit your `PR contributions`.

openKylin uses the industry's popular `git workflow`, so we need to have a little basic understanding of `git` before contributing.

#### 2.1 git environment configuration

As the old saying goes, if you want to do a good job, you need to do a good job. Before we can formally introduce how to commit our own contributions, we need to configure our local ``git environment'' now.

``bash
# debian
apt install git
# arch Linux
pacman -S git
```

After the installation is complete, type `git version` in the terminal. If the output corresponds (similar to the image below), the installation was successful.


![git版本.png](./assets/participation-guide/git版本.png)


After confirming that the git installation is complete, we can enter commands in the terminal to configure our personal information, we just need to configure the simplest user information.

```bash
git config --global user.email "zhangsan@gmail.com" # Configure the commit email
git config --global user.name "zhangsan" # Configure the committer username
```

> **TIPS!**
>
> It is recommended that the `email, name` configuration information match your gitee account `binding email` and `user ID` so that you can see your contribution history heat map in your profile!

#### 2.2 Submitting a PR

In the previous section, we completed the `git` environment locally, the next is the code contribution process, before that we need to go to `gitee` to create an account, if you do not have a `gitee` account, you can click this link to create https://gitee.com/signup, after creating a gitee account, you can submit your personal contributions to the openKylin community on gitee. The submission process is shown below, and the following is an example of how to submit PR to the openKylin community on gitee through the openKylin/ukui-menu repository

![PR流程.png](./assets/participation-guide/PR流程.png)

1. Fork a copy of the repository you are interested in to your personal account on gitee (this article uses ukui-menu as an example), as follows, click the fork button to copy the repository to your personal account


![fork示例.png](./assets/participation-guide/fork示例.png)


2. After forking the repository to your personal account, you can modify the code in our local environment by cloning the code locally with the following command

   ```bash
   cd /home/test/data/work/openkylin/code # Find a path you like
   git clone https://gitee.com/jiangwei124/ukui-menu # Clone the ukui-menu code locally
   ```

3. With the second step, we have cloned the `ukui-menu` repository code under our own account to local, next find an IDE you are comfortable with to open the `ukui-menu` project or operate directly from the command line (if you feel more familiar with it), here is the simplest demonstration, add the `README-zh.md` description document, if you are not a If you are not a techie, it is generally recommended to start submitting your community contributions from ``documentation``, as this is the easiest and easiest way to pass the community review.

   ```bash
   cd /home/test/data/work/openkylin/code/ukui-menu
   touch README-zh.md # New
   vim README-zh.md # Enter the contents
   ```

   After translating the `README.md` file and saving the translated content to the `README-zh.md` file, you can commit our new content to our personal `gitee repository` via `git`, and you can see the following image on gitee to indicate that the push was successful.

   ```bash
   cd /home/test/data/work/openkylin/code/ukui-menu
   git add README-zh.md # Add the file to the staging area
   git commit -m "add README-zh.md file" # Commit the changes
   git push # Push the changes to the remote repository
   git push

   ![成功推送修改到个人仓库.png](./assets/participation-guide/成功推送修改到个人仓库.png)

   

4. After pushing our changes, you can commit your PR on gitee by clicking on the `Pull Requests` option in the image above, then click on `New Pull Request` on the right side to create a new `Pull Request`, fill in the corresponding information on the page, click on Create Pull Request, and you You have successfully submitted your first PR.


   ![gitee提交PR.png](./assets/participation-guide/gitee提交PR.png)


   > **TIPS!**
   >
   > 1. be careful not to choose the wrong branch for your personal repository when choosing a branch, and choose the `openkylin/yangtze` branch for the openKylin community target repository;
   > 2. If you have an associated issue, you can associate this Pull Request to the issue, so that when the PR is accepted and merged, the associated issue will be automatically set to the completed status!

5. At this point, your first openKylin community contribution is basically complete, and then you will wait for CI to automatically compile your submitted code, and if the CI process is successful, it will automatically merge your submitted PRs, and if it fails, the repository administrator will step in to review your PRs!

#### 2.3 Checking PR status

After the PR submission is completed, there will be a record of the submission information on the personal homepage by clicking on it to jump to the PR details page, where you can see the information shown below


![查看PR状态.png](./assets/participation-guide/查看PR状态.png)


Clicking on the link in the green box in the image above will both jump to the build log and view detailed build information! In short, everything you care about PR can be seen on the details page of your PR submission.

If you want to check whether your submitted PR has been applied to the latest ISO distribution, you can go to [openKylin ISO download page](https://www.openkylin.top/downloads/) to download the latest ISO to check.

#### 2.4 Submitting Issues

In addition to the PR contribution method, issue is also a contribution method. For example, if you stumble upon a bug while using the `openKylin system`, you can submit a bug issue in the `openKylin` organization.

- If it is a system-level bug, then you can submit a `bug issue` under the [openKylin/community](https://gitee.com/openkylin/community/issues) repository.
- If it is an application-level bug, you can submit `bug issues` under the corresponding repository in the openKylin community. For example, if you find a bug that sometimes flickers when you use kylin video, you can submit `bug issues` under [openKylin/kylin-video](https://gitee.com/) openkylin/kylin-video/issues) repository;


![提交issue.png](./assets/participation-guide/提交issue.png)


> **TIPS!**
>
> When submitting a bug issue, describe the bug as clearly as possible, and if you have a log file also click the Upload Attachment button to upload the log file to the issue!

### 3. Join SIGs

SIG is called Special Interest Group, if you want to participate deeply in the openKylin community, you can apply to join the SIG of the direction you are more interested in, the current `openKylin` community SIG list can go to https://www.openkylin.top/join The current list of `openKylin` community SIGs can be viewed at  /sig-cn.html, and the process for developers to join SIGs is as follows.


![加入sig流程.desktop](./assets/participation-guide/加入sig流程.desktop)


Next, let's demonstrate how to join the SIG by a chestnut, if I want to join the `HTML5` SIG

1. fork `openKylin/community` repository to my personal gitee account, refer to section 2.2;

2. clone the community repository under your personal account to local, and modify `community/sig/html5/sig.yaml` and `community/sig/html5/README.md`, the files are modified as follows

   **sig.yaml**

   ```yaml
   name: html5
   description: html5 team
   owner.
   - pierre4long
   - insomnia_kylin
   maintainers.
   - name: oatoam
   - name: tylwj
   - name: wonschange
   - name: jiangwei124 # add
   packages.
   - gecko_b2g
   - gaia_homescreen
   - manifests
   - gaia_system
   - gaia-buildinkeyborad
   ```

   **README.md**

   ```mariadb
   ### Maintainers
   - Tao Mao (maotao@kylinos.cn)
   - Wenjie Li(liwenjie@yhkylin.cn)
   - Changqi Wang(wanghcangqi@kylinos.cn)
   - Jiang Wei(jiangwei1@kylinos.cn) # Add
   ````

3. After the modification is done, submit the modified content to your personal gitee repository, then submit PR to `openKylin/community` repository according to part 2.2. Next is to wait patiently for SIG owner to review, after the review is passed, the CI process will automatically merge your application into the main repository!

> **TIPS!**
>
> - The new information in the name field in the sig.yaml file is your personal `gitee id`, you must `make sure the gitee id really exists`, otherwise, even if the SIG owner passes the audit, the CI process will fail, and thus your application cannot be merged into the `openKylin/community` repository correctly! .
> - Signing CLA is the a priori condition for applying SIG, you must sign CLA before applying to join SIG

### 4. Apply for Single Package Maintainer

Because the community's SIG member privileges are too high, if you want to join the SIG in the openKylin community, you need to have certain contributions in the community before the technical committee may pass your application to join the SIG, so for general developers, it is recommended to apply for single package maintainer at the beginning, the process of single package maintainer is shown in the figure below, the specific instructions for single package maintainer can be viewed in [this link document](https://gitee.com/openkylin/community/blob/master/packages/README.md)


![申请单包维护者流程.png](./assets/participation-guide/申请单包维护者流程.png)


#### 4.1 Applying to become a new single package maintainer

Suppose you want to become a single package maintainer for `ukui-menu`, but there is no information about the package in the `community/packages` directory, you can apply to become a new single package maintainer by following the procedure below

1. First fork the community repository to your personal gitee account, and then clone it locally

   ```bash
   cd /home/test/openkylin/community # Find a path you like
   git clone https://gitee.com/jiangwei124/community.git
   ```

2. Add a new `ukui-menu.yaml` file to the `community/packages` path, and enter the following as you see fit

   ```yaml
   name: ukui-menu
   path: ukui-menu 
   maintainers.
   - name: zhangsan # gitee id
     openkylinid. 
     displayname: Zhang San
     email: zhangsan@gmail.com
   ```

   > **TIPS!**
   >
   > Note that the `path` field can be omitted if the repository path name is the same as the package name. Considering that the package name does not necessarily match the gitee repository naming convention, to avoid repository creation failure, you can specify another path!

3. Submit the above changes to your personal gitee repository, and then submit PR to the community repository of the openKylin community both, and wait for the administrator to review and approve, you have `ukui-menu` repository permissions, application for a new single package maintainer has been completed!

#### 4.2 Apply for an existing single package maintainer

Similar to the previous section on becoming a new single package maintainer, applying to become an existing single package maintainer is even simpler, first you need to fork the `openKylin/community` repository to your gitee account, then you just need to modify the corresponding package yaml file. packages/ukui-menu.yaml` file with the following content

```yaml
name: ukui-menu
path: ukui-menu 
maintainers.
- name: zhangsan
  openkylinid. 
  displayname: Zhang San
  email: zhangsan@gmail.com
# Add your personal information
- name: lisi # gitee id
  openkylinid. 
  displayname: 李四
  email: lisi@gmail.com
```

This is the end of the openKylin community participation guide, if there are other questions, you are welcome to submit issues, or join the developer exchange WeChat group!