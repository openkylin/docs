
# Experience openKylin 

If you are using openKylin for the first time and do not know how to use it, you can take a look here to get a preliminary understanding of it.

1. **What is openKylin ?**—[openKylin community profile](/en/community_profile) <!-- Need to create this file -->
2. **How to use openKylin ?**—[Dowload & Usage](https://openkylin.top/downloads/index-cn.html) <!-- Need to change to english version -->
3. **Q&A**

# Sign Contributor License Agreement(CLA)

Before participating in community contributions, you need to sign the openKylin Community Contributor License Agreement (CLA).

Depending on your participation identity, choose to sign the Individual CLA, Employee CLA, or Corporate CLA. Please click the link below to sign:

* Individual CLA: If you participate in the community as an individual, please sign the Individual CLA. [Click Here](https://cla.openkylin.top/cla/default/index) 
* Corporate CLA: If you participate in the community as a corporate, please sign the Individual CLA. [Click Here](https://cla.openkylin.top/cla/default/index) 
* Employee CLA: If you participate in the community as an employee of an organization, please sign the Employee CLA. Click here. [Click Here](https://cla.openkylin.top/cla/default/index) 

# Participate in the openKylin communitiy

## 1、Contact with us

The first step to participating in the community is to find the organization and understand the daily communication channels and communication norms of community members. The specific channels are as follows. Please click on the corresponding link to join the corresponding organization:

* Mails, [Click Here](https://mailweb.openkylin.top/postorius/lists/) <!-- Need to change to english version -->
* Forum, [Click Here](https://forum.openkylin.top/portal.php) <!-- Need to change to english version -->
* Community, [Click Here](https://openkylin.top/community/index-cn.html) <!-- Need to change to english version -->
* Documentation Guide, [Click Here](/zh/社区使用指南/文档平台使用指南) <!-- Need to change to english version -->
* Mail Guide, [Click Here](/zh/社区使用指南/邮件列表使用指南) <!-- Need to change to english version -->

## 2、Participate in community activities

You can choose to participate in the following community activities that interest you:

* Regular developer meetings
* Release events
* Live streaming
* Courses

## 3、Participate in Special Interest Group(SIG)

SIG stands for Special Interest Group. In order to manage and improve workflow better, the openKylin community is organized according to different SIGs. Therefore, before making community contributions, you need to find the SIG that interests you.

Click to view the [openKylin SIG List](https://mailweb.openkylin.top/postorius/lists/)，select the SIG you are interested in and join. Click here to learn about[the usage specification of SIGs.](/zh/SIG使用手册/SIG组章程)
<!-- Need to change to english version -->

If the field you are interested in does not have a corresponding SIG group established, but you hope to establish a new relevant SIG in the community for maintenance and development, you can create a SIG group. The specific process is as follows:

Apply on the GitHub project page -> Technical Committee review -> Create basic infrastructure such as mailing lists -> Begin operation.

## 4、Start your contribution

After completing the CLA agreement signing and joining the SIG group of your interest, you can start your community contribution journey. The first step in contributing is to configure your development environment.
<!-- ，点击这里查看[开发环境配置指南]() -->

After configuring your development environment, you can start choosing ways to contribute that interest you. Specific ways to contribute are as follows:

* **Testing**

Testing is the easiest way to contribute, and it's necessary to perform multiple tests to ensure that new versions, software or features run smoothly. If you are just starting to make contributions, you can start with testing.

<!-- 

点击这里获取目前[待测试的产品和软件列表]()

点击这里获得[社区测试规范]()

点击这里申请加入[新版本测试群组]()

-->

* **Commit Issue/Solve Issue**


 <!-- **issue提交流程**：在您感兴趣的SIG组内找到issue列表—参考[issue提交指南]()按照规范提交issue -->

**Commit issues**: Find an issue in the SIG group you are interested in and submit the issue according to the guidelines.

Click here to get a collection of [issue lists from different SIG groups.](https://gitee.com/openkylin/community/issues)

Every issue has a discussion section where you can share your thoughts and ideas. Welcome to participate in the discussion.

<!-- **issue任务处理流程**：在issue列表里领用issue（[领用列表]()）—参考[issue解决规范]()进行issue处理并提交成果 -->
**Solve issues**：Select the issue in the issue list, process the issue and submit the result.

* **Software Extention Advice**
  
<!-- 如果您在使用 openKylin 中途发现有软件的缺失，可以点击这里[提交软件适配需求]()，我们将在3天内对需求进行审核，尽量在2周内完成适配上架。-->
If you find any software missing when using openKylin, 
you can contact contact@openkylin.top, we will review the requirements within 3 days and try to upload the software to the software store within 2 weeks.

* **Contribute Code/Tool**

<!-- 如果您想为 openKylin 开发中间件或者其他工具，点击这里进行[想法提交]()，我们将分配对应研发为您提供开发工具、端口并解答在开发中遇到的问题，在开发完成之后，点击这里进行[工具提交]()，我们的开发人员将会在测试审核之后进行上架，点这里查看[贡献规范]()-->

If you want to develop middleware or other tools for openKylin, click here to go to [repository](https://gitee.com/openkylin), we will assign corresponding R&D to provide you with development tools, ports and solve any problems encountered during development. After the development is completed, you can submit the PR, and our developers will put it on the software store after testing it.

* **Contribute Non-code contents**

If you would like to make non-code contributions, click here to find the work you are interested in [Non-code Contribution Guide](/en/Community_Engagement_Guidelines/Non-code_Contribution_Guide.md).

# Develop with our community

## 1、Become a role in our community

<!-- 社区中不同的角色对应不同的权利与责任，您可以根据自己擅长的领域来申请担任不同的角色，点击这里查看[角色说明](/zh/开始贡献/openKylin社区贡献角色)，如果您找到感兴趣的角色，可以点击这里进行[申请]()。 -->
Different roles in the community correspond to different rights and responsibilities, you can apply for different roles according to your area of expertise, click here to see [Role Description](/en/Community_Engagement_Guidelines/Community_Contributor_Roles.md) and find the role of your interest.

## 2、Community Governance
<!-- Need to change to english version -->
To make the community work better, openKylin has its own governance organization, click here to see [governance organization structure](/zh/社区组织架构/社区治理组织架构). Whenever you encounter with problems in participating the community, please feel free to contact with the corresponding governance organization.

