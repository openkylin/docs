---
title: Non-Code Contribution Guidelines
description: 
published: true
date: 2022-05-17T08:23:20.757Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:36.576Z
---

# Non-code Contribution Guide

If you aren't interested in technology but want to contribute to openKylin, you can become an openKylin volunteer.

Whether you are a student, a professional programmer, or a corporate executive, as long as you're interested in Linux development and openKylin, you can apply to become an openKylin community volunteer and grow and progress with the community. The following is an overview of volunteer roles.

## Responsibilities and rights of the openKylin community volunteer team:

### 1. Responsibilities:

1). Core Organizers
- Develop procedural standards and assist with community policy;
- Organize openKylin developer competitions and meet-ups;
- Recruit talented openKylin developers and enthusiasts;
- Mirror the openKylin repository and ISO on new open source sites;
- Conduct pre-launch testing of new versions and features;

2). City Hub/University Hub
- Develop or establish openKylin city/university hubs;
- Promote openKylin developer competitions and organize regular city/university hub meetups;
- Recruit talented openKylin developers and enthusiasts and expand the scale of city/university hubs;
- Work to expand the openKylin ecosystem;
- Conduct pre-launch testing of new versions and features;

3). Media Group
- Build the openKylin brand, regularly record and promote videos on Bilibili, Douyin, etc;
- Regularly compose and publish excellent technical blog posts to the public account;
- Expand cooperation and resource exchange with KOLs and media;

4). Design Group
- Assist with visual design.

### 2. Benefits

Benefits of being a volunteeer:

- Opportunities to communicate one-on-one with openKylin core operators and technical leaders on a regular basis;
- Become an openKylin keynote speaker and enhance your exposure and influence;
- Obtain openKylin's practical activity credential;
- Consideration for internal openKylin job opportunities;
- Receive exclusive openKylin equipment gifts;
- Access to openKylin's publicity resources;

### 3. Joining process:

Send an email to contact@openkylin.top