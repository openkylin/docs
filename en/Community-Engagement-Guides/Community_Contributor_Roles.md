---
title: Non-Code Contribution Guidelines
description: 
published: true
date: 2022-05-17T08:23:20.757Z
tags: 
editor: markdown
dateCreated: 2022-03-11T03:16:36.576Z
---

# OpenKylin Community Contributor Roles

Different contributor roles in the openKylin community have different rights and responsibilities. Most these rights and responsibilities are limited to their respective SIG groups.

Every community member should be familiar with their respective community SIG's organization, role, policy, conventions, etc, as well as related technical and/or writing skill.

## Project Contributor
Contributors are active participants within their community SIG and can participate in SIG activities.

### Responsibilities
- Make relevant contributions according to the work content of the SIG you belong to.
- Respond to assigned tasks (PRs, issues, etc).
- Ability permitting, assist other community members with various contributions.
- If necessary, provide long term support and maintenance for contributions.
      
### Rights
- Participate in major SIG decisions through a 2/3 majority vote.
- Make technical or non-technical decisions about your work.
- Nominate yourself as a candidate in the SIG project core member election.

Additionally: Contributors with a high contribution frequency can apply to become project Maintainers.

## Project Maintainer

Maintainers are responsible for examining and checking code quality and accuracy.

### Responsibilities
- Review PRs submitted by Contributors.
- Update and maintain package versions.
- Find, track, and fix security bugs; disseminate fixes.
- Notify all SIG project groups of impacts caused by interface changes.
- Cooperate with the upstream community, including but not limited to pushing changes to the upstream, tracking importing upstream bugs, forwarding bugs to the upstream when help from them is required.
- In addition to providing basic test cases for testing, also responsible for providing debugging classification information of the software package when submitting a package to the testing team; responsible for providing relevant test cases when updating packages for quality inspectors to use.

### Rights

In addition to the above responsibilities, you have the following rights:

- Join the upstream mailing list and get the upstream community’s bug tracker account.
- Vote for project Owner.
- Nominate yourself as a candidate in the SIG project Owner election.

## Project Owner

The Owner is the core member of the SIG and leader of the SIG or member of other management committee. They are responsible for the project’s technical direction and internal and external resource coordination. In addition to all responsibilities and rights of Maintainers, Owners also have the following:

Responsibilities
- SIG technical direction and strategic planning; architecture evolution and internal project technical direction.
- Identify key requirements and release plans.
- Participate in community PM activities and adjust the SIG plan to match the community version milestone timeline.
- Represent the SIG in the community technical committee, steering committee activities and meetings, and other community activities.
- Hold regular SIG meetings and make relevant decisions.

### Rights

In addition to the above responsibilities, you have the following rights:
- Make urgent decisions that require action.
- Appoint new committee members with the technical committee.
- Elect and remove project core members with a 2/3 majority vote.
