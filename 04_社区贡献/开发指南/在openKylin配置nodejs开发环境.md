Node.js是一个开源和跨平台的 JavaScript 运行时环境。nede.js官方下载页面 https://nodejs.cn/en/download

使用官方仓库脚本安装nvm
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
```

使用国内镜像安装nvm
```
bash -c "$(curl -fsSL https://gitee.com/RubyMetric/nvm-cn/raw/main/install.sh)"
```
重启终端输入`nvm`
```
nvm -v
#输出当前版本
0.40.1
```

安装nodejs

```
npm install 22
```

卸载nvm
```
bash -c "$(curl -fsSL https://gitee.com/RubyMetric/nvm-cn/raw/main/uninstall.sh)"
```

推荐一个换源工具 
https://gitee.com/RubyMetric/chsrc

同时更换npm, pnpm, Yarn 的源
```
chsrc set node
```
单独更换npm的源
```
chsrc set npm
```