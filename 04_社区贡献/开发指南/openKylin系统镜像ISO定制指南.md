# 一、准备 ISO 修改环境
## 1、安装部署修改 iso 工具
```
sudo apt-get install squashfs-tools genisoimage
```
## 2、切换到 root用户
```
sudo su -
```

## 3、创建工作目录
```
cd /opt/
mkdir mnt livecd tool
```
## 4、上传要定制的系统 ISO 到 /opt/tool/ ，挂载 ISO 镜像到 /opt/mnt/下
```
mount -o loop /opt/tool/镜像 /opt/mnt/
```
## 5、复制所有的镜像⽂件到/opt/livecd/下 (注两个路径最后都有/)
```
rsync -a /opt/mnt/ /opt/livecd/
```
## 6、取消镜像挂载
```
umount /opt/mnt/
```
# 二、修改 ISO
## 1、集成软件，默认安装到系统中，修改如下：
```
cd /opt/livecd/
mkdir third-party
cp 集成的第三方软件包.deb /opt/livecd/third-party/
```
## 2、如果需要修改安装的启动参数，请修改如下文件
```
boot/grub/grub.cfg
isolinux/txt.cfg 特有#x86
```
# 重新打包ISO
## 1、跳转到 iso 打包目录
```
cd /opt/livecd/
```
## 2、根据不同的架构，分别使用如下的命令重新打包 iso 镜像：
```
mkisofs -J -r -T -joliet-long -V "openKylin-kiber" -no-emul-boot -boot-load-size 4 -boot-info-table -r -b isolinux/isolinux.bin -c isolinux/boot.cat -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -o /opt/tool/openKylin-2.0-kiber.iso /opt/livecd/
```
完成以上步骤之后，新镜像文件在/opt/tool/openKylin-2.0-kiber.iso，拷贝出来，制作启动盘


