# 在 RISC-V 版本 openKylin 系统中安装Arduino IDE

## 1\. 环境准备
- 开发板安装官网最新版 [openKylin 2.0](https://www.openkylin.top/downloads/) 镜像 
- Arduino 兼容开发板

## 2\. 安装步骤
1. 打开终端 输入`sudo apt update`更新软件源

2. 输入`sudo apt install arduino-ide` 安装 Arduino IDE

3. 在开始菜单中找到 Arduino IDE 图标，单击打开。等待自动安装AVR工具链（视网络情况和机器性能，需要等待5-30分钟，如果安装失败可在开发板管理器中安装对应开发板）

    ![](./assets/RISC-V%E7%89%88%E6%9C%ACArduino%20IDE%E5%AE%89%E8%A3%85/2024-09-13_12-57-36.png)

4. 可选： 安装算能 SG200X 系列开发板工具
    - 参考官方文档 https://milkv.io/zh/docs/duo/getting-started/arduino
    - 替换 `其他开发板管理器地址` 为以下地址
    > https://blob.bits.ink/arduino/packages/package_sg200x_index.json

    ![](./assets/RISC-V%E7%89%88%E6%9C%ACArduino%20IDE%E5%AE%89%E8%A3%85/20240913131153.png)

    - 其他步骤与官方文档相同，安装工具需要等待较长时间，请耐心等待安装完成，如遇安装失败请重新添加开发板并安装。

5.  如遇到无法访问串口设备问题，请在终端执行命令`sudo usermod -a -G dialout $USER`并重启系统即可解决。